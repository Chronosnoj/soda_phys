/*****************************************************************
Filename: editor.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../../imgui/core/imgui.h"
#include "editor.h"

#define BUTTON_TEXT( TEXT, NUM ) std::string(std::string(TEXT) + std::string("##") + std::to_string(i)).c_str();

namespace TestEngine
{
  namespace SODAEditor
  {
    SODAEditor::Editor::Editor(InputGL::InputManager& inputMan)
    {
      input = &inputMan;

      if (!input->IsFocused())
      {
        std::string startName("Space Name/File Name");
        strcpy(fileName, startName.c_str());
      }
        
    }

    void Editor::EditorTest()
    {

      ImGui::GetIO().MousePos.x = static_cast<float>(input->GetX());
      ImGui::GetIO().MousePos.y = static_cast<float>(input->GetY());

      if (ImGui::IsMouseHoveringAnyWindow())
        input->SetFocused(false);
      else
        input->SetFocused(true);
        

      ImGui::Begin("SODAPhysGL");
      ImGui::Value("Current FPS", input->GetFPS());
      SpaceControls();
      CameraControls();
      SelectedObjectControls();
      ConstraintManager();
      ThreadManager();
      ImGui::End();
    }

    void Editor::CameraControls()
    {
      if (ImGui::CollapsingHeader("Camera"))
      {
        ImGui::InputFloat3("Position", input->GetCamera()->eye.vec);
      }
    }

    void Editor::SpaceControls()
    {
      if (ImGui::CollapsingHeader("Space Controls"))
      {
        if (ImGui::CollapsingHeader("   Load/Save Space"))
        {
          ImGui::InputText("File Name", fileName, 20);
          if (ImGui::Button("Load Space")) input->LoadSpace(fileName);
          ImGui::SameLine();
          if (ImGui::Button("Save Space")) input->SaveSpace(fileName);
        }

        if (ImGui::CollapsingHeader("   Space Attributes"))
        {
          if (resumeSimulation)
            if (ImGui::Button("Resume Simulation"))
            {
              input->PauseSpace();
              resumeSimulation = false;
            }

          if (!resumeSimulation)
            if (ImGui::Button("Pause Simulation"))
            {
              input->PauseSpace();
              resumeSimulation = true;
            }
        }

        if (ImGui::CollapsingHeader("   Add Objects To Space"))
        {
          if (ImGui::BeginMenu("Add Object"))
          {
            if (ImGui::MenuItem("Box"))
              input->AddObject(TestEngine::colliderType::eBOX);

            if (ImGui::MenuItem("Sphere"))
              input->AddObject(TestEngine::colliderType::eSPHERE);

            if (ImGui::MenuItem("Capsule"))
              input->AddObject(TestEngine::colliderType::eCAPSULE);

            ImGui::EndMenu();
          }
        }
      }
    }

    void Editor::ObjectManager()
    {
      SelectedObjectControls();
    }

    void Editor::SelectedObjectControls()
    {
      if (ImGui::CollapsingHeader("Selected Object"))
      {
        PhysicsGL::Body* body = input->GetSpace()->GetBodyFromRefID(input->GetSpace()->GetActiveBody());
        if (body)
        {
          ImGui::Text("Body Information");
          BodyInformation(body);

            //positional info
          ImGui::Text("Coordinates");
          ImGui::InputFloat3("Position", body->m_position.vec);
          
            //orientation
          eulerAngles = body->GetEulerAngles();
          ImGui::InputFloat3("Orientation", eulerAngles.vec);
          input->GetSpace()->UpdateOrientation(eulerAngles);

            //velocity
          ImGui::Text("Velocities");
          ImGui::InputFloat3("Velocity", body->m_velocity.vec);
          ImGui::InputFloat3("Angular Velocity", body->m_angularVelocity.vec);

            //properties
          ImGui::Text("Properties");
          
          bool staticBody = body->m_static;
          if(ImGui::Checkbox("Static", &staticBody))
            body->FlipStatic();

          ImGui::SameLine();
          bool dragBody = body->m_drag;
          if (ImGui::Checkbox("Drag", &dragBody))
            body->SetDragFlag();

          ImGui::SameLine();
          ImGui::InputFloat("Drag Multiplier", &(body->m_dragAmount));

          if (ImGui::Button("Duplicate Selected Object"))
          {
            if (input->CheckSpace())
              input->PauseSpace();

            input->GetSpace()->DuplicateBody();
          }
        }
      }
    }

    void Editor::ConstraintManager()
    {
      if (ImGui::CollapsingHeader("Constraint Manager"))
      {
        ConstraintManagement();
        ConstraintAdder();
      }
    }

    void Editor::ThreadManager()
    {
      if (ImGui::CollapsingHeader("Thread Manager"))
      {
        if (ImGui::Checkbox("Threading Active", input->GetIslanding()))
        {
          input->GetCamera()->islandsOn = *(input->GetIslanding());
          input->ToggleThreading(*(input->GetIslanding()));
        }
        
        ImGui::Value("Active Islands", input->GetSpace()->GetNumActiveIslands());
        ImGui::Value("Number of Masters", input->GetSpace()->GetNumActiveMasters());
        ImGui::Value("Largest Island", input->GetSpace()->GetLargestIsland());
      }
    }

    void Editor::BodyInformation(PhysicsGL::Body* body)
    {
      ImGui::Text("Type:");
      ImGui::SameLine();

      switch (body->GetType())
      {
        case 0:
          ImGui::Text("Box");
          break;
        case 1:
          ImGui::Text("Sphere");
          break;
        case 2:
          ImGui::Text("Capsule");
          break;
      }

      ImGui::Value("Body Id", body->GetID());
      ImGui::Value("Aabb Id", body->GetAabbId());

        //adjustable values

        //dimensions
      dimensions = body->GetDimensions();
      ImGui::InputFloat3("Dimensions", dimensions.vec);
      body->SetDimensions(dimensions);

        //density
      ImGui::InputFloat("Density", &(body->m_density));
      ImGui::SameLine();
      if (ImGui::Button("Update Density")) body->CalculateMassInertia();
    }

    void Editor::ObjectList()
    {

    }

    void Editor::ConstraintType()
    {

      static int type = 1;

      ImGui::RadioButton("Rod", &type, 0);
      ImGui::SameLine();
      ImGui::RadioButton("Rope", &type, 1);
      ImGui::SameLine();
      ImGui::RadioButton("Soft", &type, 2);

      switch(type)
      {
        case 0:
          input->GetSpace()->SetConstraintType(TestEngine::MemoryManager::constraintType::eROD);
          break;

        case 1:
          input->GetSpace()->SetConstraintType(TestEngine::MemoryManager::constraintType::eROPE);
          break;

        case 2:
          input->GetSpace()->SetConstraintType(TestEngine::MemoryManager::constraintType::eSOFT);
          break;

      }
    }

    void Editor::ConstraintManagement()
    {
      if (ImGui::CollapsingHeader("   Constraints"))
      {
        MemoryManager::MemInterface::ConstraintList& constraints = input->GetSpace()->GetConstraintList();
        unsigned size = constraints.size();
        for (unsigned i = 0; i < size; ++i)
        {
          if (constraints[i].GetDeletedFlag())
            continue;
          
          ContraintTab(i);

          ImGui::SameLine();
          std::string deletion = BUTTON_TEXT("Delete", i);
          if (ImGui::Button(deletion.c_str()))
          {
            constraints[i].SetToDeleteFlag(true);
            if (constraintID == i)
              constraintID = UNSIGNED_MAX;
          }
          ImGui::SameLine();
          
          std::string modify = BUTTON_TEXT("Modify", i);
          std::string close = BUTTON_TEXT("Close", i);

          if (constraintID != i && ImGui::Button(modify.c_str()))
          {
            constraintID = i;
            constraints[i].m_selected = true;
          }
            

          else if (constraintID == i && ImGui::Button(close.c_str()))
          {
            constraintID = UNSIGNED_MAX;
          }
            
          if(constraintID != i)
            constraints[i].m_selected = false;

          if (constraintID == i)
            ConstraintModifier(i);
        }
      }
      
    }

    void Editor::ConstraintAdder()
    {
      if (ImGui::CollapsingHeader("   Constraint Adder"))
      {
        PhysicsGL::Body* body = input->GetSpace()->GetBodyFromRefID(input->GetSpace()->GetActiveBody());

        if (ImGui::Button("Add Constraint"))
        {
          bodyLocked[0] = false;
          bodyLocked[1] = false;
          addConstraint = true;
          resumeSimulation = false;
          if (input->CheckSpace())
          {
            resumeSimulation = true;
            input->PauseSpace();
          }

          if (body)
          {
            input->GetSpace()->SetConstraintBody(1, body->GetType(), body->m_position, body->m_orientation);
            bodyLocked[0] = true;
          }
        }


        if (body && addConstraint)
        {
          if (ImGui::Button("Attach Body 1"))
          {
            input->GetSpace()->SetConstraintBody(1, body->GetType(), body->m_position, body->m_orientation);
            bodyLocked[0] = true;
          }

          if (bodyLocked[0])
          {
            ImGui::SameLine();
            ImGui::Value("Body Id", input->GetSpace()->GetConstraintInfo()->idA);
            ImGui::SameLine();

            ImGui::Text(" Body Type: ");
            ImGui::SameLine();
            unsigned bodyType = input->GetSpace()->GetConstraintInfo()->typeA;
            switch (bodyType)
            {
            case 0:
              ImGui::Text("Box");
              break;
            case 1:
              ImGui::Text("Sphere");
              break;
            case 2:
              ImGui::Text("Capsule");
              break;
            }
          }

          if (ImGui::Button("Attach Body 2"))
          {
            input->GetSpace()->SetConstraintBody(2, body->GetType(), body->m_position, body->m_orientation);
            bodyLocked[1] = true;
          }

          if (bodyLocked[1])
          {
            ImGui::SameLine();
            ImGui::Value("Body Id", input->GetSpace()->GetConstraintInfo()->idB);
            ImGui::SameLine();

            ImGui::Text(" Body Type: ");
            ImGui::SameLine();
            unsigned bodyType = input->GetSpace()->GetConstraintInfo()->typeB;
            switch (bodyType)
            {
            case 0:
              ImGui::Text("Box");
              break;
            case 1:
              ImGui::Text("Sphere");
              break;
            case 2:
              ImGui::Text("Capsule");
              break;
            }
          }
        }


        if (bodyLocked[0] && bodyLocked[1] && addConstraint)
        {
          ConstraintType();
          ImGui::Text("Distance Between Connections");
          ImGui::SameLine();
          ImGui::Value("", input->GetSpace()->GetConstraintDistance());

          ImGui::InputFloat("Distance", &(input->GetSpace()->GetConstraintInfo()->distance));
          ImGui::InputFloat("Stiffness", &(input->GetSpace()->GetConstraintInfo()->stiffness));
          ImGui::InputFloat("Damping", &(input->GetSpace()->GetConstraintInfo()->damping));

          if (ImGui::Button("Easy Constraint"))
          {
            addConstraint = false;
            input->GetSpace()->SetEasyConstraintDistance();
            input->GetSpace()->AddConstraint();
            if (!input->CheckSpace())
            {
              if (resumeSimulation)
                input->PauseSpace();
            }
          }

          ImGui::SameLine();

          if (ImGui::Button("Complete Constraint"))
          {
            addConstraint = false;
            input->GetSpace()->AddConstraint();
            if (!input->CheckSpace())
            {
              if (resumeSimulation)
                input->PauseSpace();
            }
          }
        }
      }
    }

    void Editor::ContraintTab(unsigned i)
    {
      auto constraints = input->GetSpace()->GetConstraintList();
      
      ImGui::Value("ID", i);
      ImGui::SameLine();
      ImGui::Text(" Body A: ");
      ImGui::SameLine();
      unsigned bodyType = constraints[i].GetType(0);
      switch (bodyType)
      {
      case 0:
        ImGui::Text("Box     ");
        break;
      case 1:
        ImGui::Text("Sphere  ");
        break;
      case 2:
        ImGui::Text("Capsule ");
        break;
      }

      ImGui::SameLine();
      ImGui::Text(" Body B: ");
      ImGui::SameLine();
      bodyType = constraints[i].GetType(1);
      switch (bodyType)
      {
      case 0:
        ImGui::Text("Box     ");
        break;
      case 1:
        ImGui::Text("Sphere  ");
        break;
      case 2:
        ImGui::Text("Capsule ");
        break;
      }
    }

    void Editor::ConstraintModifier(unsigned i)
    {
      MemoryManager::MemInterface::ConstraintList& constraints = input->GetSpace()->GetConstraintList();

      ImGui::Text("Modify Selected Constraint");

      ImGui::InputFloat("Square Distance", &constraints[i].m_sqDistance);
      ImGui::InputFloat("Damping", &constraints[i].m_dampingConstant);
      ImGui::InputFloat("Stiffness", &constraints[i].m_springStiffness);

      static int type = constraints[i].m_constraintType;

      ImGui::RadioButton("Rod", &type, 0);
      ImGui::SameLine();
      ImGui::RadioButton("Rope", &type, 1);
      ImGui::SameLine();
      ImGui::RadioButton("Soft", &type, 2);

      constraints[i].m_constraintType = static_cast<MemoryManager::constraintType>(type);

    }

  }
}

