/*****************************************************************
Filename: editor.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../input/input.h"

namespace TestEngine
{
  namespace SODAEditor
  {
    class Editor
    {
      public:
        Editor(InputGL::InputManager& inputMan);

        void EditorTest();
        void CameraControls();
        void SpaceControls();
        void ObjectManager();
        void ConstraintManager();
        void ThreadManager();

          //other system access
        bool  GetConstraint()           { return addConstraint; }
        bool  GetBodyLocked(unsigned i) { return bodyLocked[i]; }

      private:
        InputGL::InputManager* input;
        char fileName[50];

          //body info
        Vec3 eulerAngles;
        Vec3 dimensions;
        void SelectedObjectControls();
        void BodyInformation(PhysicsGL::Body* body);
        void ObjectList();


          //constraints
        bool addConstraint = false;
        bool bodyLocked[2] = { false };
        bool resumeSimulation;

        void ConstraintManagement();

          //constraint addition
        void ConstraintType();
        void ConstraintAdder();

          //selected constraint
        void ContraintTab(unsigned i);
        void ConstraintModifier(unsigned i);

          //constraint access
        unsigned constraintID = UNSIGNED_MAX;
    };

  }
}

