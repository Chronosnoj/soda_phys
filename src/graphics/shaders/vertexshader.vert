#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexColor;

in vec2 texcoord;

out vec3 fragmentColor;
out vec2 Texcoord;

uniform mat4 MVP;

void main()
{
  gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
  
  fragmentColor = vertexColor;
  
  Texcoord = vec2(0,0);
}