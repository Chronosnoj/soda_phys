#version 330 core

in vec2 Texcoord;
in vec3 fragmentColor;

out vec4 color;

uniform sampler2D tex;

void main()
{
  vec4 texMap = texture(tex, Texcoord);
  color = texMap * vec4(fragmentColor, 1.0);
}