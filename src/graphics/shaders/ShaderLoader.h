/*****************************************************************
Filename: ShaderLoader.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../Base/math/mathconvert.h"

GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path);
//void SendToGLSL(Gluint program, std::string)