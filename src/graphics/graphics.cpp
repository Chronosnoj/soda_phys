/*****************************************************************
Filename: graphics.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "graphics.h"
#include <math.h>
#include "../imgui/core/imgui.h"
#include "../imgui/core/imgui_impl_glfw_gl3.h"

#include "../../external/SOIL/Simple OpenGL Image Library/src/SOIL.h"

namespace TestEngine
{
	namespace Graphics
	{
		GraphicsGL::~GraphicsGL()
		{
			//cleanup VBO
			//triangle
			glDeleteBuffers(1, &vertexbuffer);
			glDeleteVertexArrays(1, &VertexArrayID);

			glDeleteProgram(programID);
      
      ImGui_ImplGlfwGL3_Shutdown();
			glfwTerminate();

			delete camera;
		}

		int GraphicsGL::Initialize()
		{
			if (!glfwInit())
			{
				std::cout << "GLSW not initialized" << std::endl;
				return -1;
			}
			
			glfwWindowHint(GLFW_SAMPLES, 4);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

			GLFWwindow* window_;
			window_ = glfwCreateWindow(width * 2, height * 2, "TestEngine", NULL, NULL);

			  // Setup ImGui binding
			ImGui_ImplGlfwGL3_Init(window_, true);

			bool show_test_window = true;
			bool show_another_window = false;
			ImVec4 clear_color = ImColor(114, 144, 154);


			if (window_ == NULL)
			{
				std::cout << "Window Failure!" << std::endl;
				glfwTerminate();
				return -1;
			}

			window = window_;

			glfwMakeContextCurrent(window);
			glewExperimental = true;

			if (glewInit() != GLEW_OK)
			{
				std::cout << "Glew Failure" << std::endl;
				return -1;
			}

			//set background
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);

			//triangle stuff - to be replaced
			glGenVertexArrays(1, &VertexArrayID);
			glBindVertexArray(VertexArrayID);

			//shader stuff
			programID = LoadShaders("src/graphics/shaders/vertexshader.vert", "src/graphics/shaders/fragmentshader.frag");
			//programLineID = 
			MatrixID = glGetUniformLocation(programID, "MVP");

			if (CreateCamera() == -1)
				return -1;

			camera->halfScreenHeight = height;
			camera->halfScreenWidth = width;

			modelScale = 1.0f;

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

      int imageWidth, imageHeight;
      unsigned char* image = SOIL_load_image("assets/whitesquare.png", 
        &imageWidth, &imageHeight, 0, SOIL_LOAD_RGBA);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, image);
      SOIL_free_image_data(image);

			return 1;
		}

		int GraphicsGL::CreateCamera()
		{

			//camera calls
			Vec4 eye(0, 0, 20, 1);
			Vec4 lookat(0, 0, 10, 1);
			lookat = lookat - eye;
			Vec4 up(0, 1, 0, 0);
			camera = new TestEngine::Graphics::Camera(eye, lookat, up, 45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

			if (!camera)
				return -1;

			return 1;
		}


    void GraphicsGL::DisplaySplash(MemoryManager::MemInterface& memLists)
    {
      glEnable(GL_BLEND); 
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      programID = LoadShaders("src/graphics/shaders/vertexshadersplash.vert", "src/graphics/shaders/fragmentshadersplash.frag");

      int width, height;
      unsigned char* image = SOIL_load_image("assets/DigiPen_RGB_Black.png", &width, &height, 0, SOIL_LOAD_RGBA);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, image);
      SOIL_free_image_data(image);
      
      double currTime = 0;
      double prevTime;

      //display DigiPen logo
      double deltaTime = 0;
      currTime = glfwGetTime();
      prevTime = currTime;

        //splash should stay up for 3 seconds
      while (deltaTime < 3.0)
      {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(programID);

        currTime = glfwGetTime();
        deltaTime = currTime - prevTime;
        
        //just get matrices for camera and projection once per frame
        glm::mat4 Proj = camera->CameraProj();
        glm::mat4 View = camera->WorldToCamera();

        DrawQuad(Proj, View, memLists);

        // Redraw and clear screen.
        glfwPollEvents();
        glfwSwapBuffers(window);

      }

        //restore defaults
      programID = LoadShaders("src/graphics/shaders/vertexshader.vert", "src/graphics/shaders/fragmentshader.frag");
      image = SOIL_load_image("assets/whitesquare.png", &width, &height, 0, SOIL_LOAD_RGBA);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, image);
      SOIL_free_image_data(image);
    }

    void GraphicsGL::Update(MemoryManager::MemInterface& memLists)
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			//use shader
			glUseProgram(programID);	

      glGenBuffers(1, &texbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, texbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetUVSize(), mesh.GetUVs(), GL_STATIC_DRAW);
			
			  //just get matrices for camera and projection once per frame
			glm::mat4 Proj = camera->CameraProj();
			glm::mat4 View = camera->WorldToCamera();

			  //draw cubes
			DrawCubes(Proj, View, memLists);

			  //draw spheres
			DrawSpheres(Proj, View, memLists);

        //draw capsules - uses spheres from above
		  DrawCylinders(Proj, View, memLists);

        //ray verification
      if (m_ray)
      {
        DrawRays(Proj, View, *m_ray);
        DrawRayContactPoint(Proj, View);
      }

			  //do velocity and normal
			if (camera->GetDrawVecs())
				DrawVecs(Proj, View, memLists);
			
      if (camera->GetDrawNorms())
          for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
            DrawNormals(Proj, View, memLists, i);

      if (camera->GetDrawConstraints())
      {
        DrawRayContactPoint(Proj, View);
        DrawConstraints(Proj, View, memLists);
        DrawConstraintContacts(Proj, View, memLists);

        if (editor->GetConstraint())
          DrawConstraintInfoContacts(Proj, View);
      }    

			switch (camera->GetDebugInfo())
			{
				case NONE:
					break;

				case GJK:
          for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
            DrawSimplex(Proj, View, memLists, i);
					break;

				case EPA:
          for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
            DrawEPASimplex(Proj, View, memLists, i);
					break;

				case CONTACTPOINTS:
          for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
            DrawContactPoints(Proj, View, memLists, i);
					break;

				case EPACONTACT:
          for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
          {
            DrawEPASimplex(Proj, View, memLists, i);
            DrawContactPoints(Proj, View, memLists, i);
          }
					break;
			}

      //glDeleteBuffers(1, &texbuffer);

      glfwPollEvents();
      ImGui_ImplGlfwGL3_NewFrame();

        //allows drawing of gui and such, before reapplying wireframe
      if(camera->wireFrame)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

      //ImGui::ShowTestWindow();
      editor->EditorTest();
      ImGui::Render();

      if (camera->wireFrame)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			glfwSwapBuffers(window);   
		}

		void GraphicsGL::SendToShader(glm::mat4& MVP, int numIndices, bool tris)
		{
			//send to shader
			glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

			//create triangle
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glVertexAttribPointer(
				0,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				(void*)0
				);

			glEnableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glVertexAttribPointer(
				1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
				3,                                // size
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
				);

      //GLint texAttrib = glGetAttribLocation(programID, "texcoord");
      //glEnableVertexAttribArray(texAttrib);
      //glBindBuffer(GL_ARRAY_BUFFER, texbuffer);
      //glVertexAttribPointer(
      //  texAttrib,                        // attribute. No particular reason for 1, but must match the layout in the shader.
      //  2,                                // size
      //  GL_FLOAT,                         // type
      //  GL_FALSE,                         // normalized?
      //  0,                                // stride
      //  (void*)0                          // array buffer offset
      //  );

			if (tris)
			{				
				glDrawArrays(GL_TRIANGLES, 0, numIndices);
				glDisableVertexAttribArray(0);
				glDisableVertexAttribArray(1);
        //glDisableVertexAttribArray(2);
			}

			else
			{
				glDrawArrays(GL_LINES, 0, numIndices);
				glDisableVertexAttribArray(0);
				glDisableVertexAttribArray(1);
        //glDisableVertexAttribArray(2);
			}
		}

		glm::mat4 GraphicsGL::CreateModelMatrix(Vec3 & position, Mat3& orientation)
		{
			glm::mat4 trans(1.0f);
			trans[3][0] = position.x;
			trans[3][1] = position.y;
			trans[3][2] = position.z;

			orientation = orientation.CreateTranspose();

			glm::mat4 orient(1.0f);

			orient[0][0] = orientation.m_matrix[0][0];
			orient[0][1] = orientation.m_matrix[0][1];
			orient[0][2] = orientation.m_matrix[0][2];
			orient[1][0] = orientation.m_matrix[1][0];
			orient[1][1] = orientation.m_matrix[1][1];
			orient[1][2] = orientation.m_matrix[1][2];
			orient[2][0] = orientation.m_matrix[2][0];
			orient[2][1] = orientation.m_matrix[2][1];
			orient[2][2] = orientation.m_matrix[2][2];

			return trans * orient;
		}

    glm::mat4 GraphicsGL::CreateOffsetModelMatrix(Vec3 & position, Vec3 & offset, Mat3 & orientation)
    {
      glm::mat4 trans(1.0f);
      trans[3][0] = position.x;
      trans[3][1] = position.y;
      trans[3][2] = position.z;

      glm::mat4 offsetMat(1.0f);
      offsetMat[3][0] = offset.x;
      offsetMat[3][1] = offset.y;
      offsetMat[3][2] = offset.z;

      orientation = orientation.CreateTranspose();

      glm::mat4 orient(1.0f);

      orient[0][0] = orientation.m_matrix[0][0];
      orient[0][1] = orientation.m_matrix[0][1];
      orient[0][2] = orientation.m_matrix[0][2];
      orient[1][0] = orientation.m_matrix[1][0];
      orient[1][1] = orientation.m_matrix[1][1];
      orient[1][2] = orientation.m_matrix[1][2];
      orient[2][0] = orientation.m_matrix[2][0];
      orient[2][1] = orientation.m_matrix[2][1];
      orient[2][2] = orientation.m_matrix[2][2];

      return trans * orient * offsetMat;
    }

		glm::mat4 GraphicsGL::CreateModelScale(Vec3& scale)
		{
			float startScale = 1.0f;
			if (!camera->wireFrame)
				startScale = modelScale;

			glm::mat4 scaleMat(1.0f);

			scaleMat[0][0] = (startScale * scale.x);
			scaleMat[1][1] = (startScale * scale.y);
			scaleMat[2][2] = (startScale * scale.z);

			return scaleMat;
		}

		glm::mat4 GraphicsGL::CreateModelScale(float scale)
		{
			float startScale = 1.0f;
			if (!camera->wireFrame)
				startScale = modelScale;

			glm::mat4 scaleMat(1.0f);

			scaleMat[0][0] = startScale * scale;
			scaleMat[1][1] = startScale * scale;
			scaleMat[2][2] = startScale * scale;

			return scaleMat;
		}

    glm::mat4 GraphicsGL::CreateCapsuleSphereModelScale(Vec3 & scale)
    {
      float startScale = 1.0f;
      if (!camera->wireFrame)
        startScale = modelScale;

      glm::mat4 scaleMat(1.0f);

      scaleMat[0][0] = startScale * scale.x;
      scaleMat[1][1] = startScale * scale.x;
      scaleMat[2][2] = startScale * scale.x;

      return scaleMat;
    }

    glm::mat4 GraphicsGL::CreateCapsuleCylinderModelScale(Vec3 & scale)
    {
      float startScale = 1.0f;
      if (!camera->wireFrame)
        startScale = modelScale;

      glm::mat4 scaleMat(1.0f);

      scaleMat[0][0] = startScale * scale.x;
      scaleMat[1][1] = startScale * scale.y;
      scaleMat[2][2] = startScale * scale.z;

      return scaleMat;
    }

		glm::mat4 GraphicsGL::CreateVectorMVP(Vec3& endPoint, Vec3& position)
		{
			glm::mat4 trans(1.0f);
			trans[3][0] = position.x;
			trans[3][1] = position.y;
			trans[3][2] = position.z;

			endPoint.Normalize();
			Vec3 crossZ = Vec3(1, 0, 0).Cross(endPoint);
			Vec3 crossY = crossZ.Cross(Vec3(1, 0, 0));
			double x = endPoint.Dot(Vec3(1, 0, 0));
			crossY.Normalize();
			double y = endPoint.Dot(crossY);
			double radians = atan2(y, x);

			Mat3 rotation;
			rotation.RotateAboutAxis(radians, crossZ);
			rotation = rotation.CreateTranspose();

			glm::mat4 orient(1.0f);
			orient[0][0] = rotation.m_matrix[0][0];
			orient[0][1] = rotation.m_matrix[0][1];
			orient[0][2] = rotation.m_matrix[0][2];
			orient[1][0] = rotation.m_matrix[1][0];
			orient[1][1] = rotation.m_matrix[1][1];
			orient[1][2] = rotation.m_matrix[1][2];
			orient[2][0] = rotation.m_matrix[2][0];
			orient[2][1] = rotation.m_matrix[2][1];
			orient[2][2] = rotation.m_matrix[2][2];

			return trans * orient;
		}

		glm::mat4 GraphicsGL::CreateVectorTipMVP(Vec3& endPoint, Vec3& position)
		{
			Math::Mat3 rot;
			rot.SetRotationAboutAxis(static_cast<float>(PI) / 2.0f, Vec3(0, 0, 1));

			glm::mat4 trans2(1.0f);
			trans2[3][0] = position.x;
			trans2[3][1] = position.y;
			trans2[3][2] = position.z;

			float scalar = endPoint.Length();
			Vec3 normEnd = endPoint;
			normEnd.Normalize();

			if (scalar < 5.0f)
				scalar = 5.0f;

			glm::mat4 trans1(1.0f);
			trans1[3][0] = normEnd.x * scalar;
			trans1[3][1] = normEnd.y * scalar;
			trans1[3][2] = normEnd.z * scalar;

			endPoint.Normalize();
			Vec3 crossZ = Vec3(1, 0, 0).Cross(endPoint);
			Vec3 crossY = crossZ.Cross(Vec3(1, 0, 0));
			double x = endPoint.Dot(Vec3(1, 0, 0));
			crossY.Normalize();
			double y = endPoint.Dot(crossY);
			double radians = atan2(y, x);

			Mat3 rotation;
			rotation.RotateAboutAxis(radians, crossZ);
			rotation = rotation.CreateTranspose();
			rotation = rot * rotation;

			glm::mat4 orient(1.0f);
			orient[0][0] = rotation.m_matrix[0][0];
			orient[0][1] = rotation.m_matrix[0][1];
			orient[0][2] = rotation.m_matrix[0][2];
			orient[1][0] = rotation.m_matrix[1][0];
			orient[1][1] = rotation.m_matrix[1][1];
			orient[1][2] = rotation.m_matrix[1][2];
			orient[2][0] = rotation.m_matrix[2][0];
			orient[2][1] = rotation.m_matrix[2][1];
			orient[2][2] = rotation.m_matrix[2][2];

			return trans2 * trans1 * orient;
		}

		glm::mat4 GraphicsGL::CreateVectorScale(Vec3& endPoint)
		{
			float scalar = endPoint.Length();

			/*if (scalar < 5.0f)
				scalar = 5.0f;*/

			glm::mat4 scale(1.0f);

			scale[0][0] = scalar;
			scale[1][1] = scalar;
			scale[2][2] = scalar;

			return scale;
		}

		void GraphicsGL::DrawVecs(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists)
		{
			//do boxes
			auto & listBox = memLists.GetBoxList();
			auto itBox = listBox.begin();

			//draw the line
			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLine(), GL_STATIC_DRAW);

			mesh.SetLineColor(Vec3(0, 0, 1));

			glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLineColor(), GL_STATIC_DRAW);

			for (itBox = listBox.begin(); itBox != memLists.GetBoxList().end(); ++itBox)
			{
        if (itBox->GetDeleteFlag())
          continue;

				if (itBox->GetVelocity().Length() < FLT_EPSILON && itBox->GetVelocity().Length() > -FLT_EPSILON)
					continue;

				glm::mat4 Model = CreateVectorMVP(itBox->GetVelocity(), itBox->GetPosition());
				glm::mat4 Scale = CreateVectorScale(itBox->GetVelocity());
				glm::mat4 MVP = Proj * View * Model * Scale;

				SendToShader(MVP, mesh.GetLineSize() / 3, false);
			}

			//do sphere
			auto & listSphere = memLists.GetSphereList();
			auto itSphere = listSphere.begin();

			for (itSphere = listSphere.begin(); itSphere != memLists.GetSphereList().end(); ++itSphere)
			{
        if (itSphere->GetDeleteFlag())
          continue;

				if (itSphere->GetVelocity().Length() < FLT_EPSILON && itSphere->GetVelocity().Length() > -FLT_EPSILON)
					continue;

				glm::mat4 Model = CreateVectorMVP(itSphere->GetVelocity(), itSphere->GetPosition());
				glm::mat4 Scale = CreateVectorScale(itSphere->GetVelocity());
				glm::mat4 MVP = Proj * View * Model * Scale;

				SendToShader(MVP, mesh.GetLineSize() / 3, false);
			}

      auto & listCapsule = memLists.GetCapsuleList();
      auto itCapsule = listCapsule.begin();

      for (itCapsule = listCapsule.begin(); itCapsule != memLists.GetCapsuleList().end(); ++itCapsule)
      {
        if (itCapsule->GetDeleteFlag())
          continue;

        if (itCapsule->GetVelocity().Length() < FLT_EPSILON && itCapsule->GetVelocity().Length() > -FLT_EPSILON)
          continue;

        glm::mat4 Model = CreateVectorMVP(itCapsule->GetVelocity(), itCapsule->GetPosition());
        glm::mat4 Scale = CreateVectorScale(itCapsule->GetVelocity());
        glm::mat4 MVP = Proj * View * Model * Scale;

        SendToShader(MVP, mesh.GetLineSize() / 3, false);
      }

			glDeleteBuffers(1, &colorbuffer);
			glDeleteBuffers(1, &vertexbuffer);

			//draw the tip
			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetPyramidSize(), mesh.GetPyramid(), GL_STATIC_DRAW);

      mesh.SetPyramidColor(Vec3(0.0f, 0.0f, 1.0f));

			glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetPyramidSize(), mesh.GetPyramidColor(), GL_STATIC_DRAW);	

			for (itBox = listBox.begin(); itBox != memLists.GetBoxList().end(); ++itBox)
			{
        if (itBox->GetDeleteFlag())
          continue;

				if (itBox->GetVelocity().Length() < FLT_EPSILON && itBox->GetVelocity().Length() > -FLT_EPSILON)
					continue;

				glm::mat4 Model = CreateVectorTipMVP(itBox->GetVelocity(), itBox->GetPosition());
				glm::mat4 Scale = CreateModelScale(Vec3(1, 1, 1));
				glm::mat4 MVP = Proj * View * Model * Scale;

				SendToShader(MVP, mesh.GetPyramidSize() / 12, true);
			}

			for (itSphere = listSphere.begin(); itSphere != memLists.GetSphereList().end(); ++itSphere)
			{
        if (itSphere->GetDeleteFlag())
          continue;

				if (itSphere->GetVelocity().Length() < FLT_EPSILON && itSphere->GetVelocity().Length() > -FLT_EPSILON)
					continue;

				glm::mat4 Model = CreateVectorTipMVP(itSphere->GetVelocity(), itSphere->GetPosition());
				glm::mat4 Scale = CreateModelScale(Vec3(1, 1, 1));
				glm::mat4 MVP = Proj * View * Model * Scale;

				SendToShader(MVP, mesh.GetPyramidSize() / 12, true);
			}

      listCapsule = memLists.GetCapsuleList();
      itCapsule = listCapsule.begin();

      for (itCapsule = listCapsule.begin(); itCapsule != memLists.GetCapsuleList().end(); ++itCapsule)
      {
        if (itCapsule->GetDeleteFlag())
          continue;

        if (itCapsule->GetVelocity().Length() < FLT_EPSILON && itCapsule->GetVelocity().Length() > -FLT_EPSILON)
          continue;

        glm::mat4 Model = CreateVectorTipMVP(itCapsule->GetVelocity(), itCapsule->GetPosition());
        glm::mat4 Scale = CreateModelScale(Vec3(1, 1, 1));
        glm::mat4 MVP = Proj * View * Model * Scale;

        SendToShader(MVP, mesh.GetPyramidSize() / 12, true);
      }

			glDeleteBuffers(1, &colorbuffer);
			glDeleteBuffers(1, &vertexbuffer);
		}

    void GraphicsGL::DrawNormals(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists, unsigned island)
    {
      auto & listPair = memLists.GetPairList();

      if (island != UNSIGNED_MAX)
        listPair = memLists.GetIslandList(island);

      auto itPair = listPair.begin();

      if (listPair.empty())
        return;

      //draw the line
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLine(), GL_STATIC_DRAW);

      mesh.SetLineColor(Vec3(0, 1, 0));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLineColor(), GL_STATIC_DRAW);

      for (itPair = listPair.begin(); itPair != memLists.GetPairList().end(); ++itPair)
      {
        if (!itPair->GetActive())
          continue;

        if (!itPair->GetCollision())
          continue;

        glm::mat4 Model = CreateVectorMVP(itPair->GetNormal(), itPair->GetContactPoint(0,0));
        glm::mat4 Scale = CreateVectorScale(itPair->GetNormal());
        glm::mat4 MVP = Proj * View * Model * Scale;

        SendToShader(MVP, mesh.GetLineSize() / 3, false);

      }

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);

      //draw the tip
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetPyramidSize(), mesh.GetPyramid(), GL_STATIC_DRAW);

      mesh.SetPyramidColor(Vec3(0.0f, 1.0f, 0.0f));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetPyramidSize(), mesh.GetPyramidColor(), GL_STATIC_DRAW);

      for (itPair = listPair.begin(); itPair != memLists.GetPairList().end(); ++itPair)
      {
        if (!itPair->GetActive())
          continue;

        if (!itPair->GetCollision())
          continue;

        glm::mat4 Model = CreateVectorTipMVP(itPair->GetNormal(), itPair->GetContactPoint(0,0));
        glm::mat4 Scale = CreateModelScale(Vec3(1, 1, 1));
        glm::mat4 MVP = Proj * View * Model * Scale;

        SendToShader(MVP, mesh.GetPyramidSize() / 12, true);
      }

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);
    }

    void GraphicsGL::DrawQuad(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists)
    {
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetQuadSize(), mesh.GetQuad(), GL_STATIC_DRAW);

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetQuadSize(), mesh.GetQuadColor(), GL_STATIC_DRAW);

      //model matrix
      Mat3 orientation(PI, Vec3(0, 0, 1));
      orientation = Mat3(PI, Vec3(0, 1, 0)) * orientation;

      glm::mat4 Model = CreateModelMatrix(Vec3(0,0,0), orientation);
      glm::mat4 Scale = CreateModelScale(Vec3(12.45f,3,1));
      glm::mat4 MVP = Proj * View * Model * Scale;

      SendToShader(MVP, mesh.GetQuadSize() / 12, true);

      glDeleteBuffers(1, &vertexbuffer);
      glDeleteBuffers(1, &colorbuffer);
    }

		void GraphicsGL::DrawCubes(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists)
		{
			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetCubeSize(), mesh.GetCube(), GL_STATIC_DRAW);


      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetCubeSize(), mesh.GetCubeColor(), GL_STATIC_DRAW);


			//do boxes
			auto & list = memLists.GetBoxList();
			auto it = list.begin();

			for (; it != memLists.GetBoxList().end(); ++it)
			{
        if (it->GetDeleteFlag())
          continue;

				  //check collision
				if (it->GetCollision() && it->GetActive())
					mesh.SetCubeColor(Vec3(1, 1, 0));

				else if (it->GetCollision())
					mesh.SetCubeColor(Vec3(1, 0, 0));

				else if (it->GetActive())
					mesh.SetCubeColor(Vec3(0, 1, 0));

				else
					mesh.SetCubeColor(Vec3(1, 1, 1));

				glGenBuffers(1, &colorbuffer);
				glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
				glBufferData(GL_ARRAY_BUFFER, mesh.GetCubeSize(), mesh.GetCubeColor(), GL_STATIC_DRAW);

				//model matrix
				glm::mat4 Model = CreateModelMatrix(it->GetPosition(), it->GetOrientation());
				glm::mat4 Scale = CreateModelScale(it->GetScale());
				glm::mat4 MVP = Proj * View * Model * Scale;

				SendToShader(MVP, mesh.GetCubeSize() / 12, true);

				glDeleteBuffers(1, &colorbuffer);
			}

      //do aabbs
      auto & listaabb = memLists.GetAabbList();
      auto itaabb = listaabb.begin();
      
      if(camera->GetDrawAabbs())
        for (; itaabb != memLists.GetAabbList().end(); ++itaabb)
        {
        
          mesh.SetCubeColor(Vec3(0, 0, 1));

          glGenBuffers(1, &colorbuffer);
          glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
          glBufferData(GL_ARRAY_BUFFER, mesh.GetCubeSize(), mesh.GetCubeColor(), GL_STATIC_DRAW);

          //model matrix
          glm::mat4 Model = CreateModelMatrix(itaabb->GetPosition(), itaabb->GetOrientation());
          glm::mat4 Scale = CreateModelScale(itaabb->GetScale());
          glm::mat4 MVP = Proj * View * Model * Scale;

          SendToShader(MVP, mesh.GetCubeSize() / 12, true);

          glDeleteBuffers(1, &colorbuffer);
        }

			glDeleteBuffers(1, &vertexbuffer);
		}

		void GraphicsGL::DrawSpheres(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists)
		{
			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

			GLuint elementbuffer;
			glGenBuffers(1, &elementbuffer);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

			/*glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);*/

			//do spheres
			auto & list = memLists.GetSphereList();
			auto it = list.begin();

			for (; it != memLists.GetSphereList().end(); ++it)
			{
        if (it->GetDeleteFlag())
          continue;

				//check collision
				if (it->GetCollision() && it->GetActive())
					mesh.SetSphereColor(Vec3(1, 1, 0));

				else if(it->GetCollision())
					mesh.SetSphereColor(Vec3(1, 0, 0));

				else if (it->GetActive())
					mesh.SetSphereColor(Vec3(0, 1, 0));

				else
					mesh.SetSphereColor(Vec3(1, 1, 1));

				glGenBuffers(1, &colorbuffer);
				glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
				glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);

				glm::mat4 Model = CreateModelMatrix(it->GetPosition(), it->GetOrientation());
				glm::mat4 Scale = CreateModelScale(it->GetScale());
				glm::mat4 MVP = Proj * View * Model * Scale;

				//send to shader
				glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

				//create triangle
				glEnableVertexAttribArray(0);
				glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
				glVertexAttribPointer(
					0,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					(void*)0
					);

				glEnableVertexAttribArray(1);
				glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
				glVertexAttribPointer(
					1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
					3,                                // size
					GL_FLOAT,                         // type
					GL_FALSE,                         // normalized?
					0,                                // stride
					(void*)0                          // array buffer offset
					);

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

				glDrawElements(
					GL_TRIANGLES,      // mode
					mesh.GetSphere().indicesSize,    // count
					GL_UNSIGNED_INT,   // type
					(void*)0           // element array buffer offset
					);

        glDeleteBuffers(1, &colorbuffer);
			}

      //do capsules
      auto & list2 = memLists.GetCapsuleList();
      auto it2 = list2.begin();

      for (; it2 != memLists.GetCapsuleList().end(); ++it2)
      {
        if (it2->GetDeleteFlag())
          continue;

        for (int twice = 0; twice < 2; ++twice)
        {
          int top = 1;
          if (twice == 1)
            top = -1;

          //check collision
          if (it2->GetCollision() && it2->GetActive())
            mesh.SetSphereColor(Vec3(1, 1, 0));

          else if (it2->GetCollision())
            mesh.SetSphereColor(Vec3(1, 0, 0));

          else if (it2->GetActive())
            mesh.SetSphereColor(Vec3(0, 1, 0));

          else
            mesh.SetSphereColor(Vec3(1, 1, 1));

          glGenBuffers(1, &colorbuffer);
          glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
          glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);

          glm::mat4 Model = CreateOffsetModelMatrix(it2->GetPosition(), it2->GetOffset() * (const float)top, it2->GetOrientation());
          glm::mat4 Scale = CreateCapsuleSphereModelScale(it2->GetScale());
          glm::mat4 MVP = Proj * View * Model * Scale;

          //send to shader
          glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

          //create triangle
          glEnableVertexAttribArray(0);
          glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
          glVertexAttribPointer(
            0,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void*)0
            );

          glEnableVertexAttribArray(1);
          glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
          glVertexAttribPointer(
            1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
            );

          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

          glDrawElements(
            GL_TRIANGLES,      // mode
            mesh.GetSphere().indicesSize,    // count
            GL_UNSIGNED_INT,   // type
            (void*)0           // element array buffer offset
            );

          glDeleteBuffers(1, &colorbuffer);
        }
        
      }

      glDeleteBuffers(1, &vertexbuffer);
      glDeleteBuffers(1, &elementbuffer);
		}

    void GraphicsGL::DrawCylinders(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists)
    {
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetCylinderSize(), mesh.GetCylinder().vertices, GL_STATIC_DRAW);

      GLuint elementbuffer;
      glGenBuffers(1, &elementbuffer);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetCylinder().indicesSize * sizeof(unsigned int), mesh.GetCylinder().indices.data(), GL_STATIC_DRAW);

      auto & list = memLists.GetCapsuleList();
      auto it = list.begin();

      for (; it != memLists.GetCapsuleList().end(); ++it)
      {
        if (it->GetDeleteFlag())
          continue;

        //check collision
        if (it->GetCollision() && it->GetActive())
          mesh.SetCylinderColor(Vec3(1, 1, 0));

        else if (it->GetCollision())
          mesh.SetCylinderColor(Vec3(1, 0, 0));

        else if (it->GetActive())
          mesh.SetCylinderColor(Vec3(0, 1, 0));

        else
          mesh.SetCylinderColor(Vec3(1, 1, 1));

        glGenBuffers(1, &colorbuffer);
        glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
        glBufferData(GL_ARRAY_BUFFER, mesh.GetCylinderSize(), mesh.GetCylinderColor(), GL_STATIC_DRAW);

        glm::mat4 Model = CreateModelMatrix(it->GetPosition(), it->GetOrientation());
        glm::mat4 Scale = CreateModelScale(it->GetScale());
        glm::mat4 MVP = Proj * View * Model * Scale;

        //send to shader
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

        //create triangle
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
          0,
          3,
          GL_FLOAT,
          GL_FALSE,
          0,
          (void*)0
          );

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
        glVertexAttribPointer(
          1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
          3,                                // size
          GL_FLOAT,                         // type
          GL_FALSE,                         // normalized?
          0,                                // stride
          (void*)0                          // array buffer offset
          );

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        glDrawElements(
          GL_TRIANGLES,      // mode
          mesh.GetSphere().indicesSize,    // count
          GL_UNSIGNED_INT,   // type
          (void*)0           // element array buffer offset
          );

        glDeleteBuffers(1, &colorbuffer);
      }

      glDeleteBuffers(1, &vertexbuffer);
      glDeleteBuffers(1, &elementbuffer);
    }

		void GraphicsGL::DrawSimplex(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists, unsigned island)
		{
			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

			GLuint elementbuffer;
			glGenBuffers(1, &elementbuffer);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

			mesh.SetSphereColor(Vec3(0, 1, 0));

			glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);


			//do pairs
			auto & list = memLists.GetPairList();

      if (island != UNSIGNED_MAX)
        list = memLists.GetIslandList(island);

			auto it = list.begin();

			for (; it != memLists.GetPairList().end(); ++it)
			{
				int size = it->GetSimplexSize();

				if (size > 0)
				{
					glm::mat4 Model = CreateModelMatrix(Vec3(0,0,0), Mat3());
					glm::mat4 Scale = CreateModelScale(.5f);
					glm::mat4 MVP = Proj * View * Model * Scale;

					//send to shader
					glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

					//create triangle
					glEnableVertexAttribArray(0);
					glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
					glVertexAttribPointer(
						0,
						3,
						GL_FLOAT,
						GL_FALSE,
						0,
						(void*)0
						);

					glEnableVertexAttribArray(1);
					glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
					glVertexAttribPointer(
						1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
						3,                                // size
						GL_FLOAT,                         // type
						GL_FALSE,                         // normalized?
						0,                                // stride
						(void*)0                          // array buffer offset
						);

					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

					glDrawElements(
						GL_TRIANGLES,      // mode
						mesh.GetSphere().indicesSize,    // count
						GL_UNSIGNED_INT,   // type
						(void*)0           // element array buffer offset
						);

				}

				for (int i = 0; i < size; ++i)
				{
					glm::mat4 Model = CreateModelMatrix(it->GetSimplexPoint(i), Mat3());
					glm::mat4 Scale = CreateModelScale(.25f);
					glm::mat4 MVP = Proj * View * Model * Scale;

					//send to shader
					glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

					//create triangle
					glEnableVertexAttribArray(0);
					glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
					glVertexAttribPointer(
						0,
						3,
						GL_FLOAT,
						GL_FALSE,
						0,
						(void*)0
						);

					glEnableVertexAttribArray(1);
					glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
					glVertexAttribPointer(
						1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
						3,                                // size
						GL_FLOAT,                         // type
						GL_FALSE,                         // normalized?
						0,                                // stride
						(void*)0                          // array buffer offset
						);

					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

					glDrawElements(
						GL_TRIANGLES,      // mode
						mesh.GetSphere().indicesSize,    // count
						GL_UNSIGNED_INT,   // type
						(void*)0           // element array buffer offset
						);

				}
			}

			glDeleteBuffers(1, &colorbuffer);
			glDeleteBuffers(1, &vertexbuffer);
			glDeleteBuffers(1, &elementbuffer);
		}

		void GraphicsGL::DrawEPASimplex(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists, unsigned island)
		{
			//do pairs
			auto & list = memLists.GetPairList();

      if (island != UNSIGNED_MAX)
        list = memLists.GetIslandList(island);

			auto it = list.begin();

			for (; it != memLists.GetPairList().end(); ++it)
			{
				unsigned size = it->GetEPAFaceSize();

				for (unsigned i = 0; i < size; ++i)
				{

					//draw the line
					glGenBuffers(1, &vertexbuffer);
					glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
					glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLine(), GL_STATIC_DRAW);

					mesh.SetLineColor(Vec3(0, 0, 1));

					glGenBuffers(1, &colorbuffer);
					glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
					glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLineColor(), GL_STATIC_DRAW);

					for (unsigned j = 0; j < 3; ++j)
					{
						unsigned end = (j + 1) > 2 ? 0 : j + 1;

						Vec3 vector = it->GetEPASimplexPoint(it->GetEPASimplexPointIndex(i, end))
							- it->GetEPASimplexPoint(it->GetEPASimplexPointIndex(i, j));
						float length = vector.Length();
						glm::mat4 Model = CreateVectorMVP(vector,
							it->GetEPASimplexPoint(it->GetEPASimplexPointIndex(i, j)));
						//glm::mat4 Scale(length);
						glm::mat4 MVP = Proj * View * Model;

						SendToShader(MVP, mesh.GetLineSize() / 3, false);
					}

					glDeleteBuffers(1, &colorbuffer);
					glDeleteBuffers(1, &vertexbuffer);

					glGenBuffers(1, &vertexbuffer);
					glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
					glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

					GLuint elementbuffer;
					glGenBuffers(1, &elementbuffer);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

					mesh.SetSphereColor(Vec3(0, 0, 1));

					glGenBuffers(1, &colorbuffer);
					glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
					glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);


					for (unsigned j = 0; j < 3; ++j)
					{
						//draw the endpoints
						glm::mat4 Model = CreateModelMatrix(it->GetEPASimplexPoint(it->GetEPASimplexPointIndex(i, j)), Mat3());
						glm::mat4 Scale = CreateModelScale(.25f);
						glm::mat4 MVP = Proj * View * Model * Scale;

						//send to shader
						glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

						//create triangle
						glEnableVertexAttribArray(0);
						glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
						glVertexAttribPointer(
							0,
							3,
							GL_FLOAT,
							GL_FALSE,
							0,
							(void*)0
							);

						glEnableVertexAttribArray(1);
						glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
						glVertexAttribPointer(
							1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
							3,                                // size
							GL_FLOAT,                         // type
							GL_FALSE,                         // normalized?
							0,                                // stride
							(void*)0                          // array buffer offset
							);

						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

						glDrawElements(
							GL_TRIANGLES,      // mode
							mesh.GetSphere().indicesSize,    // count
							GL_UNSIGNED_INT,   // type
							(void*)0           // element array buffer offset
							);

					}
					
					glDeleteBuffers(1, &colorbuffer);
					glDeleteBuffers(1, &vertexbuffer);
					glDeleteBuffers(1, &elementbuffer);
				}
			}
		}

		void GraphicsGL::DrawContactPoints(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists, unsigned island)
		{
			//do pairs
			auto & list = memLists.GetPairList();

      if (island != UNSIGNED_MAX)
        list = memLists.GetIslandList(island);

			auto it = list.begin();

			glGenBuffers(1, &vertexbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

			GLuint elementbuffer;
			glGenBuffers(1, &elementbuffer);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

			mesh.SetSphereColor(Vec3(1, 1, 0));

			glGenBuffers(1, &colorbuffer);
			glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
			glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);

			for (; it != memLists.GetPairList().end(); ++it)
			{
        if (!it->GetActive())
          continue;

				if (!it->GetCollision())
					continue;

				for (unsigned i = 0; i < it->GetStoredContactSize(); ++i)
				{
          for (unsigned j = 0; j < 2; ++j)
          {
            //draw the contact points
            glm::mat4 Model = CreateModelMatrix(it->GetContactPoint(i, j), Mat3());
            glm::mat4 Scale = CreateModelScale(.05f);
            glm::mat4 MVP = Proj * View * Model * Scale;

            //send to shader
            glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

            //create triangle
            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glVertexAttribPointer(
              0,
              3,
              GL_FLOAT,
              GL_FALSE,
              0,
              (void*)0
              );

            glEnableVertexAttribArray(1);
            glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
            glVertexAttribPointer(
              1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
              3,                                // size
              GL_FLOAT,                         // type
              GL_FALSE,                         // normalized?
              0,                                // stride
              (void*)0                          // array buffer offset
              );

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

            glDrawElements(
              GL_TRIANGLES,      // mode
              mesh.GetSphere().indicesSize,    // count
              GL_UNSIGNED_INT,   // type
              (void*)0           // element array buffer offset
              );
          }
				}	
			}

			glDeleteBuffers(1, &colorbuffer);
			glDeleteBuffers(1, &vertexbuffer);
			glDeleteBuffers(1, &elementbuffer);
		}

    void GraphicsGL::DrawConstraints(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists)
    {
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLine(), GL_STATIC_DRAW);

      auto & list = memLists.GetConstraintList();
      auto it = list.begin();

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      //glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLineColor(), GL_STATIC_DRAW);

      for (; it != memLists.GetConstraintList().end(); ++it)
      {
        if (it->GetDeletedFlag() || it->GetToDeleteFlag())
          continue;

        if(it->GetSelected())
          mesh.SetLineColor(Vec3(0, 1, 0));
        else
          mesh.SetLineColor(Vec3(0, 0, 1));

        glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLineColor(), GL_STATIC_DRAW);

        //model matrix
        glm::mat4 Model = CreateVectorMVP(it->GetNormal(), it->GetConstraintPosition());
        glm::mat4 Scale = CreateVectorScale(it->GetConstraintLength());
        glm::mat4 MVP = Proj * View * Model * Scale;

        SendToShader(MVP, mesh.GetLineSize() / 3, true); 
      }

      glDeleteBuffers(1, &colorbuffer);
    }

    void GraphicsGL::DrawConstraintContacts(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists)
    {
      auto & list = memLists.GetConstraintList();

      auto it = list.begin();

      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

      GLuint elementbuffer;
      glGenBuffers(1, &elementbuffer);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

      mesh.SetSphereColor(Vec3(1, 1, 0));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);

      for (; it != memLists.GetConstraintList().end(); ++it)
      {
        if (it->GetDeletedFlag())
          continue;
       
        for (unsigned j = 0; j < 2; ++j)
        {
          //draw the contact points
          glm::mat4 Model = CreateModelMatrix(it->GetContactPoint(j), Mat3());
          glm::mat4 Scale = CreateModelScale(.05f);
          glm::mat4 MVP = Proj * View * Model * Scale;

          //send to shader
          glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

          //create triangle
          glEnableVertexAttribArray(0);
          glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
          glVertexAttribPointer(
            0,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void*)0
            );

          glEnableVertexAttribArray(1);
          glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
          glVertexAttribPointer(
            1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
            );

          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

          glDrawElements(
            GL_TRIANGLES,      // mode
            mesh.GetSphere().indicesSize,    // count
            GL_UNSIGNED_INT,   // type
            (void*)0           // element array buffer offset
            );
        }
      }

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);
      glDeleteBuffers(1, &elementbuffer);
    }

    void GraphicsGL::DrawConstraintInfoContacts(glm::mat4 & Proj, glm::mat4 & View)
    {
      if (!editor->GetConstraint())
        return;

      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

      GLuint elementbuffer;
      glGenBuffers(1, &elementbuffer);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

      mesh.SetSphereColor(Vec3(1, 1, 0));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);

      for (unsigned j = 0; j < 2; ++j)
      {
        if (!editor->GetBodyLocked(j))
          continue;
        //draw the contact points
        glm::mat4 Model = CreateModelMatrix(m_constraint->worldSpaceConnectionPoint[j], Mat3());
        glm::mat4 Scale = CreateModelScale(.05f);
        glm::mat4 MVP = Proj * View * Model * Scale;

        //send to shader
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

        //create triangle
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
          0,
          3,
          GL_FLOAT,
          GL_FALSE,
          0,
          (void*)0
          );

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
        glVertexAttribPointer(
          1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
          3,                                // size
          GL_FLOAT,                         // type
          GL_FALSE,                         // normalized?
          0,                                // stride
          (void*)0                          // array buffer offset
          );

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        glDrawElements(
          GL_TRIANGLES,      // mode
          mesh.GetSphere().indicesSize,    // count
          GL_UNSIGNED_INT,   // type
          (void*)0           // element array buffer offset
          );
      }

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);
      glDeleteBuffers(1, &elementbuffer);
    }

    void GraphicsGL::DrawRays(glm::mat4& Proj, glm::mat4& View, PhysicsGL::Ray& ray)
    {
      //draw the line
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLine(), GL_STATIC_DRAW);

      mesh.SetLineColor(Vec3(0, 1, 1));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetLineSize(), mesh.GetLineColor(), GL_STATIC_DRAW);

      glm::mat4 Model = CreateVectorMVP(ray.m_direction, ray.m_origin);
      glm::mat4 Scale = CreateVectorScale(ray.m_direction * 100);
      glm::mat4 MVP = Proj * View * Model * Scale;

      SendToShader(MVP, mesh.GetLineSize() / 3, false);

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);

      //draw the tip
      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetPyramidSize(), mesh.GetPyramid(), GL_STATIC_DRAW);

      mesh.SetPyramidColor(Vec3(0.0f, 1.0f, 1.0f));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetPyramidSize(), mesh.GetPyramidColor(), GL_STATIC_DRAW);

      Model = CreateVectorTipMVP(ray.m_direction * 100, ray.m_origin);
      Scale = CreateModelScale(Vec3(1, 1, 1));
      MVP = Proj * View * Model * Scale;

      SendToShader(MVP, mesh.GetPyramidSize() / 12, true);

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);

    }

    void GraphicsGL::DrawRayContactPoint(glm::mat4 & Proj, glm::mat4 & View)
    {

      if (!m_result || !m_result->colliding)
        return;

      glGenBuffers(1, &vertexbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphere().vertices, GL_STATIC_DRAW);

      GLuint elementbuffer;
      glGenBuffers(1, &elementbuffer);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.GetSphere().indicesSize * sizeof(unsigned int), mesh.GetSphere().indices.data(), GL_STATIC_DRAW);

      mesh.SetSphereColor(Vec3(0, 0, 1));

      glGenBuffers(1, &colorbuffer);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glBufferData(GL_ARRAY_BUFFER, mesh.GetSphereSize(), mesh.GetSphereColor(), GL_STATIC_DRAW);

      //draw the contact points
      glm::mat4 Model = CreateModelMatrix(m_result->worldCollisionPoint, Mat3());
      glm::mat4 Scale = CreateModelScale(.05f);
      glm::mat4 MVP = Proj * View * Model * Scale;

      //send to shader
      glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

      //create triangle
      glEnableVertexAttribArray(0);
      glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
      glVertexAttribPointer(
        0,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void*)0
        );

      glEnableVertexAttribArray(1);
      glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
      glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
        );

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

      glDrawElements(
        GL_TRIANGLES,      // mode
        mesh.GetSphere().indicesSize,    // count
        GL_UNSIGNED_INT,   // type
        (void*)0           // element array buffer offset
        );

      glDeleteBuffers(1, &colorbuffer);
      glDeleteBuffers(1, &vertexbuffer);
      glDeleteBuffers(1, &elementbuffer);
    }
	}
}