/*****************************************************************
Filename: camera.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "camera.h"

namespace TestEngine
{
	namespace Graphics
	{
		Camera::Camera() : eye(Vec4(0, 0, 0, 1)), lookAtPoint(Vec4(0,0,0)), right(Vec4(1, 0, 0)), up(Vec4(0, 1, 0)), back(Vec4(0, 0, 1)),
			distance(0.5f), width(1.0f), height(1.0f), near(0.0f), far(1.0f), aspect(4.0f /3.0f), fov(45.0f), wireFrame(true), drawAabbs(false),
			info(NONE), drawVecs(false), drawNorms(false), drawConstraints(true)
		{
		}

		Camera::Camera(const Vec4 & eye_, const Vec4& lookAtPoint_, const Vec4 & up_, float fov_, float aspect_, float near_, float far_)
			: eye(eye_), lookAtPoint(lookAtPoint_), near(near_), far(far_), fov(fov_), aspect(aspect_), wireFrame(true), drawAabbs(false), info(NONE),
			drawVecs(false), drawNorms(false), drawConstraints(true)
		{
			//normalize back vector and copy up vector
			back = eye - lookAtPoint;
			back.Normalize();
			Vec4 gravup = up_;
			gravup.Normalize();

			//create right vector using -(n X v')
			right = -(back.Cross(gravup));

			//(n X v)
			up = back.Cross(right);

			//average value
			distance = (near + far) / 2.0f;

			float degreevalue = tan(fov / 2.0f);

			width = 2 * distance * degreevalue;

			height = width / aspect;
		}

		Camera& Camera::Zoom(float factor)
		{
			fov += factor;

			return *this;
		}

		Camera& Camera::Look(float factor)
		{
			Vec4 dir = eye - lookAtPoint;

			dir.Normalize();
			Vec4 newLookAtPoint = lookAtPoint + dir * factor;
			Vec4 dirNew = eye - newLookAtPoint;
			if (dirNew.Length() < 1)
				return *this;

			lookAtPoint = newLookAtPoint;

			return *this;
		}

		Camera& Camera::Forward(float distance)
		{
			eye = eye + (back * (-distance));
			lookAtPoint = lookAtPoint + (back * (-distance));
			return *this;
		}

		Camera& Camera::Right(float distance)
		{
			eye = eye + (right * (distance));
			lookAtPoint = lookAtPoint + (right * (distance));
			return *this;
		}

    Camera& Camera::XMove(float distance)
    {
      eye = eye + (Vec3(right.Dot(Vec3(1,0,0)), 0.0f, right.Dot(Vec3(0,0,1))) * (distance));;
      return *this;
    }

		Camera& Camera::Up(float distance)
		{
			eye = eye + (up * (distance));
			lookAtPoint = lookAtPoint + (up * (distance));
			return *this;
		}

    Camera& Camera::YMove(float distance)
    {
      eye = eye + (Vec3(0,1,0) * (distance));;
      return *this;
    }

		Camera& Camera::Yaw(float angle)
		{
			Mat4 rot = Mat4(angle, up);

			right = rot * right;
			back = rot * back;
			Vec4 look = lookAtPoint - eye;
			look = rot * look;
			lookAtPoint = look + eye;

			return *this;
		}

		Camera& Camera::Pitch(float angle)
		{
			Mat4 rot = Mat4((double)angle, right);

			up = rot * up;
			back = rot * back;
			Vec4 look = lookAtPoint - eye;
			look = rot * look;
			lookAtPoint = look + eye;
			return *this;
		}

		Camera& Camera::Roll(float angle)
		{
			Mat4 rot = Mat4(angle, back);

			up = rot * up;
			right = rot * right;
			Vec4 look = lookAtPoint - eye;
			look = rot * look;
			lookAtPoint = look + eye;
			return *this;
		}

    Camera & Camera::RotateRight(float distance)
    {
      eye = eye + (Vec4(right.x, 0, right.z) * (distance));
      lookAtPoint = lookAtPoint + (right * (distance));
      return *this;
    }

    Camera & Camera::RotateUp(float distance)
    {
      return *this; // TODO: insert return statement here
    }

		void Camera::Reset()
		{
			//normalize back vector and copy up vector
			back = eye - lookAtPoint;
			back.Normalize();
			Vec4 gravup = up;
			gravup.Normalize();

			//create right vector using -(n X v')
			right = -(back.Cross(gravup));

			//(n X v)
			up = back.Cross(right);
			
		}

		void Camera::SetWireFrame()
		{
			wireFrame = !wireFrame;

			if (wireFrame)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

    void Camera::SetDrawAabbs()
    {
      drawAabbs = !drawAabbs;
    }

		void Camera::SetSimplexDraw()
		{
			info = static_cast<DebugInfo>((info + 1) >= DEBUG ? NONE : info + 1);
		}

		void Camera::SetDrawVecs()
		{
			drawVecs = !drawVecs;
		}

    void Camera::SetDrawNorms()
    {
      drawNorms = !drawNorms;
    }

    void Camera::SetDrawConstraints()
    {
      drawConstraints = !drawConstraints;
    }


		Vec3 Camera::ViewportGeometry() const
		{
			return Vec3(width, height, distance);
		}

		glm::mat4 Camera::WorldToCamera()
		{
      //Mat4 worldCam(right, up, back, -eye);
      ////worldCam.m_matrix[2][3] = -eye.z;
      //Mat4 testCam = worldCam.CreateTranspose();

      //glm::mat4 worldCamera;
      //worldCamera[0][0] = testCam.m_matrix[0][0];
      //worldCamera[0][1] = testCam.m_matrix[0][1];
      //worldCamera[0][2] = testCam.m_matrix[0][2];
      //worldCamera[0][3] = testCam.m_matrix[0][3];
      //worldCamera[1][0] = testCam.m_matrix[1][0];
      //worldCamera[1][1] = testCam.m_matrix[1][1];
      //worldCamera[1][2] = testCam.m_matrix[1][2];
      //worldCamera[1][3] = testCam.m_matrix[1][3];
      //worldCamera[2][0] = testCam.m_matrix[2][0];
      //worldCamera[2][1] = testCam.m_matrix[2][1];
      //worldCamera[2][2] = testCam.m_matrix[2][2];
      //worldCamera[2][3] = testCam.m_matrix[2][3];
      //worldCamera[3][0] = testCam.m_matrix[3][0];
      //worldCamera[3][1] = testCam.m_matrix[3][1];
      //worldCamera[3][2] = testCam.m_matrix[3][2];
      //worldCamera[3][3] = -testCam.m_matrix[3][3];

      //return worldCamera;

			glm::vec3 eyeC(eye.x, eye.y, eye.z);
			glm::vec3 lookAtC(lookAtPoint.x, lookAtPoint.y, lookAtPoint.z);
			glm::vec3 upC(up.x, up.y, up.z);
      return glm::lookAt(eyeC, lookAtC, upC);
		}

		glm::mat4 Camera::CameraProj()
		{
			return glm::perspective(fov, aspect, near, far);
		}
	}
}


