/*****************************************************************
Filename: camera.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../Base/math/MathConvert.h"

namespace TestEngine
{
	namespace Graphics
	{
		enum DebugInfo
		{
			NONE,
			GJK,
			EPA,
			CONTACTPOINTS,
			EPACONTACT,
			DEBUG
		};

		class Camera
		{
			public:
				Camera();
				Camera(const Vec4& eye_, const Vec4& lookAtPoint_, const Vec4& up_, float fov_, float aspect = 1.0f, float near_ = 0.0f, float far_ = 100.0f);

				Camera& Zoom(float factor);
				Camera& Look(float factor);
				Camera& Forward(float distance);
				Camera& Right(float distance);
        Camera& XMove(float distance);
				Camera& Up(float distance);
        Camera& YMove(float distance);
				Camera& Yaw(float angle);
				Camera& Pitch(float angle);
				Camera& Roll(float angle);

          //rotate functions
        Camera& RotateRight(float distance);
        Camera& RotateUp(float distance);

				void Reset();

				void SetWireFrame();
        void SetDrawAabbs();
				void SetSimplexDraw();
				void SetDrawVecs();
        void SetDrawNorms();
        void SetDrawConstraints();

        bool GetDrawAabbs()             { return drawAabbs; }
				bool GetDrawVecs()              { return drawVecs; }
        bool GetDrawNorms()             { return drawNorms; }
        bool GetDrawConstraints()       { return drawConstraints; }
				inline DebugInfo GetDebugInfo() { return info; }

				Vec3 ViewportGeometry() const;

				glm::mat4 WorldToCamera();
				glm::mat4 CameraProj();
				//Mat4 CameraProjToNDC();

				Vec4 eye, lookAtPoint;
				Vec4 right, up, back;
				float width, height, distance, near, far, fov, aspect;

				bool wireFrame;
				bool drawVecs;
        bool drawNorms;
        bool drawAabbs;
        bool drawConstraints;
        bool islandsOn;
				DebugInfo info;

				float halfScreenWidth, halfScreenHeight;
		};

	}
}