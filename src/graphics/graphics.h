/*****************************************************************
Filename: graphics.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../Base/math/MathConvert.h"
#include "camera/camera.h"
#include "mesh/mesh.h"
#include "shaders/shaderloader.h"
#include "../imgui/editor/editor.h"

//needs to be replaced with better method after testing
#include "../physics/raycasting/Raycast.h"

namespace TestEngine
{
	namespace Graphics
	{
		class GraphicsGL
		{
			public:
				GraphicsGL() {};
				~GraphicsGL();

				int Initialize();
				int CreateCamera();

        void DisplaySplash(MemoryManager::MemInterface& memLists);
				void Update(MemoryManager::MemInterface& memLists);

				Camera* camera;
				GLFWwindow* window;
        SODAEditor::Editor* editor;

          //REPLACE
        void DrawRays(glm::mat4& Proj, glm::mat4& View, PhysicsGL::Ray& ray);
        void DrawRayContactPoint(glm::mat4& Proj, glm::mat4& View);
        PhysicsGL::Ray* m_ray;
        RaycastResult* m_result;
        ConstraintInfo* m_constraint;

			private:
				void SendToShader(glm::mat4& MVP, int numIndices, bool tris);
        //GLuint LoadTexture(const char* textureName);

				glm::mat4 CreateModelMatrix(Vec3& position, Mat3& orientation);
        glm::mat4 CreateOffsetModelMatrix(Vec3& position, Vec3& offset, Mat3& orientation);
        glm::mat4 CreateModelScale(Vec3& scale);
				glm::mat4 CreateModelScale(float scale);
        glm::mat4 CreateCapsuleSphereModelScale(Vec3& scale);
        glm::mat4 CreateCapsuleCylinderModelScale(Vec3& scale);

				glm::mat4 CreateVectorMVP(Vec3& endPoint, Vec3& position);
				glm::mat4 CreateVectorTipMVP(Vec3& endPoint, Vec3& position);
				glm::mat4 CreateVectorScale(Vec3& endPoint);

				void DrawVecs(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists);
        void DrawNormals(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists, unsigned island);
        void DrawQuad(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists);
				void DrawCubes(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists);
				void DrawSpheres(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists);
        void DrawCylinders(glm::mat4 & Proj, glm::mat4 & View, MemoryManager::MemInterface & memLists);
				void DrawSimplex(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists, unsigned island);
				void DrawEPASimplex(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists, unsigned island);
				void DrawContactPoints(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists, unsigned island);
        void DrawConstraints(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists);
        void DrawConstraintContacts(glm::mat4& Proj, glm::mat4& View, MemoryManager::MemInterface& memLists);
        void DrawConstraintInfoContacts(glm::mat4& Proj, glm::mat4& View);
				
				static const int height = 384;
				static const int width = 512;

				  //GLuint variables
				GLuint VertexArrayID;
				GLuint programID;
				GLuint MatrixID;
				GLuint vertexbuffer;
				GLuint colorbuffer;
        GLuint texbuffer;

				Mesh mesh;

				float modelScale;  //for difference between model and wireframe
		};
	}
}