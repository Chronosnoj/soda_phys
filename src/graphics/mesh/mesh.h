/*****************************************************************
Filename: mesh.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../Base/math/MathConvert.h"

namespace TestEngine
{
	namespace Graphics
	{
		struct MeshInfo
		{
			MeshInfo(GLfloat* vertices_, std::vector<unsigned>& indices_, int numIndices, int numVerts) :
				vertices(vertices_), indices(indices_), indicesSize(numIndices), vertSize(numVerts) {}

			GLfloat* vertices;
			std::vector<unsigned> indices;
			int indicesSize;
			int vertSize;
		};

		class Mesh
		{
			public:
        static GLfloat* GetUVs();
        static unsigned GetUVSize();

        static GLfloat* CreateQuad();
        static GLfloat* CreateQuadColor();

				static MeshInfo CreateSphere();
				static GLfloat* CreateSphereColor();
				static void SetSphereColor(Vec3& color);

        static Vec3 GetCircleVector(unsigned i, unsigned tessellation);
        static void CreateCylinderCap(std::vector<Vec3>& vertices, std::vector<unsigned>& indices,
          unsigned tessellation, float height, float radius, bool isTop);
        static MeshInfo CreateCylinder();
        static GLfloat* CreateCylinderColor();
        static void SetCylinderColor(Vec3& color);

				static GLfloat* CreateCube();
				static GLfloat* CreateCubeColor();
				static void SetCubeColor(Vec3& color);

				static GLfloat* CreateTetrahedron();
				static GLfloat* CreateTetrahedronColor();
				static void SetTetrahedronColor(Vec3& color);

				static GLfloat* CreatePyramid();
				static GLfloat* CreatePyramidColor();
				static void SetPyramidColor(Vec3& color);

				static GLfloat* CreateLine();
				static GLfloat* CreateLineColor();
				
				static void SetLineColor(Vec3& color);

          //quad
        static GLfloat* GetQuad()
        {
          static GLfloat* quad = CreateQuad();
          return quad;
        }

        static unsigned int GetQuadSize();

        static GLfloat* GetQuadColor()
        {
          static GLfloat* quadColor = CreateQuadColor();
          return quadColor;
        }

				  //sphere
				static MeshInfo GetSphere()
				{
					static MeshInfo sphere = CreateSphere();
					return sphere;
				}

				static unsigned int GetSphereSize();

				static GLfloat* GetSphereColor()
				{
					static GLfloat* sphereColor = CreateSphereColor();
					return sphereColor;
				}

          //capsule
        static MeshInfo GetCylinder()
        {
          static MeshInfo cylinder = CreateCylinder();
          return cylinder;
        }

        static unsigned int GetCylinderSize();

        static GLfloat* GetCylinderColor()
        {
          static GLfloat* cylinderColor = CreateCylinderColor();
          return cylinderColor;
        }

				  //cube
				static GLfloat* GetCube()
				{
					static GLfloat* cube = CreateCube();
					return cube;
				}

				static unsigned int GetCubeSize();

				static GLfloat* GetCubeColor()
				{
					static GLfloat* cubeColor = CreateCubeColor();
					return cubeColor;
				}

				  //tetra
				static GLfloat* GetTetrahedron()
				{
					static GLfloat* tetrahedron = CreateTetrahedron();
					return tetrahedron;
				}

				static unsigned int GetTetrahedronSize();

				static GLfloat* GetTetrahedronColor()
				{
					static GLfloat* tetraColor = CreateTetrahedronColor();
					return tetraColor;
				}

				  //pyramid
				static GLfloat* GetPyramid()
				{
					static GLfloat* pyramid = CreatePyramid();
					return pyramid;
				}

				static unsigned int GetPyramidSize();

				static GLfloat* GetPyramidColor()
				{
					static GLfloat* pyramidColor = CreatePyramidColor();
					return pyramidColor;
				}

				  //line
				static GLfloat* GetLine()
				{
					static GLfloat* line = CreateLine();
					return line;
				}

				static unsigned int GetLineSize();

				static GLfloat* GetLineColor()
				{
					static GLfloat* line = CreateLineColor();
					return line;
				}
		};
	}

}