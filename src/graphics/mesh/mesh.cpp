/*****************************************************************
Filename: mesh.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "mesh.h"

namespace TestEngine
{
	namespace Graphics
	{
    static unsigned int uvSize;
    static unsigned int quadSize;
		static unsigned int sphereSize;
    static unsigned int cylinderSize;
		static unsigned int cubeSize;
		static unsigned int tetrahedronSize;
		static unsigned int pyramidSize;
		static unsigned int lineSize;

    GLfloat * Mesh::GetUVs()
    {
      static GLfloat g_uv_buffer_data[] = { 0.0f, 0.0f };

      uvSize = sizeof(g_uv_buffer_data);

      return g_uv_buffer_data;
    }

    unsigned Mesh::GetUVSize()
    {
      return uvSize;
    }

    GLfloat* Mesh::CreateQuad()
    {
      static GLfloat g_vertex_buffer_data[] = {
        -1.0f,-1.0f,-1.0f, // triangle 1 : begin
        -1.0f, 1.0f,-1.0f,
         1.0f,-1.0f,-1.0f, // triangle 1 : end

         1.0f, 1.0f,-1.0f, // triangle 2 : begin
         1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f // triangle 2 : end
      };

      quadSize = sizeof(g_vertex_buffer_data);

      return g_vertex_buffer_data;
    }

    GLfloat * Mesh::CreateQuadColor()
    {
      // One color for each vertex. They were generated randomly.
      static GLfloat g_color_buffer_data[] = {
       0.0f,  0.0f,  1.0f,
       0.0f,  1.0f,  1.0f,
       1.0f,  0.0f,  1.0f,
      
       1.0f,  1.0f,  1.0f,
       1.0f,  0.0f,  1.0f,
       0.0f,  1.0f,  1.0f
      };

      return g_color_buffer_data;
    }

    MeshInfo Mesh::CreateSphere()
		{
			std::vector<Vec3> vertices;
			static std::vector<unsigned> indices;

			unsigned tessellation = 16;
			unsigned verticalSegments = tessellation;
			unsigned horizontalSegments = tessellation * 2;

			float radius = 1.0f;

			for (unsigned i = 0; i <= verticalSegments; ++i)
			{
				float v = 1 - (float)i / verticalSegments;

				float latitude = (i * static_cast<float>(PI) / verticalSegments) - static_cast<float>(PI) / 2.0f;
				float deltaY = sinf(latitude);
				float deltaXZ = cosf(latitude);

				for (unsigned j = 0; j <= horizontalSegments; ++j)
				{
					float u = (float)j / horizontalSegments;
					float longitutde = j * (static_cast<float>(PI) * 2) / horizontalSegments;
					float deltaX = sinf(longitutde);
					float deltaZ = cosf(longitutde);

					deltaX *= deltaXZ;
					deltaZ *= deltaXZ;

					Vec3 normal(deltaX, deltaY, deltaZ);

					vertices.push_back(normal * radius);
				}
			}

			unsigned stride = horizontalSegments + 1;

			for (unsigned i = 0; i < verticalSegments; ++i)
			{
				for (unsigned j = 0; j <= horizontalSegments; ++j)
				{
					unsigned nextI = i + 1;
					unsigned nextJ = (j + 1) % stride;

					indices.push_back(i * stride + j);
					indices.push_back(i * stride + nextJ);
					indices.push_back(nextI * stride + j);

					indices.push_back(i * stride + nextJ);
					indices.push_back(nextI * stride + nextJ);
					indices.push_back(nextI * stride + j);
				}
			}

			for (auto it = indices.begin(); it != indices.end(); it += 3)
			{
				std::swap(*it, *(it + 2));
			}

			unsigned indSize = indices.size();


			  //hardcoded value for size...
			static GLfloat s_vertex_buffer_data[1683];
			for (unsigned i = 0; i < 561; ++i)
			{
				s_vertex_buffer_data[3 * i] = vertices[i].x;
				s_vertex_buffer_data[3 * i + 1] = vertices[i].y;
				s_vertex_buffer_data[3 * i + 2] = vertices[i].z;
			}

			sphereSize = sizeof(s_vertex_buffer_data);

			return MeshInfo(s_vertex_buffer_data, indices, indSize, sphereSize);
		}

		GLfloat* Mesh::CreateSphereColor()
		{
			static GLfloat s_color_buffer_data[1683];
			float val1, val2, val3;
			
			for (unsigned i = 0; i < 561; ++i)
			{
				if ((i % 75) == 0)
				{
					val1 = (std::rand() % 100) / 100.0f;
					val2 = (std::rand() % 100) / 100.0f;
					val3 = (std::rand() % 100) / 100.0f;
				}
					
				s_color_buffer_data[3 * i] = val1;
				s_color_buffer_data[3 * i + 1] = val2;
				s_color_buffer_data[3 * i + 2] = val3;
			}

			return s_color_buffer_data;
		}

		void Mesh::SetSphereColor(Vec3& color)
		{
			GLfloat* sphere = GetSphereColor();

			for (unsigned i = 0; i < (GetSphereSize() / 12); ++i)
			{
				sphere[3 * i + 0] = color.x;
				sphere[3 * i + 1] = color.y;
				sphere[3 * i + 2] = color.z;
			}
		}

      //Cylinder
    // Helper computes a point on a unit circle, aligned to the x/z plane and centered on the origin.
    inline Vec3 Mesh::GetCircleVector(unsigned i, unsigned tessellation)
    {
      float angle = i * (static_cast<float>(PI) * 2.0f) / tessellation;
      float dx = sin(angle);
      float dz = cos(angle);

      return Vec3(dx, 0, dz);
    }

    // Helper creates a triangle fan to close the end of a cylinder / cone
    void Mesh::CreateCylinderCap(std::vector<Vec3>& vertices, std::vector<unsigned>& indices, unsigned tessellation, float height, float radius, bool isTop)
    {
      // Create cap indices.
      for (unsigned i = 0; i < tessellation - 2; i++)
      {
        unsigned i1 = (i + 1) % tessellation;
        unsigned i2 = (i + 2) % tessellation;

        if (isTop)
        {
          std::swap(i1, i2);
        }

        unsigned vbase = vertices.size();
        indices.push_back(vbase + i1);
        indices.push_back(vbase);
        indices.push_back(vbase + i2);
      }

      Vec3 normal(0, 1, 0);
      Vec3 textureScale(-.5f, -.5f, -.5f);

      if (!isTop)
      {
        normal = -normal;
        textureScale.x *= -1;
      }

      // Create cap vertices.
      for (unsigned i = 0; i < tessellation; i++)
      {
        Vec3 circleVector = GetCircleVector(i, tessellation);

        Vec3 position = (circleVector * radius) + (normal * height);

        vertices.push_back(position);
      }
    }

    // Creates a cylinder primitive.
    MeshInfo Mesh::CreateCylinder()
    {
      float height = 1.0;
      float diameter = 2.0f;
      unsigned tessellation = 16;

      std::vector<Vec3> vertices;
      std::vector<unsigned> indices;

      height /= 2;

      Vec3 topOffset = Vec3(0, 1, 0) * height;

      float radius = diameter / 2;
      unsigned stride = tessellation + 1;

      // Create a ring of triangles around the outside of the cylinder.
      for (unsigned i = 0; i <= tessellation; i++)
      {
        Vec3 normal = GetCircleVector(i, tessellation);

        Vec3 sideOffset = normal * radius;

        float u = (float)i / tessellation;

        vertices.push_back(Vec3(sideOffset + topOffset));
        vertices.push_back(Vec3(sideOffset - topOffset));

        indices.push_back((i * 2 + 2) % (stride * 2));
        indices.push_back(i * 2);
        indices.push_back(i * 2 + 1);

        indices.push_back((i * 2 + 2) % (stride * 2));
        indices.push_back(i * 2 + 1);
        indices.push_back((i * 2 + 3) % (stride * 2));
      }

      // Create flat triangle fan caps to seal the top and bottom.
      CreateCylinderCap(vertices, indices, tessellation, height, radius, true);
      CreateCylinderCap(vertices, indices, tessellation, height, radius, false);
      
      
      unsigned indSize = indices.size();
      Mesh* primitive = new Mesh;
      vertices.size();
      //if (rightHand)
        //ReverseWinding(vertices, indices);
      static GLfloat s_vertex_buffer_data[198];
      for (unsigned i = 0; i < 66; ++i)
      {
        s_vertex_buffer_data[3 * i] = vertices[i].x;
        s_vertex_buffer_data[3 * i + 1] = vertices[i].y;
        s_vertex_buffer_data[3 * i + 2] = vertices[i].z;
      }

      cylinderSize = sizeof(s_vertex_buffer_data);

      return MeshInfo(s_vertex_buffer_data, indices, indSize, cylinderSize);
    }

    GLfloat* Mesh::CreateCylinderColor()
    {
      static GLfloat s_color_buffer_data[1683];
      float val1, val2, val3;

      for (unsigned i = 0; i < 561; ++i)
      {
        if ((i % 75) == 0)
        {
          val1 = (std::rand() % 100) / 100.0f;
          val2 = (std::rand() % 100) / 100.0f;
          val3 = (std::rand() % 100) / 100.0f;
        }

        s_color_buffer_data[3 * i] = val1;
        s_color_buffer_data[3 * i + 1] = val2;
        s_color_buffer_data[3 * i + 2] = val3;
      }

      return s_color_buffer_data;
    }

    void Mesh::SetCylinderColor(Vec3 & color)
    {
      GLfloat* cylinder = GetCylinderColor();

      for (unsigned i = 0; i < (GetCylinderSize() / 12); ++i)
      {
        cylinder[3 * i + 0] = color.x;
        cylinder[3 * i + 1] = color.y;
        cylinder[3 * i + 2] = color.z;
      }
    }

      //cube
		GLfloat* Mesh::CreateCube()
		{
			static GLfloat g_vertex_buffer_data[] = {
				-1.0f,-1.0f,-1.0f, // triangle 1 : begin
				-1.0f,-1.0f, 1.0f,
				-1.0f, 1.0f, 1.0f, // triangle 1 : end
				
				1.0f, 1.0f,-1.0f, // triangle 2 : begin
				-1.0f,-1.0f,-1.0f,
				-1.0f, 1.0f,-1.0f, // triangle 2 : end
				
				1.0f,-1.0f, 1.0f,
				-1.0f,-1.0f,-1.0f,
				1.0f,-1.0f,-1.0f,
				
				1.0f, 1.0f,-1.0f,
				1.0f,-1.0f,-1.0f,
				-1.0f,-1.0f,-1.0f,
				
				-1.0f,-1.0f,-1.0f,
				-1.0f, 1.0f, 1.0f,
				-1.0f, 1.0f,-1.0f,
				
				1.0f,-1.0f, 1.0f,
				-1.0f,-1.0f, 1.0f,
				-1.0f,-1.0f,-1.0f,
				
				-1.0f, 1.0f, 1.0f,
				-1.0f,-1.0f, 1.0f,
				1.0f,-1.0f, 1.0f,
				
				1.0f, 1.0f, 1.0f,
				1.0f,-1.0f,-1.0f,
				1.0f, 1.0f,-1.0f,
				
				1.0f,-1.0f,-1.0f,
				1.0f, 1.0f, 1.0f,
				1.0f,-1.0f, 1.0f,
				
				1.0f, 1.0f, 1.0f,
				1.0f, 1.0f,-1.0f,
				-1.0f, 1.0f,-1.0f,
				
				1.0f, 1.0f, 1.0f,
				-1.0f, 1.0f,-1.0f,
				-1.0f, 1.0f, 1.0f,
				
				1.0f, 1.0f, 1.0f,
				-1.0f, 1.0f, 1.0f,
				1.0f,-1.0f, 1.0f

			};

			cubeSize = sizeof(g_vertex_buffer_data);

			return g_vertex_buffer_data;
		}

		GLfloat* Mesh::CreateCubeColor()
		{
			// One color for each vertex. They were generated randomly.
			static GLfloat g_color_buffer_data[] = {
				0.583f,  0.771f,  0.014f,
				0.609f,  0.115f,  0.436f,
				0.327f,  0.483f,  0.844f,
				
				0.822f,  0.569f,  0.201f,
				0.435f,  0.602f,  0.223f,
				0.310f,  0.747f,  0.185f,
				
				0.597f,  0.770f,  0.761f,
				0.559f,  0.436f,  0.730f,
				0.359f,  0.583f,  0.152f,
				
				0.483f,  0.596f,  0.789f,
				0.559f,  0.861f,  0.639f,
				0.195f,  0.548f,  0.859f,
				
				0.014f,  0.184f,  0.576f,
				0.771f,  0.328f,  0.970f,
				0.406f,  0.615f,  0.116f,
				
				0.676f,  0.977f,  0.133f,
				0.971f,  0.572f,  0.833f,
				0.140f,  0.616f,  0.489f,
				
				0.997f,  0.513f,  0.064f,
				0.945f,  0.719f,  0.592f,
				0.543f,  0.021f,  0.978f,
				
				0.279f,  0.317f,  0.505f,
				0.167f,  0.620f,  0.077f,
				0.347f,  0.857f,  0.137f,
				
				0.055f,  0.953f,  0.042f,
				0.714f,  0.505f,  0.345f,
				0.783f,  0.290f,  0.734f,
				
				0.722f,  0.645f,  0.174f,
				0.302f,  0.455f,  0.848f,
				0.225f,  0.587f,  0.040f,
				
				0.517f,  0.713f,  0.338f,
				0.053f,  0.959f,  0.120f,
				0.393f,  0.621f,  0.362f,
				
				0.673f,  0.211f,  0.457f,
				0.820f,  0.883f,  0.371f,
				0.982f,  0.099f,  0.879f
			};

			return g_color_buffer_data;
		}

		void Mesh::SetCubeColor(Vec3 & color)
		{
			GLfloat* cube = GetCubeColor();

			for (unsigned i = 0; i < (GetCubeSize() / 12); ++i)
			{
				cube[3 * i + 0] = color.x;
				cube[3 * i + 1] = color.y;
				cube[3 * i + 2] = color.z;
			}

		}

		GLfloat* Mesh::CreateTetrahedron()
		{
			static GLfloat t_vertex_buffer_data[] = {
				-0.5f, 0.0f,-0.5f, // triangle 1 : begin
				0.5f, 0.0f, 0.5f,
				-0.5f, 0.0f, 0.5f,// triangle 1 : end
				
				0.0f, 1.0f, 0.0f, // triangle 2 : begin
				-0.5f, 0.0f, 0.5f,
				-0.5f, 0.0f,-0.5f, // triangle 2 : end
				
				0.0f, 1.0f, 0.0f,
				-0.5f, 0.0f,-0.5f,
				0.5f, 0.0f, 0.5f,
				
				0.0f, 1.0f, 0.0f,
				0.5f, 0.0f, 0.5f,
				-0.5f, 0.0f, 0.5f
			};

			tetrahedronSize = sizeof(t_vertex_buffer_data);

			return t_vertex_buffer_data;
		}

		GLfloat * Mesh::CreateTetrahedronColor()
		{
			static GLfloat t_color_buffer_data[] = {
				0.583f,  0.771f,  0.014f,
				0.609f,  0.115f,  0.436f,
				0.327f,  0.483f,  0.844f,

				0.822f,  0.569f,  0.201f,
				0.435f,  0.602f,  0.223f,
				0.310f,  0.747f,  0.185f,

				0.597f,  0.770f,  0.761f,
				0.559f,  0.436f,  0.730f,
				0.359f,  0.583f,  0.152f,

				0.483f,  0.596f,  0.789f,
				0.559f,  0.861f,  0.639f,
				0.195f,  0.548f,  0.859f,
			};

			return t_color_buffer_data;
		}

		void Mesh::SetTetrahedronColor(Vec3 & color)
		{
			GLfloat* tetrahedron = GetTetrahedronColor();

			for (unsigned i = 0; i < (GetTetrahedronSize() / 12); ++i)
			{
				tetrahedron[3 * i + 0] = color.x;
				tetrahedron[3 * i + 1] = color.y;
				tetrahedron[3 * i + 2] = color.z;
			}
		}

		GLfloat * Mesh::CreatePyramid()
		{
			static GLfloat p_vertex_buffer_data[] = {
				0.0f, 0.0f,-0.5f, // triangle 1 : begin
				0.5f, 0.0f, 0.0f,
				-0.5f, 0.0f, 0.0f,// triangle 1 : end
				
				0.0f, 0.0f,0.5f, 
				-0.5f, 0.0f, 0.0f,
				0.5f, 0.0f, 0.0f,
				
				0.0f, 1.0f, 0.0f, 
				-0.5f, 0.0f, 0.0f,
				0.0f, 0.0f, -0.5f,
				
				0.0f, 1.0f, 0.0f,
				0.0, 0.0f,-0.5f,
				0.5f, 0.0f, 0.0f,
				
				0.0f, 1.0f, 0.0f,
				0.5f, 0.0f, 0.0f,
				0.0f, 0.0f, 0.5f,
				
				0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.5f,
				-0.5f, 0.0f, 0.0f
			};

			pyramidSize = sizeof(p_vertex_buffer_data);

			return p_vertex_buffer_data;
		}

		GLfloat * Mesh::CreatePyramidColor()
		{
			static GLfloat p_color_buffer_data[] = {
				0.583f,  0.771f,  0.014f,
				0.609f,  0.115f,  0.436f,
				0.327f,  0.483f,  0.844f,

				0.822f,  0.569f,  0.201f,
				0.435f,  0.602f,  0.223f,
				0.310f,  0.747f,  0.185f,

				0.597f,  0.770f,  0.761f,
				0.559f,  0.436f,  0.730f,
				0.359f,  0.583f,  0.152f,

				0.483f,  0.596f,  0.789f,
				0.559f,  0.861f,  0.639f,
				0.195f,  0.548f,  0.859f,

				0.014f,  0.184f,  0.576f,
				0.771f,  0.328f,  0.970f,
				0.406f,  0.615f,  0.116f,

				0.676f,  0.977f,  0.133f,
				0.971f,  0.572f,  0.833f,
				0.140f,  0.616f,  0.489f,
			};

			return p_color_buffer_data;
		}

		void Mesh::SetPyramidColor(Vec3 & color)
		{
			GLfloat* pyramid = GetPyramidColor();

			for (unsigned i = 0; i < (GetPyramidSize() / 12); ++i)
			{
				pyramid[3 * i + 0] = color.x;
				pyramid[3 * i + 1] = color.y;
				pyramid[3 * i + 2] = color.z;
			}

		}

		GLfloat* Mesh::CreateLine()
		{
			static GLfloat l_vertex_buffer_data[] = {
				0.0f, 0.0f, 0.0f,
				1.0f, 0.0f, 0.0f
			};

			lineSize = sizeof(l_vertex_buffer_data);

			return l_vertex_buffer_data;
		}

		GLfloat* Mesh::CreateLineColor()
		{
			static GLfloat l_color_buffer_data[] = {
				1.0f,  0.0f,  0.0f,
				1.0f,  0.0f,  0.0f
			};

			return l_color_buffer_data;
		}

		void Mesh::SetLineColor(Vec3 & color)
		{
			GLfloat* line = GetLineColor();

			for (int i = 0; i < 2; ++i)
			{
				line[3 * i + 0] = color.x;
				line[3 * i + 1] = color.y;
				line[3 * i + 2] = color.z;
			}

		}

		  //sizes
    unsigned int Mesh::GetQuadSize()
    {
      return quadSize;
    }

		unsigned int Mesh::GetSphereSize()
		{
			return sphereSize;
		}
		
    unsigned int Mesh::GetCylinderSize()
    {
      return cylinderSize;
    }

		unsigned int Mesh::GetCubeSize()
		{
			return cubeSize;
		}
		
		unsigned int Mesh::GetTetrahedronSize()
		{
			return tetrahedronSize;
		}
		unsigned int Mesh::GetPyramidSize()
		{
			return pyramidSize;
		}

		unsigned int Mesh::GetLineSize()
		{
			return lineSize;
		}
	}
}