/*****************************************************************
Filename: spaceconstraints.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "space.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    void Space::UpdateConstraintPairs()
    {
      auto itPair = m_memManager.GetConstraintList().begin();

      for (; itPair != m_memManager.GetConstraintList().end(); ++itPair)
      {
        for (unsigned i = 0; i < 2; ++i)
        {
          switch (itPair->GetType(i))
          {
          case colliderType::eBOX:
            itPair->SetBody(i, &(m_memManager.GetBoxList()[itPair->GetId(i)]));
            break;

          case colliderType::eCAPSULE:
            itPair->SetBody(i, &(m_memManager.GetCapsuleList()[itPair->GetId(i)]));
            break;

          case colliderType::eSPHERE:
            itPair->SetBody(i, &(m_memManager.GetSphereList()[itPair->GetId(i)]));
            break;
          }
        }
      }
    }

    void Space::ConstraintTest()
    {
      //constraint testing and setup
      auto itConstraint = m_memManager.GetConstraintList().begin();

      for (; itConstraint != m_memManager.GetConstraintList().end(); ++itConstraint)
      {
        if(!itConstraint->TestConstraint())
          m_memManager.DeleteConstraint(itConstraint - m_memManager.GetConstraintList().begin());
      }
        
    }

    void Space::AddConstraint(ConstraintInfo& info)
    {
      m_constraintQueue.push(info);
    }

    void Space::AddConstraint()
    {
      m_constraintQueue.push(m_constraint);
    }

    float Space::GetConstraintDistance()
    {
      unsigned refA = m_constraint.idA;

      refA = (m_constraint.typeA != colliderType::eBOX) ? (refA + m_memManager.GetBoxList().size()) : refA;
      refA = (m_constraint.typeA == colliderType::eCAPSULE) ? (refA + m_memManager.GetSphereList().size()) : refA;

      unsigned refB = m_constraint.idB;

      refB = (m_constraint.typeB != colliderType::eBOX) ? (refB + m_memManager.GetBoxList().size()) : refB;
      refB = (m_constraint.typeB == colliderType::eCAPSULE) ? (refB + m_memManager.GetSphereList().size()) : refB;

      Body* A = GetBodyFromRefID(refA);
      Body* B = GetBodyFromRefID(refB);

      m_constraint.worldSpaceConnectionPoint[0] = A->GetOrientation() * m_constraint.connectionPoint[0] + A->GetPosition();
      m_constraint.worldSpaceConnectionPoint[1] = B->GetOrientation() * m_constraint.connectionPoint[1] + B->GetPosition();

      return (m_constraint.worldSpaceConnectionPoint[0] - m_constraint.worldSpaceConnectionPoint[1]).Length();
    }

    void Space::SetConstraintBody(unsigned bodyNumber, colliderType type, Vec3& worldPosition, Mat3& orientation)
    {
      m_constraint.createBodies = false;

      switch (bodyNumber)
      {
      case 1:
        m_constraint.idA = m_body->GetID();
        m_constraint.typeA = type;
        m_constraint.connectionPoint[0] = m_recentResult.collisionPoint;
        m_constraint.worldSpaceConnectionPoint[0] = worldPosition + orientation * m_recentResult.collisionPoint;
        break;

      case 2:
        m_constraint.idB = m_body->GetID();
        m_constraint.typeB = type;
        m_constraint.connectionPoint[1] = m_recentResult.collisionPoint;
        m_constraint.worldSpaceConnectionPoint[1] = worldPosition + orientation * m_recentResult.collisionPoint;
        break;
      }
    }

    void Space::SetConstraintDistance(float distance)
    {
      m_constraint.distance = distance;
    }

    void Space::SetConstraintStiffness(float stiffness)
    {
      m_constraint.stiffness = stiffness;
    }

    void Space::SetConstraintDamping(float damping)
    {
      m_constraint.damping = damping;
    }

    void Space::SetConstraintType(MemoryManager::constraintType type)
    {
      m_constraint.type = type;
    }

    void Space::SetEasyConstraintDistance()
    {
      unsigned refA = m_constraint.idA; 

      refA = (m_constraint.typeA != colliderType::eBOX) ? (refA + m_memManager.GetBoxList().size()) : refA;
      refA = (m_constraint.typeA == colliderType::eCAPSULE) ? (refA + m_memManager.GetSphereList().size()) : refA;

      unsigned refB = m_constraint.idB;

      refB = (m_constraint.typeB != colliderType::eBOX) ? (refB + m_memManager.GetBoxList().size()) : refB;
      refB = (m_constraint.typeB == colliderType::eCAPSULE) ? (refB + m_memManager.GetSphereList().size()) : refB;

      Vec3 worldPositionA = GetBodyFromRefID(refA)->GetPosition();
      Vec3 worldPositionB = GetBodyFromRefID(refB)->GetPosition();

      Vec3 directionVec = worldPositionB - worldPositionA;
      directionVec.Normalize();

      Vec3 directionA = GetBodyFromRefID(refA)->GetOrientation().CreateInverse() * directionVec;
      Vec3 directionB = GetBodyFromRefID(refB)->GetOrientation().CreateInverse() * -directionVec;

      m_constraint.connectionPointA = GetBodyFromRefID(refA)->CalculateConstraintPoint(directionA);
      m_constraint.worldSpaceConnectionPointA = GetBodyFromRefID(refA)->GetOrientation() * m_constraint.connectionPointA + worldPositionA;
      m_constraint.connectionPointB = GetBodyFromRefID(refB)->CalculateConstraintPoint(directionB);
      m_constraint.worldSpaceConnectionPointB = GetBodyFromRefID(refB)->GetOrientation() * m_constraint.connectionPointB + worldPositionB;

      m_constraint.distance = (m_constraint.worldSpaceConnectionPointB - m_constraint.worldSpaceConnectionPointA).Length();
    }

    ConstraintInfo* Space::GetConstraintInfo()
    {
      return &m_constraint;
    }
  }
}