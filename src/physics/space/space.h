/*****************************************************************
Filename: space.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../raycasting/Raycast.h"
#include "../../meminterface/meminterface.h"
#include "../collision/broadphase/sweepandprune/sap.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		class Space
		{
			public:
				Space() {};

				void  Initialize(float time);
				void  TestInitialize();

        MemoryManager::MemInterface& GetMemManager()                    { return m_memManager; }

          //set thread managers
        void  SetJobManager(Base::JobManager& jobManager)               { m_jobManager = &jobManager; }

          //timestep
				float GetDT()                                                   { return m_dt; }
				void  SetDT(float dt);

          //main run functions
        void  UpdateSpace();
				void  RunStep();     
        void  FinishStep();
				
				  //add bodies
				void      AddBody();
        void      DuplicateBody();
				void      SetBodyInfo(BodyInfo& info);
				void      SetBodyInfo(colliderType type, Vec3& position, Vec3& dimensions, Mat3& orientation, 
							      float density, bool rotationLock, bool staticBody);

          //turn islanding and threading on and off
        void      SetIslanding(bool islanding);
        void      SetNumActiveIslands(unsigned num)                   { m_sap.SetNumActiveIslands(num); }
        void      SetNumActiveMasters(unsigned num)                   { m_sap.SetNumActiveMasters(num); }
        void      SetLargestIsland(unsigned num)                      { m_sap.SetLargestIsland(num); }

        bool      GetIslanding();
        unsigned  GetNumActiveIslands()                               { return m_sap.GetNumActiveIslands(); }
        unsigned  GetNumActiveMasters()                               { return m_sap.GetNumActiveMasters(); }
        unsigned  GetLargestIsland()                                  { return m_sap.GetLargestIsland(); }

          //add constraint
        void      AddConstraint(ConstraintInfo& info);
        void      AddConstraint();
        float     GetConstraintDistance();
        void      SetConstraintBody(unsigned bodyNumber, colliderType type, Vec3& worldPosition, Mat3& orientation);
        void      SetConstraintDistance(float distance);
        void      SetConstraintStiffness(float stiffness);
        void      SetConstraintDamping(float damping);
        void      SetConstraintType(MemoryManager::constraintType type);
        void      SetEasyConstraintDistance();
        
        ConstraintInfo*    GetConstraintInfo();

          //manage constraints
        MemoryManager::MemInterface::ConstraintList& GetConstraintList()  { return m_memManager.GetConstraintList(); }
        void      DeleteSelectedConstraint(unsigned i)                { m_memManager.DeleteConstraint(i); }

				  //single body access
				bool      BodyActive()                                        { return m_bodyActive; }
				void      GetNextBody();
				void      SetNoActiveBody();
				unsigned  GetActiveBody();

				Body*     GetBodyFromRefID(unsigned refID);
				Body*     GetBox(unsigned boxID);
				Body*     GetSphere(unsigned sphereID);
				Body*     GetCapsule(unsigned capsuleID);
    
          //raycast result
        Vec3      GetRaycastCollision()                               { return m_recentResult.collisionPoint; }
        RaycastResult* GetRaycastResult()                             { return &m_recentResult; }
        void      UpdateWorldRaycastPoint();

				  //interact with currently selected body
				void      MoveBody(Vec3& move);

         //rotation
				void      RotateBody(float radians);
				void      RotateBody(float radians, Vec3& axis);
        void      ReturnToZeroOrientation();
        void      UpdateOrientation(Vec3& eulerAngles);

          //body modification
				void      SwitchAxis();
				void      ModifyBodyDimensions(float amount);
				void      ModifyBodyDimensions(Vec3& axis, float amount);


          //delete bodies - deletes current active body
        void      DeleteBody();

				  //raycasting
				RaycastResult   CastRay(Vec3& origin, Vec3& direction);
        RaycastResult   CastAndStoreRay(Vec3& origin, Vec3& direction);

          //public and static for use with jobs
        static void ThreadJob(unsigned island, void* space, bool master, unsigned servant);

			private:
        void CreateBodies();  //ensures bodies are entered at one specific time to prevent vector movement
        void UpdatePairs();   //used to ensure that collision pair pointers are accurate
				void BroadPhase();    //sap
				void NarrowPhase();
				void Resolution();
				void Movement();
        void DeletionPhase();

        void AddJobs();  //kicks off the new set of jobs for the frame

        void CollisionNonIsland();
        void UpdateCollisionPairsNonIsland();
        void ResolveNonIsland();
        void PositionCorrectionNonIsland();

          //must be static for function pointer assignment
        void UpdateCollisionPairsIsland(unsigned island);

        static void CollisionIsland(unsigned island, Space& space);
        static void ResolveIsland(unsigned island, Space& space);
        static void ResolveMasterIsland(unsigned master, unsigned servant, Space& space);
        static void PositionCorrectionIsland(unsigned island, Space& space);
        static void SetThreadSafe(unsigned island, bool safe, Space& space) { space.m_sap.SetThreadSafe(island, safe); }

        void ConstraintTest();
        void UpdateConstraintPairs();
        void ResolveConstraint();

          //only one space active at a time - static enforced for job system
				MemoryManager::MemInterface m_memManager;

        SweepAndPrune               m_sap;

				float                       m_dt;

				  //current object to add
				BodyInfo                    m_info;
        bool                        m_duplicate = false;

				  //currently selected body
				Body*                       m_body;  
				bool                        m_bodyActive;
				unsigned                    m_bodyRef;

          //current constraint info
        ConstraintInfo              m_constraint;

          //most recent raycast info
        RaycastResult               m_recentResult;

				  //axis for rotation, dimension changes
				Vec3                        m_changeAxis;
				unsigned                    m_axisIndex;

          //for threading with jobs
        Base::JobManager*           m_jobManager;

          //for creating bodies and constraints
        std::queue<BodyInfo>        m_bodyQueue;
        std::queue<ConstraintInfo>  m_constraintQueue;

        std::queue<DeletionInfo>    m_deletionQueue;
		};
	}

}