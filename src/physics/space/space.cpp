/*****************************************************************
Filename: space.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "space.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
    Space* s_space;
    static unsigned s_numImpulseIterations = 10;

		void Space::Initialize(float time)
		{
			m_dt = time;
			m_bodyActive = false;
			m_body = nullptr;
      m_bodyRef = static_cast<unsigned>(UNSIGNED_MAX);
      m_axisIndex = 0;
      m_changeAxis = Vec3(1, 0, 0);
      
      m_sap.Initialize(&m_memManager);
      m_sap.SetNumActiveIslands(0);
      m_sap.SetNumActiveMasters(0);
      m_sap.SetLargestIsland(0);

      s_space = this;
		}

    void Space::UpdateSpace()
    {
      if (!m_jobManager->m_jobQueue.empty())
        return;

      CreateBodies();
      UpdatePairs();

        //debug
      UpdateWorldRaycastPoint();
    }

    void Space::RunStep()
		{
      if (!m_jobManager->m_jobQueue.empty())
        return;

      BroadPhase();

      switch (m_sap.IslandingState())
      {
        case eOFF:   
          NarrowPhase();
          Resolution();
          Movement();
          break;

        case eON:
          ConstraintTest();
          ResolveConstraint();
          Movement();
          AddJobs();
          break;
      }      
		}

    void Space::FinishStep()
    {
      DeletionPhase();
    }

    void Space::SetDT(float dt)
    {
      m_dt = dt;
    }
		
		void Space::AddBody()
		{
      m_bodyQueue.push(m_info);
		}

    void Space::DuplicateBody()
    {
      if (!m_bodyActive)
        return;

      m_info.position = GetBodyFromRefID(GetActiveBody())->GetPosition();
      m_info.orientation = GetBodyFromRefID(GetActiveBody())->GetOrientation();
      m_info.collider = GetBodyFromRefID(GetActiveBody())->GetType();
      m_info.scale = GetBodyFromRefID(GetActiveBody())->GetScale();
      m_info.offset = GetBodyFromRefID(GetActiveBody())->GetOffset();
      m_info.velocity = GetBodyFromRefID(GetActiveBody())->GetVelocity();
      m_info.angularVelocity = GetBodyFromRefID(GetActiveBody())->GetAngVelocity();
      m_info.density = GetBodyFromRefID(GetActiveBody())->GetDensity();
      m_info.staticObj = GetBodyFromRefID(GetActiveBody())->GetStatic();
      m_info.rotationLock = GetBodyFromRefID(GetActiveBody())->GetRotationLock();
      m_info.ghost = false;
      m_info.dimensions[0] = GetBodyFromRefID(GetActiveBody())->GetDimensions().x;
      m_info.dimensions[1] = GetBodyFromRefID(GetActiveBody())->GetDimensions().y;
      m_info.dimensions[2] = GetBodyFromRefID(GetActiveBody())->GetDimensions().z;
      m_info.deleted = GetBodyFromRefID(GetActiveBody())->GetDeleteFlag();


      AddBody();
      m_duplicate = true;
    }

    void Space::CreateBodies()
    {
      unsigned size = m_bodyQueue.size();
      unsigned ref;
      for (unsigned i = 0; i < size; ++i)
      {
        ref = m_memManager.AddBody(m_bodyQueue.front());
        m_bodyQueue.pop();
      }

      if (m_duplicate)
      {
        GetBodyFromRefID(m_bodyRef)->SetActive(false);
        m_bodyRef = ref;

        if((m_info.collider == MemoryManager::eSPHERE) || (m_info.collider == MemoryManager::eCAPSULE))
            m_bodyRef += m_memManager.GetBoxList().size();

        if (m_info.collider == MemoryManager::eCAPSULE)
          m_bodyRef += m_memManager.GetSphereList().size();

        GetBodyFromRefID(m_bodyRef)->SetActive(true);

        m_duplicate = false;
      }

      size = m_constraintQueue.size();
      for (unsigned i = 0; i < size; ++i)
      {
        ConstraintInfo info = m_constraintQueue.front();
        m_constraintQueue.pop();

        unsigned bodyIdA;
        unsigned bodyIdB;

        if (info.createBodies)
        {
          bodyIdA = m_memManager.AddBody(info.bodyA);
          bodyIdB = m_memManager.AddBody(info.bodyB);
        }
        
        else
        {
          bodyIdA = info.idA;
          bodyIdB = info.idB;
        }

        //body info
        PhysicsGL::Body* A = nullptr;
        PhysicsGL::Body* B = nullptr;

        switch (info.typeA)
        {
          case MemoryManager::colliderType::eBOX:
            A = &(m_memManager.GetBoxList())[bodyIdA];
            break;

          case MemoryManager::colliderType::eCAPSULE:
            A = &(m_memManager.GetCapsuleList())[bodyIdA];
            break;

          case MemoryManager::colliderType::eSPHERE:
            A = &(m_memManager.GetSphereList())[bodyIdA];
            break;
        } 

        switch (info.typeB)
        {
          case MemoryManager::colliderType::eBOX:
            B = &(m_memManager.GetBoxList())[bodyIdB];
            break;

          case MemoryManager::colliderType::eCAPSULE:
            B = &(m_memManager.GetCapsuleList())[bodyIdB];
            break;

          case MemoryManager::colliderType::eSPHERE:
            B = &(m_memManager.GetSphereList())[bodyIdB];
            break;
        }

        unsigned constraintId = m_memManager.AddConstraint(A, B);
        ConstraintPair* constraint = &(m_memManager.GetConstraintList())[constraintId];
        constraint->SetConstraintType(info.type);
        constraint->SetConstraintDist(info.distance);
        constraint->SetContact(0, info.connectionPoint[0]);
        constraint->SetContact(1, info.connectionPoint[1]);
        constraint->SetWorldContact(0, info.worldSpaceConnectionPoint[0]);
        constraint->SetWorldContact(1, info.worldSpaceConnectionPoint[1]);
        constraint->SetConstraintStiffness(info.stiffness);
        constraint->SetConstraintDamping(info.damping);
        constraint->SetDeletedFlag(info.deleted);
        constraint->Initialize();
      }
    }

    void Space::SetBodyInfo(BodyInfo& info)
    {
      m_info = info;
    }

		void Space::SetBodyInfo(colliderType type, Vec3& position, Vec3& dimensions, Mat3& orientation, 
      float density, bool rotationLock, bool staticBody)
		{
			m_info.collider = type;
			switch (m_info.collider)
			{
				case MemoryManager::eBOX:
          m_info.dimensions[0] = dimensions.x;
          m_info.dimensions[1] = dimensions.y;
          m_info.dimensions[2] = dimensions.z;
					break;

        case MemoryManager::eSPHERE:
          m_info.dimensions[0] = dimensions.x;
          break;

        case MemoryManager::eCAPSULE:
          m_info.dimensions[0] = dimensions.x;
          m_info.dimensions[1] = dimensions.y;
          break;
			}

      m_info.position = position;
      m_info.orientation = orientation;
      m_info.density = density;
      m_info.rotationLock = rotationLock;
      m_info.staticObj = staticBody;

      m_info.velocity = Vec3(0.0f, 0.0f, 0.0f);
      m_info.angularVelocity = Vec3(0.0f, 0.0f, 0.0f);
		}
    
		void Space::NarrowPhase()
		{
      for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
        CollisionIsland(i, *this);

      ConstraintTest();
		}

    void Space::CollisionNonIsland()
    {
      //collision testing and setup
      auto itPair = m_memManager.GetPairList().begin();

      for (; itPair != m_memManager.GetPairList().end(); ++itPair)
      {
        if (itPair->GetActive())
        {
          itPair->PrepForNextFrame();

          if (itPair->TestGJK())
          {
            itPair->GetA()->SetCollision(true);
            itPair->GetB()->SetCollision(true);
          }
        }
      }
    }

    void Space::CollisionIsland(unsigned island, Space& space)
    {
      //collision testing and setup
      auto itPair = space.m_memManager.GetIslandList(island).begin();

      for (; itPair != space.m_memManager.GetIslandList(island).end(); ++itPair)
      {
        if (itPair->GetActive())
        {
          itPair->PrepForNextFrame();

          if (itPair->TestGJK())
          {
            itPair->GetA()->SetCollision(true);
            itPair->GetB()->SetCollision(true);
          }
        }
      }
    }

    void Space::Resolution()
    {
      for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
      {
        ResolveIsland(i, *this);
        PositionCorrectionIsland(i, *this);
      }
   
      ResolveConstraint();
    }

    void Space::ResolveNonIsland()
    {
      auto itPair = m_memManager.GetPairList().begin();

      for (; itPair != m_memManager.GetPairList().end(); ++itPair)
      {
        if (itPair->GetActive() && itPair->GetCollision())
          itPair->PrepForCollision();
      }

      for (unsigned i = 0; i < s_numImpulseIterations; ++i)
      {
        itPair = m_memManager.GetPairList().begin();

        for (; itPair != m_memManager.GetPairList().end(); ++itPair)
        {
          if (itPair->GetActive() && itPair->GetCollision())
          {
            unsigned size = itPair->GetStoredContactSize();
            for (unsigned j = 0; j < size; ++j)
              itPair->Resolve(m_dt, j, i, s_numImpulseIterations);
          }
        }
      }
    }

    void Space::ResolveIsland(unsigned island, Space& space)
    {
      auto itPair = space.m_memManager.GetIslandList(island).begin();

      for (; itPair != space.m_memManager.GetIslandList(island).end(); ++itPair)
      {
        if (itPair->GetActive() && itPair->GetCollision())
          itPair->PrepForCollision();
      }

      for (unsigned i = 0; i < s_numImpulseIterations; ++i)
      {
        itPair = space.m_memManager.GetIslandList(island).begin();

        for (; itPair != space.m_memManager.GetIslandList(island).end(); ++itPair)
        {
          if (itPair->GetActive() && itPair->GetCollision())
          {
            unsigned size = itPair->GetStoredContactSize();
            for (unsigned j = 0; j < size; ++j)
              itPair->Resolve(space.m_dt, j, i, s_numImpulseIterations);
          }
        }
      }
    }

    void Space::ResolveMasterIsland(unsigned master, unsigned servant, Space& space)
    {
      auto itPair = space.m_memManager.GetIslandList(master).begin();

      for (; itPair != space.m_memManager.GetIslandList(master).end(); ++itPair)
      {
        if (itPair->GetActive() && itPair->GetCollision())
          itPair->PrepForCollision();
      }

      itPair = space.m_memManager.GetIslandList(servant).begin();

      for (; itPair != space.m_memManager.GetIslandList(servant).end(); ++itPair)
      {
        if (itPair->GetActive() && itPair->GetCollision())
          itPair->PrepForCollision();
      }

      for (unsigned i = 0; i < s_numImpulseIterations; ++i)
      {
        itPair = space.m_memManager.GetIslandList(master).begin();

        for (; itPair != space.m_memManager.GetIslandList(master).end(); ++itPair)
        {
          if (itPair->GetActive() && itPair->GetCollision())
          {
            unsigned size = itPair->GetStoredContactSize();
            for (unsigned j = 0; j < size; ++j)
              itPair->Resolve(space.m_dt, j, i, s_numImpulseIterations);
          }
        }

        itPair = space.m_memManager.GetIslandList(servant).begin();

        for (; itPair != space.m_memManager.GetIslandList(servant).end(); ++itPair)
        {
          if (itPair->GetActive() && itPair->GetCollision())
          {
            unsigned size = itPair->GetStoredContactSize();
            for (unsigned j = 0; j < size; ++j)
              itPair->Resolve(space.m_dt, j, i, s_numImpulseIterations);
          }
        }

      }
    }

    void Space::PositionCorrectionNonIsland()
    {
      auto itPair = m_memManager.GetPairList().begin();

      for (; itPair != m_memManager.GetPairList().end(); ++itPair)
      {
        if (itPair->GetCollision())
          itPair->PositionCorrection();
      }
    }

    void Space::PositionCorrectionIsland(unsigned island, Space& space)
    {
      auto itPair = space.m_memManager.GetIslandList(island).begin();

      for (; itPair != space.m_memManager.GetIslandList(island).end(); ++itPair)
      {
        if (itPair->GetActive() && itPair->GetCollision())
          itPair->PositionCorrection();
      }
    }

    void Space::ResolveConstraint()
    {
      //resolve constraints
      for (unsigned i = 0; i < s_numImpulseIterations; ++i)
      {
        auto itConstraint = m_memManager.GetConstraintList().begin();

        for (; itConstraint != m_memManager.GetConstraintList().end(); ++itConstraint)
          if (itConstraint->GetResolve())
            itConstraint->ResolveConstraint(m_dt);
      }
    }

		void Space::Movement()
		{
        //move objects and update their aabbs
		  auto itBox = m_memManager.GetBoxList().begin();

      for (; itBox != m_memManager.GetBoxList().end(); ++itBox)
      {
        (*itBox).Movement(m_dt);
        (m_memManager.GetAabbList()[(*itBox).GetAabbId()]).SetPosition((*itBox).GetPosition());
        (m_memManager.GetAabbList()[(*itBox).GetAabbId()]).SetDimensions((*itBox).CalculateAABB());
      }
			  

		  auto itSphere = m_memManager.GetSphereList().begin();

		  for (; itSphere != m_memManager.GetSphereList().end(); ++itSphere)
      {
			  (*itSphere).Movement(m_dt);
        (m_memManager.GetAabbList()[(*itSphere).GetAabbId()]).SetPosition((*itSphere).GetPosition());
        (m_memManager.GetAabbList()[(*itSphere).GetAabbId()]).SetDimensions((*itSphere).CalculateAABB());
      }

      auto itCapsule = m_memManager.GetCapsuleList().begin();

      for (; itCapsule != m_memManager.GetCapsuleList().end(); ++itCapsule)
      {
        (*itCapsule).Movement(m_dt);
        (m_memManager.GetAabbList()[(*itCapsule).GetAabbId()]).SetPosition((*itCapsule).GetPosition());
        (m_memManager.GetAabbList()[(*itCapsule).GetAabbId()]).SetDimensions((*itCapsule).CalculateAABB());
      }   
		}

    void Space::DeletionPhase()
    {
      unsigned size = m_deletionQueue.size();
      for (unsigned i = 0; i < size; ++i)
      {
        DeletionInfo info = m_deletionQueue.front();
        m_deletionQueue.pop();
        m_memManager.DeleteBody(info);   
      }
    }
	}
}