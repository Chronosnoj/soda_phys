/*****************************************************************
Filename: spacethreading.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "space.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    void Space::AddJobs()
    {
      if (!m_jobManager)
        return;

      Base::JobFunction jobFunc = Space::ThreadJob;
      m_jobManager->m_jobMutex.lock();

      for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
      {
        if (!m_sap.ActiveIsland(i) || m_sap.GetIslandStatus(i) == eSERVANT)
          continue;

        bool master = false;
        if (m_sap.GetIslandStatus(i) == 1)
          master = true;

        Base::Job newJob(jobFunc, i, this, master, m_sap.GetIslandCompanion(i));
        m_jobManager->m_jobQueue.push(newJob);
      }

      m_jobManager->m_jobMutex.unlock();
    }

    void Space::ThreadJob(unsigned island, void* space, bool master, unsigned servant)
    {
      Space* pSpace = static_cast<Space*>(space);

      if(!master)
      {
        CollisionIsland(island, *pSpace);
        ResolveIsland(island, *pSpace);
        PositionCorrectionIsland(island, *pSpace);
      }
      
      else
      {
        CollisionIsland(island, *pSpace);
        CollisionIsland(servant, *pSpace);
        ResolveMasterIsland(island, servant, *pSpace);  //needs to be run together to work with iterations
        PositionCorrectionIsland(island, *pSpace);
        PositionCorrectionIsland(servant, *pSpace);
      }
      
    }

    void Space::SetIslanding(bool islanding)
    {
      if (islanding)
        m_sap.ToggleIslanding(eON);

      else
        m_sap.ToggleIslanding(eOFF);
    }

    bool Space::GetIslanding()
    {
      switch (m_sap.IslandingState())
      {
        case eON:
          return true;

        case eOFF:
          return false;
      }

      return false;
    }

  }
}

