/*****************************************************************
Filename: spacebroadphase.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "space.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    void Space::UpdatePairs()
    {
      for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
      {
        if (!m_sap.ActiveIsland(i))
          continue;

        UpdateCollisionPairsIsland(i);
      }
     
      UpdateConstraintPairs();
    }

    void Space::BroadPhase()
    {
      m_sap.SortAndSweep();
    }

    void Space::UpdateCollisionPairsNonIsland()
    {
      auto itPair = m_memManager.GetPairList().begin();

      for (; itPair != m_memManager.GetPairList().end(); ++itPair)
      {
        if (!(itPair->GetActive()))
          continue;

        for (unsigned i = 0; i < 2; ++i)
        {
          switch (itPair->GetType(i))
          {
          case colliderType::eBOX:
            itPair->SetBody(i, &(m_memManager.GetBoxList()[itPair->GetId(i)]));
            break;

          case colliderType::eCAPSULE:
            itPair->SetBody(i, &(m_memManager.GetCapsuleList()[itPair->GetId(i)]));
            break;

          case colliderType::eSPHERE:
            itPair->SetBody(i, &(m_memManager.GetSphereList()[itPair->GetId(i)]));
            break;
          }
        }
      }
    }

    void Space::UpdateCollisionPairsIsland(unsigned island)
    {
      auto itPair = m_memManager.GetIslandList(island).begin();

      for (; itPair != m_memManager.GetIslandList(island).end(); ++itPair)
      {
        if (!(itPair->GetActive()))
          continue;

        for (unsigned i = 0; i < 2; ++i)
        {
          switch (itPair->GetType(i))
          {
          case colliderType::eBOX:
            itPair->SetBody(i, &(m_memManager.GetBoxList()[itPair->GetId(i)]));
            break;

          case colliderType::eCAPSULE:
            itPair->SetBody(i, &(m_memManager.GetCapsuleList()[itPair->GetId(i)]));
            break;

          case colliderType::eSPHERE:
            itPair->SetBody(i, &(m_memManager.GetSphereList()[itPair->GetId(i)]));
            break;
          }
        }
      } 
    }

  }
}