/*****************************************************************
Filename: spacehelpers.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "space.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    void Space::TestInitialize()
    {
        //test stuff
      //Vec3 rayOrig(0.0f, 0.0f, 50.0f);
      //Vec3 rayDir(0, 0, -1);
      //
      //m_ray = new Ray(rayOrig, rayDir, this);


      //Vec3 trans(0, 20, -20);
      //Mat3 rot;
      //////45.0f / 180.0f * static_cast<float>(PI)
      ////rot.SetRotationAboutAxis(-83.0f / 180.0f * static_cast<float>(PI), Vec3(1, 0, 0));

      //BodyInfo body;
      //body.collider = MemoryManager::eBOX;
      //body.dimensions[0] = 1.5f;
      //body.dimensions[1] = 1.5f;
      //body.dimensions[2] = 1.5f;
      //body.density = 1.0f;
      //body.position = trans;
      //body.orientation = rot;
      //body.rotationLock = false;
      //body.staticObj = true;

      //unsigned id = m_memManager.AddBody(body);
      //m_memManager.GetBoxList()[id].SetVelocity(Vec3(0.0f, 0.0f, 0.0f));
      //m_memManager.GetBoxList()[id].SetID(id);

      //////rot.SetAsIdentity();
      /////*trans.y = -5.0f;

      ////body.collider = MemoryManager::eBOX;
      ////body.dimensions[0] = 1.0f;
      ////body.dimensions[1] = 1.0f;
      ////body.dimensions[2] = 1.0f;
      ////body.position = trans;
      ////body.orientation = rot;

      ////id = m_memManager.AddBody(body);
      ////m_memManager.GetBoxList()[id].SetVelocity(Vec3(0.0f, 0.0f, 0.0f));
      ////m_memManager.GetBoxList()[id].SetAngVelocity(Vec3(0.0f, 0.0f, 0.0f));
      ////m_memManager.GetBoxList()[id].SetID(id);*/

      //trans.y = 25.0f;
      //body.collider = MemoryManager::eBOX;
      //body.dimensions[0] = 10.0f;
      //body.dimensions[1] = 1.0f;
      //body.dimensions[2] = 10.0f;
      //body.position = trans;
      //body.orientation = rot;
      //body.staticObj = true;
      //id = m_memManager.AddBody(body);

      //m_memManager.GetBoxList()[id].SetVelocity(Vec3(0.0f, 0.0f, 0.0f));
      //m_memManager.GetBoxList()[id].SetAngVelocity(Vec3(0.0f, 0.0f, 0.0f));
      //m_memManager.GetBoxList()[id].SetID(id);

      ////trans.y = 10.0;

      //body.collider = MemoryManager::eCAPSULE;
      //body.dimensions[0] = 1.0f;
      //body.dimensions[1] = 30.0f;
      //body.dimensions[2] = 0.0f;
      //body.density = 1.0f;
      //body.position = trans;
      //body.orientation = rot;
      //body.rotationLock = false;
      //body.staticObj = false;

      //id = m_memManager.AddBody(body);

      //m_memManager.GetCapsuleList()[id].SetVelocity(Vec3(0.0f, 0.0f, 0.0f));
      //m_memManager.GetCapsuleList()[id].SetAngVelocity(Vec3(0.0f, 0.0f, 0.0f));
      //m_memManager.GetCapsuleList()[id].SetID(id);
      //////sphere
      ////trans.y = 7.0f;
      ////body.collider = MemoryManager::eSPHERE;
      ////body.radius = 1.0f;
      ////body.position = trans;
      ////body.orientation = rot;
      ////body.staticObj = false;

      ////id = m_memManager.AddBody(body);
      ////m_memManager.GetSphereList()[id].SetVelocity(Vec3(0.0f, -3.0f, 0.0f));
      ////m_memManager.GetSphereList()[id].SetAngVelocity(Vec3(0.0f, 0.0f, 0.0f));
      ////m_memManager.GetSphereList()[id].SetID(id);


    }

    void Space::GetNextBody()
    {
      m_bodyActive = true;
      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      m_bodyRef = (m_bodyRef >= (boxSize + sphereSize + capsuleSize) ? 0 : m_bodyRef);

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        box->SetActive(false);
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (unsigned i = 0; i < m_bodyRef - boxSize; ++i)
          ++sphere;
        sphere->SetActive(false);
      }

      else
      {
        for (unsigned i = 0; i < m_bodyRef - sphereSize - boxSize; ++i)
          ++capsule;
        capsule->SetActive(false);
      }

      ++m_bodyRef;

      m_bodyRef = (m_bodyRef >= (boxSize + sphereSize + capsuleSize) ? 0 : m_bodyRef);

      box = m_memManager.GetBoxList().begin();
      sphere = m_memManager.GetSphereList().begin();
      capsule = m_memManager.GetCapsuleList().begin();

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        box->SetActive(true);
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - boxSize); ++i)
          ++sphere;
        sphere->SetActive(true);
      }

      else
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - sphereSize - boxSize); ++i)
          ++capsule;
        capsule->SetActive(true);
      }
    }

    unsigned Space::GetActiveBody()
    {
      return m_bodyRef;
    }

    Body* Space::GetBodyFromRefID(unsigned refID)
    {
      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      if (refID < boxSize)
      {
        for (unsigned i = 0; i < refID; ++i)
          ++box;
        return &(*box);
      }

      else if (refID < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(refID - boxSize); ++i)
          ++sphere;
        return &(*sphere);
      }

      else if (refID < (boxSize + sphereSize + capsuleSize))
      {
        for (int i = 0; i < static_cast<int>(refID - sphereSize - boxSize); ++i)
          ++capsule;
        return &(*capsule);
      }

      return nullptr;
    }

    void Space::SetNoActiveBody()
    {
      m_bodyActive = false;
      m_bodyRef = UNSIGNED_MAX;
    }

    void Space::UpdateWorldRaycastPoint()
    {
      if (!m_bodyActive)
        return;

      Body* object = GetBodyFromRefID(m_bodyRef);
      m_recentResult.worldCollisionPoint = object->GetPosition() + object->GetOrientation() * m_recentResult.collisionPoint;
    }

    void Space::MoveBody(Vec3& move)
    {
      if (!m_bodyActive)
        return;

      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      m_bodyRef = m_bodyRef >= boxSize + sphereSize + capsuleSize ? 0 : m_bodyRef;

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        box->AddToPosition(move);
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - boxSize); ++i)
          ++sphere;
        sphere->AddToPosition(move);
      }

      else
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - sphereSize - boxSize); ++i)
          ++capsule;
        capsule->AddToPosition(move);
      }
    }

    void Space::RotateBody(float radians)
    {
      if (!m_bodyActive)
        return;

      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      m_bodyRef = m_bodyRef >= boxSize + sphereSize + capsuleSize ? 0 : m_bodyRef;

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        box->AddToRotation(radians, m_changeAxis);
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - boxSize); ++i)
          ++sphere;
        sphere->AddToRotation(radians, m_changeAxis);
      }

      else
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - sphereSize - boxSize); ++i)
          ++capsule;
        capsule->AddToRotation(radians, m_changeAxis);
      }

    }

    void Space::RotateBody(float radians, Vec3& axis)
    {
      if (!m_bodyActive)
        return;

      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      m_bodyRef = m_bodyRef >= boxSize + sphereSize + capsuleSize ? 0 : m_bodyRef;

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        box->AddToRotation(radians, axis);
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - boxSize); ++i)
          ++sphere;
        sphere->AddToRotation(radians, axis);
      }

      else
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - sphereSize - boxSize); ++i)
          ++capsule;
        capsule->AddToRotation(radians, axis);
      }

    }

    void Space::ReturnToZeroOrientation()
    {
      if (!m_bodyActive)
        return;

      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      m_bodyRef = m_bodyRef >= boxSize + sphereSize + capsuleSize ? 0 : m_bodyRef;

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        box->SetOrientation(Mat3());
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - boxSize); ++i)
          ++sphere;
        sphere->SetOrientation(Mat3());
      }

      else
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - sphereSize - boxSize); ++i)
          ++capsule;
        capsule->SetOrientation(Mat3());
      }
    }

    void Space::UpdateOrientation(Vec3 & eulerAngles)
    {
      if (!m_bodyActive)
        return;

      float radiansX = eulerAngles.x * static_cast<float>(PI) / 180.0f;
      float radiansY = eulerAngles.y * static_cast<float>(PI) / 180.0f;
      float radiansZ = eulerAngles.z * static_cast<float>(PI) / 180.0f;


      ReturnToZeroOrientation();
      RotateBody(radiansZ, Vec3(0, 0, 1));  
      RotateBody(radiansY, Vec3(0, 1, 0));
      RotateBody(radiansX, Vec3(1, 0, 0));
    }

    void Space::SwitchAxis()
    {
      m_axisIndex = ((++m_axisIndex) > 2) ? 0 : m_axisIndex;

      switch (m_axisIndex)
      {
      case 0:
        m_changeAxis = Vec3(1, 0, 0);
        break;

      case 1:
        m_changeAxis = Vec3(0, 1, 0);
        break;

      case 2:
        m_changeAxis = Vec3(0, 0, 1);
        break;
      }

    }

    void Space::ModifyBodyDimensions(float amount)
    {
      if (!m_bodyActive)
        return;

      auto box = m_memManager.GetBoxList().begin();
      auto sphere = m_memManager.GetSphereList().begin();
      auto capsule = m_memManager.GetCapsuleList().begin();
      unsigned boxSize = m_memManager.GetBoxList().size();
      unsigned sphereSize = m_memManager.GetSphereList().size();
      unsigned capsuleSize = m_memManager.GetCapsuleList().size();

      m_bodyRef = m_bodyRef >= boxSize + sphereSize + capsuleSize ? 0 : m_bodyRef;

      if (m_bodyRef < boxSize)
      {
        for (unsigned i = 0; i < m_bodyRef; ++i)
          ++box;
        m_body = (Body*)(&(*box));
      }

      else if (m_bodyRef < (boxSize + sphereSize))
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - boxSize); ++i)
          ++sphere;
        m_body = (Body*)(&(*sphere));
      }

      else
      {
        for (int i = 0; i < static_cast<int>(m_bodyRef - sphereSize - boxSize); ++i)
          ++capsule;
        m_body = (Body*)(&(*capsule));
      }

      if (!m_body)
        return;

      Vec3 dimensions = m_body->GetDimensions();

      Vec3 additionVec = m_changeAxis * amount;

      switch (m_body->GetType())
      {
      case colliderType::eBOX:
        dimensions += additionVec;
        break;

      case colliderType::eCAPSULE:
        dimensions.x += additionVec.x;
        dimensions.y += additionVec.y;
        dimensions.x += additionVec.z;
        break;

      case colliderType::eSPHERE:
        dimensions.x += additionVec.x;
        dimensions.x += additionVec.y;
        dimensions.x += additionVec.z;
        break;
      }

      dimensions.x = abs(dimensions.x);
      dimensions.y = abs(dimensions.y);
      dimensions.z = abs(dimensions.z);

      m_body->SetDimensions(dimensions);
    }

    void Space::ModifyBodyDimensions(Vec3 & axis, float amount)
    {
      if (!m_body)
        return;

      Vec3 dimensions = m_body->GetDimensions();

      Vec3 additionVec = axis * amount;

      switch (m_body->GetType())
      {
      case colliderType::eBOX:
        dimensions += additionVec;
        break;

      case colliderType::eCAPSULE:
        dimensions.x += additionVec.x;
        dimensions.y += additionVec.y;
        dimensions.x += additionVec.z;
        break;

      case colliderType::eSPHERE:
        dimensions.x += additionVec.x;
        dimensions.x += additionVec.y;
        dimensions.x += additionVec.z;
        break;
      }

      m_body->SetDimensions(dimensions);
    }

    void Space::DeleteBody()
    {
      if (!m_bodyActive)
        return;

      DeletionInfo body;

      if (m_body->GetDeleteFlag())
        return;

      body.collider = m_body->GetType();
      body.id = m_body->GetID();
      m_deletionQueue.push(body);

      (m_memManager.GetAabbList())[m_body->GetAabbId()].SetDeletion(true);
      (m_memManager.GetAabbList())[m_body->GetAabbId()].SetCounter(m_sap.GetCounter());

    }

    RaycastResult Space::CastRay(Vec3& origin, Vec3& direction)
    {
      Ray ray(origin, direction, this);
      return ray.CastRay();
    }

    RaycastResult Space::CastAndStoreRay(Vec3 & origin, Vec3 & direction)
    {
      Ray ray(origin, direction, this);
      m_recentResult = ray.CastRay();
      return m_recentResult;
    }

    Body* Space::GetBox(unsigned boxID)
    {
      auto box = m_memManager.GetBoxList().begin();
      for (unsigned i = 0; i < boxID; ++i)
        ++box;
      m_body = (Body*)(&(*box));
      m_body->SetActive(true);
      m_bodyActive = true;
      m_bodyRef = boxID;
      return m_body;
    }

    Body* Space::GetSphere(unsigned sphereID)
    {
      auto sphere = m_memManager.GetSphereList().begin();
      for (unsigned i = 0; i < sphereID; ++i)
        ++sphere;
      m_body = (Body*)(&(*sphere));
      m_body->SetActive(true);
      m_bodyActive = true;
      m_bodyRef = sphereID + m_memManager.GetBoxList().size();
      return m_body;
    }

    Body* Space::GetCapsule(unsigned capsuleID)
    {
      auto capsule = m_memManager.GetCapsuleList().begin();
      for (unsigned i = 0; i < capsuleID; ++i)
        ++capsule;
      m_body = (Body*)(&(*capsule));
      m_body->SetActive(true);
      m_bodyActive = true;
      m_bodyRef = capsuleID + m_memManager.GetBoxList().size() + m_memManager.GetSphereList().size();
      return m_body;
    }

  }

}