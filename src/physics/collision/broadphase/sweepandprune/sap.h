/*****************************************************************
Filename: sap.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../island/island.h"
#include <string>
#include <unordered_map>

namespace TestEngine
{
  namespace PhysicsGL
  {
    class SweepAndPrune
    {
      public:
        SweepAndPrune()  {}
        ~SweepAndPrune() {}

        void Initialize(MemoryManager::MemInterface* memInterface) { m_memInterface = memInterface; m_activeIslanding = eOFF;}
        void ToggleIslanding(Islanding active)                     { m_activeIslanding = active; }

        void SortAndSweep();

          //used to grab current counter for aabb deletions and increment it
        unsigned GetCounter()                                     { return m_counter++; }

        Islanding     IslandingState()                            { return m_activeIslanding; }
        bool          ActiveIsland(unsigned island)               { return m_islands[island].m_islandActive; }
        bool          ThreadSafe(unsigned island)                 { return m_islands[island].m_islandThreadSafe; }
        void          SetThreadSafe(unsigned island, bool safe)   { m_islands[island].m_islandThreadSafe = safe; }
        void          SetNumActiveIslands(unsigned num)           { s_numActiveIslands  = num; }
        void          SetNumActiveMasters(unsigned num)           { s_numActiveMasters  = num; }
        void          SetLargestIsland(unsigned num)              { s_largestIsland     = num; }

        IslandStatus  GetIslandStatus(unsigned island)            { return m_islands[island].m_status; }
        unsigned      GetIslandCompanion(unsigned island)         { return m_islands[island].m_companion; }
        unsigned      GetNumActiveIslands()                       { return s_numActiveIslands; }
        unsigned      GetNumActiveMasters()                       { return s_numActiveMasters; }
        unsigned      GetLargestIsland()                          { return s_largestIsland; }
        
      private:
          //updates the lists with the new aabbs
          //may eventually do an insert/sort
        void UpdateLists();
        void UpdateNodes();
        void ExpandLists();

        enum MergeType
        {
          eNEW,          //two neutral islands, don't merge
          eEXISTING,     //already linked master and servant, don't merge
          eISLAND0,      //merge into island 0
          eISLAND1       //merge into island 1
        };

        enum SortType
        {
          eINSERT,
          eREMOVE
        };

        struct SortNode
        {
          Axis      axis;
          unsigned  aabbID1;
          unsigned  aabbID2;
          SortType  type   = eINSERT;
          bool      active = true;
        };

        void Sort();

        void SortAxis(Axis axis);
        void FinishSort();

          //insert
        void      InsertSort(Axis axis, unsigned id1, unsigned id2);
        void      InsertAxis(Axis axis, unsigned id1, unsigned id2);
        void      InsertX(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);
        void      InsertZ(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);
        void      InsertPair(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);

        void      InsertNonIslanding(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);
        void      InsertForIslanding(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);
        
        unsigned  DetermineIslands(unsigned id1, unsigned id2, unsigned* islands);         //used to determine islands involved in potential pair
        void      InsertIntoIsland(unsigned island, unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);

        MergeType DetermineMerge(unsigned numberOfIslands, unsigned* islands);             //used to determine if islands should merge or not
        MergeType DetermineMerge(unsigned island0, unsigned island1);
        void      CreateMasterServant(unsigned master, unsigned servant);
        void      InsertIntoMaster(unsigned master, unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);

        void      CombineIslands(unsigned numberOfIslands, unsigned* islands, unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2);
        void      MergeIslands(unsigned numberOfIslands, unsigned* islands);
        void      MergeIslands(unsigned island1, unsigned island2);

          //remove
        void      RemoveSort(Axis axis, unsigned aabbId1, unsigned aabbId2);
        void      Remove(Axis axis, unsigned id1, unsigned id2);
        void      RemoveX(unsigned mapId1, unsigned mapId2);
        void      RemoveZ(unsigned mapId1, unsigned mapId2);
        void      RemovePair(unsigned mapId1, unsigned mapId2);

        void      RemoveNonIslanding(unsigned mapId1, unsigned mapId2);
        void      RemoveForIslanding(unsigned mapId1, unsigned mapId2);

        unsigned  DetermineIsland(unsigned mapId1, unsigned mapId2);
        void      RemoveFromIsland(unsigned island, unsigned mapId1, unsigned mapId2);
        void      DetermineSplit(unsigned island, unsigned mapId1, unsigned mapId2);
        void      CheckServant(unsigned island, unsigned id1, unsigned id2);
        void      CheckMaster(unsigned island, unsigned id1, unsigned id2);
        bool      CheckSharedMap(unsigned master, unsigned id);
        void      SeparateIslands(unsigned master, unsigned servant);

        std::vector<OrderNode>& GetOrderAxis(Axis axis);

        Body* GetBody(colliderType collider, unsigned id);

          //members
          //whether to use islands, or the lists within sap
        Islanding                               m_activeIslanding;

          //link to the memory manager - both islands and non-islands
        MemoryManager::MemInterface*            m_memInterface;

          //should be the same size as twice the aabb vector (min/max)
          //y axis is not ordered or inserted into
        std::vector<OrderNode>                  m_xOrder;
        std::vector<OrderNode>                  m_zOrder;


          //used to track deletions - allows exact number of active objects
          //to be known - starts at zero and used to 
        //unsigned     m_currSize = 0;
        unsigned     m_counter = 0;

          //used to handle cases where islands inadvertently merge
          //allows objects to filter through broad phase before pairs added
        std::vector<SortNode>                   m_sort;
        std::unordered_map<unsigned, PairNode>  m_sortPosition;
        std::list<unsigned>                     m_openSort;

          //used for keeping tracks of pairs from previous axis
          //pairmap is always used, even for islanding, to
          //ensure accuracy of broadphase
        std::unordered_map<unsigned, PairNode>  m_pairMap;
        std::list<unsigned>                     m_openPairs;

          //unordered map for storing 'active' pairs
          //active pairs are objects that have been sorted
          //and created pairs in the x axis
          //that are currently within mix-max range
        std::unordered_map<unsigned, MapNode>  m_activeXMap;
        std::unordered_map<unsigned, MapNode>  m_activeZMap;
        
           //Islanding**************************************
        Island                                 m_islands[MemoryManager::s_maxNumberOfIslands];
    };


    //sort objects as per normal.  Mark any moved object with dirty flag in order array.  (put into new array for handling?)
    //store the end points of the moved boxes in the array (only need to flag boxes that move during their phase of sort, not that move
    //as a result of another box)
    //store all pairs from previous frame
    //store map of pairs, use that to identify them in the array of pairs
    //only update/remove pairs that change
    //need to flag all pairs that move - if pairs have to change, must grab them and store them in remove list
    //add to list everytime min goes to left, max goes to right - do not self mark
    //remove from list everytime a min goes to right, or max goes to left
    //open list for available pair positions?

    //things to do for SAP
    //save pair list - do not clear from frame to frame
    //create two lists for new pairs/removal of pairs
    //store a map of pairs, for reference from frame to frame of position within array
    //  -make sure map is updated every time the pair location changes
    //remove old pairs by marking their spots as vacant - add flag to pairs for checking?
    //create list for available spots in pair list
    
  }
}