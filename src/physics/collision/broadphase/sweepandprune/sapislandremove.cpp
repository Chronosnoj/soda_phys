/*****************************************************************
Filename: sapislandremove.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "sap.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    void SweepAndPrune::RemoveNonIslanding(unsigned mapId1, unsigned mapId2)
    {
      m_pairMap[mapId1].active = false;
      m_pairMap[mapId2].active = false;

      unsigned position = m_pairMap[mapId1].pairPosition;

      (m_memInterface->GetPairList())[position].SetActive(false);

      m_openPairs.push_front(position);
      m_pairMap[mapId1].pairPosition = UNSIGNED_MAX;
      m_pairMap[mapId2].pairPosition = UNSIGNED_MAX;
    }

    void SweepAndPrune::RemoveForIslanding(unsigned mapId1, unsigned mapId2)
    {
      m_pairMap[mapId1].active = false;
      m_pairMap[mapId2].active = false;

      unsigned island = DetermineIsland(mapId1, mapId2);

      RemoveFromIsland(island, mapId1, mapId2);
      DetermineSplit(island, mapId1, mapId2);
    }

    //for removal search - find single island pair is part of
    unsigned SweepAndPrune::DetermineIsland(unsigned mapId1, unsigned mapId2)
    {
      for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
      {
        if (m_islands[i].m_pairMap[mapId1].active)
          return i;
      }

      return UNSIGNED_MAX;
    }

    void SweepAndPrune::RemoveFromIsland(unsigned island, unsigned mapId1, unsigned mapId2)
    {
      unsigned id1 = mapId1 >> 16;
      unsigned id2 = mapId2 >> 16;

      unsigned position = m_islands[island].m_pairMap[mapId1].pairPosition;

      if (position == UNSIGNED_MAX)
        return;

      //'remove' pair from list
      (m_memInterface->GetIslandList(island))[position].SetActive(false);

      //clean up pair information in island
      m_islands[island].m_openPairs.push_front(position);
      m_islands[island].m_pairMap[mapId1].pairPosition = UNSIGNED_MAX;
      m_islands[island].m_pairMap[mapId2].pairPosition = UNSIGNED_MAX;
      m_islands[island].m_pairMap[mapId1].active = false;
      m_islands[island].m_pairMap[mapId2].active = false;

      //reduce number of pair references for objects in islands
      --m_islands[island].m_activeObjects[id1];
      --m_islands[island].m_activeObjects[id2];

      --m_islands[island].m_islandSize;

      if (m_islands[island].m_islandActive == true && m_islands[island].m_islandSize <= 0)
      {
        m_islands[island].m_islandActive = false;
        --s_numActiveIslands;
      }
    }

    void SweepAndPrune::DetermineSplit(unsigned island, unsigned mapId1, unsigned mapId2)
    {
      if (m_islands[island].m_status == eNEUTRAL)
        return;

      unsigned id1 = mapId1 >> 16;
      unsigned id2 = mapId2 >> 16;

      if (m_islands[island].m_status == eSERVANT)
        CheckServant(island, id1, id2);

      else
        CheckMaster(island, id1, id2);

      return;
    }

    void SweepAndPrune::CheckServant(unsigned island, unsigned id1, unsigned id2)
    {
      bool splitIslands = false;

      if (m_islands[island].m_activeObjects[id1] == 0)
        splitIslands = CheckSharedMap(m_islands[island].m_companion, id1);

      if (splitIslands)
      {
        SeparateIslands(m_islands[island].m_companion, island);
        return;
      }

        //only check the islands for split if they haven't already been split
      if (m_islands[island].m_activeObjects[id2] == 0)
        splitIslands = CheckSharedMap(m_islands[island].m_companion, id2);

      if (splitIslands)
        SeparateIslands(m_islands[island].m_companion, island);
    }

    void SweepAndPrune::CheckMaster(unsigned island, unsigned id1, unsigned id2)
    {
      bool splitIslands = false;

      if (m_islands[island].m_activeObjects[id1] == 0)
        splitIslands = CheckSharedMap(island, id1);

      if (splitIslands)
        SeparateIslands(island, m_islands[island].m_companion);

      splitIslands = false;

      if (m_islands[island].m_activeObjects[id2] == 0)
        splitIslands = CheckSharedMap(island, id2);

      if (splitIslands)
        SeparateIslands(island, m_islands[island].m_companion);
    }

    bool SweepAndPrune::CheckSharedMap(unsigned master, unsigned id)
    {
      if (master == UNSIGNED_MAX)
        return false;

      if (m_islands[master].m_masterInfo.m_linkMap[id] == true)
      {
        m_islands[master].m_masterInfo.m_linkMap[id] = false;
        
          //if the map is empty with object's removal, 
          //the relationship can be split
        if (--m_islands[master].m_masterInfo.m_linkMapSize == 0)
          return true;
      }

      return false;
    }


    void SweepAndPrune::SeparateIslands(unsigned master, unsigned servant)
    {
      //servant
      m_islands[servant].m_status = eNEUTRAL;
      m_islands[servant].m_companion = UNSIGNED_MAX;

      //master
      m_islands[master].m_status = eNEUTRAL;
      m_islands[master].m_masterInfo.m_linkMapSize = 0;
      m_islands[master].m_masterInfo.m_linkMap.clear();

      --s_numActiveMasters;
    }
  }
}
