/*****************************************************************
Filename: sap.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "sap.h"
#include <stdlib.h>     /* qsort */
//sweep along an axis, having an end point set, and a pair list set
//if end point set empties, that is an island
//do next axis
//compare two lists and create final islands

namespace TestEngine
{
  namespace PhysicsGL
  {
    void SweepAndPrune::SortAndSweep()
    {
      UpdateLists();
      Sort();
    }

    void SweepAndPrune::UpdateLists()
    {
      UpdateNodes();
      ExpandLists();
    }

    void SweepAndPrune::UpdateNodes()
    {
        //if the aabb add list has entries
        //grab openings from xOrder and zOrder lists
        //and add new aabbs
      if (m_counter <= 0)
        return;

      MemoryManager::MemInterface::AddAabbs& aabbs = m_memInterface->GetAddedAabbs();
      unsigned orderSize = m_xOrder.size();

      while (aabbs.size() > 0)
      {
        unsigned aabbID = aabbs.front();
        aabbs.pop();

        unsigned offset = m_counter * 2;

        m_xOrder[orderSize - offset].aabbID = aabbID;
        m_zOrder[orderSize - offset].aabbID = aabbID;

        m_xOrder[orderSize - offset + 1].aabbID = aabbID;
        m_zOrder[orderSize - offset + 1].aabbID = aabbID;

        --m_counter;
      }
    }

    void SweepAndPrune::ExpandLists()
    {
      unsigned currSize = m_xOrder.size() / 2;
      unsigned aabbSize = m_memInterface->GetAabbList().size();
      unsigned sizeChange = aabbSize - currSize;

      //m_currSize += sizeChange;

      if (sizeChange <= 0 || aabbSize <= 0)
        return;

      Aabb aabb = (m_memInterface->GetAabbList())[currSize];

      for (unsigned i = 0; i < sizeChange; ++i)
      {
        aabb = (m_memInterface->GetAabbList())[currSize + i];
        OrderNode node;
        node.aabbID = aabb.GetID();
        node.type = eMIN;
        m_xOrder.push_back(node);
        m_zOrder.push_back(node);

        node.type = eMAX;
        m_xOrder.push_back(node);
        m_zOrder.push_back(node);
      }
    }

    void SweepAndPrune::Sort()
    {
      SortAxis(eXAXIS);
      SortAxis(eZAXIS);
    }

    void SweepAndPrune::SortAxis(Axis axis)
    {
      int size = static_cast<int>((GetOrderAxis(axis)).size());
      std::vector<OrderNode>& axisVec = GetOrderAxis(axis);
      
      for (int i = 1; i < size; ++i)
      {
          //values of axis
        OrderNode node = axisVec[i];
        float value = ((m_memInterface->GetAabbList())[node.aabbID]).GetMinMax(node.type, axis);

          //stored for final update
        int j;

        for (j = i - 1; j >= 0; --j)
        {
          if ((value < ((m_memInterface->GetAabbList())[axisVec[j].aabbID]).GetMinMax(axisVec[j].type, axis)))
          {
            axisVec[j + 1] = axisVec[j];

            if (node.aabbID == axisVec[j].aabbID)
              continue;

              //insert a pair if i is min and j is max
            if (node.type == MinMax::eMIN && axisVec[j].type == MinMax::eMAX)
              InsertSort(axis, node.aabbID, axisVec[j].aabbID);

            //remove a pair if i is max and j is min OR if object is due to be deleted
            else if ((node.type == MinMax::eMAX && axisVec[j].type == MinMax::eMIN))
              RemoveSort(axis, node.aabbID, axisVec[j].aabbID);              
          }

          else
            break;
        }

        axisVec[j + 1] = node;
      }

      FinishSort();
    }

    void SweepAndPrune::FinishSort()
    {
      //clean up sort maps and vector
      unsigned sortSize = m_sort.size();
      for (unsigned k = 0; k < sortSize; ++k)
      {
        if (!m_sort[k].active)
          continue;

        if (m_sort[k].type == eINSERT)
          InsertAxis(m_sort[k].axis, m_sort[k].aabbID1, m_sort[k].aabbID2);

        else
          Remove(m_sort[k].axis, m_sort[k].aabbID1, m_sort[k].aabbID2);
      }

      m_sort.clear();
      m_sortPosition.clear();
      m_openSort.clear();
    }

    void SweepAndPrune::InsertSort(Axis axis, unsigned aabbId1, unsigned aabbId2)
    {
      //InsertAxis(axis, node.aabbID, axisVec[j].aabbID);
      //create sort node
      SortNode sortNode;
      sortNode.axis = axis;
      sortNode.aabbID1 = aabbId1;
      sortNode.aabbID2 = aabbId2;

      //insert position into map
      unsigned sortPosition1 = aabbId1;
      sortPosition1 = sortPosition1 << 16;
      sortPosition1 += aabbId2;

      unsigned sortPosition2 = aabbId2;
      sortPosition2 = sortPosition2 << 16;
      sortPosition2 += aabbId1;

      m_sortPosition[sortPosition1].active = true;
      m_sortPosition[sortPosition2].active = true;

      //insert into vector
      if (m_openSort.size() > 0)
      {
        unsigned vecPosition = m_openSort.front();
        m_openSort.pop_front();
        m_sort[vecPosition] = sortNode;
        m_sortPosition[sortPosition1].pairPosition = vecPosition;
        m_sortPosition[sortPosition2].pairPosition = vecPosition;
      }

      else
      {
        m_sortPosition[sortPosition1].pairPosition = m_sort.size();
        m_sortPosition[sortPosition2].pairPosition = m_sort.size();
        m_sort.push_back(sortNode);
      }
    }

    void SweepAndPrune::InsertAxis(Axis axis, unsigned id1, unsigned id2)
    {
      unsigned mapId1 = id1;
      mapId1 = mapId1 << 16;
      mapId1 += id2;

      unsigned mapId2 = id2;
      mapId2 = mapId2 << 16;
      mapId2 += id1;

      switch (axis)
      {
        case Axis::eXAXIS:
          InsertX(id1, id2, mapId1, mapId2);
          break;

        case Axis::eZAXIS:
          InsertZ(id1, id2, mapId1, mapId2);
          break;
      }
    }

    void SweepAndPrune::InsertX(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      MapNode node;
      node.active = true;
      node.aabb1 = id1;
      node.aabb2 = id2;

      m_activeXMap[mapId1] = node;

      node.aabb2 = id1;
      node.aabb1 = id2;

      m_activeXMap[mapId2] = node;

      if (m_activeZMap[mapId1].active && m_pairMap[mapId1].active == false)
        InsertPair(id1, id2, mapId1, mapId2);
    }

    void SweepAndPrune::InsertZ(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      MapNode node;
      node.active = true;
      node.aabb1 = id1;
      node.aabb2 = id2;

      m_activeZMap[mapId1] = node;

      node.aabb2 = id1;
      node.aabb1 = id2;

      m_activeZMap[mapId2] = node;

      if(m_activeXMap[mapId1].active && m_pairMap[mapId1].active == false)
        InsertPair(id1, id2, mapId1, mapId2);
    }

    void SweepAndPrune::InsertPair(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      InsertForIslanding(id1, id2, mapId1, mapId2);
    }

    void SweepAndPrune::InsertNonIslanding(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      unsigned position;

      Body* itBody1 = GetBody((m_memInterface->GetAabbList())[id1].GetColliderType(),
        (m_memInterface->GetAabbList())[id1].GetColliderID());
      Body* itBody2 = GetBody((m_memInterface->GetAabbList())[id2].GetColliderType(),
        (m_memInterface->GetAabbList())[id2].GetColliderID());

      if (m_openPairs.size() > 0)
      {
        position = m_openPairs.front();
        m_openPairs.pop_front();

        (m_memInterface->GetPairList())[position].SetBody(0, itBody1);
        (m_memInterface->GetPairList())[position].SetBody(1, itBody2);
        (m_memInterface->GetPairList())[position].SetActive(true);
      }

      else
      {
        position = (m_memInterface->GetPairList()).size();
        m_memInterface->AddPair(itBody1, itBody2);
      }

      PairNode node;
      node.pairPosition = position;
      node.active = true;

      m_pairMap[mapId1] = node;
      m_pairMap[mapId2] = node;
    }

    void SweepAndPrune::RemoveSort(Axis axis, unsigned aabbId1, unsigned aabbId2)
    {
        //Remove(axis, node.aabbID, axisVec[j].aabbID);
        //determine position in map
      unsigned sortPosition1 = aabbId1;
      sortPosition1 = sortPosition1 << 16;
      sortPosition1 += aabbId2;

      unsigned sortPosition2 = aabbId2;
      sortPosition2 = sortPosition2 << 16;
      sortPosition2 += aabbId1;

        //remove pair from list if active, otherwise append to list
      if (m_sortPosition[sortPosition1].active == true)
      {
        unsigned vecPosition = m_sortPosition[sortPosition1].pairPosition;
        m_sort[vecPosition].active = false;
        m_openSort.push_front(vecPosition);
        m_sortPosition[sortPosition1].active = false;
        m_sortPosition[sortPosition2].active = false;
        return;
      }

      //otherwise, the removal needs to be added as a pair
      //InsertAxis(axis, node.aabbID, axisVec[j].aabbID);
      //create sort node
      SortNode sortNode;
      sortNode.axis = axis;
      sortNode.aabbID1 = aabbId1;
      sortNode.aabbID2 = aabbId2;
      sortNode.type = eREMOVE;

      m_sortPosition[sortPosition1].active = true;
      m_sortPosition[sortPosition2].active = true;

      //insert into vector
      if (m_openSort.size() > 0)
      {
        unsigned vecPosition = m_openSort.front();
        m_openSort.pop_front();
        m_sort[vecPosition] = sortNode;
        m_sortPosition[sortPosition1].pairPosition = vecPosition;
        m_sortPosition[sortPosition2].pairPosition = vecPosition;
      }

      else
      {
        m_sortPosition[sortPosition1].pairPosition = m_sort.size();
        m_sortPosition[sortPosition2].pairPosition = m_sort.size();
        m_sort.push_back(sortNode);
      }
    }

    void SweepAndPrune::Remove(Axis axis, unsigned id1, unsigned id2)
    {
      unsigned mapId1 = id1;
      mapId1 = mapId1 << 16;
      mapId1 += id2;

      unsigned mapId2 = id2;
      mapId2 = mapId2 << 16;
      mapId2 += id1;

      switch (axis)
      {
        case Axis::eXAXIS:
          RemoveX(mapId1, mapId2);
          break;

        case Axis::eZAXIS:
          RemoveZ(mapId1, mapId2);
          break;
      }
    }

    void SweepAndPrune::RemoveX(unsigned mapId1, unsigned mapId2)
    {
      m_activeXMap[mapId1].active = false;
      m_activeXMap[mapId2].active = false;

      if (m_pairMap[mapId1].active)
        RemovePair(mapId1, mapId2);
    }

    void SweepAndPrune::RemoveZ(unsigned mapId1, unsigned mapId2)
    {
      m_activeZMap[mapId1].active = false;
      m_activeZMap[mapId2].active = false;

      if (m_pairMap[mapId1].active)
        RemovePair(mapId1, mapId2);
    }

    void SweepAndPrune::RemovePair(unsigned mapId1, unsigned mapId2)
    {
      RemoveForIslanding(mapId1, mapId2);
    }

    std::vector<OrderNode>& SweepAndPrune::GetOrderAxis(Axis axis)
    {
      switch (axis)
      {
        case eXAXIS:
          return m_xOrder;

        case eZAXIS:
          return m_zOrder;
      }

        //default case
      return m_xOrder;
    }

    Body* SweepAndPrune::GetBody(colliderType collider, unsigned id)
    {
      switch (collider)
      {
        case colliderType::eBOX:
          return (Body*)(&(m_memInterface->GetBoxList()[id]));

        case colliderType::eCAPSULE:
          return (Body*)(&(m_memInterface->GetCapsuleList()[id]));

        case colliderType::eSPHERE:
          return (Body*)(&(m_memInterface->GetSphereList()[id]));
      }

      return nullptr;
    }
  }
}