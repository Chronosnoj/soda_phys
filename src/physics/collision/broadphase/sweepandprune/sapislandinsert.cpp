/*****************************************************************
Filename: sapislandinsert.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "sap.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    unsigned s_numActiveIslands = 0;
    unsigned s_numActiveMasters = 0;
    unsigned s_largestIsland = 0;

    void SweepAndPrune::InsertForIslanding(unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      //array for the identified islands
      unsigned islands[2];

      //determine which islands the objects are tied to
      unsigned numOfIslands = DetermineIslands(id1, id2, islands);

      //if associated with one island or more, insert in first of array
      if (numOfIslands > 0)
      {
        if (numOfIslands == 1)
          InsertIntoIsland(islands[0], id1, id2, mapId1, mapId2);

        //if more than one island associated, merge other island into first
        else
          CombineIslands(numOfIslands, islands, id1, id2, mapId1, mapId2);

      }

      //else if not associated, choose the islands with the smallest number of pairs
      else
      {
        unsigned bestIsland;
        unsigned bestIslandSize = UNSIGNED_MAX;

        for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
        {
          if (m_islands[i].m_islandSize < bestIslandSize)
          {
            bestIslandSize = m_islands[i].m_islandSize;
            bestIsland = i;

            if (bestIslandSize == 0)
              break;
          }
        }

        InsertIntoIsland(bestIsland, id1, id2, mapId1, mapId2);
      }
    }

    //returns the number of islands that the potential pair is associated with
    //fills the islands array with the identified islands - this should not exceed
    //3 islands between the two objects
    unsigned SweepAndPrune::DetermineIslands(unsigned id1, unsigned id2, unsigned* islands)
    {
      unsigned numOfIslands = 0;

      for (unsigned i = 0; i < MemoryManager::s_maxNumberOfIslands; ++i)
      {
        if (m_islands[i].m_activeObjects[id1] > 0)
        {
          //prevents master from determining pair with itself when servant is present
          if (!(m_islands[i].m_status == eMASTER && m_islands[i].m_masterInfo.m_linkMap[id1] == true))
          {
            //static objects don't cause islands to be thrown together
            if (!m_memInterface->GetAabbList()[id1].GetStatic())
            {
              islands[numOfIslands] = i;
              ++numOfIslands;
              continue;  //no need to check id2 if object is already found
            }
          }
        }

        if (m_islands[i].m_activeObjects[id2] > 0)
        {
          if (m_islands[i].m_status == eMASTER && m_islands[i].m_masterInfo.m_linkMap[id2] == true)
            continue;

          if (m_memInterface->GetAabbList()[id2].GetStatic())
            continue;

          islands[numOfIslands] = i;
          ++numOfIslands;
        }
      }

      if (numOfIslands > 2)
        return UNSIGNED_MAX;

      return numOfIslands;
    }

    void SweepAndPrune::InsertIntoIsland(unsigned island, unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      unsigned position;

      Body* itBody1 = GetBody((m_memInterface->GetAabbList())[id1].GetColliderType(),
        (m_memInterface->GetAabbList())[id1].GetColliderID());
      Body* itBody2 = GetBody((m_memInterface->GetAabbList())[id2].GetColliderType(),
        (m_memInterface->GetAabbList())[id2].GetColliderID());

      if (m_islands[island].m_openPairs.size() > 0)
      {
        position = m_islands[island].m_openPairs.front();
        m_islands[island].m_openPairs.pop_front();

        (m_memInterface->GetIslandList(island))[position].SetBody(0, itBody1);
        (m_memInterface->GetIslandList(island))[position].SetBody(1, itBody2);
        (m_memInterface->GetIslandList(island))[position].SetActive(true);
      }

      else
      {
        position = (m_memInterface->GetIslandList(island)).size();
        m_memInterface->AddIslandPair(island, itBody1, itBody2);
      }

      PairNode node;
      node.pairPosition = position;
      node.active = true;

      //used to track overall pairs for sap
      m_pairMap[mapId1] = node;
      m_pairMap[mapId2] = node;

      m_islands[island].m_pairMap[mapId1] = node;
      m_islands[island].m_pairMap[mapId2] = node;

      //increment the number in the activeObjects map for that object
      ++m_islands[island].m_activeObjects[id1];
      ++m_islands[island].m_activeObjects[id2];

      ++m_islands[island].m_islandSize;

      if (m_islands[island].m_islandSize > s_largestIsland)
        s_largestIsland = m_islands[island].m_islandSize;

      if (m_islands[island].m_islandActive == false && m_islands[island].m_islandSize > 0)
      {
        m_islands[island].m_islandActive = true;
        ++s_numActiveIslands;
      }       
    }

    SweepAndPrune::MergeType SweepAndPrune::DetermineMerge(unsigned numberOfIslands, unsigned* islands)
    {
      return DetermineMerge(islands[0], islands[1]);
    }

    SweepAndPrune::MergeType SweepAndPrune::DetermineMerge(unsigned island0, unsigned island1)
    {
      if (m_islands[island0].m_status == eNEUTRAL && m_islands[island1].m_status == eNEUTRAL)
        return eNEW;

      if (m_islands[island0].m_companion == island1 && m_islands[island1].m_companion == island0)
        return eEXISTING;

      if (m_islands[island0].m_status == eNEUTRAL)
        return eISLAND1;

      if (m_islands[island1].m_status == eNEUTRAL)
        return eISLAND0;

      if (m_islands[island0].m_islandSize < m_islands[island1].m_islandSize)
        return eISLAND1;

      return eISLAND0;
    }

    void SweepAndPrune::CreateMasterServant(unsigned master, unsigned servant)
    {
      m_islands[master].m_status      = eMASTER;
      m_islands[master].m_companion   = servant;

      m_islands[servant].m_status     = eSERVANT;
      m_islands[servant].m_companion  = master;

      ++s_numActiveMasters;

      return;
    }

    void SweepAndPrune::InsertIntoMaster(unsigned master, unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
        //add the servant's object to the shared map
      if (m_islands[m_islands[master].m_companion].m_activeObjects[id1])
      {
        if (m_islands[master].m_masterInfo.m_linkMap[id1] == false)
        {
          m_islands[master].m_masterInfo.m_linkMap[id1] = true;
          ++m_islands[master].m_masterInfo.m_linkMapSize;
        }  
      }

      else if (m_islands[master].m_masterInfo.m_linkMap[id2] == false)
      {
        m_islands[master].m_masterInfo.m_linkMap[id2] = true;
        ++m_islands[master].m_masterInfo.m_linkMapSize;
      }

        //insert the actual pair
      InsertIntoIsland(master, id1, id2, mapId1, mapId2);
    }

    void SweepAndPrune::CombineIslands(unsigned numberOfIslands, unsigned* islands,
      unsigned id1, unsigned id2, unsigned mapId1, unsigned mapId2)
    {
      //MergeIslands(numberOfIslands, islands);
      //InsertIntoIsland(islands[0], id1, id2, mapId1, mapId2);
      //return;

      //determine the type of merge to be performed
      MergeType merge = DetermineMerge(islands[0], islands[1]);

      //if neutral or none, don't merge
      switch (merge)
      {
          //if neutral, create a new master servant relationship
        case eNEW:
          CreateMasterServant(islands[0], islands[1]);
          //add new pair to master and servant - falls through from above
        case eEXISTING:
          if (m_islands[islands[0]].m_status == eMASTER)
            InsertIntoMaster(islands[0], id1, id2, mapId1, mapId2);
          else
            InsertIntoMaster(islands[1], id1, id2, mapId1, mapId2);
          break;

          //if one of the objects is already a master or a servant
          //merge them by merging neutral into master or servant,
          //or smaller into larger if a pair of master/servants
          //and insert into island
        case eISLAND0:
          if (m_islands[islands[1]].m_status != eNEUTRAL)
            MergeIslands(islands[0], m_islands[islands[1]].m_companion);
          MergeIslands(islands[0], islands[1]);
          InsertIntoIsland(islands[0], id1, id2, mapId1, mapId2);
          break;

        case eISLAND1:
          if (m_islands[islands[0]].m_status != eNEUTRAL)
            MergeIslands(islands[1], m_islands[islands[0]].m_companion);
          MergeIslands(islands[1], islands[0]);
          InsertIntoIsland(islands[1], id1, id2, mapId1, mapId2);
          break;
      }
    }

    void SweepAndPrune::MergeIslands(unsigned numberOfIslands, unsigned* islands)
    {
      for (unsigned i = 1; i < numberOfIslands; ++i)
        MergeIslands(islands[0], islands[i]);
    }

    void SweepAndPrune::MergeIslands(unsigned island1, unsigned island2)
    {
      auto itPair = m_memInterface->GetIslandList(island2).begin();

      for (; itPair != m_memInterface->GetIslandList(island2).end(); ++itPair)
      {
        if (!itPair->GetActive())
          continue;

        unsigned id1 = itPair->GetA()->GetAabbId();
        unsigned id2 = itPair->GetB()->GetAabbId();

        unsigned mapId1 = id1;
        mapId1 = mapId1 << 16;
        mapId1 += id2;

        unsigned mapId2 = id2;
        mapId2 = mapId2 << 16;
        mapId2 += id1;

        InsertIntoIsland(island1, id1, id2, mapId1, mapId2);
        RemoveFromIsland(island2, mapId1, mapId2);
        DetermineSplit(island1, mapId1, mapId2);
      }

      //  //set the island to neutral
      //m_islands[island2].m_companion  = UNSIGNED_MAX;
      //m_islands[island2].m_status     = eNEUTRAL;
      //m_islands[island2].m_masterInfo.m_linkMap.clear();
      //m_islands[island2].m_masterInfo.m_linkMapSize = 0;

    }
  }
}