/*****************************************************************
Filename: island.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../../../meminterface/meminterface.h"
#include <string>
#include <unordered_map>

namespace TestEngine
{
  namespace PhysicsGL
  {
    enum Islanding
    {
      eOFF = 0,
      eON = 1
    };

    enum IslandStatus
    {
      eSERVANT  = 0,
      eMASTER   = 1,
      eNEUTRAL  = 2
    };

    struct OrderNode
    {
      unsigned  aabbID;
      MinMax    type;
    };

      //for debugging
    extern unsigned s_numActiveIslands;
    extern unsigned s_numActiveMasters;
    extern unsigned s_largestIsland;

    struct MapNode
    {
      bool active = false;
      unsigned aabb1 = UNSIGNED_MAX;
      unsigned aabb2 = UNSIGNED_MAX;
    };

    struct PairNode
    {
      unsigned pairPosition = UNSIGNED_MAX;
      bool     active;
    };

    struct MasterNode
    {
      std::unordered_map<unsigned, bool>      m_linkMap;        //uses objects
      unsigned                                m_linkMapSize = 0;
    };

      //the island only stores the information for using
      //and accessing the island from the memory manager
    struct Island
    {
      bool          m_islandActive      = false;
      bool          m_islandThreadSafe  = true;

        //denotes if an island is part of servant-master relationship
      IslandStatus  m_status            = eNEUTRAL;
      unsigned      m_companion         = UNSIGNED_MAX;   //companion island, master or servant
      MasterNode    m_masterInfo;                            //if the island is a master - can be expanded to list

        //default information for island
      unsigned      m_islandSize        = 0;

        //the current pairs in the island
      std::unordered_map<unsigned, PairNode>  m_pairMap;

        //openPairs on the list, for filling
      std::list<unsigned>                     m_openPairs;

        //used to determine if an object is in the island
        //keeps track of number of active pairs for removal
      std::unordered_map<unsigned, unsigned>  m_activeObjects;

    };
  }
}

