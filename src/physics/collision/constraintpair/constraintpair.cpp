/*****************************************************************
Filename: constraintpair.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "constraintpair.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    static float s_distBaumgarte = 1.0f;

    ConstraintPair::ConstraintPair(Body * A, Body * B)
    {
      m_idA                   = A->GetID();
      m_typeA                 = A->GetType();

      m_idB                   = B->GetID();
      m_typeB                 = B->GetType();

      m_sqDistance            = FLT_MAX;
      m_constraintType        = MemoryManager::eROPE;

      m_localContacts[0]      = Vec3(0, 0, 0);
      m_localContacts[1]      = Vec3(0, 0, 0);

      m_accumulatedLambda     = 0;
      m_accumulatedLastFrame  = 0;
      m_dampingConstant       = 1;
      m_springStiffness       = 1;
      m_lockBody[0]           = false;
      m_lockBody[1]           = false;

      m_resolve               = false;
    }

    void ConstraintPair::SetBody(unsigned body, Body* pBody)
    {
      m_bodies[body] = pBody;
      m_bodyIds[body] = pBody->GetID();
      m_types[body] = pBody->GetType();
    }

    void ConstraintPair::LockBody(unsigned i)
    {
      m_lockBody[i] = !m_lockBody[i];
    }

    bool ConstraintPair::ConstraintObjectsActive()
    {
      if (m_A->GetDeleteFlag() || m_B->GetDeleteFlag())
        return false;

      return true;
    }

    bool ConstraintPair::TestConstraint()
    {
      m_resolve = false;

      if (GetDeletedFlag())
        return true;

      if (!ConstraintObjectsActive())
      {
        if (!GetDeletedFlag())
          return false;

        return true;
      }

      if (GetToDeleteFlag())
        return false;

      switch (m_constraintType)
      {
        case MemoryManager::eROD:
          RodTest();
          break;

        case MemoryManager::eROPE:
          RopeTest();
          break;

        case MemoryManager::eSOFT:
           SoftTest();
           break;
      }

      return true;
    }

    void ConstraintPair::ResolveConstraint(float dt)
    {
      switch (m_constraintType)
      {
      case MemoryManager::eROD:
        ResolveRodConstraint(dt);
        break;

      case MemoryManager::eROPE:
        ResolveRopeConstraint(dt);
        break;

      case MemoryManager::eSOFT:
        ResolveSoftConstraint(dt);
        break;
      }
    }

    void ConstraintPair::ResolveRopeConstraint(float dt)
    {
      //contact points
      Vec3 contactA = m_adjustedContacts[0];
      Vec3 contactB = m_adjustedContacts[1];

      //jacobian terms
      Vec3 negNormal = -m_normal;
      Vec3 negContactACrossNorm = -contactA.Cross(m_normal);
      Vec3 contactBCrossNorm = contactB.Cross(m_normal);

      Mat1x12 jacobian(negNormal, negContactACrossNorm, m_normal, contactBCrossNorm);
      Vec12 velocity(m_A->GetVelocity(), m_A->GetAngVelocity(), m_B->GetVelocity(), m_B->GetAngVelocity());

      //create a fake inverse mass matrix
      Mat1x12 invMass(m_A->GetInverseMass(), m_A->GetInverseInertia(), m_B->GetInverseMass(), m_B->GetInverseInertia());

      float baumgarteBias = m_excessDistance * s_distBaumgarte;

      //lambda = -(jacobian * v + baumgarte) / (jacobian * massInverse * jacobianTranspose)
      float lambda = 0;

      Vec12 invMassJacobian = invMass.Mat12Mult(jacobian.Transpose());

      lambda = -(jacobian * velocity + baumgarteBias);
      lambda /= (jacobian * invMassJacobian);

      float oldLambda = m_accumulatedLambda;

      m_accumulatedLambda += lambda;

      //if (m_accumulatedLambda < 0)
      //m_accumulatedLambda = 0;
      m_accumulatedLambda = Math::Max(-FLT_MAX, Math::Min(0, m_accumulatedLambda));

      lambda = m_accumulatedLambda - oldLambda;

      Vec12 deltaV = invMassJacobian * lambda;
      Vec3 deltaVelA(deltaV.vec[0], deltaV.vec[1], deltaV.vec[2]);
      Vec3 deltaAngA(deltaV.vec[3], deltaV.vec[4], deltaV.vec[5]);
      Vec3 deltaVelB(deltaV.vec[6], deltaV.vec[7], deltaV.vec[8]);
      Vec3 deltaAngB(deltaV.vec[9], deltaV.vec[10], deltaV.vec[11]);

      m_A->m_velocity += deltaVelA;
      if (!m_lockBody[0])
        m_A->m_angularVelocity += deltaAngA;

      m_B->m_velocity += deltaVelB;
      if (!m_lockBody[1])
        m_B->m_angularVelocity += deltaAngB;
    }

    void ConstraintPair::ResolveRodConstraint(float dt)
    {
      //contact points
      Vec3 contactA = m_adjustedContacts[0];
      Vec3 contactB = m_adjustedContacts[1];

      //jacobian terms
      Vec3 negNormal = -m_normal;
      Vec3 negContactACrossNorm = -contactA.Cross(m_normal);
      Vec3 contactBCrossNorm = contactB.Cross(m_normal);

      Mat1x12 jacobian(negNormal, negContactACrossNorm, m_normal, contactBCrossNorm);
      Vec12 velocity(m_A->GetVelocity(), m_A->GetAngVelocity(), m_B->GetVelocity(), m_B->GetAngVelocity());

      //create a fake inverse mass matrix
      Mat1x12 invMass(m_A->GetInverseMass(), m_A->GetInverseInertia(), m_B->GetInverseMass(), m_B->GetInverseInertia());

      float baumgarteBias = m_excessDistance * s_distBaumgarte;

      //lambda = -(jacobian * v + baumgarte) / (jacobian * massInverse * jacobianTranspose)
      float lambda = 0;

      Vec12 invMassJacobian = invMass.Mat12Mult(jacobian.Transpose());

      lambda = -(jacobian * velocity + baumgarteBias);
      lambda /= (jacobian * invMassJacobian);

      float oldLambda = m_accumulatedLambda;

      m_accumulatedLambda += lambda;

      lambda = m_accumulatedLambda - oldLambda;

      Vec12 deltaV = invMassJacobian * lambda;
      Vec3 deltaVelA(deltaV.vec[0], deltaV.vec[1], deltaV.vec[2]);
      Vec3 deltaAngA(deltaV.vec[3], deltaV.vec[4], deltaV.vec[5]);
      Vec3 deltaVelB(deltaV.vec[6], deltaV.vec[7], deltaV.vec[8]);
      Vec3 deltaAngB(deltaV.vec[9], deltaV.vec[10], deltaV.vec[11]);

      m_A->m_velocity += deltaVelA;
      if (!m_lockBody[0])
        m_A->m_angularVelocity += deltaAngA;

      m_B->m_velocity += deltaVelB;
      if (!m_lockBody[1])
        m_B->m_angularVelocity += deltaAngB;
    }

    void ConstraintPair::ResolveSoftConstraint(float dt)
    {
      //contact points
      Vec3 contactA = m_adjustedContacts[0];
      Vec3 contactB = m_adjustedContacts[1];

      //jacobian terms
      Vec3 negNormal = -m_normal;

      Vec3 negContactACrossNorm = -contactA.Cross(m_normal);
      Vec3 contactBCrossNorm = contactB.Cross(m_normal);

      Mat1x12 jacobian(negNormal, negContactACrossNorm, m_normal, contactBCrossNorm);
      Vec12 velocity(m_A->GetVelocity(), m_A->GetAngVelocity(), m_B->GetVelocity(), m_B->GetAngVelocity());

      //create a fake inverse mass matrix
      Mat1x12 invMass(m_A->GetInverseMass(), m_A->GetInverseInertia(), m_B->GetInverseMass(), m_B->GetInverseInertia());

      //calculate beta and gamma for soft constraint
      float beta = dt * m_springStiffness;
      float gamma = (m_dampingConstant + beta);
      beta /= gamma;
      gamma = 1 / gamma;

      float baumgarteBias = beta / dt * m_excessDistance + gamma * (m_accumulatedLambda - m_accumulatedLastFrame);

      //lambda = -(jacobian * v + baumgarte) / (jacobian * massInverse * jacobianTranspose)
      float lambda = 0;

      Vec12 invMassJacobian = invMass.Mat12Mult(jacobian.Transpose());

      lambda = -(jacobian * velocity + baumgarteBias);
      lambda /= (jacobian * invMassJacobian);

      float oldLambda = m_accumulatedLambda;
      m_accumulatedLambda += lambda;

      lambda = m_accumulatedLambda - oldLambda;

      Vec12 deltaV = invMassJacobian * lambda;
      Vec3 deltaVelA(deltaV.vec[0], deltaV.vec[1], deltaV.vec[2]);
      Vec3 deltaAngA(deltaV.vec[3], deltaV.vec[4], deltaV.vec[5]);
      Vec3 deltaVelB(deltaV.vec[6], deltaV.vec[7], deltaV.vec[8]);
      Vec3 deltaAngB(deltaV.vec[9], deltaV.vec[10], deltaV.vec[11]);

      m_A->m_velocity += deltaVelA;
      if (!m_lockBody[0])
        m_A->m_angularVelocity += deltaAngA;

      m_B->m_velocity += deltaVelB;
      if (!m_lockBody[1])
        m_B->m_angularVelocity += deltaAngB;
    }

    void ConstraintPair::RopeTest()
    {
      m_adjustedContacts[0] = m_A->GetOrientation() * m_localContacts[0];
      m_adjustedContacts[1] = m_B->GetOrientation() * m_localContacts[1];

      m_contacts[0] = m_adjustedContacts[0] + m_bodies[0]->GetPosition();
      m_contacts[1] = m_adjustedContacts[1] + m_bodies[1]->GetPosition();

      m_normal = m_contacts[1] - m_contacts[0];
      float sqDistance = m_normal.SqLength();
      m_normal.Normalize();

      if (sqDistance > m_sqDistance)
      {
        m_excessDistance = sqDistance - m_sqDistance;
        m_resolve = true;
      }

      else
      {
        m_accumulatedLambda = 0;
        m_resolve = false;
      }
    }
    
    void ConstraintPair::RodTest()
    {
      m_adjustedContacts[0] = m_A->GetOrientation() * m_localContacts[0];
      m_adjustedContacts[1] = m_B->GetOrientation() * m_localContacts[1];

      m_contacts[0] = m_adjustedContacts[0] + m_bodies[0]->GetPosition();
      m_contacts[1] = m_adjustedContacts[1] + m_bodies[1]->GetPosition();

      m_normal = m_contacts[1] - m_contacts[0];
      float sqDistance = m_normal.SqLength();
      m_normal.Normalize();

      m_excessDistance = sqDistance - m_sqDistance;

      m_resolve = true;
    }

    void ConstraintPair::SoftTest()
    {
      m_adjustedContacts[0] = m_A->GetOrientation() * m_localContacts[0];
      m_adjustedContacts[1] = m_B->GetOrientation() * m_localContacts[1];

      m_contacts[0] = m_adjustedContacts[0] + m_bodies[0]->GetPosition();
      m_contacts[1] = m_adjustedContacts[1] + m_bodies[1]->GetPosition();

      m_normal = m_contacts[1] - m_contacts[0];
      float sqDistance = m_normal.SqLength();
      m_normal.Normalize();

      m_excessDistance = sqDistance - m_sqDistance;
      m_accumulatedLastFrame = m_accumulatedLambda;

      m_resolve = true;
    }
  }
}