/*****************************************************************
Filename: constraintpair.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "..\..\body\BodyList.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    class ConstraintPair
    {

      public:
        ConstraintPair(Body* A, Body* B);

        void Initialize() { m_normal = (m_contacts[1] - m_contacts[0]).Normalize(); }

          //gettors
          //id and info
        inline unsigned     GetId(unsigned body)                            { return m_bodyIds[body]; }
        inline colliderType GetType(unsigned body)                          { return m_types[body]; }
        
          //determine if constraint should be applied
        inline bool         GetResolve()                                    { return m_resolve; }

          //contact info for drawing
        inline Vec3         GetConstraintLength()                           { return m_contacts[1] - m_contacts[0]; }
        inline Vec3         GetConstraintPosition()                         { return m_contacts[0]; }
        inline Vec3         GetNormal()                                     { return m_normal; }
        inline bool         GetSelected()                                   { return m_selected; }

          //settors
        void SetConstraintType(MemoryManager::constraintType type)          { m_constraintType = type; }
        void SetConstraintDist(float distance)                              { m_sqDistance = distance * distance; }
        void SetConstraintDamping(float dampingConstant)                    { m_dampingConstant = dampingConstant; }
        void SetConstraintStiffness(float springStiffness)                  { m_springStiffness = springStiffness; }
        void SetContact(unsigned body, Vec3& localPosition)                 { m_localContacts[body] = localPosition; }
        void SetWorldContact(unsigned body, Vec3& worldPosition)            { m_contacts[body] = worldPosition; }
        void SetDeletedFlag(bool deletion)                                  { m_deleted = deletion; }
        void SetToDeleteFlag(bool deletion)                                 { m_toDelete = deletion; }

        void SetBody(unsigned body, Body* pBody);     //used for updating body pointers every frame
        void LockBody(unsigned i);                    //prevent body from rotating with constraint

        void SetSelected(bool selected)                                     { m_selected = selected; }

          //update
        bool GetDeletedFlag()                                               { return m_deleted; }
        bool GetToDeleteFlag()                                              { return m_toDelete; }
        bool ConstraintObjectsActive();
        bool TestConstraint();  //see if step should be run
        void ResolveConstraint(float dt);  //resolve constraint

          //for debugging
        Vec3        GetContactPoint(unsigned i)                             { return m_contacts[i]; }
        

      public:
        void ResolveRopeConstraint(float dt);
        void ResolveRodConstraint(float dt);
        void ResolveSoftConstraint(float dt);

        //specific constraints
        void RopeTest();
        void RodTest();
        void SoftTest();

        //body info for testing - updated every frame and used for resolution
        union
        {
          struct
          {
            Body*         m_A;
            Body*         m_B;
          };

          Body*           m_bodies[2];
        };

          //for storing ids for use of pairs over multiple frames
          //not updated
        union
        {
          struct
          {
            unsigned      m_idA;
            unsigned      m_idB;
          };

          unsigned        m_bodyIds[2];
        };

        union
        {
          struct
          {
            colliderType  m_typeA;
            colliderType  m_typeB;
          };

          colliderType    m_types[2];
        };


          //constraint info for determining whether to resolve
        bool                              m_resolve;
        MemoryManager::constraintType     m_constraintType;
        float                             m_sqDistance;

          //for resolution
        Vec3                              m_normal;           //normal is from A to B - different from collision system

          //contact point information
          //should be a local point on each body
 
        Vec3              m_localContacts[2]; 
        Vec3              m_adjustedContacts[2];
        Vec3              m_contacts[2];
        
        float             m_accumulatedLambda;
        float             m_accumulatedLastFrame;
        float             m_excessDistance;

        union
        {
          struct
          {
            bool          m_lockBodyA;
            bool          m_lockBodyB;
          };

          bool            m_lockBody[2];
        };
        

          //for soft constraints
        float             m_dampingConstant;
        float             m_springStiffness;
        float             m_beta;
        float             m_gamma;

          //deletion
        bool              m_deleted = false;
        bool              m_toDelete = false;

          //debug
        bool              m_selected = false;
    }; 
  }
}