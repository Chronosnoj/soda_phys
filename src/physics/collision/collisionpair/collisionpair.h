/*****************************************************************
Filename: collisionpair.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "..\..\body\BodyList.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		struct SimplexPoint
		{
			SimplexPoint() {}

			Vec3 support;
			Vec3 supportA;
			Vec3 supportB;
		};

		struct SimplexEdge
		{
			int indexStart;
			int indexEnd;
			int count;

			bool operator==(SimplexEdge& rhs)
			{
				return ((indexStart == rhs.indexStart && indexEnd == rhs.indexEnd) ||
					(indexStart == rhs.indexEnd && indexEnd == rhs.indexStart));
			}
		};

		struct SimplexFace
		{
			union
			{
				struct
				{
					unsigned indexA;
					unsigned indexB;
					unsigned indexC;
				};
				
				unsigned index[3];
			};
			

			Vec3 normal;
			float distance;  //for testing termination
		};

		class CollisionPair
		{
			public:

          //constructors
				CollisionPair(Body* A, Body* B);

        void                PrepForNextFrame();

          //settors
        void                SetBody(unsigned body, Body* pBody);
        inline void         SetActive(bool active)                                    { m_active = active; }
        inline void         SetCollision(bool collision)                              { m_collision = collision; }
 
				  //gettors
          //check if pair is active for testing
        inline bool         GetActive()                                               { return m_active; }

          //id and info
        inline unsigned     GetId(unsigned body)                                      { return m_bodyIds[body]; }
        inline colliderType GetType(unsigned body)                                    { return m_types[body]; }

          //useful for collision
				inline Body*        GetA()														                        { return m_A; }
				inline Body*        GetB()														                        { return m_B; }
				inline Body*        GetBody(unsigned i)										                    { return m_bodies[i]; }
				inline bool         GetCollision()												                    { return m_collision; }
				inline Vec3&        GetNormal()												                        { return m_normal; }
				inline Vec3*        GetContacts()												                      { return m_contacts; }
				inline Vec3&        GetSimplexPoint(unsigned i)								                { return m_simplex[i].support; }
				inline Vec3&        GetEPASimplexPoint(unsigned i)								            { return m_EPASimplex[i].support; }
				inline unsigned     GetEPASimplexPointIndex(unsigned face, unsigned index)	  { return m_EPAFaces[face].index[index]; }
				inline unsigned     GetSimplexSize()										                      { return m_simplexSize; }
				inline unsigned     GetEPAFaceSize()										                      { return m_EPAFacesSize; }
        inline unsigned     GetStoredContactSize()                                    { return m_storedSize; }
				inline Vec3&        GetContactPointA(unsigned i)								              { return m_storedContactsA[i]; }
        inline Vec3&        GetContactPointB(unsigned i)                              { return m_storedContactsB[i]; }

        Vec3        GetContactPoint(unsigned i, unsigned body);

				  //collision tests
        void ResetSimplex();
				bool TestGJK();

          //resolution
        //void PrepConstraints();
        void PrepForCollision();
        void Resolve(float dt, unsigned contact, unsigned iteration, unsigned maxIterations);
        void PositionCorrection();

			private:
				void GetSupportPoint(Vec3& direction, SimplexPoint& support);

				  //GJK simplex functions
				bool Simplex(Vec3& newDirection);
				bool SimplexLine(Vec3& newDirection);
				bool SimplexTriangle(Vec3& newDirection);
				bool SimplexTetrahedron(Vec3& newDirection);
				bool GJKContactPoints();
				bool GJKBarycentric();
				bool GJKBarycentricBi();
				bool GJKBarycentricTri();
				bool GJKBarycentricTetra();

				  //adjust simplex values in array
				  //goal is to always end up with tetrahedron in format:
				  //       A
				  //     / | \
				  //    /  |  \
				  //   /  -C-  \
				  // B__________D

				bool SetAO();
				bool SetAB();
				bool SetAC();
				bool SetAD();
				bool SetABC();
				bool SetACB();
				bool SetACD();
				bool SetADB();

				  //EPA functions
				bool		  TestEPA();
				unsigned	GetClosestFace();
				bool		  CalculateFaceNormal(unsigned index);			//will not rearrange points if normal is inward
				bool		  CheckFaces(SimplexPoint& point);				//bool represents success of operation
				bool		  AddPoint(SimplexPoint& point);					//only adds a point to simplex, does not modify faces
				void		  RemovePoint(unsigned index);					//this can cause problems if faces not updated
				bool		  AddFace(unsigned a, unsigned b, unsigned c);	//a start, b and c should create outward normal
				void		  RemoveFace(unsigned index);
				bool		  CheckEdges(unsigned face);
				bool		  AddEdge(unsigned start, unsigned end);
				void		  ClearEdges();
				bool		  CalculateBarycentric(unsigned face, float* baryVals);
				bool		  CalculateContactInfo(unsigned face);
				
          //persistent manifold helpers
        void      CalculateNewContactArea();
        float     CalculateArea();

          //body info for testing
				union
				{
					struct
					{
						Body*         m_A;
						Body*         m_B;
					};
					
					Body*           m_bodies[2];
				};

          //for storing ids for use of pairs between frames
        union
        {
          struct
          {
            unsigned      m_idA;
            unsigned      m_idB;
          };

          unsigned        m_bodyIds[2];
        };

        union
        {
          struct
          {
            colliderType  m_typeA;
            colliderType  m_typeB;
          };

          colliderType    m_types[2];
        };

          //normal is from A to B
				Vec3              m_normal;
				Vec3              m_contacts[2];
				bool              m_collision;
				float             m_penetration;

				  //simplexes included in collision pairs for debugging and drawing
				  //GJK
				SimplexPoint	    m_simplex[4];
				unsigned		      m_simplexSize;

				  //EPA
				static const int  s_maxEPASimplexVerts = 30;
				static const int  s_maxEPASimplexFaces = 30;
				static const int  s_maxEPASimplexEdges = 30;

				SimplexPoint	    m_EPASimplex[s_maxEPASimplexVerts];
				SimplexFace		    m_EPAFaces[s_maxEPASimplexFaces];
				SimplexEdge		    m_EPAEdges[s_maxEPASimplexVerts];
				unsigned		      m_EPASimplexSize;
				unsigned		      m_EPAFacesSize;
				unsigned		      m_EPAEdgesSize;

          //resolution
        float             m_accumulatedLambda;

          //up to 4 sets of contacts (8 points), 4 accumulated lambdas
        unsigned          m_storedSize;
        unsigned          m_penContact;  //for penetration and baumgarte bias
        Vec3              m_storedContactsA[4];
        Vec3              m_storedContactsB[4];
        float             m_storedLambdas[4];
        float             m_frictionAxis1Lambda[4];
        float             m_frictionAxis2Lambda[4];
        //float             m_bestArea;
        //bool              m_addContact;


          //broadphase - checks if pair is actually present for testing
        bool              m_active;
		};

	}
}