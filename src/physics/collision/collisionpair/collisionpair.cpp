/*****************************************************************
Filename: collisionpair.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "collisionpair.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		CollisionPair::CollisionPair(Body* A, Body* B): m_A(A), m_B(B), m_simplexSize(0), 
			m_EPASimplexSize(0), m_EPAFacesSize(0), m_EPAEdgesSize(0), m_collision(false), 
      m_penetration(FLT_MAX), m_accumulatedLambda(0.0f), m_active(true)
		{
      m_idA = A->GetID();
      m_typeA = A->GetType();
      
      m_idB = B->GetID();
      m_typeB = B->GetType();

      m_storedSize = 0;
		}

    void CollisionPair::PrepForNextFrame()
    {
      ResetSimplex();

      m_penetration = FLT_MAX;
      m_collision = false;
    }

    void CollisionPair::SetBody(unsigned body, Body * pBody)
    {
      if (!pBody)
        std::cout << "Throw an error" << std::endl;

      m_bodies[body] = pBody;
      m_bodyIds[body] = pBody->GetID();
      m_types[body] = pBody->GetType();
      pBody->SetCollision(false);
    }

    Vec3 CollisionPair::GetContactPoint(unsigned i, unsigned body)
    {
      switch (body)
      {
        case 0: 
          return GetContactPointA(i) + m_A->GetPosition();

        case 1:
          return GetContactPointB(i) + m_B->GetPosition();
      }

      return GetContactPointA(i) + m_A->GetPosition();
    }
	}
}