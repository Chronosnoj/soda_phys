/*****************************************************************
Filename: gjk.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../collisionpair/collisionpair.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		static const float s_GJKDistanceEpsilon = .005f;
    static const unsigned s_GJKIterations = 10;

    void CollisionPair::ResetSimplex()
    {
      m_simplexSize = 0;
      m_EPAEdgesSize = 0;
      m_EPAFacesSize = 0;
      m_EPASimplexSize = 0;
    }

    bool CollisionPair::TestGJK()
		{
			Vec3 newDirection(1, 0, 0);
			SimplexPoint support;

			GetSupportPoint(newDirection, support);

			m_simplex[1] = support;
			++m_simplexSize;

			newDirection = -m_simplex[1].support;

			  //loop for result
			for (unsigned i = 0; i < s_GJKIterations; ++i)
			{
				GetSupportPoint(newDirection, support);

				/*if (support.support.Dot(newDirection) < -s_GJKDistanceEpsilon)
					return false;*/

        if (support.support.Dot(newDirection) < 0)
          return false;

				m_simplex[0] = support;
				++m_simplexSize;

				if (Simplex(newDirection))
				{
					m_collision = TestEPA();
          return m_collision;
				}
			}

			return false;
		}

		void CollisionPair::GetSupportPoint(Vec3& direction, SimplexPoint& support)
		{
			direction.Normalize();

			Vec3 supportA = m_A->GetSupport(m_A->GetOrientation().CreateTranspose() * direction);
			supportA = m_A->GetOrientation() * supportA;
			supportA = m_A->GetPosition() + supportA;
			support.supportA = supportA;

			Vec3 supportB = m_B->GetSupport(m_B->GetOrientation().CreateTranspose() * -direction);
			supportB = m_B->GetOrientation() * supportB;
			supportB = m_B->GetPosition() + supportB;
			support.supportB = supportB;

			support.support = supportA - supportB;

			return;
		}

		bool CollisionPair::GJKContactPoints()
		{
			switch (m_simplexSize)
			{
				case 1:
					GJKBarycentric();
					break;

				case 2:
					GJKBarycentricBi();
					break;

				case 3:
					GJKBarycentricTri();
					break;

				case 4:
					GJKBarycentricTetra();
					break;
			}
			return true;
		}

		bool CollisionPair::GJKBarycentric()
		{
			m_contacts[0] = m_simplex[0].supportA * .5f + m_simplex[1].supportB * .5f;
			m_contacts[1] = m_simplex[0].supportA * .5f + m_simplex[1].supportB * .5f;

			m_normal = (m_simplex[0].supportB - m_simplex[0].supportA).Normalize();
			return true;
		}

		bool CollisionPair::GJKBarycentricBi()
		{
			Vec3 segAO = -m_simplex[0].support;
			Vec3 segAB = m_simplex[1].support - m_simplex[0].support;
			Vec3 proj = segAB;
			proj.Normalize();

			  //get the vector to the projected origin point
			float tVal = proj.Dot(segAO);
			tVal = tVal / segAB.Length();

			m_contacts[0] = m_simplex[0].supportA * (1 - tVal) + m_simplex[1].supportA * tVal;
			m_contacts[1] = m_simplex[0].supportB * (1 - tVal) + m_simplex[1].supportB * tVal;

			return true;
		}

		bool CollisionPair::GJKBarycentricTri()
		{
			//determine origin point project onto triangle
			Vec3 segAB = m_simplex[1].support - m_simplex[0].support;
			Vec3 segAC = m_simplex[2].support - m_simplex[0].support;
			Vec3 segAtetraO = -m_simplex[0].support;

			Vec3 normal = segAB.Cross(segAC);
			normal.Normalize();
			float distance = normal.Dot(segAtetraO);				

			Vec3 originOnFace = normal * distance;
			Vec3 segAO = originOnFace - m_simplex[0].support;

			float ABdotAB = segAB.Dot(segAB);
			float ABdotAC = segAB.Dot(segAC);
			float ACdotAC = segAC.Dot(segAC);
			float AOdotAB = segAO.Dot(segAB);
			float AOdotAC = segAO.Dot(segAC);

			float denom = (ABdotAB * ACdotAC - ABdotAC * ABdotAC);
			if (denom == 0.0f)
				return false;

			float baryVals[3];
			baryVals[1] = (ACdotAC * AOdotAB - ABdotAC * AOdotAC) / denom;
			baryVals[2] = (ABdotAB * AOdotAC - ABdotAC * AOdotAB) / denom;
			baryVals[0] = 1 - baryVals[2] - baryVals[1];

			m_contacts[0] = (m_simplex[0].supportA * baryVals[0])
				+ (m_simplex[1].supportA * baryVals[1])
				+ (m_simplex[2].supportA * baryVals[2]);

			m_contacts[1] = (m_simplex[0].supportB * baryVals[0])
				+ (m_simplex[0].supportB * baryVals[1])
				+ (m_simplex[0].supportB * baryVals[2]);

			m_normal = normal;

			return true;
		}

		bool CollisionPair::GJKBarycentricTetra()
		{
			return true;
		}
	}
}