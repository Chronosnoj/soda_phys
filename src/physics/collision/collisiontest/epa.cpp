/*****************************************************************
Filename: epa.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../collisionpair/collisionpair.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		static int maxCount = 10;
		static const float s_EPAepsilon = .05f;
    static const float s_collisionDetectEpsilon = .0001f;

		bool CollisionPair::TestEPA()
		{
			int epaCount = 0;

			  //add original simplex
			  //add points
			  //should never occur with simplex other than 4
			for (unsigned i = 0; i < m_simplexSize; ++i)
				m_EPASimplex[i] = m_simplex[i];
			m_EPASimplexSize = m_simplexSize;

			  //add faces
			AddFace(0, 2, 1);  //acb
			AddFace(0, 3, 2);  //adc
			AddFace(0, 1, 3);  //abd
			AddFace(1, 2, 3);  //bcd

			SimplexPoint support;
			while (epaCount < maxCount)
			{
        //quit if something goes wrong
        if (m_EPAFacesSize >= s_maxEPASimplexFaces)
          return false;

        if (m_EPAEdgesSize >= s_maxEPASimplexEdges)
          return false;

        if (m_EPASimplexSize >= s_maxEPASimplexVerts)
          return false;

				  //find closest face
				unsigned index = GetClosestFace();

				  //get support point in direction of face normal
				GetSupportPoint(m_EPAFaces[index].normal, support);

				  //check distance to see if we should exit
				  //also exit if approaching max iterations and just return results
				float epsilonDist = support.support.Dot(m_EPAFaces[index].normal);
				epsilonDist = epsilonDist - m_EPAFaces[index].distance;
				if (epsilonDist < 0)
					epsilonDist *= -1;
				if (epsilonDist < s_EPAepsilon || epaCount == (maxCount - 1))
				{
					  //if so, store data
					if (!CalculateContactInfo(index))
						return false;
					return true;
				}

				  //it can be expanded
				else
				{
					  //add support point
					if (!AddPoint(support))
						return false;

					  //check faces against support point for 'visibility'
					if (!CheckFaces(support))
						return false;
				}

				++epaCount;
			}

			return false;
		}

		unsigned CollisionPair::GetClosestFace()
		{
			float minDist = FLT_MAX;
			unsigned bestIndex = 0;

			for (unsigned i = 0; i < m_EPAFacesSize; ++i)
			{
				if (m_EPAFaces[i].distance < minDist)
				{
					minDist = m_EPAFaces[i].distance;
					bestIndex = i;
				}
			}

			return bestIndex;
		}

		bool CollisionPair::CalculateFaceNormal(unsigned index)
		{
			Vec3 segAB = m_EPASimplex[m_EPAFaces[index].indexB].support - m_EPASimplex[m_EPAFaces[index].indexA].support;
			Vec3 segAC = m_EPASimplex[m_EPAFaces[index].indexC].support - m_EPASimplex[m_EPAFaces[index].indexA].support;

			Vec3 normal = segAB.Cross(segAC);
			normal.Normalize();
			Vec3 segAO = m_EPASimplex[m_EPAFaces[index].indexA].support;

			m_EPAFaces[index].distance = normal.Dot(segAO);
      m_EPAFaces[index].normal = normal;

      if (m_EPAFaces[index].distance < 0)
      {
        m_EPAFaces[index].distance *= -1;
        return false;
      }

			return true;
		}

		bool CollisionPair::CheckFaces(SimplexPoint& point)
		{
			  //remove 'visible' faces
			for (unsigned i = 0; i < m_EPAFacesSize; ++i)
			{
				if (m_EPAFaces[i].normal.Dot(point.support) - m_EPAFaces[i].distance > 0)
				{
					  //determine edges of impacted face
					if (!CheckEdges(i))
						return false;

					  //remove face
					RemoveFace(i);
					--i;  //since we are removing a face, don't want to skip replacement
				}
			}

			  //reconnect edges by adding new faces
			for (unsigned i = 0; i < m_EPAEdgesSize; ++i)
			{
				if (m_EPAEdges[i].count == 1)
					AddFace(m_EPAEdges[i].indexStart, m_EPAEdges[i].indexEnd, m_EPASimplexSize - 1);
			}

			  //clear edges
			ClearEdges();

			return true;
		}

		bool CollisionPair::AddPoint(SimplexPoint& point)
		{
			if (m_EPASimplexSize >= s_maxEPASimplexVerts)
				return false;

			m_EPASimplex[m_EPASimplexSize] = point;
			++m_EPASimplexSize;

			return true;
		}

		void CollisionPair::RemovePoint(unsigned index)
		{
			for (unsigned i = index; i < m_EPASimplexSize - 1; ++i)
				m_EPASimplex[i] = m_EPASimplex[i + 1];

			--m_EPASimplexSize;
			return;

		}

		bool CollisionPair::AddFace(unsigned a, unsigned b, unsigned c)
		{
			if (m_EPAFacesSize >= s_maxEPASimplexFaces)
				return false;

			m_EPAFaces[m_EPAFacesSize].indexA = a;
			m_EPAFaces[m_EPAFacesSize].indexB = b;
			m_EPAFaces[m_EPAFacesSize].indexC = c;

			if (!CalculateFaceNormal(m_EPAFacesSize))
			{
				m_EPAFaces[m_EPAFacesSize].indexB = c;
				m_EPAFaces[m_EPAFacesSize].indexC = b;
				m_EPAFaces[m_EPAFacesSize].normal = -m_EPAFaces[m_EPAFacesSize].normal;
			}

			++m_EPAFacesSize;
			return true;
		}

		void CollisionPair::RemoveFace(unsigned index)
		{
			for (unsigned i = index; i < m_EPAFacesSize - 1; ++i)
				m_EPAFaces[i] = m_EPAFaces[i + 1];

			--m_EPAFacesSize;
			return;
		}

		bool CollisionPair::CheckEdges(unsigned face)
		{
			if (!AddEdge(m_EPAFaces[face].indexA, m_EPAFaces[face].indexB))
				return false;

			if (!AddEdge(m_EPAFaces[face].indexB, m_EPAFaces[face].indexC))
				return false;

			if (!AddEdge(m_EPAFaces[face].indexC, m_EPAFaces[face].indexA))
				return false;

			return true;
		}

		bool CollisionPair::AddEdge(unsigned start, unsigned end)
		{
			SimplexEdge newEdge;
			newEdge.indexStart = start;
			newEdge.indexEnd = end;

      bool foundEdge = false;

			for (unsigned i = 0; i < m_EPAEdgesSize; ++i)
			{
        if (newEdge == m_EPAEdges[i])
        {
          ++m_EPAEdges[i].count;
          foundEdge = true;
        }				
			}

      if (m_EPAEdgesSize == s_maxEPASimplexEdges)
        return false;

      if (!foundEdge)
      {
        m_EPAEdges[m_EPAEdgesSize] = newEdge;
        m_EPAEdges[m_EPAEdgesSize].count = 1;
        ++m_EPAEdgesSize;
      }

			return true;
		}

		void CollisionPair::ClearEdges()
		{
			m_EPAEdgesSize = 0;
		}

		bool CollisionPair::CalculateBarycentric(unsigned face, float* baryVals)
		{
			//determine origin point project onto triangle
			Vec3 originOnFace = m_EPAFaces[face].normal * m_EPAFaces[face].distance;

			Vec3 segAB = m_EPASimplex[m_EPAFaces[face].indexB].support - m_EPASimplex[m_EPAFaces[face].indexA].support;
			Vec3 segAC = m_EPASimplex[m_EPAFaces[face].indexC].support - m_EPASimplex[m_EPAFaces[face].indexA].support;
			Vec3 segAO = originOnFace - m_EPASimplex[m_EPAFaces[face].indexA].support;

			float ABdotAB = segAB.Dot(segAB);
			float ABdotAC = segAB.Dot(segAC);
			float ACdotAC = segAC.Dot(segAC);
			float AOdotAB = segAO.Dot(segAB);
			float AOdotAC = segAO.Dot(segAC);

			float denom = (ABdotAB * ACdotAC - ABdotAC * ABdotAC);
			if (denom == 0.0f)
				return false;

			baryVals[1] = (ACdotAC * AOdotAB - ABdotAC * AOdotAC) / denom;
			baryVals[2] = (ABdotAB * AOdotAC - ABdotAC * AOdotAB) / denom;
			baryVals[0] = 1 - baryVals[2] - baryVals[1];

			return true;
		}

		bool CollisionPair::CalculateContactInfo(unsigned face)
		{
			//compute barycentric values first
			float baryVals[3];
			if (!CalculateBarycentric(face, baryVals))
				return false;

			m_contacts[0] = (m_EPASimplex[m_EPAFaces[face].indexA].supportA * baryVals[0])
				+ (m_EPASimplex[m_EPAFaces[face].indexB].supportA * baryVals[1])
				+ (m_EPASimplex[m_EPAFaces[face].indexC].supportA * baryVals[2]);

			m_contacts[1] = (m_EPASimplex[m_EPAFaces[face].indexA].supportB * baryVals[0])
				+ (m_EPASimplex[m_EPAFaces[face].indexB].supportB * baryVals[1])
				+ (m_EPASimplex[m_EPAFaces[face].indexC].supportB * baryVals[2]);

			m_normal = m_EPAFaces[face].normal;

      if (m_normal.SqLength() < s_collisionDetectEpsilon)
        return false;

      if (m_contacts[0].SqLength() > 100000 || m_contacts[1].SqLength() > 100000)
        return false;

			m_penetration = m_EPAFaces[face].distance;

			return true;
		}
	}
}