/*****************************************************************
Filename: simplex.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../collisionpair/collisionpair.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		bool CollisionPair::Simplex(Vec3& newDirection)
		{
			switch (m_simplexSize)
			{
				case 2:
					return SimplexLine(newDirection);
					break;

				case 3:
					return SimplexTriangle(newDirection);
					break;

				case 4:
					return SimplexTetrahedron(newDirection);
					break;
			}

			return false;
		}

		bool CollisionPair::SimplexLine(Vec3& newDirection)
		{
			Vec3 segAB = m_simplex[1].support - m_simplex[0].support;
			Vec3 segAO = -m_simplex[0].support;

			if (segAO.Dot(segAB) > 0)
			{

				newDirection = segAB.Cross(segAO.Cross(segAB));

				if (newDirection.Length() == 0)
					newDirection.y += .000001f;

				return SetAB();
			}

			newDirection = segAO;
			return SetAO();
		}

		bool CollisionPair::SimplexTriangle(Vec3& newDirection)
		{
			Vec3 segAB = m_simplex[1].support - m_simplex[0].support;
			Vec3 segAC = m_simplex[2].support - m_simplex[0].support;

			Vec3 segAO = -m_simplex[0].support;

			Vec3 normABC = segAB.Cross(segAC);

			if ((segAB.Cross(normABC)).Dot(segAO) > 0)
			{
				if (segAB.Dot(segAO) > 0)
				{
					newDirection = segAB.Cross(segAO.Cross(segAB));
					return SetAB();
				}

				newDirection = segAO;
				return SetAO();
			}

			else if ((normABC.Cross(segAC)).Dot(segAO) > 0)
			{
				if (segAC.Dot(segAO) > 0)
				{
					newDirection = (segAC.Cross(segAO)).Cross(segAC);
					return SetAC();
				}
				
				newDirection = segAO;
				return SetAO();
			}

			else
			{
				newDirection = normABC;

				if (newDirection.Dot(segAO) < 0)
				{
					newDirection = -normABC;
					return SetABC();
				}
					
				return SetACB();
			}
		}

		bool CollisionPair::SimplexTetrahedron(Vec3& newDirection)
		{
			//get necessary segments
			//some tests can be eliminated by underlying assumptions
			//tetrahedron
			//       A
			//     / | \
			//    /  |  \
			//   /  -C-  \
			// B__________D


			  //grab segments
			Vec3 segAB = m_simplex[1].support - m_simplex[0].support;
			Vec3 segAC = m_simplex[2].support - m_simplex[0].support;
			Vec3 segAD = m_simplex[3].support - m_simplex[0].support;

			Vec3 segAO = -m_simplex[0].support;

			  //normals face inward
			Vec3 normACB = segAC.Cross(segAB);
			Vec3 normADC = segAD.Cross(segAC);
			Vec3 normABD = segAB.Cross(segAD);

			if (normACB.Dot(segAO) <= 0 && normADC.Dot(segAO) <= 0 && normABD.Dot(segAO) <= 0)
				return true;

			if (segAB.Dot(segAO) < 0 && segAC.Dot(segAO) < 0 && segAD.Dot(segAO) < 0)
			{
				newDirection = segAO;
				return SetAO();
			}

			if (normACB.Dot(segAO) > 0)
			{
				  //planes on edges with normals inward
				Vec3 planeAB = segAB.Cross(normACB);
				Vec3 planeAC = normACB.Cross(segAC);

				if (planeAB.Dot(segAO) > 0)
				{
					if (planeAC.Dot(segAO) > 0)
					{
						newDirection = normACB;
						return SetABC();
					}

					newDirection = (segAC.Cross(segAO)).Cross(segAC);
					return SetAC();
					
				}
					
				newDirection = segAB.Cross(segAO.Cross(segAB));
				return SetAB();
			}

			else if (normABD.Dot(segAO) > 0)
			{
				Vec3 planeAB = normABD.Cross(segAB);
				Vec3 planeAD = segAD.Cross(normABD);
				
				if (planeAB.Dot(segAO) > 0)
				{
					if (planeAD.Dot(segAO) > 0)
					{
						newDirection = normABD;
						return SetADB();
					}

					newDirection = segAD.Cross(segAO.Cross(segAD));
					return SetAD();
				}

				newDirection = (segAB.Cross(segAO)).Cross(segAB);
				return SetAB();
			}

			else
			{
				Vec3 planeAC = segAC.Cross(normADC);
				Vec3 planeAD = normADC.Cross(segAD);

				if (planeAC.Dot(segAO) > 0)
				{
					if (planeAD.Dot(segAO) > 0)
					{
						newDirection = normADC;
						return SetACD();
					}

					newDirection = (segAD.Cross(normADC)).Cross(segAD);
					return SetAD();
				}

				newDirection = (segAC.Cross(segAO)).Cross(segAC);
				return SetAC();
				
			}

			return true;
		}
	}
}