/*****************************************************************
Filename: setsimplex.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../collisionpair/collisionpair.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		bool CollisionPair::SetAO()
		{
			m_simplex[1] = m_simplex[0];
			m_simplexSize = 1;
			return false;
		}

		bool CollisionPair::SetAB()
		{
			m_simplex[2] = m_simplex[1];
			m_simplex[1] = m_simplex[0];
			m_simplexSize = 2;
			return false;
		}

		bool CollisionPair::SetAC()
		{
			m_simplex[1] = m_simplex[0];
			m_simplexSize = 2;
			return false;
		}

		bool CollisionPair::SetAD()
		{
			m_simplex[2] = m_simplex[3];
			m_simplex[1] = m_simplex[0];
			m_simplexSize = 2;
			return false;
		}

		bool CollisionPair::SetABC()
		{
			m_simplex[3] = m_simplex[2];
			m_simplex[2] = m_simplex[1];
			m_simplex[1] = m_simplex[0];
			
			m_simplexSize = 3;
			return false;
		}

		bool CollisionPair::SetACB()
		{
			m_simplex[3] = m_simplex[2];
			m_simplex[2] = m_simplex[0];

			m_simplexSize = 3;
			return false;
		}

		bool CollisionPair::SetACD()
		{
			m_simplex[1] = m_simplex[0];

			m_simplexSize = 3;
			return false;
		}

		bool CollisionPair::SetADB()
		{
			m_simplex[2] = m_simplex[0];

			m_simplexSize = 3;
			return false;
		}

	}
}