/*****************************************************************
Filename: resolution.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../collisionpair/collisionpair.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    static float s_baumgarte          = .0008f;
    static float s_contactEpsilon     = .001f;
    static float s_storeEpsilon       = .0008f;
    static float s_positionCorrection = .047f;

    //static float s_baumgarte = .0008f;
    //static float s_contactEpsilon = .0008f;
    //static float s_storeEpsilon = .0008f;
    //static float s_positionCorrection = .047f;

      //should be a property of the objects
    static float s_frictionCoeff = .3f;

    void CollisionPair::PrepForCollision()
    {

        //check most recent set of contacts vs stored contacts
        //compare every set vs epsilon - store local space, assume adjacent
        //check current coords in realspace for distance
        //if a 'match' - determine change and update the contacts

        //whether new contact should be added
      //m_addContact = false;

      Vec3 contactA = m_contacts[0] - m_A->GetPosition();
      Vec3 contactB = m_contacts[1] - m_B->GetPosition();

      bool matchFound = false;
      unsigned found = 0;  //used for skipping distance check

      for (unsigned i = 0; i < m_storedSize; ++i)
      {
          //check in local space vs original contact pair
        Vec3 testContactA = m_storedContactsA[i] - contactA;
        Vec3 testContactB = m_storedContactsB[i] - contactB;

        float testDistA = testContactA.SqLength();
        float testDistB = testContactB.SqLength();

        if (testDistA < s_contactEpsilon && testDistB < s_contactEpsilon)
        {
            //contact is valid to reuse
            //replace contact
          m_storedContactsA[i] = contactA;
          m_storedContactsB[i] = contactB;
          matchFound = true;
          found = i;
          m_penContact = i;
          break;
        }
      }

        //check current contacts 
      for (unsigned i = 0; i < m_storedSize; ++i)
      {
            //check each set of contacts
            //remove old contacts by copying over them and reducing size
            //of m_storedSize

          //don't bother with recently added pair
        if (matchFound && i == found)
          continue;

          //check in world space for distance between pair
          //assume pair were at 0 world space distance to begin with
        Vec3 positionA = m_storedContactsA[i] + m_A->GetPosition();
        Vec3 positionB = m_storedContactsB[i] + m_B->GetPosition();

        float testDistance = (positionB - positionA).SqLength();

        bool removeContact = false;

          //check if distance is within contact epsilon
        if (testDistance < s_storeEpsilon)
        {
          Vec3 testB = positionA - m_B->GetPosition();
          Vec3 testA = positionB - m_A->GetPosition();

            //snap B to A
          if (testB.x * testB.x < m_B->GetDimensions().x * m_B->GetDimensions().x
            && testB.y * testB.y < m_B->GetDimensions().y * m_B->GetDimensions().y
            && testB.z * testB.z < m_B->GetDimensions().z * m_B->GetDimensions().z)
            m_storedContactsB[i] = testB;

            //otherwise snap A to B
          else if (testA.x * testA.x < m_A->GetDimensions().x * m_A->GetDimensions().x
            && testA.y * testA.y < m_A->GetDimensions().y * m_A->GetDimensions().y
            && testA.z * testA.z < m_A->GetDimensions().z * m_A->GetDimensions().z)
            m_storedContactsA[i] = testA;

            //otherwise point is outside both bodies so recompute
          else
            removeContact = true;
        }

        else
          removeContact = true;
          
          //remove pairs that exceed contact epsilon length
        if (removeContact)
        {
          for (unsigned j = i; j < m_storedSize - 1; ++j)
          {
            m_storedContactsA[j] = m_storedContactsA[j + 1];
            m_storedContactsB[j] = m_storedContactsB[j + 1];
            m_storedLambdas[j] = m_storedLambdas[j + 1];
          }

          --m_storedSize;
        }
      }

      if (!matchFound)
      {
          //push contacts back and add new contact with 0 for accumulated lambda
        if (m_storedSize == 4)
        {
          CalculateNewContactArea();
          m_storedSize = 3;
        }
          
        
        else
        {
          for (int i = m_storedSize; i > 0; --i)
          {
            m_storedContactsA[i] = m_storedContactsA[i - 1];
            m_storedContactsB[i] = m_storedContactsB[i - 1];
            m_storedLambdas[i] = m_storedLambdas[i - 1];
          }

          //m_addContact = true;
        }
        
        //if (m_addContact)
        //{
          //intialize values for new contact
          m_storedContactsA[0] = contactA;
          m_storedContactsB[0] = contactB;
          m_storedLambdas[0] = 0;
          m_frictionAxis1Lambda[0] = 0;
          m_frictionAxis2Lambda[0] = 0;
          m_penContact = 0;
          ++m_storedSize;

          //if (m_storedSize == 4)
            //CalculateArea();
        //}  
      }

        //reset friction to zero
      for (unsigned i = 0; i < m_storedSize; ++i)
      {
        m_frictionAxis1Lambda[i] = 0;
        m_frictionAxis2Lambda[i] = 0;
      }
    }

    void CollisionPair::CalculateNewContactArea()
    {
      Vec3 contactA = m_contacts[0] - m_A->GetPosition();
      Vec3 contactNorm = contactA;
      contactNorm.Normalize();

      if (Math::Abs(contactNorm.Dot(m_normal)) > .99f)
      {
        contactNorm = m_normal.Cross(Vec3(1, 0, 0));
        if (contactNorm.SqLength() <= .01)
          contactNorm = m_normal.Cross(Vec3(0, 1, 0));
      }
        //determine point 2
      float bestDist = -FLT_MAX;
      unsigned p2;
      for (unsigned i = 0; i < m_storedSize; ++i)
      {
        float projection = contactNorm.Dot(m_storedContactsA[i]);
        if (projection > bestDist)
        {
          bestDist = projection;
          p2 = i;
        }      
      }

        //determine point 3
        //get segment between new point and point 2
      Vec3 segment12 = m_storedContactsA[p2] - contactA;
      Vec3 seg12Normal = segment12.Cross(m_normal);
      if (seg12Normal.Dot(contactA) > 0)
        seg12Normal = -seg12Normal;

        //now find point 3
      bestDist = -FLT_MAX;
      unsigned p3;
      for (unsigned i = 0; i < m_storedSize; ++i)
      {
        if (i == p2)
          continue;

        float projection = seg12Normal.Dot(m_storedContactsA[i] - contactA);
        if (projection > bestDist)
        {
          bestDist = projection;
          p3 = i;
        }
      }

        //determine point 4
        //reverse segment 12 normal to search outside tri
      seg12Normal = -seg12Normal;

        //segment 23
      Vec3 segment23 = m_storedContactsA[p3] - m_storedContactsA[p2];
      Vec3 seg23Normal = segment23.Cross(m_normal);
      if (seg23Normal.Dot(m_storedContactsA[p2]) < 0)
        seg23Normal = -seg23Normal;

      //segment 31
      Vec3 segment31 = contactA - m_storedContactsA[p3];
      Vec3 seg31Normal = segment31.Cross(m_normal);
      if (seg31Normal.Dot(m_storedContactsA[p3]) > 0)
        seg31Normal = -seg31Normal;

      //now find point 4
      bestDist = 0;// -FLT_MAX;
      unsigned p4 = UNSIGNED_MAX;
      for (unsigned i = 0; i < m_storedSize; ++i)
      {
        if (i == p2 || i == p3)
          continue;

        float projection = seg12Normal.Dot(m_storedContactsA[i] - contactA);
        if (projection > bestDist)
        {
          bestDist = projection;
          p4 = i;
        }

        projection = seg23Normal.Dot(m_storedContactsA[i] - m_storedContactsA[p2]);
        if (projection > bestDist)
        {
          bestDist = projection;
          p4 = i;
        }

        projection = seg31Normal.Dot(m_storedContactsA[i] - m_storedContactsA[p3]);
        if (projection > bestDist)
        {
          bestDist = projection;
          p4 = i;
        }
      }

      if (p4 == UNSIGNED_MAX)
        m_storedSize = 2;

      Vec3 tempA[4];
      Vec3 tempB[4];
      tempA[0] = m_storedContactsA[0];
      tempA[1] = m_storedContactsA[1];
      tempA[2] = m_storedContactsA[2];
      tempA[3] = m_storedContactsA[3];

      m_storedContactsA[1] = tempA[p2];
      m_storedContactsA[2] = tempA[p3];

      if(p4 != UNSIGNED_MAX)
        m_storedContactsA[3] = tempA[p4];

      tempB[0] = m_storedContactsB[0];
      tempB[1] = m_storedContactsB[1];
      tempB[2] = m_storedContactsB[2];
      tempB[3] = m_storedContactsB[3];

      m_storedContactsB[1] = tempB[p2];
      m_storedContactsB[2] = tempB[p3];

      if(p4 != UNSIGNED_MAX)
        m_storedContactsB[3] = tempB[p4];

      //m_addContact = true;
      /*float area = CalculateArea();
      if (area > m_bestArea)
      {
        m_bestArea = area;
        m_addContact = true;
      }
         
      else
      {
        m_storedContactsA[0] = tempA[0];
        m_storedContactsA[1] = tempA[1];
        m_storedContactsA[2] = tempA[2];
        m_storedContactsA[3] = tempA[3];

        m_storedContactsB[0] = tempB[0];
        m_storedContactsB[1] = tempB[1];
        m_storedContactsB[2] = tempB[2];
        m_storedContactsB[3] = tempB[3];
      }*/
    }

    float CollisionPair::CalculateArea()
    {
      return 0.0f;
    }

    void CollisionPair::Resolve(float dt, unsigned contact, unsigned iteration, unsigned maxIterations)
    {
        //contact points
      Vec3 contactA = m_storedContactsA[contact];
      Vec3 contactB = m_storedContactsB[contact];

        //jacobian terms
      Vec3 negNormal = -m_normal;
      Vec3 negContactACrossNorm = -contactA.Cross(m_normal);
      Vec3 contactBCrossNorm = contactB.Cross(m_normal);

      Mat1x12 jacobian(negNormal, negContactACrossNorm, m_normal, contactBCrossNorm);
      Vec12 velocity(m_A->GetVelocity(), m_A->GetAngVelocity(), m_B->GetVelocity(), m_B->GetAngVelocity());

        //create a fake inverse mass matrix
      Mat1x12 invMass(m_A->GetInverseMass(), m_A->GetInverseInertia(), m_B->GetInverseMass(), m_B->GetInverseInertia());

      float baumgarteBias = 0;
      if(iteration == (maxIterations - 1) && m_penContact == contact)
        baumgarteBias = m_penetration * s_baumgarte;

        //formula for deltalambda
        //deltaLambda = -(jacobian * v + baumgarte) / (jacobian * massInverse * jacobianTranspose)
      float deltaLambda = 0;

      Vec12 invMassJacobian = invMass.Mat12Mult(jacobian.Transpose());

      deltaLambda = -(jacobian * velocity + baumgarteBias);
      deltaLambda /= (jacobian * invMassJacobian);

      if(deltaLambda != deltaLambda)
        return;

      float oldLambda = m_storedLambdas[contact];

      m_storedLambdas[contact] += deltaLambda;

        //clamp stored lambda to positive values
      m_storedLambdas[contact] = Math::Min(FLT_MAX, Math::Max(0, m_storedLambdas[contact]));
       
      deltaLambda = m_storedLambdas[contact] - oldLambda;

      Vec12 deltaV = invMassJacobian * deltaLambda;
      Vec3 deltaVelA(deltaV.vec[0], deltaV.vec[1], deltaV.vec[2]);
      Vec3 deltaAngA(deltaV.vec[3], deltaV.vec[4], deltaV.vec[5]);
      Vec3 deltaVelB(deltaV.vec[6], deltaV.vec[7], deltaV.vec[8]);
      Vec3 deltaAngB(deltaV.vec[9], deltaV.vec[10], deltaV.vec[11]);

      m_A->m_velocity += deltaVelA;
      m_A->m_angularVelocity += deltaAngA;
      
      m_B->m_velocity += deltaVelB;
      m_B->m_angularVelocity += deltaAngB;

        //friction
      Vec12 frictionVelocity(m_A->GetVelocity(), m_A->GetAngVelocity(), m_B->GetVelocity(), m_B->GetAngVelocity());

        //determine friction axes
      Vec3 frictionAxis1 = m_normal.Cross(Vec3(0, 0, 1));
      if (frictionAxis1.x == 0 && frictionAxis1.y == 0 && frictionAxis1.z == 0)
        frictionAxis1 = m_normal.Cross(Vec3(0, 1, 0));
      frictionAxis1.Normalize();

      Vec3 frictionAxis2 = frictionAxis1.Cross(m_normal);
      frictionAxis2.Normalize();

        //axis1
      Vec3 negFrictionAxis1 = -frictionAxis1;
      Vec3 negContactACrossFrictionAxis1 = -contactA.Cross(frictionAxis1);
      Vec3 contactBCrossFrictionAxis1 = contactB.Cross(frictionAxis1);

      Mat1x12 frictionAxis1Jacobian(negFrictionAxis1, negContactACrossFrictionAxis1, frictionAxis1, contactBCrossFrictionAxis1);
      Vec12 invMassfrictionAxis1Jacobian = invMass.Mat12Mult(frictionAxis1Jacobian.Transpose());

      float deltaLambdaAxis1 = -(frictionAxis1Jacobian * frictionVelocity);
      deltaLambdaAxis1 /= (frictionAxis1Jacobian * invMassfrictionAxis1Jacobian);

      float oldLambdaAxis1 = m_frictionAxis1Lambda[contact];
      m_frictionAxis1Lambda[contact] += deltaLambdaAxis1;

        //clamp stored lambda to normal force
      if (Math::Abs(m_frictionAxis1Lambda[contact]) > Math::Abs(m_storedLambdas[contact]) * s_frictionCoeff)
      {
        int sign = Math::Sign(m_frictionAxis1Lambda[contact]);
        m_frictionAxis1Lambda[contact] = m_storedLambdas[contact] * s_frictionCoeff;
        m_frictionAxis1Lambda[contact] *= sign;
      }

      Vec12 deltaVAxis1 = invMassfrictionAxis1Jacobian * deltaLambdaAxis1;
      Vec3 deltaVelAAxis1(deltaVAxis1.vec[0], deltaVAxis1.vec[1], deltaVAxis1.vec[2]);
      Vec3 deltaAngAAxis1(deltaVAxis1.vec[3], deltaVAxis1.vec[4], deltaVAxis1.vec[5]);
      Vec3 deltaVelBAxis1(deltaVAxis1.vec[6], deltaVAxis1.vec[7], deltaVAxis1.vec[8]);
      Vec3 deltaAngBAxis1(deltaVAxis1.vec[9], deltaVAxis1.vec[10], deltaVAxis1.vec[11]);

      m_A->m_velocity += deltaVelAAxis1;
      m_A->m_angularVelocity += deltaAngAAxis1;

      m_B->m_velocity += deltaVelBAxis1;
      m_B->m_angularVelocity += deltaAngBAxis1;

        //axis2
      Vec3 negFrictionAxis2 = -frictionAxis2;
      Vec3 negContactACrossFrictionAxis2 = -contactA.Cross(frictionAxis2);
      Vec3 contactBCrossFrictionAxis2 = contactB.Cross(frictionAxis2);
 
      Mat1x12 frictionAxis2Jacobian(negFrictionAxis2, negContactACrossFrictionAxis2, frictionAxis2, contactBCrossFrictionAxis2);
      Vec12 invMassfrictionAxis2Jacobian = invMass.Mat12Mult(frictionAxis2Jacobian.Transpose());

      float deltaLambdaAxis2 = -(frictionAxis2Jacobian * frictionVelocity);
      deltaLambdaAxis2 /= (frictionAxis2Jacobian * invMassfrictionAxis2Jacobian);

      float oldLambdaAxis2 = m_frictionAxis2Lambda[contact];
      m_frictionAxis2Lambda[contact] += deltaLambdaAxis2;

        //clamp stored lambda to normal force
      if (Math::Abs(m_frictionAxis2Lambda[contact]) > Math::Abs(m_storedLambdas[contact]) * s_frictionCoeff)
      {
        int sign = Math::Sign(m_frictionAxis1Lambda[contact]);
        m_frictionAxis2Lambda[contact] = m_storedLambdas[contact] * s_frictionCoeff;
        m_frictionAxis2Lambda[contact] *= sign;
      }

      Vec12 deltaVAxis2 = invMassfrictionAxis2Jacobian * deltaLambdaAxis2;
      Vec3 deltaVelAAxis2(deltaVAxis2.vec[0], deltaVAxis2.vec[1], deltaVAxis2.vec[2]);
      Vec3 deltaAngAAxis2(deltaVAxis2.vec[3], deltaVAxis2.vec[4], deltaVAxis2.vec[5]);
      Vec3 deltaVelBAxis2(deltaVAxis2.vec[6], deltaVAxis2.vec[7], deltaVAxis2.vec[8]);
      Vec3 deltaAngBAxis2(deltaVAxis2.vec[9], deltaVAxis2.vec[10], deltaVAxis2.vec[11]);

      m_A->m_velocity += deltaVelAAxis2;
      m_A->m_angularVelocity += deltaAngAAxis2;
      
      m_B->m_velocity += deltaVelBAxis2;
      m_B->m_angularVelocity += deltaAngBAxis2;
    }

    void CollisionPair::PositionCorrection()
    {
      Vec3 correction = m_normal * m_penetration * s_positionCorrection;
      if (m_A->m_static || m_B->m_static)
        correction = correction * 2.0f;

      if(!m_A->m_static)
        m_A->AddToPosition(-correction);
      
      if (!m_B->m_static)
        m_B->AddToPosition(correction);
    }
  }
}