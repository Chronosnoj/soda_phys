/*****************************************************************
Filename: Raycast.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../body/BodyList.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    class Space;

    class Ray
    {
      public:
        Ray(Vec3& origin, Vec3& direction, Space* space) : m_origin(origin), m_direction(direction), m_space(space) {}

        RaycastResult CastRay();

      //private:
        bool          RayOriginContained(Box& box);
        RaycastResult RayAABB(Box& box);

        RaycastResult RaySphere(Sphere& sphere);
        RaycastResult RayBox(Box& box);
        RaycastResult RayCapsule(Capsule& capsule);

        RaycastResult SphereTest(Vec3 sphereOrigin, float sphereRadius);
        bool PlaneTest(Vec3 planePoint, Vec3 planeNormal, float& t);

        Vec3 m_origin;
        Vec3 m_direction;

        Space* m_space;
    };
  }
}