/*****************************************************************
Filename: Raycast.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "Raycast.h"
#include "../space/Space.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    RaycastResult Ray::CastRay()
    {
      RaycastResult resultFinal, result;

      //ray vs... everything
      auto itBox = m_space->GetMemManager().GetBoxList().begin();
      for (; itBox != m_space->GetMemManager().GetBoxList().end(); ++itBox)
      {
        if (itBox->GetDeleteFlag())
          continue;

          result = RayBox(*itBox);
        if (result.colliding)
        {
          if (!resultFinal.colliding)
            resultFinal = result;
          else if (resultFinal.depth > result.depth)
            resultFinal = result;
        }
      }

      auto itSphere = m_space->GetMemManager().GetSphereList().begin();
      for (; itSphere != m_space->GetMemManager().GetSphereList().end(); ++itSphere)
      {
        if (itSphere->GetDeleteFlag())
          continue;

        result = RaySphere(*itSphere);
        if (result.colliding)
        {
          if (!resultFinal.colliding)
            resultFinal = result;
          else if (resultFinal.depth > result.depth)
            resultFinal = result;
        }
      }

      auto itCapsule = m_space->GetMemManager().GetCapsuleList().begin();
      for (; itCapsule != m_space->GetMemManager().GetCapsuleList().end(); ++itCapsule)
      {
        if (itCapsule->GetDeleteFlag())
          continue;

        result = RayCapsule(*itCapsule);
        if (result.colliding)
        {
          if (!resultFinal.colliding)
            resultFinal = result;
          else if (resultFinal.depth > result.depth)
            resultFinal = result;
        }
      }

      return resultFinal;
    }

    bool Ray::RayOriginContained(Box& box)
    {
      float minX = box.GetPosition().x - box.GetDimensions().x;
      float maxX = box.GetPosition().x + box.GetDimensions().x;
      float minY = box.GetPosition().y - box.GetDimensions().y;
      float maxY = box.GetPosition().y + box.GetDimensions().y;
      float minZ = box.GetPosition().z - box.GetDimensions().z;
      float maxZ = box.GetPosition().z + box.GetDimensions().z;

      if (m_origin.x < minX || m_origin.x > maxX)
        return false;

      if (m_origin.y < minY || m_origin.y > maxY)
        return false;

      if (m_origin.z < minZ || m_origin.z > maxZ)
        return false;

      return true;
    }

    RaycastResult Ray::RayAABB(Box& aabb)
    {
      RaycastResult result;

      float minX = -aabb.GetDimensions().x;
      float maxX = aabb.GetDimensions().x;
      float minY = -aabb.GetDimensions().y;
      float maxY = aabb.GetDimensions().y;
      float minZ = -aabb.GetDimensions().z;
      float maxZ = aabb.GetDimensions().z;

      float tMin = -FLT_MAX;
      float tMax = FLT_MAX;


        //check x values
      if (m_direction.x == 0.0f)
      {
        if (m_origin.x < minX || m_origin.x > maxX)
          return result;
      }
        
      else
      {
        tMin = (minX - m_origin.x) / (m_direction.x);
        tMax = (maxX - m_origin.x) / (m_direction.x);

        if (m_direction.x < 0)
        {
          float temp = tMin;
          tMin = tMax;
          tMax = temp;
        }
      }


        //check y values
      if (m_direction.y == 0.0f)
      {
        if (m_origin.y < minY || m_origin.y > maxY)
          return result;
      }
       
      else
      {
        float tMinY = (minY - m_origin.y) / (m_direction.y);
        float tMaxY = (maxY - m_origin.y) / (m_direction.y);

        if (m_direction.y < 0)
        {
          float temp = tMinY;
          tMinY = tMaxY;
          tMaxY = temp;
        }

          //update values if min is larger and/or max is smaller
        if (tMinY > tMin)
          tMin = tMinY;

        if (tMax > tMaxY)
          tMax = tMaxY;
      }


        //check z values
      if (m_direction.z == 0.0f)
      {
        if (m_origin.z < minZ || m_origin.z > maxZ)
          return result;
      }
        
      else
      {
        float tMinZ = (minZ - m_origin.z) / (m_direction.z);
        float tMaxZ = (maxZ - m_origin.z) / (m_direction.z);

        if (m_direction.z < 0)
        {
          float temp = tMinZ;
          tMinZ = tMaxZ;
          tMaxZ = temp;
        }

          //update values if min is larger and/or max is smaller
        if (tMinZ > tMin)
          tMin = tMinZ;

        if (tMax > tMaxZ)
          tMax = tMaxZ;
      }


        //determine if ray hits
      if (tMin > tMax || (tMin < 0 && tMax < 0))
        return result;

      result.colliding = true;

        //ray starts inside
      if (tMin < 0)
        result.depth = 0;

      else
        result.depth = tMin;

      result.collisionPoint = m_origin + (m_direction * tMin);

      result.collider = aabb.GetType();
      result.objectID = aabb.GetID();

      return result;
    }

    RaycastResult Ray::RaySphere(Sphere& sphere)
    {
      RaycastResult result;
      result.colliding = false;

      result = SphereTest(sphere.GetPosition(), sphere.GetDimensions().x);
      result.worldCollisionPoint = result.collisionPoint + sphere.GetPosition();
      result.collisionPoint = sphere.m_orientation.CreateInverse() * result.collisionPoint;
      result.collider = sphere.GetType();
      result.objectID = sphere.GetID();
 
      return result;
    }

    RaycastResult Ray::RayBox(Box& box)
    {
        // move everything so that the box is at origin
      Vec3 boxPosition = box.GetPosition();
      Vec3 newRayOrigin = m_origin - boxPosition;

        // rotate everything so that the box is axis-aligned
      Mat3 inverseOrientation = box.GetOrientation();
      inverseOrientation.Invert();

      Vec3 newRayDirection = inverseOrientation * m_direction;
      newRayDirection.Normalize();

      newRayOrigin = inverseOrientation * newRayOrigin;

        // now create a new ray and new AABB and call the test on that
      Ray newRay(newRayOrigin, newRayDirection, m_space);

        // now that we have the new ray, make a new ray and do the test with AABB
      RaycastResult result = newRay.RayAABB(box);

      result.worldCollisionPoint = box.GetOrientation() * result.collisionPoint + box.GetPosition();

      return result;
    }

    RaycastResult Ray::RayCapsule(Capsule& capsule)
    {
        //in order to handle all edges cases, we will eventually perform
        //three sphere ray tests - top, bot, and somewhere in middle
        //the best t value will be the collision point
      RaycastResult result;

        //move everything so that the box is at origin
      Vec3 capsulePosition = capsule.GetPosition();
      Vec3 newRayOrigin = m_origin - capsulePosition;

        // rotate everything so that the box is axis-aligned
      Mat3 inverseOrientation = capsule.GetOrientation();
      inverseOrientation.Invert();

      Vec3 newRayDirection = inverseOrientation * m_direction;
      newRayDirection.Normalize();

      newRayOrigin = inverseOrientation * newRayOrigin;

        // now create a new ray and new AABB and call the test on that
      Ray newRay(newRayOrigin, newRayDirection, m_space);

        //sphere are top
      Vec3 centerTop(0.0f, capsule.GetDimensions().y, 0.0f);

        //sphere at bot
      Vec3 centerBot(0.0f, -capsule.GetDimensions().y, 0.0f);

        //now begin actual test with cyclider oriented in its coordinates
        //get triangle of three points, rayorigin, and two capsule segment ends
      Vec3 triTop = centerTop - newRayOrigin;
      Vec3 triBot = centerBot - newRayOrigin;

      Vec3 triNormal = triBot.Cross(triTop);

        //use the tri's normal to calculate plane normal
        //edge case where triangle is degenerate is handled in
        //RayPlane
      Vec3 planeNormal = (centerBot - centerTop).Cross(triNormal);
      planeNormal.Normalize();

        //now that we have normal, perform ray-plane test
      float t;
      if (newRay.PlaneTest(centerTop, planeNormal, t))
      {
          //now get the intersection point
         Vec3 planePoint = newRayOrigin + newRayDirection * t;

          //use this to find the closest point on the capsule, which will be used in sphere test
        Vec3 axisVector = centerTop - centerBot;
        axisVector.Normalize();
        Vec3 sphereCenter = centerBot + axisVector * axisVector.Dot(planePoint - centerBot);

          //make sure the point is not outside the segment of the capsule
        if (sphereCenter.y > centerTop.y)
          sphereCenter.y = centerTop.y;

        if (sphereCenter.y < centerBot.y)
          sphereCenter.y = centerBot.y;

          //test against the sphere we have created
        result = newRay.SphereTest(sphereCenter, capsule.GetDimensions().x);
        
        if(result.colliding)
          result.worldCollisionPoint = result.collisionPoint + sphereCenter + capsule.GetPosition();
      }

        //now make sure that the ray isn't intersecting the ends of the capsule without being
        //intersecting the plane
      if (!result.colliding)
      {
        RaycastResult topEnd = SphereTest(centerTop, capsule.GetDimensions().x);
        if (topEnd.colliding == true && topEnd.depth < result.depth)
        {
          result = topEnd;
          result.worldCollisionPoint + capsule.GetPosition() + centerTop;
        }
          

        RaycastResult botEnd = SphereTest(centerBot, capsule.GetDimensions().x);
        if (botEnd.colliding == true && botEnd.depth < result.depth)
        {
          result = botEnd;
          result.worldCollisionPoint + capsule.GetPosition() + centerBot;
        }
      }
      
      if (result.colliding)
      {
          //if the result is true, move the value back into real space
        inverseOrientation = capsule.GetOrientation();
        result.collisionPoint = inverseOrientation * result.collisionPoint;
        result.collider = capsule.GetType();
        result.objectID = capsule.GetID();
      }

      return result;
    }
    
    RaycastResult Ray::SphereTest(Vec3 sphereOrigin, float sphereRadius)
    {
      RaycastResult result;

      Vec3 originToSphere = sphereOrigin - m_origin;

      float projection = m_direction.Dot(originToSphere);

      if (projection <= 0)
        return result;

        // move the point to the same plane as the sphere
      Vec3 depthPoint = m_origin + (m_direction * projection);
      Vec3 depthVector = depthPoint - sphereOrigin;

      float depthRadiusSq = depthVector.SqLength();
      float sphereRadiusSq = sphereRadius * sphereRadius;

      if (depthRadiusSq > sphereRadiusSq)
        return result;

      float distToSurface = sphereRadiusSq - depthRadiusSq;
      distToSurface = sqrt(distToSurface);

      Vec3 firstContact = depthPoint - (m_direction * distToSurface);
      result.depth = (firstContact - m_origin).Length();

        // set the colliding flag to true
      result.colliding = true;
      result.collisionPoint = firstContact - sphereOrigin;

      return result;
    }
    
    bool Ray::PlaneTest(Vec3 planePoint, Vec3 planeNormal, float & t)
    {
      float planeDist = planeNormal.Dot(planePoint);

      float denom = planeNormal.Dot(m_direction);
      if (denom < FLT_EPSILON && denom > -FLT_EPSILON)
        return false;

      t = (planeDist - m_origin.Dot(planeNormal)) / denom;

      if (t > 0)
        return true;

      return false;
    }
  }
}