/*****************************************************************
Filename: sphere.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "sphere.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		void Sphere::Initialize(MemoryManager::BodyInfo& info)
		{
			Body::Initialize(info);

			m_radius = info.radius;

      CalculateMassInertia();
		}

		Vec3 Sphere::GetScale()
		{
			return Vec3(m_radius, m_radius, m_radius);
		}

		Vec3 Sphere::GetSupport(Vec3& direction)
		{
			return direction * m_radius;
		}

    float Sphere::GetVolume()
    {
      return (4.0f / 3.0f * static_cast<float>(PI) * m_radius * m_radius * m_radius);
    }

    void Sphere::CalculateMassInertia()
    {
      float mass = m_density * GetVolume();
      m_inverseMass = 0;

      if (m_static)
        mass = 0;

      if (mass != 0)
        m_inverseMass = 1 / mass;

      float massAdjusted = mass * 2.0f / 5.0f * m_radius * m_radius;

      m_inverseInertiaTensor.m_matrix[0][0] = massAdjusted;
      m_inverseInertiaTensor.m_matrix[1][1] = massAdjusted;
      m_inverseInertiaTensor.m_matrix[2][2] = massAdjusted;

      m_inverseInertiaTensor.Invert();
    }

    Vec3 Sphere::CalculateAABB()
    {
      return Vec3(m_radius, m_radius, m_radius);
    }

    bool Sphere::operator==(const Sphere & rhs) const
    {
      return false;
    }
	}
}