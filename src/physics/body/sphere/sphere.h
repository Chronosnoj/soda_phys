/*****************************************************************
Filename: sphere.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../body/body.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		class Sphere : public Body
		{
		public:
			Sphere() : m_radius(1.0f) {}

			virtual void  Initialize(MemoryManager::BodyInfo& info);

      Vec3          GetDimensions()          { return Vec3(m_radius, 0, 0); }
      colliderType  GetType()                { return colliderType::eSPHERE; }
      unsigned      GetID()                  { return m_id; }

      void  SetDimensions(Vec3& sphereAxes)  { m_radius = sphereAxes.x; CalculateMassInertia(); }
			void  SetID(unsigned int id)           { m_id = id; };

			Vec3  GetScale();
			Vec3  GetSupport(Vec3& direction);

      float GetVolume();
      void  CalculateMassInertia();

      Vec3  CalculateAABB();

        //serialization only
      bool operator==(const Sphere& rhs) const;

		public:
			float m_radius;

			//index id;
			unsigned int m_id;
		};
	}
}