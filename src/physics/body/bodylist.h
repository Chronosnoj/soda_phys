/*****************************************************************
Filename: bodylist.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "box/box.h"
#include "sphere/sphere.h"
#include "capsule/capsule.h"

typedef TestEngine::PhysicsGL::Box Box;
typedef TestEngine::PhysicsGL::Sphere Sphere;
typedef TestEngine::PhysicsGL::Capsule Capsule;
