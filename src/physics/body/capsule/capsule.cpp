/*****************************************************************
Filename: capsule.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "capsule.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    void Capsule::Initialize(MemoryManager::BodyInfo& info)
    {
      Body::Initialize(info);

      m_radius = info.dimensions[0];
      m_halfHeight = info.dimensions[1];
      m_offset = Vec3(0, m_halfHeight, 0);
      CalculateMassInertia();
    }

    Vec3 Capsule::GetOffset()
    {
      return Vec3(0.0f, m_halfHeight, 0.0f);
    }

    Vec3 Capsule::GetScale()
    {
      return Vec3(m_radius, m_halfHeight * 2, m_radius);
    }

    Vec3 Capsule::GetSupport(Vec3& direction)
    {
      //calculate for sphere portion
      Vec3 support = direction * m_radius;

      //calculate for cylinder segment
      float yValue;
      if (direction.y > 0)
        yValue = m_halfHeight;
      else
        yValue = -m_halfHeight;

      //combine and move into world reference frame
      support.y += yValue;

      return support;
    }

    float Capsule::GetVolume()
    {
      float volume = GetCylinderVolume();
      volume += GetSphereVolume();

      return volume;
    }

    void Capsule::CalculateMassInertia()
    {
      float density = m_density;
      
      if (m_static)
        density = 0.0;

      float cylinder = GetCylinderVolume();
      float sphere = GetSphereVolume();

      float mass = density * (cylinder + sphere);

      if (mass != 0)
        m_inverseMass = 1 / mass;
      else
        m_inverseMass = 0;

      float fullHeight = m_halfHeight * 2;

      m_inverseInertiaTensor.m_matrix[0][0] = m_inverseInertiaTensor.m_matrix[2][2] =
        cylinder * density * (fullHeight * fullHeight + m_radius * m_radius * 3) / 12
        + sphere * density * (2 * m_radius * m_radius / 5);

      m_inverseInertiaTensor.m_matrix[1][1] = cylinder * density * (m_radius * m_radius / 2) + sphere * density * (2 * m_radius * m_radius / 5);

      m_inverseInertiaTensor.Invert();
    }

    Vec3 Capsule::CalculateAABB()
    {
      Vec3 extents;

      extents.x = abs(m_orientation.m_matrix[0][0] * m_radius) 
        + abs(m_orientation.m_matrix[0][1] * (m_radius + m_halfHeight))
        + abs(m_orientation.m_matrix[0][2] * m_radius);
      extents.y = abs(m_orientation.m_matrix[1][0] * m_radius)
        + abs(m_orientation.m_matrix[1][1] * (m_radius + m_halfHeight))
        + abs(m_orientation.m_matrix[1][2] * m_radius);
      extents.z = abs(m_orientation.m_matrix[2][0] * m_radius)
        + abs(m_orientation.m_matrix[2][1] * (m_radius + m_halfHeight))
        + abs(m_orientation.m_matrix[2][2] * m_radius);

      return extents;
    }

    Vec3 Capsule::CalculateConstraintPoint(Vec3 & direction)
    {
      Vec3 support = GetSupport(direction);

      Vec3 trialPoint = Vec3(0, support.y, 0);

      float testDot = trialPoint.Dot(direction);

      float bestDot = testDot;
      Vec3 bestPoint = trialPoint;

      trialPoint = Vec3(support.x, 0, support.z);
      testDot = trialPoint.Dot(direction);
      if (testDot > bestDot)
        bestPoint = trialPoint;

      return bestPoint;
    }

    bool Capsule::operator==(const Capsule & rhs) const
    {
      return false;
    }

    float Capsule::GetCylinderVolume()
    {
      return static_cast<float>(PI * m_radius * m_radius * 2 * m_halfHeight);
    }

    float Capsule::GetSphereVolume()
    {
      return static_cast<float>(4.0f / 3.0f * PI * m_radius * m_radius * m_radius);
    }
  }
}