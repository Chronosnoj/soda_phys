/*****************************************************************
Filename: capsule.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../body/body.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    class Capsule : public Body
    {
    public:
      Capsule() : m_radius(1.0f), m_halfHeight(1.0f) {}

      virtual void Initialize(MemoryManager::BodyInfo& info);
      virtual Vec3 GetOffset();

      Vec3         GetDimensions()               { return Vec3(m_radius, m_halfHeight, 0); }
      colliderType GetType()                     { return colliderType::eCAPSULE; }
      unsigned     GetID()                       { return m_id; }

      void  SetDimensions(Vec3& capsulehalfAxes) { m_radius = capsulehalfAxes.x; m_halfHeight =  capsulehalfAxes.y; CalculateMassInertia(); }
      void  SetID(unsigned int id)               { m_id = id; };

      Vec3  GetScale();
      Vec3  GetSupport(Vec3& direction);

      float GetVolume();
      void  CalculateMassInertia();

      Vec3  CalculateAABB();

      Vec3  CalculateConstraintPoint(Vec3& direction);

        //serialization only
      bool operator==(const Capsule& rhs) const;

    public:
      float GetCylinderVolume();
      float GetSphereVolume();


      float m_radius;
      float m_halfHeight;

      //index id;
      unsigned int m_id;
    };
  }
}