/*****************************************************************
Filename: body.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "body.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
    static const float s_gravity = -9.8f;
    static const float s_maxSpeed = 10000.0f;

		void Body::Initialize(MemoryManager::BodyInfo& info)
		{
			m_position = info.position;
			m_orientation = info.orientation;

			m_velocity = info.velocity;
			m_angularVelocity = info.angularVelocity;

      m_density = info.density;
      m_inverseMass = 1.0f;

			  //bools for handling special cases
			m_rotationLock = info.rotationLock;
			m_static = info.staticObj;

			m_collision = false;
			m_active = false;
      m_delete = info.deleted;
		}

    void Body::FlipStatic()
    {
      m_static = !m_static;
      CalculateMassInertia();

      SetVelocity(Vec3());
      SetAngVelocity(Vec3());
    }

    Vec3 Body::GetEulerAngles()
    {
      Vec3 euler;
      
      euler.x = atan2(m_orientation.m_matrix[1][2], m_orientation.m_matrix[2][2]);

      float c = sqrt((m_orientation.m_matrix[0][0] * m_orientation.m_matrix[0][0]) 
        + (m_orientation.m_matrix[0][1] * m_orientation.m_matrix[0][1]));

      euler.y = atan2(-m_orientation.m_matrix[0][2], c);

      float s1 = sinf(euler.x);
      float c1 = cosf(euler.x);

      euler.z = atan2(s1 * m_orientation.m_matrix[2][0] - c1 * m_orientation.m_matrix[1][0],
        c1 * m_orientation.m_matrix[1][1] - s1 * m_orientation.m_matrix[2][1]);

      euler.x = euler.x * -180.0f / static_cast<float>(PI);
      euler.y = euler.y * -180.0f / static_cast<float>(PI);
      euler.z = euler.z * -180.0f / static_cast<float>(PI);

      return euler;
    }

    void Body::AddToPosition(Vec3 & move)
		{
			m_position += move;
		}

    void Body::AddToRotation(float radians, Vec3& axis)
    {
      Mat3 rot = Mat3(radians, axis);
      m_orientation = rot * m_orientation;
    }

		void Body::Movement(float dt)
		{
      if (m_delete)
        return;

      if(!m_static)
			  m_velocity.y += dt * s_gravity;

      if (m_velocity.SqLength() > s_maxSpeed || m_velocity.x != m_velocity.x)
        m_velocity = Vec3(0, 0, 0);

			m_position += m_velocity * dt;

			if (!m_rotationLock)
			{
				Vec3 axis = m_angularVelocity;
				axis.Normalize();
				float angle = m_angularVelocity.Length() * dt;
				m_orientation = Mat3(angle, axis) * m_orientation;
			}

        //numerical damping
      if (m_drag)
      {
        m_velocity = m_velocity * m_dragAmount;
        m_angularVelocity = m_angularVelocity * m_dragAmount;
      }
      
		}
	}
}
