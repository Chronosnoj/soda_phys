/*****************************************************************
Filename: body.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../../meminterface/message.h"


/* New Body CheckList:
	1) Add new collider class
		- Create support point function

	2) Physics space
		- Add collider type to movement
		- Add collider type to collision detection and resolution

	3) Add body to memory manager 
		- Create new type with custom list 
		- Add<Body> function

	3) Graphics
		- Add mesh
		- Add mem lists to model creation, as well as vector drawing

  4) Add Serialization Information
*/

namespace TestEngine
{
  namespace PhysicsGL
  {

    class Body
    {
      public:
        Body() {};

        virtual ~Body() {};

        virtual void  Initialize(MemoryManager::BodyInfo& info);

		    virtual Vec3  GetPosition()			    { return m_position; }
        virtual Vec3  GetVelocity()			    { return m_velocity; }
        virtual Vec3  GetAngVelocity()		  { return m_angularVelocity; }
        virtual Vec3  GetOffset()           { return m_offset; }
        virtual Mat3  GetOrientation()		  { return m_orientation; }
        virtual Mat3  GetInverseInertia()	  { return m_inverseInertiaTensor; }

		    virtual bool  GetCollision()			  { return m_collision; }
		    virtual bool  GetActive()			      { return m_active; }
        virtual bool  GetStatic()           { return m_static; }
        virtual bool  GetRotationLock()     { return m_rotationLock; }
        virtual bool  GetDragFlag()         { return m_drag; }
        virtual bool  GetDeleteFlag()       { return m_delete; }

		    virtual float GetDensity()			    { return m_density; }
        virtual float GetInverseMass()      { return m_inverseMass; }
        virtual float GetDragAmount()       { return m_dragAmount; }

        
        virtual void  SetVelocity(Vec3& velocity)		      { m_velocity = velocity; }
        virtual void  SetAngVelocity(Vec3& angVeloc)		  { m_angularVelocity = angVeloc; }
		    virtual void  SetPosition(Vec3& position)		      { m_position = position; }
		    virtual void  SetOrientation(Mat3& orientation)   { m_orientation = orientation; }

        virtual void  SetCollision(bool collision)        { m_collision = collision; }
        virtual void  SetActive(bool active)              { m_active = active; }
        virtual void  SetDeletion(bool mark)              { m_delete = mark; }

        virtual void  SetDensity(float density)           { m_density = density; }
        virtual void  SetDragAmount(float amount)         { m_dragAmount = amount; }

        virtual void  SetDragFlag()                       { m_drag = !m_drag; }
        virtual void  FlipStatic();

        virtual Vec3  GetEulerAngles();

		    virtual void  AddToPosition(Vec3& move);
        virtual void  AddToRotation(float radians, Vec3& axis);
        virtual void  Movement(float dt);

          //pure virtual functions
        virtual Vec3          GetDimensions() = 0;
        virtual colliderType  GetType() = 0;

        virtual void          SetDimensions(Vec3& dimensions) = 0;
		    
        virtual void          SetID(unsigned int id) = 0;
        virtual unsigned      GetID() = 0;
        virtual void          SetAabbID(unsigned aabbID)  { m_aabbID = aabbID; }
        virtual unsigned      GetAabbId()                 { return m_aabbID; }

          //collider specific functions
		    virtual Vec3          GetScale() = 0;
        virtual float         GetVolume() = 0;
		    virtual void          CalculateMassInertia() = 0;

		      //takes the normalized direction in the body's ref frame and 
		      //returns the support in the same reference frame
		    virtual Vec3          GetSupport(Vec3& direction) = 0;

          //recalculate aabb
        virtual Vec3          CalculateAABB() = 0;

          //uses support point except in case of box
        virtual Vec3          CalculateConstraintPoint(Vec3& direction) { return GetSupport(direction); }

      public:
          //general info
        Vec3    m_position;
        Mat3    m_orientation;
        Vec3    m_offset;

          //rigid body info
        Vec3    m_velocity;
        Vec3    m_angularVelocity;

          //mass information
        float   m_density;
        float   m_inverseMass;
        Mat3    m_inverseInertiaTensor;

          //flags
        bool    m_static;
        bool    m_rotationLock;
		    bool	  m_resolve;
        bool    m_drag = false;
        bool    m_delete = false;

          //other info
        float   m_dragAmount = .998f;

		      //collision last frame
		      //for debug
		    bool	  m_collision;
		    bool    m_active;

          //aabb id for broad phase - currently not active
        unsigned m_aabbID;

    };
  }
}
