/*****************************************************************
Filename: box.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "box.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		void Box::Initialize(MemoryManager::BodyInfo& info)
		{
			Body::Initialize(info);
			
      m_boxHalfAxes.x = info.dimensions[0];
      m_boxHalfAxes.y = info.dimensions[1];
      m_boxHalfAxes.z = info.dimensions[2];
 
      CalculateMassInertia();
		}

		Vec3 Box::GetScale()
		{
			return Vec3(m_boxHalfAxes.x, m_boxHalfAxes.y, m_boxHalfAxes.z);
		}

		Vec3 Box::GetSupport(Vec3& direction)
		{
			Vec3 support = m_boxHalfAxes;

			if (direction.x < 0)
				support.x *= -1;

			if (direction.y < 0)
				support.y *= -1;

			if (direction.z < 0)
				support.z *= -1;

			return support;
		}

    float Box::GetVolume()
    {
      Vec3 fullAxes = m_boxHalfAxes;
      fullAxes = fullAxes * 2;
      return fullAxes.x * fullAxes.y * fullAxes.z;
    }

    void Box::CalculateMassInertia()
		{
      float mass = m_density * GetVolume();
      m_inverseMass = 0;
      
      if (m_static)
        mass = 0;

      if (mass != 0)
        m_inverseMass = 1 / mass;
      else
        m_inverseMass = mass;

      float massAdjusted = mass * 1.0f / 12.0f;

      Vec3 fullAxes = m_boxHalfAxes;
      fullAxes = fullAxes * 2;

      m_inverseInertiaTensor.m_matrix[0][0] = massAdjusted * (fullAxes.y * fullAxes.y
        + fullAxes.z * fullAxes.z);
      m_inverseInertiaTensor.m_matrix[1][1] = massAdjusted * (fullAxes.x * fullAxes.x
        + fullAxes.z * fullAxes.z);
      m_inverseInertiaTensor.m_matrix[2][2] = massAdjusted * (fullAxes.x * fullAxes.x
        + fullAxes.y * fullAxes.y);

      m_inverseInertiaTensor.Invert();
		}

    Vec3 Box::CalculateAABB()
    {
      Vec3 extents;

      extents.x = abs(m_orientation.m_matrix[0][0] * m_boxHalfAxes.x)
        + abs(m_orientation.m_matrix[0][1] * m_boxHalfAxes.y)
        + abs(m_orientation.m_matrix[0][2] * m_boxHalfAxes.z);
      extents.y = abs(m_orientation.m_matrix[1][0] * m_boxHalfAxes.x)
        + abs(m_orientation.m_matrix[1][1] * m_boxHalfAxes.y)
        + abs(m_orientation.m_matrix[1][2] * m_boxHalfAxes.z);
      extents.z = abs(m_orientation.m_matrix[2][0] * m_boxHalfAxes.x)
        + abs(m_orientation.m_matrix[2][1] * m_boxHalfAxes.y)
        + abs(m_orientation.m_matrix[2][2] * m_boxHalfAxes.z);

      return extents;
    }

    Vec3 Box::CalculateConstraintPoint(Vec3 & direction)
    {
      Vec3 support = GetSupport(direction);

      Vec3 trialPoint = Vec3(support.x, 0, 0);

      float testDot = trialPoint.Dot(direction);

      float bestDot = testDot;
      Vec3 bestPoint = trialPoint;

      trialPoint = Vec3(0, 0, support.z);
      testDot = trialPoint.Dot(direction);
      if (testDot > bestDot)
      {
        bestDot = testDot;
        bestPoint = trialPoint;
      }
      
      trialPoint = Vec3(0, support.y, 0);
      testDot = trialPoint.Dot(direction);
      if (testDot > bestDot)
        bestPoint = trialPoint;

      return bestPoint;
    }


    bool Box::operator==(const Box & rhs) const
    {
      return false;
    }
	}
}