/*****************************************************************
Filename: box.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../body/body.h"

namespace TestEngine
{
	namespace PhysicsGL
	{
		class Box : public Body
		{
			public:
				Box() : m_boxHalfAxes(Vec3(1.0f,1.0f,1.0f)) {}

				virtual void  Initialize(MemoryManager::BodyInfo& info);

        Vec3          GetDimensions()         { return m_boxHalfAxes; }
        colliderType  GetType()               { return colliderType::eBOX; }
        unsigned      GetID()                 { return m_id; }

        void  SetDimensions(Vec3& boxHalfAxes)  { m_boxHalfAxes = boxHalfAxes; CalculateMassInertia(); }
				void  SetID(unsigned int id)			      { m_id = id; };

				Vec3  GetScale();
				Vec3  GetSupport(Vec3& direction);

        float GetVolume();
				void  CalculateMassInertia();

        Vec3  CalculateAABB();

        Vec3  CalculateConstraintPoint(Vec3& direction);

          //for serialization
        bool operator==(const Box& rhs) const;

			public:
				Vec3 m_boxHalfAxes;

				//index id;
				unsigned int m_id;
		};
	}
}