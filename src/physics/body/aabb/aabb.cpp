/*****************************************************************
Filename: aabb.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "AABB.h"


namespace TestEngine
{
  namespace PhysicsGL
  {
    void Aabb::Initialize(MemoryManager::BodyInfo& info, Vec3& calculatedAxes, unsigned id)
    {
      m_halfAxes = calculatedAxes;

      m_position = info.position;
      m_colliderType = info.collider;

      m_colliderID = id;
    }

    Vec3 Aabb::GetScale()
    {
      return Vec3(m_halfAxes.x, m_halfAxes.y, m_halfAxes.z);
    }

    Vec3 Aabb::GetSupport(Vec3& direction)
    {
      Vec3 support = m_halfAxes;

      if (direction.x < 0)
        support.x *= -1;

      if (direction.y < 0)
        support.y *= -1;

      if (direction.z < 0)
        support.z *= -1;

      return support;
    }

    float Aabb::GetMinMax(MinMax type, Axis axis)
    {
      switch (type)
      {
        case eMIN:
          return GetMin(axis);

        case eMAX:
          return GetMax(axis);
      }

      return 0.0f;
    }

    float Aabb::GetMax(Axis axis)
    { 
      if (m_delete) 
        return FLT_MAX - m_counter * s_posOffset;
      
      return m_position.vec[axis] + m_halfAxes.vec[axis]; 
    }

    float Aabb::GetMin(Axis axis)
    { 
      if (m_delete)
        return FLT_MAX - m_counter * s_posOffset;

      return m_position.vec[axis] - m_halfAxes.vec[axis]; 
    }
  }
}