/*****************************************************************
Filename: aabb.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../../meminterface/message.h"
#include "../BodyList.h"

namespace TestEngine
{
  namespace PhysicsGL
  {
    enum Axis
    {
      eXAXIS,
      eYAXIS,
      eZAXIS,
      eAXES
    };

    enum MinMax
    {
      eMIN = 0,
      eMAX = 1
    };

    class Aabb
    {
      public:
        Aabb() : m_halfAxes(Vec3(1.0f, 1.0f, 1.0f)) {}

        virtual ~Aabb() {};

        void  Initialize(MemoryManager::BodyInfo& info, Vec3& calculatedAxes, unsigned id);

        Vec3          GetPosition()         { return m_position; }
        Vec3          GetDimensions()       { return m_halfAxes; }
        Mat3          GetOrientation()      { return Mat3(); }
        colliderType  GetType()             { return m_colliderType; }
        unsigned      GetID()               { return m_id; }

        void  SetPosition(Vec3& position)   { m_position = position; }
        void  SetDimensions(Vec3& halfAxes) { m_halfAxes = halfAxes; }
        void  SetID(unsigned int id)        { m_id = id; };
        void  SetStatic(bool staticObj)     { m_static = staticObj; }
        void  SetDeletion(bool mark)        { m_delete = mark; }
        void  SetCounter(unsigned counter)  { m_counter = counter; }

        Vec3  GetScale();
        Vec3  GetSupport(Vec3& direction);

          //values for sort and sweep
        float GetMinMax(MinMax type, Axis axis);
        float GetMax(Axis axis);
        float GetMin(Axis axis);

        bool  GetStatic()                   { return m_static; }

        colliderType  GetColliderType()     { return m_colliderType; }
        unsigned      GetColliderID()       { return m_colliderID; }

      public:
          //general info
        Vec3          m_position;
        Vec3          m_halfAxes;

          //index id for AABB;
        unsigned int  m_id;

          //collider and id for identification of parent object
        colliderType  m_colliderType;
        unsigned      m_colliderID;

        bool          m_static;

          //deletion
        bool          m_delete = false;
        unsigned      m_counter;  //per aabb, only set when deleted

          //for deletions, moves deleted objects away from each other
          //assumes objects less than this size on an axis
        const static unsigned     s_posOffset = 100000;
    };
  }
}