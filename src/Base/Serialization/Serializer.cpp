/*****************************************************************
Filename: Serializer.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "Serializer.h"
#include "../Manager/Manager.h"

namespace TestEngine
{
  namespace Base
  {
    static const int s_paddingDefault = 2;

    void Serializer::Serialize(File* file, const Variable& var)
    {
      //get the object and write the name
      const TypeInfo* info = var.GetTypeInfo();
      file->Write("%s\n", info->Name());

      //prepare to write members

      Padding(file, m_paddingLevel);
      m_paddingLevel += s_paddingDefault;
      file->Write("{\n");

      //write all members
      for (unsigned i = 0; i < (info->Members()).size(); ++i)
      {
        const Member* member = &(info->Members())[i];
        Padding(file, m_paddingLevel);

        const TypeInfo* memberTypeInfo = member->Type();
        file->Write("%s ", member->Name());

        void* offset = static_cast<char*>(var.GetData()) + member->Offset();
        memberTypeInfo->Serialize(file, Variable(memberTypeInfo, offset));
      }

      m_paddingLevel -= s_paddingDefault;
      Padding(file, m_paddingLevel);
      file->Write("}\n");    
    }

    void Serializer::SerializeStart(File * file, const Variable & var)
    {
      var.GetTypeInfo()->Serialize(file, var);
    }

    void Serializer::DeserializeStart(File * file, Variable& var)
    {
      var.GetTypeInfo()->Deserialize(file, var);
    }

    void Serializer::Deserialize(File* file, Variable& var)
    {
      const TypeInfo* info = PeekType(file);

        //throw an error
      if (!info)
        return;

      unsigned startLevel = m_paddingLevel;

      do
      {
        const Member* pMember = PeekMember(file, info->Members(), startLevel);

        if (pMember)
        {
          Variable member(pMember->Type(), static_cast<char*>(var.GetData()) + pMember->Offset());
          pMember->Type()->Deserialize(file, member);
        }

      } while (m_paddingLevel > startLevel);
    }


    void Serializer::Padding(File* file, unsigned int count)
    {
      for (unsigned i = 0; i < count; ++i)
        file->Write(" ");
    }

    const TypeInfo* Serializer::PeekType(File* file, unsigned stoplevel)
    {
      while (true)
      {
        file->Read("%s", m_nameBuffer);
        if (feof(file->fp))
          return nullptr;

        else if (strcmp(m_nameBuffer, "{") == 0)
        {
          m_paddingLevel += s_paddingDefault;
          continue;
        }

        else if (strcmp(m_nameBuffer, "}") == 0)
        {
          m_paddingLevel -= s_paddingDefault;
          if (m_paddingLevel == stoplevel)
            return nullptr;
          continue;
        }

          //we have something, let's try to read it
        else
          break;
      }

      return Manager::Get()->GetType(m_nameBuffer);
      
    }

    const Member* Serializer::PeekMember(File* file, const std::vector<Member>& members, int startLevel)
    {
      while (true)
      {
        file->Read("%s", m_nameBuffer);
        if (feof(file->fp))
          return nullptr;

        else if (strcmp(m_nameBuffer, "{") == 0)
        {
          m_paddingLevel += s_paddingDefault;
          continue;
        }

        else if (strcmp(m_nameBuffer, "}") == 0)
        {
          m_paddingLevel -= s_paddingDefault;
          if (m_paddingLevel == startLevel)
            return nullptr;

          continue;
        }

        else
          break;
      }

      for (unsigned i = 0; i < members.size(); ++i)
      {
        const Member* pMember = &(members.front()) + i;

        if (strcmp(pMember->Name(), m_nameBuffer) == 0)
          return pMember;
      }

      return nullptr;
    }

    unsigned int& Serializer::GetPadLevel()
    {
      return m_paddingLevel;
    }
  }
}