/*****************************************************************
Filename: File.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "File.h"
#include <stdarg.h>

namespace TestEngine
{
  namespace Base
  {
    File::File()
    {
      fp = nullptr;
    }
    File::File(FILE * file)
    {
      fp = file;
    }
    File::~File()
    {
      Close();
    }
    
    File* File::Open(const char * name, FileAccess type)
    {
      switch (type)
      {
        case FileAccess::Read:
          fp = fopen(name, "r");
          break;
        case FileAccess::Write:
          fp = fopen(name, "w");
          break;
        case FileAccess::Append:
          fp = fopen(name, "a");
          break;
      }

      fileName = name;
      return this;
    }
    
    void File::Close()
    {
      if (fp != nullptr && fp != stdout)
        fclose(fp);
    }
    
    bool File::IsOpen()
    {
      if (fp)
        return true;
      return false;
    }

    void File::Write(const char * format, ...)
    {
      va_list args;
      va_start(args, format);
      int success = vfprintf(fp, format, args);
      va_end(args);
    }
    
    void File::Read(const char * format, ...)
    {
      va_list args;
      va_start(args, format);
      int success = vfscanf(fp, format, args);
      va_end(args);
    }
    
    std::string File::GetLine(char delim)
    {
      std::string temp;
      char c;

        //use '/n' for line breaks
      while (true)
      {
        c = static_cast<char>(fgetc(fp));

        if (feof(fp))
          break;
        else if (c == delim)
          break;

        temp += c;
      }

      return std::move(temp);
    }
  }

}