/*****************************************************************
Filename: SerializePOD.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../Type/TypeInfo.h"
#include "../File.h"

namespace TestEngine
{
  namespace Base
  {
    template<typename T>
    void SerializePOD(File* file, const Variable& var)
    {

    }

    template<>
    void SerializePOD<unsigned>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      file->Write("%u\n", var.GetValue<unsigned>());
    }

    template<>
    void SerializePOD<int>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      file->Write("%d\n", var.GetValue<int>());
    }

    template<>
    void SerializePOD<float>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      file->Write("%f\n", var.GetValue<float>());
    }

    template<>
    void SerializePOD<double>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      file->Write("%lf\n", var.GetValue<double>());
    }

    template<>
    void SerializePOD<bool>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      if (var.GetValue<bool>())
        file->Write("%s\n", "true");
      else
        file->Write("%s\n", "false");
    }

    template<>
    void SerializePOD<char>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      file->Write("%c\n", var.GetValue<char>());
    }

    template<>
    void SerializePOD<char*>(File* file, const Variable& var)
    {
      file->Write("%s ", var.GetTypeInfo()->Name());
      file->Write("%s ", *(char**)var.GetData());
    }
  }
}