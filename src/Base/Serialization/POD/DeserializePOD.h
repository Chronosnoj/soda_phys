/*****************************************************************
Filename: DeserializePOD.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../Type/TypeInfo.h"
#include "../File.h"

namespace TestEngine
{
  namespace Base
  {
    template<typename T>
    void DeserializePOD(File* file, Variable& var)
    {

    }

    template<>
    void DeserializePOD<unsigned>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      file->Read("%u", var.GetData());
    }

    template<>
    void DeserializePOD<int>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      file->Read("%i", var.GetData());
    }

    template<>
    void DeserializePOD<float>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      file->Read("%f", var.GetData());
    }

    template<>
    void DeserializePOD<double>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      file->Read("%lf", var.GetData());
    }

    template<>
    void DeserializePOD<bool>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      char value[16];
      file->Read("%s", value);
      if (strcmp(value, "true") == 0)
        var.SetData<bool>(true);
      else
        var.SetData<bool>(false);
    }

    template<>
    void DeserializePOD<char>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      file->Read("%c", var.GetData());
    }

    template<>
    void DeserializePOD<char*>(File* file, Variable& var)
    {
      Serializer::Get()->PeekType(file, Serializer::Get()->GetPadLevel());
      //add logic for this;
    }
  }
}
