/*****************************************************************
Filename: Serializer.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "File.h"
#include "../Type/TypeInfo.h"

namespace TestEngine
{
  namespace Base
  {
    class Serializer
    {
      public:
        static Serializer* Get(void)
        {
          static Serializer instance;
          return &instance;
        }

        void  Serialize(File* file, const Variable& var);
        void  SerializeStart(File* file, const Variable& var);
        void  Deserialize(File* file, Variable& var);
        void  DeserializeStart(File* file, Variable& var);

        void  Padding(File* file, unsigned int count);

        const TypeInfo* PeekType(File* file, unsigned stoplevel = 0);

        const Member*   PeekMember(File* file, const std::vector<Member>& vec, int startLevel);

        unsigned int&  GetPadLevel();

      private:
        unsigned  m_paddingLevel;
        void*     m_userData;

        char      m_nameBuffer[256];   //used exclusively to hold names for PeekType

        Serializer(): m_paddingLevel(0)  {}
        ~Serializer() {}

        friend class Factory;

    };

  }

}