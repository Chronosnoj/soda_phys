/*****************************************************************
Filename: SerializeSpace.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../../Type/TypeInfo.h"
#include "../File.h"
#include "../../../physics/space/Space.h"
#include "../../Manager/Manager.h"


//these are specialized functions designed specifically for the included physics engine
namespace TestEngine
{
  namespace Base
  {
    ConstraintInfo ConvertConstraint(PhysicsGL::ConstraintPair constraint)
    {
      ConstraintInfo info;
      info.createBodies = false;
      info.idA = constraint.m_idA;
      info.idB = constraint.m_idB;
      info.typeA = constraint.m_typeA;
      info.typeB = constraint.m_typeB;
      info.worldSpaceConnectionPoint[0] = constraint.m_contacts[0];
      info.worldSpaceConnectionPoint[1] = constraint.m_contacts[1];
      info.connectionPointA = constraint.m_localContacts[0];
      info.connectionPointB = constraint.m_localContacts[1];
      info.distance = sqrt(constraint.m_sqDistance);
      info.stiffness = constraint.m_springStiffness;
      info.damping = constraint.m_dampingConstant;
      info.rotateA = constraint.m_lockBodyA;
      info.rotateB = constraint.m_lockBodyB;
      info.type = constraint.m_constraintType;
      info.deleted = constraint.m_deleted;
      return info;
    }

    void SerializeSpace(File* file, PhysicsGL::Space* space)
    {
      std::string spaceStr = "Space\n";
      std::string boxStr = "Boxes\n";
      std::string sphereStr = "Spheres\n";
      std::string capsuleStr = "Capsules\n";
      std::string constraintStr = "Constraints\n";
      std::string lbrack = "{\n";
      std::string rbrack = "}\n";
      file->Write("%s", spaceStr.c_str());
      file->Write("%f\n", space->GetDT());
      file->Write("%s", boxStr.c_str());
      
      
      Manager* manager = Manager::Get();
      TestEngine::Base::Serializer::Get()->GetPadLevel() += 2;
        
      //do boxes
      auto itBox = space->GetMemManager().GetBoxList();
      file->Write("%u\n", itBox.size());
      file->Write("%s", lbrack.c_str());
      
      for (unsigned i = 0; i != itBox.size(); ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("Box")), manager->GetType(std::string("Box"))->New());
        var.SetData<TestEngine::PhysicsGL::Box>(itBox[i]);
        TestEngine::Base::Serializer::Get()->SerializeStart(file, var);
      }

      file->Write("%s", rbrack.c_str());

        //then spheres
      file->Write("%s", sphereStr.c_str());
      auto itSphere = space->GetMemManager().GetSphereList();
      file->Write("%u\n", itSphere.size());
      file->Write("%s", lbrack.c_str());
      
      for (unsigned i = 0; i != itSphere.size(); ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("Sphere")), manager->GetType(std::string("Sphere"))->New());
        var.SetData<TestEngine::PhysicsGL::Sphere>(itSphere[i]);
        TestEngine::Base::Serializer::Get()->SerializeStart(file, var);
      }

      file->Write("%s", rbrack.c_str());


        //then capsules
      file->Write("%s", capsuleStr.c_str());
      auto itCapsule = space->GetMemManager().GetCapsuleList();
      file->Write("%u\n", itCapsule.size());
      file->Write("%s", lbrack.c_str());

      for (unsigned i = 0; i != itCapsule.size(); ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("Capsule")), manager->GetType(std::string("Capsule"))->New());
        var.SetData<TestEngine::PhysicsGL::Capsule>(itCapsule[i]);
        TestEngine::Base::Serializer::Get()->SerializeStart(file, var);
      }

      file->Write("%s", rbrack.c_str());

      //then constraints
      file->Write("%s", constraintStr.c_str());
      auto itConstraint = space->GetMemManager().GetConstraintList();
      file->Write("%u\n", itConstraint.size());
      file->Write("%s", lbrack.c_str());

      for (unsigned i = 0; i != itConstraint.size(); ++i)
      {
        ConstraintInfo info = ConvertConstraint(itConstraint[i]);
        TestEngine::Base::Variable var(manager->GetType(std::string("ConstraintInfo")), manager->GetType(std::string("ConstraintInfo"))->New());
        var.SetData<TestEngine::MemoryManager::ConstraintInfo>(info);
        TestEngine::Base::Serializer::Get()->SerializeStart(file, var);
      }

      file->Write("%s", rbrack.c_str());

      TestEngine::Base::Serializer::Get()->GetPadLevel() -= 2;
    }

    void DeserializeBoxes(File* file, PhysicsGL::Space* space)
    {
      Manager* manager = Manager::Get();
      char value[16];
      unsigned size = 0;
      file->Read("%s", value);
      if (strcmp(value, "Boxes") != 0)
        return;

      file->Read("%s", value);
      size = static_cast<unsigned>(atoi(value));

      file->Read("%s", value);
      if (strcmp(value, "{") != 0)
        return;

      for (unsigned i = 0; i < size; ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("Box")), manager->GetType(std::string("Box"))->New());
        TestEngine::Base::Serializer::Get()->DeserializeStart(file, var);
        TestEngine::PhysicsGL::Box* box = static_cast<TestEngine::PhysicsGL::Box*>(var.GetData());
        BodyInfo info;
        info.collider = TestEngine::colliderType::eBOX;
        info.density = box->m_density;
        info.dimensions[0] = box->m_boxHalfAxes.x;
        info.dimensions[1] = box->m_boxHalfAxes.y;
        info.dimensions[2] = box->m_boxHalfAxes.z;
        info.velocity = box->m_velocity;
        info.angularVelocity = box->m_angularVelocity;
        info.ghost = false;
        info.offset = box->m_offset;
        info.orientation = box->m_orientation;
        info.position = box->m_position;
        info.rotationLock = box->m_rotationLock;
        info.staticObj = box->m_static;
        info.deleted = box->m_delete;
        space->SetBodyInfo(info);
        space->AddBody();
      }

      file->Read("%s", value);
      if (strcmp(value, "}") == 0)
        return;
    }


    void DeserializeSpheres(File* file, PhysicsGL::Space* space)
    {
      Manager* manager = Manager::Get();
      char value[16];
      unsigned size = 0;
      file->Read("%s", value);
      if (strcmp(value, "Spheres") != 0)
        return;

      file->Read("%s", value);
      size = static_cast<unsigned>(atoi(value));

      file->Read("%s", value);
      if (strcmp(value, "{") != 0)
        return;

      for (unsigned i = 0; i < size; ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("Sphere")), manager->GetType(std::string("Sphere"))->New());
        TestEngine::Base::Serializer::Get()->DeserializeStart(file, var);
        TestEngine::PhysicsGL::Sphere* sphere = static_cast<TestEngine::PhysicsGL::Sphere*>(var.GetData());
        BodyInfo info;
        info.collider = TestEngine::colliderType::eSPHERE;
        info.density = sphere->m_density;
        info.dimensions[0] = sphere->m_radius;
        info.dimensions[1] = 0;
        info.dimensions[2] = 0;
        info.velocity = sphere->m_velocity;
        info.angularVelocity = sphere->m_angularVelocity;
        info.ghost = false;
        info.offset = sphere->m_offset;
        info.orientation = sphere->m_orientation;
        info.position = sphere->m_position;
        info.rotationLock = sphere->m_rotationLock;
        info.staticObj = sphere->m_static;
        info.deleted = sphere->m_delete;
        space->SetBodyInfo(info);
        space->AddBody();
      }

      file->Read("%s", value);
      if (strcmp(value, "}") == 0)
        return;
    }

    void DeserializeCapsules(File* file, PhysicsGL::Space* space)
    {
      Manager* manager = Manager::Get();
      char value[16];
      unsigned size = 0;
      file->Read("%s", value);
      if (strcmp(value, "Capsules") != 0)
        return;

      file->Read("%s", value);
      size = static_cast<unsigned>(atoi(value));

      file->Read("%s", value);
      if (strcmp(value, "{") != 0)
        return;

      for (unsigned i = 0; i < size; ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("Capsule")), manager->GetType(std::string("Capsule"))->New());
        TestEngine::Base::Serializer::Get()->DeserializeStart(file, var);
        TestEngine::PhysicsGL::Capsule* capsule = static_cast<TestEngine::PhysicsGL::Capsule*>(var.GetData());
        BodyInfo info;
        info.collider = TestEngine::colliderType::eCAPSULE;
        info.density = capsule->m_density;
        info.dimensions[0] = capsule->m_radius;
        info.dimensions[1] = capsule->m_halfHeight;
        info.dimensions[2] = 0;
        info.velocity = capsule->m_velocity;
        info.angularVelocity = capsule->m_angularVelocity;
        info.ghost = false;
        info.offset = capsule->m_offset;
        info.orientation = capsule->m_orientation;
        info.position = capsule->m_position;
        info.rotationLock = capsule->m_rotationLock;
        info.staticObj = capsule->m_static;
        info.deleted = capsule->m_delete;
        space->SetBodyInfo(info);
        space->AddBody();
      }

      file->Read("%s", value);
      if (strcmp(value, "}") == 0)
        return;
    }

    void DeserializeConstraints(File* file, PhysicsGL::Space* space)
    {
      Manager* manager = Manager::Get();
      char value[16];
      unsigned size = 0;
      file->Read("%s", value);
      if (strcmp(value, "Constraints") != 0)
        return;

      file->Read("%s", value);
      size = static_cast<unsigned>(atoi(value));

      file->Read("%s", value);
      if (strcmp(value, "{") != 0)
        return;

      for (unsigned i = 0; i < size; ++i)
      {
        TestEngine::Base::Variable var(manager->GetType(std::string("ConstraintInfo")), manager->GetType(std::string("ConstraintInfo"))->New());
        TestEngine::Base::Serializer::Get()->DeserializeStart(file, var);
        TestEngine::MemoryManager::ConstraintInfo* constraint = static_cast<TestEngine::MemoryManager::ConstraintInfo*>(var.GetData());
        ConstraintInfo* info = space->GetConstraintInfo();
        info->createBodies = constraint->createBodies;
        info->idA = constraint->idA;
        info->idB = constraint->idB;
        info->typeA = constraint->typeA;
        info->typeB = constraint->typeB;
        info->worldSpaceConnectionPointA = constraint->worldSpaceConnectionPointA;
        info->worldSpaceConnectionPointB = constraint->worldSpaceConnectionPointB;
        info->connectionPointA = constraint->connectionPointA;
        info->connectionPointB = constraint->connectionPointB;
        info->distance = constraint->distance;
        info->stiffness = constraint->stiffness;
        info->damping = constraint->damping;
        info->rotateA = constraint->rotateA;
        info->rotateB = constraint->rotateB;
        info->type = constraint->type;
        info->deleted = constraint->deleted;
        space->AddConstraint();
      }

      file->Read("%s", value);
      if (strcmp(value, "}") == 0)
        return;
    }

    void DeserializeObjects(File* file, PhysicsGL::Space* space)
    {
      DeserializeBoxes(file, space);
      DeserializeSpheres(file, space);
      DeserializeCapsules(file, space);
      DeserializeConstraints(file, space);
    }

    void DeserializeSpace(File* file, PhysicsGL::Space* space)
    {
      char value[16];

      file->Read("%s", value);
      if (strcmp(value, "Space") != 0)
        return;

      file->Read("%s", value);
      float dt = static_cast<float>(atof(value));
      space->Initialize(dt);
      DeserializeObjects(file, space);
    }
  }
}
