/*****************************************************************
Filename: File.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include <iostream>

namespace TestEngine
{
  namespace Base
  {

    enum FileAccess
    {
      Read,
      Write,
      Append
    };

    class File
    {
      public:
        File();
        File(FILE* file);
        ~File();

        File* Open(const char* name, FileAccess type);
        void Close();

        bool IsOpen();

        void Write(const char* format, ...);
        void Read(const char* format, ...);

        std::string GetLine(char delim);

        FILE*       fp;
        std::string fileName;

    };
  }

}