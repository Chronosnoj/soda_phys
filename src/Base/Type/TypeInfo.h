/*****************************************************************
Filename: TypeInfo.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../Member/MemberInfo.h"
#include <string>
#include <vector>

namespace TestEngine
{
  namespace Base
  {
    typedef void(*SerializeCallBack)(File*, const Variable&);
    typedef void(*DeserializeCallBack)(File*, Variable&);
    typedef std::vector<Member> MemberList;


    class TypeInfo
    {
      public:
        TypeInfo() : m_serialize(nullptr), m_deserialize(nullptr) {};

        void    Init(const char* name, unsigned size);
        Member* AddMember(const TypeInfo* typeInfo, const char* name, unsigned offset);

          //get common values
        unsigned    Size() const                                { return m_size; }
        const char* Name() const                                { return m_name.c_str(); }
        const MemberList& Members() const                       { return m_members; }
        const Member* GetMember(const char* memberName) const;
        const Member* GetMember(unsigned id) const;

        void  SetSerializer(SerializeCallBack serializer)       { m_serialize = serializer; }
        void  SetDeserializer(DeserializeCallBack deserializer)  { m_deserialize = deserializer; }

        void  Serialize(File* file, const Variable& var) const;
        void  Deserialize(File* file, Variable& var) const;

        void* New() const                                       { return m_new(); };
        void  Delete(void* data) const                          { m_delete(data); };
        void  Copy(void* data, const void* src) const           { m_copy(data, src); };
        void  NewCopy(void** dest, const void* src) const       { m_newCopy(dest, src); };
        bool  Equals(const void* lhs, const void* rhs) const    { return m_equals(lhs, rhs); }

        void  PlacementNew(void* data) const                    { m_placementNew(data); }
        void  PlacementDelete(void* data) const                 { m_placementDelete(data); }
        void  PlacementCopy(void* data, const void* src) const  { m_placementCopy(data, src); }


        friend class Manager;

      private:
        unsigned    m_size = 0;
        std::string m_name;
        MemberList  m_members;
        bool        m_isPOD;

        SerializeCallBack   m_serialize;
        DeserializeCallBack m_deserialize;

        void* (*m_new)(void);
        void  (*m_delete)(void* data);
        void  (*m_copy)(void* data, const void* src);
        void  (*m_newCopy)(void** dest, const void* src);

        void  (*m_placementNew)(void* data);
        void  (*m_placementDelete)(void* data);
        void  (*m_placementCopy)(void* data, const void *src);

        bool  (*m_equals)(const void* lhs, const void* rhs);
    };
  }
}