/*****************************************************************
Filename: TypeInfo.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "TypeInfo.h"
#include "../Serialization/Serializer.h"

namespace TestEngine
{
  namespace Base
  {
    void    TypeInfo::Init(const char* name, unsigned size)
    {
      m_name = std::string(name);
      m_size = size;
    }

    Member* TypeInfo::AddMember(const TypeInfo* typeInfo, const char* name, unsigned offset)
    {
      Member member;
      member.m_name = name;
      member.m_offset = offset;
      member.m_typeInfo = typeInfo;
      member.m_ownerType = this;

      m_members.push_back(member);
      return &(m_members.back());
    }

    const Member* TypeInfo::GetMember(const char* memberName) const
    {
      unsigned size = m_members.size();
      for (unsigned i = 0; i < size; ++i)
        if (m_members[i].m_name == memberName)
          return &(m_members[i]);

      return nullptr;

    }

    const Member* TypeInfo::GetMember(unsigned id) const
    {
      return &(m_members[id]);
    }

    void  TypeInfo::Serialize(File* file, const Variable& var) const
    {
      if (m_serialize)
        m_serialize(file, var);
      else
        Serializer::Get()->Serialize(file, var);
    }

    void  TypeInfo::Deserialize(File* file, Variable& var) const
    {
      if (m_deserialize)
        m_deserialize(file, var);
      else
        Serializer::Get()->Deserialize(file, var);
    }
  }

}