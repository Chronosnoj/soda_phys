/*****************************************************************
Filename: AllocationFunctions.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include <string>

namespace TestEngine
{
  namespace Base
  {
      //cast functions, used for conversion
    template <typename T>
    T* Cast(void* data)
    {
      return reinterpret_cast<T*>(data);
    }

    template <typename T>
    const T* Cast(const void* data)
    {
      return reinterpret_cast<const T*>(data);
    }


      //allocation functions
    template <typename T>
    void* New()
    {
      T* data = (T*)malloc(sizeof(T));
      new (data)T();
      return data;
    }
   
    template <typename T>
    void Delete(void* data)
    {
      Cast<T>(data)->~T();
      free(data);
    }

    template <typename T>
    void Copy(void* dest, const void* src)
    {
      *(Cast<T>(dest)) = *(Cast<T>(src));
    }

    template <typename T>
    void NewCopy(void** dest, const void* src)
    {
      *dest = malloc(sizeof(T));
      std::memcpy(*dest, src, sizeof(T));
    }

    template <typename T>
    void PlacementNew(void* data)
    {
      new (data)T();
    }

    template <typename T>
    void PlacementDelete(void* data)
    {
      Cast<T>(data)->~T();
    }

    template <typename T>
    void PlacementCopy(void* dest, const void* src)
    {
      new(dest)T(*(Cast<T>(src)));
    }


      //POD functions
    template <typename T>
    void* NewPOD()
    {
      return malloc(sizeof(T));
    }

    template <typename T>
    void DeletePOD(void* data)
    {
      free(data);
    }
    
    template <typename T>
    void CopyPOD(void* dest, const void* src)
    {
      std::memcpy(dest, src, sizeof(T));
    }


      //not allocation, but necessary for comparison
    template <typename T>
    bool Equals(const void* lhs, const void* rhs)
    {
      return *(Cast<T>(lhs)) == *(Cast<T>(rhs));
    }

  }
}