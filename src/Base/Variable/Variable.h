/*****************************************************************
Filename: Variable.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


namespace TestEngine
{
  namespace Base
  {
    class File;
    class TypeInfo;

    class Variable
    {
      public:

          //constructors
        Variable() : m_data(nullptr), m_typeInfo(nullptr) {}
        Variable(const TypeInfo* typeInfo, void* data);
        Variable(const TypeInfo* typeInfo, const void* data);
        Variable(const Variable& rhs);

        template <typename T>
        Variable(const T& rhs);

        template <typename T>
        Variable(const T* rhs);

        template <typename T>
        Variable(T* rhs);


          //gettors and settors
        void* GetData(void) const               { return m_data; }
        const TypeInfo* GetTypeInfo(void) const { return m_typeInfo; }

        //void SetName(std::string name)          { m_name = name; }
        //std::string GetName()                   { return m_name; }

        template <typename T>
        void  SetData(T data)                   { *((T*)m_data) = data; }

        template <typename T>
        T&    GetValue(void);

        template <typename T>
        const T& GetValue(void) const;


      private:
        //std::string     m_name;
        void*           m_data;
        const TypeInfo* m_typeInfo;

    };

    template <typename T>
    struct CastHelper
    {
      static T& Cast(void*& data)                 { return *(T*&)(data); }
      static const T& Cast(void* const& data)     { return *(T* const&)(data); }
    };

    template <typename T>
    struct CastHelper< T* >
    {
      static T*& Cast(void*& data)                { return (T*&)data; };
      static const T*& Cast(void* const& data)    { return (T* const&)data; };
    };

    template <typename T>
    T& Variable::GetValue(void)
    {
      T& value = CastHelper<T>::Cast(m_data);
      return value;
    }

    template <typename T>
    const T& Variable::GetValue(void) const
    {
      const T& value = CastHelper<T>::Cast(m_data);
      return value;
    }
  }
}