/*****************************************************************
Filename: Variable.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../Type/TypeInfo.h"
#include "../Manager/Manager.h"

namespace TestEngine
{
  namespace Base
  {
    Variable::Variable(const TypeInfo* typeInfo, void* data) : m_data(data), m_typeInfo(typeInfo) {}

    Variable::Variable(const TypeInfo* typeInfo, const void* data) : m_data(const_cast<void*>(data)), m_typeInfo(typeInfo) {}

    Variable::Variable(const Variable& rhs)
    {
      m_data = rhs.GetData();
      m_typeInfo = rhs.GetTypeInfo();
    }


    template <typename T>
    Variable::Variable(const T& rhs)
    {
      m_data = (T*)&rhs;
      m_typeInfo = Manager::Get()->GetType<T>();
    }

    template <typename T>
    Variable::Variable(const T* rhs)
    {
      m_data = (T*)rhs;
      m_typeInfo = Manager::Get()->GetType<T>();
    }

    template <typename T>
    Variable::Variable(T* rhs)
    {
      m_data = (T*)&rhs;
      m_typeInfo = Manager::Get()->GetType();
    }

  }
}