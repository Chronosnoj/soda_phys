/*****************************************************************
Filename: MemberInfo.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "../Type/TypeInfo.h"

namespace TestEngine
{
  namespace Base
  {
    #define PTR_ADD(PTR, OFFSET) \
    ((char*)(PTR) + (OFFSET))

    #define PTR_SUB(PTR, OFFSET) \
    ((char*)(PTR) - (OFFSET))

      //get
    void Member::Get(Base::Variable& owner, Base::Variable& value) const
    {
      value.GetTypeInfo()->Copy(value.GetData(), PTR_ADD(owner.GetData(), Offset()));
    }

    void Member::Get(void* owner, Base::Variable& value) const
    {
      value.GetTypeInfo()->Copy(value.GetData(), PTR_ADD(owner, Offset()));
    }

      //set
    void Member::Set(Variable& owner, const Variable value) const
    {
      value.GetTypeInfo()->Copy(PTR_ADD(owner.GetData(), Offset()), value.GetData());
    }

    void Member::Set(void* owner, const Variable value) const
    {
      value.GetTypeInfo()->Copy(PTR_ADD(owner, Offset()), value.GetData());
    }

  }
}