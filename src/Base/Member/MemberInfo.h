/*****************************************************************
Filename: MemberInfo.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../math/vectors/Vec3.h"
#include "../Variable/Variable.h"

namespace TestEngine
{
  namespace Base
  {
    class TypeInfo;

    class Member
    {
      public:
        Member() {};

        const       TypeInfo* Type(void) const  { return m_typeInfo;  }
        unsigned    Offset(void) const          { return m_offset; }
        const char* Name(void) const            { return m_name; }

          //gettors
        void Get(Base::Variable& owner, Base::Variable& value)  const;
        void Get(void* owner, Base::Variable& value)            const;

          //settors
        void Set(Variable& owner, const Variable value) const;
        void Set(void* owner, const Variable value)     const;

      private:
        const char*     m_name;
        unsigned        m_offset;
        const TypeInfo* m_typeInfo;
        const TypeInfo* m_ownerType;

        friend class TypeInfo;
    };

  }

}
