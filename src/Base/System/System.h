/*****************************************************************
Filename: System.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../Message/Message.h"
#include <string>

namespace TestEngine
{
  namespace Base
  {
    class System
    {
      public:
        virtual ~System() {};

        virtual void Initialize() {};
        virtual void Update(float dt) = 0;
        
        virtual std::string GetName() = 0;

        virtual void SendMessage(Message& message) {};

        

    };
  }

}
