/*****************************************************************
Filename: Manager.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "Manager.h"

namespace TestEngine
{
  namespace Base
  {
    const TypeInfo* Manager::GetType(const char * typeName) const
    {
      std::string typeNameStr = typeName;

      if (m_typeMap.find(typeNameStr) != m_typeMap.end())
        return m_typeMap.at(typeNameStr);

      return nullptr;
    }
    TypeInfo* Manager::GetType(const char * typeName)
    {
      std::string typeNameStr = typeName;

      if (m_typeMap.find(typeNameStr) != m_typeMap.end())
        return m_typeMap.at(typeNameStr);

      return nullptr;
    }
    const TypeInfo* Manager::GetType(std::string & typeName) const
    {
      if (m_typeMap.find(typeName) != m_typeMap.end())
        return m_typeMap.at(typeName);

      return nullptr;
    }
    TypeInfo* Manager::GetType(std::string & typeName)
    {
      if (m_typeMap.find(typeName) != m_typeMap.end())
        return m_typeMap.at(typeName);

      return nullptr;
    }

    const std::unordered_map<std::string, TypeInfo*>& Manager::GetTypeMap() const
    {
      return m_typeMap;
    }

  }
}