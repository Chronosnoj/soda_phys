/*****************************************************************
Filename: Manager.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#include "../Type/TypeInfo.h"
#include "../Type/AllocationFunctions.h"
#include <unordered_map>

namespace TestEngine
{
  namespace Base
  {
    class Manager
    {
      public:
        static Manager* Get()
        {
          static Manager manager;
          return &manager;
        }

        template <typename T>
        const TypeInfo* GetType(void) const;

        const TypeInfo* GetType(const char* typeName) const;
        TypeInfo* GetType(const char* typeName);

        const TypeInfo* GetType(std::string& typeName) const;
        TypeInfo* GetType(std::string& typeName);

        template<typename T>
        const TypeInfo* RegisterType(unsigned int size, const char* name, bool isPOD);

        const std::unordered_map<std::string, TypeInfo*>& GetTypeMap() const;

      private:
        std::unordered_map<std::string, TypeInfo*> m_typeMap;

        Manager()   {};
        ~Manager()  {};
    };

    template <typename T>
    const TypeInfo* Manager::GetType(void) const
    {
      static TypeInfo instance;
      return &instance;
    }

    template <typename T>
    const TypeInfo* Manager::RegisterType(unsigned int size, const char* name, bool isPOD)
    {
      TypeInfo* typeInfo = (TypeInfo*)GetType<T>();
      typeInfo->Init(name, size);
      typeInfo->m_isPOD = isPOD;

      if (typeInfo->m_isPOD)
      {
        typeInfo->m_new             = NewPOD<T>;
        typeInfo->m_delete          = DeletePOD<T>;
        typeInfo->m_copy            = CopyPOD<T>;
        typeInfo->m_placementNew    = nullptr;
        typeInfo->m_placementDelete = DeletePOD<T>;
        typeInfo->m_placementCopy   = CopyPOD<T>;

        typeInfo->m_equals          = Equals<T>;
      }

      else
      {
        typeInfo->m_new             = New<T>;
        typeInfo->m_delete          = Delete<T>;
        typeInfo->m_copy            = Copy<T>;
        typeInfo->m_placementNew    = PlacementNew<T>;
        typeInfo->m_placementDelete = PlacementDelete<T>;
        typeInfo->m_placementCopy   = PlacementCopy<T>;

        typeInfo->m_equals          = Equals<T>;
      }

      m_typeMap[typeInfo->m_name] = typeInfo;
      return typeInfo;
    }
  } 
}