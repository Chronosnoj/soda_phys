/*****************************************************************
Filename: threadmanager.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "jobmanager.h"
#include "../../input/input.h"
#include <thread>

namespace TestEngine
{
  namespace Base
  {
    const static unsigned s_maxNumberOfThreads = 4;

    class ThreadManager
    {
      public:
        ThreadManager()             {}
        
        void Initialize();

        JobManager* GetJobManager()                             { return &m_jobManager; }

        void SetThread(std::thread* thread, unsigned threadID)  { m_threads[threadID] = thread; }

          //set the input manager and tie the input manager to this thread manager
        void SetInputManager(InputGL::InputManager& input)      { m_input = &input;  m_input->AddJobManager(&m_jobManager); }

          //public function that pulls from jobqueue
        static void ThreadFunction(InputGL::InputManager* input, ThreadManager* threadManager, unsigned threadID);

          //for determining if a thread is ready to proceed
        bool  GetThreadReady(unsigned thread)                   { return m_threadReady[thread]; }
        void  SetThreadReady(bool ready, unsigned thread)       { m_threadReady[thread] = ready; }
        bool  CheckThreads();

      private:
          //needs to have an array of threads that are preserved
        std::thread*              m_threads[s_maxNumberOfThreads];
        bool                      m_threadReady[s_maxNumberOfThreads];

          //job manager holds the queue of jobs
        JobManager                m_jobManager;

          //for input and escape
        InputGL::InputManager*    m_input;

          //mutex for verifying threads and jobs
        std::mutex                m_threadLock[4];
    };
  }
}