/*****************************************************************
Filename: threadmanager.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "threadmanager.h"

namespace TestEngine
{
  namespace Base
  {
    void ThreadManager::Initialize()
    {
      for (unsigned i = 0; i < s_maxNumberOfThreads; ++i)
        m_threadReady[i] = true;
    }
    void ThreadManager::ThreadFunction(InputGL::InputManager* input, ThreadManager* threadManager, unsigned threadID)
    {
      while (true)
      {
        if (!input->CheckEscape())
          break;

        Job currentJob;

          //lock and protect job queue for removal of jobs
        if (threadManager->GetJobManager()->m_jobMutex.try_lock())
        {
          if (!threadManager->GetJobManager()->m_jobQueue.empty())
          {
            currentJob = threadManager->GetJobManager()->m_jobQueue.front();
            threadManager->GetJobManager()->m_jobQueue.pop();
          }

          threadManager->GetJobManager()->m_jobMutex.unlock();
        }
        
          //run job
        if (currentJob.m_function != nullptr)
        {
          //don't let the step proceed while any thread is still working
          threadManager->m_threadLock[threadID].lock();
          threadManager->SetThreadReady(false, threadID);
          threadManager->m_threadLock[threadID].unlock();

          currentJob.m_function(currentJob.m_island, currentJob.m_space, currentJob.m_master, currentJob.m_servant);

          //thread finished job and is ready to proceed again to avoid same island being run twice
          threadManager->m_threadLock[threadID].lock();
          threadManager->SetThreadReady(true, threadID);
          threadManager->m_threadLock[threadID].unlock();
        }
      }
    }
    bool ThreadManager::CheckThreads()
    {
      bool testThreads = true;

      for (unsigned i = 0; i < s_maxNumberOfThreads; ++i)
      {
        if (!m_threadLock[i].try_lock())
          continue;

        if (!m_threadReady[i])
          testThreads = false;

        m_threadLock[i].unlock();
      }      

      return testThreads;
    }
  }
}


