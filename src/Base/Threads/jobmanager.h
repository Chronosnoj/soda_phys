/*****************************************************************
Filename: jobmanager.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include <queue>
#include <mutex>

namespace TestEngine
{
  namespace Base
  {
    typedef void(*JobFunction)(unsigned island, void* space, bool master, unsigned servant);

    struct Job
    {
      Job() { m_function = nullptr; }
      Job(JobFunction function, unsigned island, void* space, bool master, unsigned servant) 
      {
        m_function = function; m_island = island;  m_space = space;  m_master = master; m_servant = servant;
      }

      JobFunction             m_function;  //the current function to perform
      unsigned                m_island;    //the island to work on
      void*                   m_space;

        //master status
      bool                    m_master;
      unsigned                m_servant;
    };

    struct JobManager
    {
      JobManager() {}

      std::queue<Job> m_jobQueue;
      std::mutex      m_jobMutex;
    };


  }
}