/*****************************************************************
Filename: RegisteredTypes.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


  //registration macros for ease of addition and use
  //gets the offset of the member by pretending the object is at address 0 and getting the unsigned value offset
#define GET_OFFSET_OF( TYPE, MEMBER ) ((unsigned)(&(((TYPE*)NULL)->MEMBER)))

  //used to determine the type of the base member
#define GET_TYPE_OF( TYPE, MEMBER )  manager->GetType<decltype(TYPE::MEMBER)>()

#define REGISTER_TYPE( TYPE, NAMESPACE ) manager->RegisterType<NAMESPACE::TYPE>(sizeof(NAMESPACE::TYPE), std::string(#TYPE).c_str(), false)

  //adds a member to the class
#define ADD_MEMBER( TYPE, MEMBER ) \
manager->GetType(std::string(#TYPE))->AddMember(GET_TYPE_OF(TYPE, MEMBER), #MEMBER, GET_OFFSET_OF(TYPE, MEMBER))

#define ADD_ENUM_MEMBER( TYPE, MEMBER ) \
manager->GetType(std::string(#TYPE))->AddMember(manager->GetType<int>(), #MEMBER, GET_OFFSET_OF(TYPE, MEMBER))


namespace TestEngine
{
  namespace Base 
  {
    void RegisterTypes();
  }
}

