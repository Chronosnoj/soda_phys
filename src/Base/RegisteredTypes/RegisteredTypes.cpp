/*****************************************************************
Filename: RegisterTypes.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "RegisteredTypes.h"
#include "..\Manager\Manager.h"
#include "..\math\vectors\Vec3.h"
#include "..\math\matrices\Mat3.h"
#include "..\Serialization\Serializer.h"
#include "..\Serialization\POD\SerializePOD.h"
#include "..\Serialization\POD\DeserializePOD.h"
#include "..\..\physics\space\Space.h"

namespace TestEngine
{
  namespace Base
  {
    void RegisterTypes()
    {
      Manager* manager = Manager::Get();

      manager->RegisterType<char>(sizeof(char), std::string("char").c_str(), true);
      manager->GetType(std::string("char"))->SetSerializer(SerializePOD<char>);
      manager->GetType(std::string("char"))->SetDeserializer(DeserializePOD<char>);

      manager->RegisterType<unsigned>(sizeof(unsigned), std::string("unsigned").c_str(), true);
      manager->GetType(std::string("unsigned"))->SetSerializer(SerializePOD<unsigned>);
      manager->GetType(std::string("unsigned"))->SetDeserializer(DeserializePOD<unsigned>);

      manager->RegisterType<int>(sizeof(int), std::string("int").c_str(), true);
      manager->GetType(std::string("int"))->SetSerializer(SerializePOD<int>);
      manager->GetType(std::string("int"))->SetDeserializer(DeserializePOD<int>);

      manager->RegisterType<float>(sizeof(float), std::string("float").c_str(), true);
      manager->GetType(std::string("float"))->SetSerializer(SerializePOD<float>);
      manager->GetType(std::string("float"))->SetDeserializer(DeserializePOD<float>);

      manager->RegisterType<double>(sizeof(double), std::string("double").c_str(), true);
      manager->GetType(std::string("double"))->SetSerializer(SerializePOD<double>);
      manager->GetType(std::string("double"))->SetDeserializer(DeserializePOD<double>);

      manager->RegisterType<bool>(sizeof(bool), std::string("bool").c_str(), true);
      manager->GetType(std::string("bool"))->SetSerializer(SerializePOD<bool>);
      manager->GetType(std::string("bool"))->SetDeserializer(DeserializePOD<bool>);

        //reference for macros
      //manager->RegisterType<Math::Vec3>(sizeof(Math::Vec3), std::string("Vec3").c_str(), false);
      //manager->GetType(std::string("Vec3"))->AddMember(GET_TYPE_OF(Vec3, x), "x", GET_OFFSET_OF(Vec3, x));
      //manager->GetType(std::string("Vec3"))->AddMember(manager->GetType<float>(), "y", GET_OFFSET_OF(Vec3, y));
      //manager->GetType(std::string("Vec3"))->AddMember(manager->GetType<float>(), "z", GET_OFFSET_OF(Vec3, z));
      
        //used for reference of memory offsets and objects
      /*manager->RegisterType<PhysicsGL::Body>(sizeof(PhysicsGL::Body), std::string("Body").c_str(), false);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<Vec3>(), "m_position", 0);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<Mat3>(), "m_orientation", sizeof(Vec3));
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<Vec3>(), "m_offset", sizeof(Mat3) + sizeof(Vec3));
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<Vec3>(), "m_velocity", sizeof(Mat3) + sizeof(Vec3) * 2);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<Vec3>(), "m_angularVelocity", sizeof(Mat3) + sizeof(Vec3) * 3);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<float>(), "m_density", sizeof(Mat3) + sizeof(Vec3) * 4);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<float>(), "m_inverseMass", sizeof(Mat3) + sizeof(Vec3) * 4 + sizeof(float));
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<Mat3>(), "m_inverseInertiaTensor", sizeof(Mat3) + sizeof(Vec3) * 4 + sizeof(float) * 2);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<bool>(), "m_static", sizeof(Mat3) * 2 + sizeof(Vec3) * 4 + sizeof(float) * 2);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<bool>(), "m_rotationLock", sizeof(Mat3) * 2 + sizeof(Vec3) * 4 + sizeof(float) * 2 + sizeof(bool));
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<bool>(), "m_resolve", sizeof(Mat3) * 2 + sizeof(Vec3) * 4 + sizeof(float) * 2 + sizeof(bool) * 2);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<bool>(), "m_collision", sizeof(Mat3) * 2 + sizeof(Vec3) * 4 + sizeof(float) * 2 + sizeof(bool) * 3);
      manager->GetType(std::string("Body"))->AddMember(manager->GetType<bool>(), "m_active", sizeof(Mat3) * 2 + sizeof(Vec3) * 4 + sizeof(float) * 2 + sizeof(bool) * 4);*/

      REGISTER_TYPE(Vec3, Math);
      ADD_MEMBER(Vec3, x);
      ADD_MEMBER(Vec3, y);
      ADD_MEMBER(Vec3, z);
      
      REGISTER_TYPE(Mat3, Math);
      ADD_MEMBER(Mat3, x00);
      ADD_MEMBER(Mat3, x01);
      ADD_MEMBER(Mat3, x02);
      ADD_MEMBER(Mat3, x10);
      ADD_MEMBER(Mat3, x11);
      ADD_MEMBER(Mat3, x12);
      ADD_MEMBER(Mat3, x20);
      ADD_MEMBER(Mat3, x21);
      ADD_MEMBER(Mat3, x22);

      
      REGISTER_TYPE(Box, PhysicsGL);
      ADD_MEMBER(Box, m_position);
      ADD_MEMBER(Box, m_orientation);
      ADD_MEMBER(Box, m_offset);
      ADD_MEMBER(Box, m_velocity);
      ADD_MEMBER(Box, m_angularVelocity);
      ADD_MEMBER(Box, m_density);
      ADD_MEMBER(Box, m_inverseMass);
      ADD_MEMBER(Box, m_inverseInertiaTensor);
      ADD_MEMBER(Box, m_static);
      ADD_MEMBER(Box, m_rotationLock);
      ADD_MEMBER(Box, m_resolve);
      ADD_MEMBER(Box, m_collision);
      ADD_MEMBER(Box, m_active);
      ADD_MEMBER(Box, m_delete);
      ADD_MEMBER(Box, m_boxHalfAxes);
      ADD_MEMBER(Box, m_id);

      REGISTER_TYPE(Sphere, PhysicsGL);
      ADD_MEMBER(Sphere, m_position);
      ADD_MEMBER(Sphere, m_orientation);
      ADD_MEMBER(Sphere, m_offset);
      ADD_MEMBER(Sphere, m_velocity);
      ADD_MEMBER(Sphere, m_angularVelocity);
      ADD_MEMBER(Sphere, m_density);
      ADD_MEMBER(Sphere, m_inverseMass);
      ADD_MEMBER(Sphere, m_inverseInertiaTensor);
      ADD_MEMBER(Sphere, m_static);
      ADD_MEMBER(Sphere, m_rotationLock);
      ADD_MEMBER(Sphere, m_resolve);
      ADD_MEMBER(Sphere, m_collision);
      ADD_MEMBER(Sphere, m_active);
      ADD_MEMBER(Sphere, m_delete);
      ADD_MEMBER(Sphere, m_radius);
      ADD_MEMBER(Sphere, m_id);

      REGISTER_TYPE(Capsule, PhysicsGL);
      ADD_MEMBER(Capsule, m_position);
      ADD_MEMBER(Capsule, m_orientation);
      ADD_MEMBER(Capsule, m_offset);
      ADD_MEMBER(Capsule, m_velocity);
      ADD_MEMBER(Capsule, m_angularVelocity);
      ADD_MEMBER(Capsule, m_density);
      ADD_MEMBER(Capsule, m_inverseMass);
      ADD_MEMBER(Capsule, m_inverseInertiaTensor);
      ADD_MEMBER(Capsule, m_static);
      ADD_MEMBER(Capsule, m_rotationLock);
      ADD_MEMBER(Capsule, m_resolve);
      ADD_MEMBER(Capsule, m_collision);
      ADD_MEMBER(Capsule, m_active);
      ADD_MEMBER(Capsule, m_delete);
      ADD_MEMBER(Capsule, m_radius);
      ADD_MEMBER(Capsule, m_halfHeight);
      ADD_MEMBER(Capsule, m_id);

      REGISTER_TYPE(ConstraintInfo, MemoryManager);
      ADD_MEMBER(ConstraintInfo, createBodies);
      ADD_MEMBER(ConstraintInfo, idA);
      ADD_MEMBER(ConstraintInfo, idB);
      ADD_ENUM_MEMBER(ConstraintInfo, typeA);
      ADD_ENUM_MEMBER(ConstraintInfo, typeB);
      ADD_MEMBER(ConstraintInfo, worldSpaceConnectionPointA);
      ADD_MEMBER(ConstraintInfo, worldSpaceConnectionPointB);
      ADD_MEMBER(ConstraintInfo, connectionPointA);
      ADD_MEMBER(ConstraintInfo, connectionPointB);
      ADD_MEMBER(ConstraintInfo, distance);
      ADD_MEMBER(ConstraintInfo, stiffness);
      ADD_MEMBER(ConstraintInfo, damping);
      ADD_MEMBER(ConstraintInfo, rotateA);
      ADD_MEMBER(ConstraintInfo, rotateB);
      ADD_ENUM_MEMBER(ConstraintInfo, type);
      ADD_MEMBER(ConstraintInfo, deleted);

    }
  }
}
