/*****************************************************************
Filename: Message.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

namespace TestEngine
{
  namespace Base
  {
    enum MessageType
    {
      Quit,

      NumMessageTypes
    };

    struct Message
    {
    public:
      virtual ~Message() {};

      Message(MessageType type) : m_type(type) {};

      MessageType m_type;

    };

  }
}