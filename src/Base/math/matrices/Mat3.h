/*****************************************************************
Filename: Mat3.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#include "../vectors/Vec3.h"

namespace TestEngine
{
	namespace Math
	{
		struct Mat3
		{
			Mat3();
			Mat3(const Vec3& translation);
			Mat3(const Vec3& right, const Vec3& up, const Vec3& back);
			Mat3(double radians, Vec3& axis);
			Mat3(float scale);

			void RotateAboutAxis(double radians, Vec3 axis);

			void SetAsTranslation(float x, float y, float z = 1.0f);
			void SetRotationRadians(float radians);
			void SetRotationAboutAxis(float radians, Vec3 axis);
			void SetAsScale(float scale);
			void SetAsIdentity();
			void SetAsZero();

			void SwitchHandedness();

      float Cofactor(unsigned int row, unsigned int column) const;

			Mat3 CreateInverse() const;
			void Invert();

			Mat3 CreateTranspose();

			Mat3 operator*(const Mat3& rhs) const;
			Vec3 operator*(const Vec3& rhs) const;

			Mat3& operator=(const Mat3& rhs);

        //serialization testing
      bool operator==(const Mat3& rhs) const;

			union
			{
				float m_matrix[3][3];
				float m_matrixLinear[9];

        struct {
          float x00;
          float x01;
          float x02;
          float x10;
          float x11;
          float x12;
          float x20;
          float x21;
          float x22;
        };

			};
		};
	}
}