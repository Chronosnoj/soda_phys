/*****************************************************************
Filename: Mat4.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "mat4.h"

namespace TestEngine
{
	namespace Math
	{
		Mat4::Mat4()
		{
			SetAsIdentity();
		}

		Mat4::Mat4(const Vec3 & translation)
		{
			SetAsIdentity();
			m_matrix[0][3] = translation.x;
			m_matrix[1][3] = translation.y;
			m_matrix[2][3] = translation.z;
		}
		
		Mat4::Mat4(const Vec4 & translation)
		{
			SetAsIdentity();
			m_matrix[0][3] = translation.x;
			m_matrix[1][3] = translation.y;
			m_matrix[2][3] = translation.z;
		}
		Mat4::Mat4(Vec3& right, Vec3& up, Vec3 & back)
		{
			m_matrix[0][0] = right.x;
			m_matrix[1][0] = right.y;
			m_matrix[2][0] = right.z;

			m_matrix[0][1] = up.x;
			m_matrix[1][1] = up.y;
			m_matrix[2][1] = up.z;

			m_matrix[0][2] = back.x;
			m_matrix[1][2] = back.y;
			m_matrix[2][2] = back.z;
		}

		Mat4::Mat4(Vec4 & up, Vec4 & right, Vec4 & back, Vec4 & translation)
		{
			// roll
			m_matrix[0][0] = up.x;
			m_matrix[1][0] = up.y;
			m_matrix[2][0] = up.z;
			m_matrix[3][0] = up.w;

			// pitch
			m_matrix[0][1] = right.x;
			m_matrix[1][1] = right.y;
			m_matrix[2][1] = right.z;
			m_matrix[3][1] = right.w;

			// yaw
			m_matrix[0][2] = back.x;
			m_matrix[1][2] = back.y;
			m_matrix[2][2] = back.z;
			m_matrix[3][2] = back.w;

			// position
			m_matrix[0][3] = translation.x;
			m_matrix[1][3] = translation.y;
			m_matrix[2][3] = translation.z;
			m_matrix[3][3] = translation.w;
		}

		Mat4::Mat4(double radians, Vec4 & axis)
		{
			SetAsRotationAboutAxis(radians, axis);
		}

		Mat4::Mat4(float scale)
		{
			SetAsIdentity();
			m_matrix[0][0] = scale;
			m_matrix[1][1] = scale;
			m_matrix[2][2] = scale;
		}

		void Mat4::SetAsRotationAboutAxis(double radians, Vec4 axis)
		{
			SetAsIdentity();

			axis.Normalize();

			float rCos = cosf(static_cast<float>(radians));
			float rSin = sinf(static_cast<float>(radians));

			m_matrix[0][0] = rCos + (axis.x * axis.x) * (1 - rCos);
			m_matrix[0][1] = (axis.x * axis.y) * (1 - rCos) - axis.z * rSin;
			m_matrix[0][2] = (axis.x * axis.z) * (1 - rCos) + axis.y * rSin;

			m_matrix[1][0] = (axis.y * axis.x) * (1 - rCos) + axis.z * rSin;
			m_matrix[1][1] = rCos + (axis.y * axis.y) * (1 - rCos);
			m_matrix[1][2] = (axis.y * axis.z) * (1 - rCos) - axis.x * rSin;

			m_matrix[2][0] = (axis.z * axis.x) * (1 - rCos) - axis.y * rSin;
			m_matrix[2][1] = (axis.z * axis.y) * (1 - rCos) + axis.x * rSin;
			m_matrix[2][2] = rCos + (axis.z * axis.z) * (1 - rCos);

		}

		void Mat4::SetAsTranslation(float x, float y, float z, float w)
		{
			SetAsIdentity();
			m_matrix[0][3] = x;
			m_matrix[1][3] = y;
			m_matrix[2][3] = z;
			m_matrix[3][3] = w;
		}

		void Mat4::SetAsTranslation(Vec4 & translation)
		{
			SetAsIdentity();
			m_matrix[0][3] = translation.x;
			m_matrix[1][3] = translation.y;
			m_matrix[2][3] = translation.z;
			m_matrix[3][3] = translation.w;

		}

		void Mat4::SetAsScale(float scale)
		{
			SetAsIdentity();
			m_matrix[0][0] = scale;
			m_matrix[1][1] = scale;
			m_matrix[2][2] = scale;
		}
	
		void Mat4::SetAsIdentity()
		{
			m_matrixLinear[0] = 1.0f;
			m_matrixLinear[1] = 0.0f;
			m_matrixLinear[2] = 0.0f;
			m_matrixLinear[3] = 0.0f;

			m_matrixLinear[4] = 0.0f;
			m_matrixLinear[5] = 1.0f;
			m_matrixLinear[6] = 0.0f;
			m_matrixLinear[7] = 0.0f;

			m_matrixLinear[8] = 0.0f;
			m_matrixLinear[9] = 0.0f;
			m_matrixLinear[10] = 1.0f;
			m_matrixLinear[11] = 0.0f;

			m_matrixLinear[12] = 0.0f;
			m_matrixLinear[13] = 0.0f;
			m_matrixLinear[14] = 0.0f;
			m_matrixLinear[15] = 1.0f;

		}

		void Mat4::SetAsZero()
		{
			m_matrixLinear[0] = 0.0f;
			m_matrixLinear[1] = 0.0f;
			m_matrixLinear[2] = 0.0f;
			m_matrixLinear[3] = 0.0f;

			m_matrixLinear[4] = 0.0f;
			m_matrixLinear[5] = 0.0f;
			m_matrixLinear[6] = 0.0f;
			m_matrixLinear[7] = 0.0f;

			m_matrixLinear[8] = 0.0f;
			m_matrixLinear[9] = 0.0f;
			m_matrixLinear[10] = 0.0f;
			m_matrixLinear[11] = 0.0f;

			m_matrixLinear[12] = 0.0f;
			m_matrixLinear[13] = 0.0f;
			m_matrixLinear[14] = 0.0f;
			m_matrixLinear[15] = 0.0f;
		}

		//determines the cofactor value for a specific row and column
		float Cofactor(const Mat4& A, unsigned int row, unsigned int column)
		{
			//determine sign of cofactor value
			//based on row and column
			float sign = 1;
			for (unsigned int i = 0; i < row; ++i)
				sign *= -1;
			for (unsigned int i = 0; i < column; ++i)
				sign *= -1;

			float vals[4] = { 0 };

			unsigned int k = 0;

			//fill the vals array 
			for (unsigned int i = 0; i < 3; ++i)
			{
				if (i == row)
					continue;
				for (unsigned int j = 0; j < 3; ++j)
				{
					if (j == column)
						continue;

					vals[k] = A.m_matrix[i][j];
					++k;
				}
			}

			//returns the single cofactor value
			return sign * (vals[0] * vals[3] - vals[1] * vals[2]);
		}


		Mat4 Mat4::CreateInverse() const
		{
			Mat4 inver;
			Mat4 cofact;
			Mat4 transl;

			for (unsigned int i = 0; i < 3; ++i)
				for (unsigned int j = 0; j < 3; ++j)
					cofact.m_matrix[i][j] = Cofactor(*this, i, j);

			//use the cofactor values for the determinent
			float determinant = cofact.m_matrix[0][0] * m_matrix[0][0] +
				cofact.m_matrix[0][1] * m_matrix[0][1] + cofact.m_matrix[0][2] * m_matrix[0][2];

			//check if determinant is zero - if so, it is not invertible
			if (determinant == 0)
				return cofact;

			//transform cofact
			cofact = (1 / determinant) * cofact.CreateTranspose();
			cofact.m_matrix[3][3] = 1;

			transl.m_matrix[0][3] = -(m_matrix[0][3]);
			transl.m_matrix[1][3] = -(m_matrix[1][3]);
			transl.m_matrix[2][3] = -(m_matrix[2][3]);

			inver = cofact * transl;

			return inver;
		}

		void Mat4::Invert()
		{
			*this = CreateInverse();
		}

		Mat4 Mat4::CreateTranspose()
		{
			return Mat4
				(Vec4(m_matrix[0][0], m_matrix[0][1], m_matrix[0][2], m_matrix[0][3]),
					Vec4(m_matrix[1][0], m_matrix[1][1], m_matrix[1][2], m_matrix[1][3]),
					Vec4(m_matrix[2][0], m_matrix[2][1], m_matrix[2][2], m_matrix[2][3]),
					Vec4(m_matrix[3][0], m_matrix[3][1], m_matrix[3][2], m_matrix[3][3]));
		}

		Mat4 Mat4::operator*(const Mat4 & rhs) const
		{
			Mat4 mult;

			mult.m_matrix[0][0] = (m_matrix[0][0] * rhs.m_matrix[0][0]) + (m_matrix[0][1] * rhs.m_matrix[1][0]) + (m_matrix[0][2] * rhs.m_matrix[2][0]) + (m_matrix[0][3] * rhs.m_matrix[3][0]);
			mult.m_matrix[0][1] = (m_matrix[0][0] * rhs.m_matrix[0][1]) + (m_matrix[0][1] * rhs.m_matrix[1][1]) + (m_matrix[0][2] * rhs.m_matrix[2][1]) + (m_matrix[0][3] * rhs.m_matrix[3][1]);
			mult.m_matrix[0][2] = (m_matrix[0][0] * rhs.m_matrix[0][2]) + (m_matrix[0][1] * rhs.m_matrix[1][2]) + (m_matrix[0][2] * rhs.m_matrix[2][2]) + (m_matrix[0][3] * rhs.m_matrix[3][2]);
			mult.m_matrix[0][3] = (m_matrix[0][0] * rhs.m_matrix[0][3]) + (m_matrix[0][1] * rhs.m_matrix[1][3]) + (m_matrix[0][2] * rhs.m_matrix[2][3]) + (m_matrix[0][3] * rhs.m_matrix[3][3]);
			
			mult.m_matrix[1][0] = (m_matrix[1][0] * rhs.m_matrix[0][0]) + (m_matrix[1][1] * rhs.m_matrix[1][0]) + (m_matrix[1][2] * rhs.m_matrix[2][0]) + (m_matrix[1][3] * rhs.m_matrix[3][0]);
			mult.m_matrix[1][1] = (m_matrix[1][0] * rhs.m_matrix[0][1]) + (m_matrix[1][1] * rhs.m_matrix[1][1]) + (m_matrix[1][2] * rhs.m_matrix[2][1]) + (m_matrix[1][3] * rhs.m_matrix[3][1]);
			mult.m_matrix[1][2] = (m_matrix[1][0] * rhs.m_matrix[0][2]) + (m_matrix[1][1] * rhs.m_matrix[1][2]) + (m_matrix[1][2] * rhs.m_matrix[2][2]) + (m_matrix[1][3] * rhs.m_matrix[3][2]);
			mult.m_matrix[1][3] = (m_matrix[1][0] * rhs.m_matrix[0][3]) + (m_matrix[1][1] * rhs.m_matrix[1][3]) + (m_matrix[1][2] * rhs.m_matrix[2][3]) + (m_matrix[1][3] * rhs.m_matrix[3][3]);
			
			mult.m_matrix[2][0] = (m_matrix[2][0] * rhs.m_matrix[0][0]) + (m_matrix[2][1] * rhs.m_matrix[1][0]) + (m_matrix[2][2] * rhs.m_matrix[2][0]) + (m_matrix[2][3] * rhs.m_matrix[3][0]);
			mult.m_matrix[2][1] = (m_matrix[2][0] * rhs.m_matrix[0][1]) + (m_matrix[2][1] * rhs.m_matrix[1][1]) + (m_matrix[2][2] * rhs.m_matrix[2][1]) + (m_matrix[2][3] * rhs.m_matrix[3][1]);
			mult.m_matrix[2][2] = (m_matrix[2][0] * rhs.m_matrix[0][2]) + (m_matrix[2][1] * rhs.m_matrix[1][2]) + (m_matrix[2][2] * rhs.m_matrix[2][2]) + (m_matrix[2][3] * rhs.m_matrix[3][2]);
			mult.m_matrix[2][3] = (m_matrix[2][0] * rhs.m_matrix[0][3]) + (m_matrix[2][1] * rhs.m_matrix[1][3]) + (m_matrix[2][2] * rhs.m_matrix[2][3]) + (m_matrix[2][3] * rhs.m_matrix[3][3]);

			mult.m_matrix[3][0] = (m_matrix[3][0] * rhs.m_matrix[0][0]) + (m_matrix[3][1] * rhs.m_matrix[1][0]) + (m_matrix[3][2] * rhs.m_matrix[2][0]) + (m_matrix[3][3] * rhs.m_matrix[3][0]);
			mult.m_matrix[3][1] = (m_matrix[3][0] * rhs.m_matrix[0][1]) + (m_matrix[3][1] * rhs.m_matrix[1][1]) + (m_matrix[3][2] * rhs.m_matrix[2][1]) + (m_matrix[3][3] * rhs.m_matrix[3][1]);
			mult.m_matrix[3][2] = (m_matrix[3][0] * rhs.m_matrix[0][2]) + (m_matrix[3][1] * rhs.m_matrix[1][2]) + (m_matrix[3][2] * rhs.m_matrix[2][2]) + (m_matrix[3][3] * rhs.m_matrix[3][2]);
			mult.m_matrix[3][3] = (m_matrix[3][0] * rhs.m_matrix[0][3]) + (m_matrix[3][1] * rhs.m_matrix[1][3]) + (m_matrix[3][2] * rhs.m_matrix[2][3]) + (m_matrix[3][3] * rhs.m_matrix[3][3]);

			return mult;
		}

		Vec4 Mat4::operator*(const Vec4 & rhs) const
		{
			Vec4 mult;

			mult.x =	m_matrix[0][0] * rhs.x +
						m_matrix[0][1] * rhs.y +
						m_matrix[0][2] * rhs.z +
						m_matrix[0][3] * rhs.w;
										 
			mult.y =	m_matrix[1][0] * rhs.x +
						m_matrix[1][1] * rhs.y +
						m_matrix[1][2] * rhs.z +
						m_matrix[1][3] * rhs.w;
										 
			mult.z =	m_matrix[2][0] * rhs.x +
						m_matrix[2][1] * rhs.y +
						m_matrix[2][2] * rhs.z +
						m_matrix[2][3] * rhs.w;
										 
			mult.w =	m_matrix[3][0] * rhs.x +
						m_matrix[3][1] * rhs.y +
						m_matrix[3][2] * rhs.z +
						m_matrix[3][3] * rhs.w;

			return mult;
		}

		Mat4& Mat4::operator=(const Mat4& rhs)
		{
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
					m_matrix[i][j] = rhs.m_matrix[i][j];
			}

			return *this;
		}



		Mat4 operator*(float scalar, const Mat4 & rhs)
		{
			Mat4 mult = rhs;
			for (unsigned int i = 0; i < 16; ++i)
				*(*mult.m_matrix + i) = scalar * *(*mult.m_matrix + i);
			mult.m_matrix[3][3] = 1;

			return mult;
		}

}
}