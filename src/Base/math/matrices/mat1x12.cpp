/*****************************************************************
Filename: mat1z12.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "mat1x12.h"

namespace TestEngine
{
  namespace Math
  {
    Vec12::Vec12(Vec3 vecA, Vec3 vecB, Vec3 vecC, Vec3 vecD)
    {
      vec[0] = vecA.x;
      vec[1] = vecA.y;
      vec[2] = vecA.z;
      vec[3] = vecB.x;
      vec[4] = vecB.y;
      vec[5] = vecB.z;
      vec[6] = vecC.x;
      vec[7] = vecC.y;
      vec[8] = vecC.z;
      vec[9] = vecD.x;
      vec[10] = vecD.y;
      vec[11] = vecD.z;
    }

    Vec12& Vec12::operator*(float scalar)
    {
      for (unsigned i = 0; i < 12; ++i)
        vec[i] *= scalar;
      return *this;
    }

    Mat1x12::Mat1x12(Vec3 vecA, Vec3 vecB, Vec3 vecC, Vec3 vecD)
    {
      mat[0] = vecA.x;
      mat[1] = vecA.y;
      mat[2] = vecA.z;
      mat[3] = vecB.x;
      mat[4] = vecB.y;
      mat[5] = vecB.z;
      mat[6] = vecC.x;
      mat[7] = vecC.y;
      mat[8] = vecC.z;
      mat[9] = vecD.x;
      mat[10] = vecD.y;
      mat[11] = vecD.z;
    }

    Mat1x12::Mat1x12(float invMassA, Mat3 inertiaTensorA, float invMassB, Mat3 inertiaTensorB)
    {
      for (unsigned i = 0; i < 3; ++i)
        mat[i] = invMassA;

      for (unsigned i = 0; i < 3; ++i)
        mat[i + 3] = inertiaTensorA.m_matrix[i][i];

      for (unsigned i = 0; i < 3; ++i)
        mat[i + 6] = invMassB;

      for (unsigned i = 0; i < 3; ++i)
        mat[i + 9] = inertiaTensorB.m_matrix[i][i];
    }

    float Mat1x12::operator*(Vec12 & vec)
    {
      float total = 0;

      for (unsigned i = 0; i < 12; ++i)
       total += vec.vec[i] * mat[i];

      return total;
    }

    Vec12 Mat1x12::Mat12Mult(Vec12 & vec)
    {
      Vec12 mult;
      for (unsigned i = 0; i < 12; ++i)
        mult.vec[i] = mat[i] * vec.vec[i];

      return mult;
    }
    Vec12 Mat1x12::Transpose()
    {
      Vec12 vec;

      for (unsigned i = 0; i < 12; ++i)
        vec.vec[i] = mat[i];

      return vec;
    }
  }
}

