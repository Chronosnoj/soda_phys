/*****************************************************************
Filename: Mat4.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#include "../vectors/Vec3.h"
#include "../vectors/Vec4.h"

namespace TestEngine
{
	namespace Math
	{
		struct Mat4
		{
			Mat4();
			Mat4(const Vec3& translation);
			Mat4(const Vec4& translation);
			Mat4(Vec3& right, Vec3& up, Vec3& back);
			Mat4(Vec4& up, Vec4& right, Vec4& back, Vec4& translation);
			Mat4(double radians, Vec4& axis);
			Mat4(float scale);

			void SetAsRotationAboutAxis(double radians, Vec4 axis);

			void SetAsTranslation(float x, float y, float z, float w = 1.0f);
			void SetAsTranslation(Vec4& translation);

			void SetAsScale(float scale);

			void SetAsIdentity();
			void SetAsZero();

			Mat4 CreateInverse() const;
			void Invert();

			Mat4 CreateTranspose();

			Mat4 operator*(const Mat4& rhs) const;
			Vec4 operator*(const Vec4& rhs) const;

			Mat4& operator=(const Mat4& rhs);

			union
			{
				float m_matrix[4][4];
				float m_matrixLinear[16];
			};
		};

		Mat4 operator*(float scalar, const Mat4& rhs);
	}
}