/*****************************************************************
Filename: mat1x12.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#pragma once
#include "../vectors/Vec3.h"
#include "../vectors/Vec4.h"
#include "Mat3.h"

namespace TestEngine
{
  namespace Math
  {
    struct Vec12
    {
      Vec12() {}
      Vec12(Vec3 vecA, Vec3 vecB, Vec3 vecC, Vec3 vecD);

      Vec12& operator*(float scalar);

      float vec[12];
    };

    struct Mat1x12
    {
      Mat1x12(Vec3 vecA, Vec3 vecB, Vec3 vecC, Vec3 vecD);

      //fake invMass matrix for diagonal inertia tensor
      Mat1x12(float invMassA, Mat3 inertiaTensorA, float invMassB, Mat3 inertiaTensorB);

      float operator*(Vec12& vec);
      Vec12 Mat12Mult(Vec12& vec);

      Vec12 Transpose();

      float mat[12];

    };
  }
}