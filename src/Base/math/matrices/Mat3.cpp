/*****************************************************************
Filename: Mat3.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "Mat3.h"

namespace TestEngine
{
	namespace Math
	{
		Mat3::Mat3()
		{
			SetAsIdentity();
		}

		Mat3::Mat3(const Vec3 & translation)
		{
			SetAsIdentity();
			m_matrix[0][2] = translation.x;
			m_matrix[1][2] = translation.y;
			m_matrix[2][2] = translation.z;
		}

		Mat3::Mat3(const Vec3& right, const Vec3& up, const Vec3& back)
		{
			m_matrix[0][0] = right.x;
			m_matrix[1][0] = right.y;
			m_matrix[2][0] = right.z;

			m_matrix[0][1] = up.x;
			m_matrix[1][1] = up.y;
			m_matrix[2][1] = up.z;

			m_matrix[0][2] = back.x;
			m_matrix[1][2] = back.y;
			m_matrix[2][2] = back.z;
		}

		Mat3::Mat3(double radians, Vec3 & axis)
		{
			RotateAboutAxis(radians, axis);
		}

		Mat3::Mat3(float scale)
		{
			m_matrix[0][0] *= scale;
			m_matrix[2][0] *= scale;
			m_matrix[1][0] *= scale;
						   	   
			m_matrix[0][1] *= scale;
			m_matrix[1][1] *= scale;
			m_matrix[2][1] *= scale;
						   	  
			m_matrix[0][2] *= scale;
			m_matrix[1][2] *= scale;
			m_matrix[2][2] *= scale;

		}

		void Mat3::RotateAboutAxis(double radians, Vec3 axis)
		{
			SetAsIdentity();

			axis.Normalize();

			double rCos = cos(radians);
			double rSin = sin(radians);

			m_matrix[0][0] = static_cast<float>(rCos + (axis.x * axis.x) * (1 - rCos));
			m_matrix[0][1] = static_cast<float>((axis.x * axis.y) * (1 - rCos) - axis.z * rSin);
			m_matrix[0][2] = static_cast<float>((axis.x * axis.z) * (1 - rCos) + axis.y * rSin);

			m_matrix[1][0] = static_cast<float>((axis.y * axis.x) * (1 - rCos) + axis.z * rSin);
			m_matrix[1][1] = static_cast<float>(rCos + (axis.y * axis.y) * (1 - rCos));
			m_matrix[1][2] = static_cast<float>((axis.y * axis.z) * (1 - rCos) - axis.x * rSin);

			m_matrix[2][0] = static_cast<float>((axis.z * axis.x) * (1 - rCos) - axis.y * rSin);
			m_matrix[2][1] = static_cast<float>((axis.z * axis.y) * (1 - rCos) + axis.x * rSin);
			m_matrix[2][2] = static_cast<float>(rCos + (axis.z * axis.z) * (1 - rCos));

		}

		void Mat3::SetAsTranslation(float x, float y, float z)
		{
			SetAsIdentity();
			m_matrix[0][2] = x;
			m_matrix[1][2] = y;
			m_matrix[2][2] = z;

		}

		void Mat3::SetRotationRadians(float radians)
		{
			float sine = static_cast<float>(sin(radians));
			float cosine = static_cast<float>(cos(radians));

			SetAsIdentity();

			m_matrix[0][0] = cosine;
			m_matrix[0][1] = -sine;
			m_matrix[1][0] = sine;
			m_matrix[1][1] = cosine;
		}

		void Mat3::SetRotationAboutAxis(float radians, Vec3 axis)
		{
			SetAsIdentity();

			axis.Normalize();

			float rCos = cosf(radians);
			float rSin = sinf(radians);

			m_matrix[0][0] = rCos + (axis.x * axis.x) * (1 - rCos);
			m_matrix[0][1] = (axis.x * axis.y) * (1 - rCos) - axis.z * rSin;
			m_matrix[0][2] = (axis.x * axis.z) * (1 - rCos) + axis.y * rSin;

			m_matrix[1][0] = (axis.y * axis.x) * (1 - rCos) + axis.z * rSin;
			m_matrix[1][1] = rCos + (axis.y * axis.y) * (1 - rCos);
			m_matrix[1][2] = (axis.y * axis.z) * (1 - rCos) - axis.x * rSin;

			m_matrix[2][0] = (axis.z * axis.x) * (1 - rCos) - axis.y * rSin;
			m_matrix[2][1] = (axis.z * axis.y) * (1 - rCos) + axis.x * rSin;
			m_matrix[2][2] = rCos + (axis.z * axis.z) * (1 - rCos);
		}

		void Mat3::SetAsScale(float scale)
		{
			SetAsIdentity();

			m_matrix[0][0] = scale;
			m_matrix[1][1] = scale;
			m_matrix[2][2] = scale;
		}

		void Mat3::SetAsIdentity()
		{
			m_matrixLinear[0] = 1.0f;
			m_matrixLinear[1] = 0.0f;
			m_matrixLinear[2] = 0.0f;

			m_matrixLinear[3] = 0.0f;
			m_matrixLinear[4] = 1.0f;
			m_matrixLinear[5] = 0.0f;

			m_matrixLinear[6] = 0.0f;
			m_matrixLinear[7] = 0.0f;
			m_matrixLinear[8] = 1.0f;
		}

		void Mat3::SetAsZero()
		{
			m_matrixLinear[0] = 0.0f;
			m_matrixLinear[1] = 0.0f;
			m_matrixLinear[2] = 0.0f;

			m_matrixLinear[3] = 0.0f;
			m_matrixLinear[4] = 0.0f;
			m_matrixLinear[5] = 0.0f;

			m_matrixLinear[6] = 0.0f;
			m_matrixLinear[7] = 0.0f;
			m_matrixLinear[8] = 0.0f;
		}

		void Mat3::SwitchHandedness()
		{
			m_matrix[0][2] = -m_matrix[0][2];
			m_matrix[1][2] = -m_matrix[1][2];

			m_matrix[2][0] = -m_matrix[2][0];
			m_matrix[2][1] = -m_matrix[2][1];
		}

    //determines the cofactor value for a specific row and column
    float Mat3::Cofactor(unsigned int row, unsigned int column) const
    {
      //determine sign of cofactor value
      //based on row and column
      float sign = 1;
      for (unsigned int i = 0; i < row; ++i)
        sign *= -1;
      for (unsigned int i = 0; i < column; ++i)
        sign *= -1;

      float vals[4] = { 0 };

      unsigned int k = 0;

      //fill the vals array 
      for (unsigned int i = 0; i < 3; ++i)
      {
        if (i == row)
          continue;
        for (unsigned int j = 0; j < 3; ++j)
        {
          if (j == column)
            continue;

          vals[k] = m_matrix[i][j];
          ++k;
        }
      }
      
      //returns the single cofactor value
      return sign * (vals[0] * vals[3] - vals[1] * vals[2]);
    }

		Mat3 Mat3::CreateInverse() const
		{
      Mat3 cofact;
      cofact.SetAsIdentity();

      for (unsigned int i = 0; i < 3; ++i)
        for (unsigned int j = 0; j < 3; ++j)
          cofact.m_matrix[i][j] = Cofactor(i, j);

      //use the cofactor values for the determinent
      float determinant = cofact.m_matrix[0][0] * m_matrix[0][0] +
        cofact.m_matrix[0][1] * m_matrix[0][1] + cofact.m_matrix[0][2] * m_matrix[0][2];

      //check if determinant is zero - if so, it is not invertible
      if (determinant == 0)
        return cofact;

      //transform cofact
      cofact = cofact.CreateTranspose();
      float inverseDeterminant = 1.0f / determinant;

      for (unsigned int i = 0; i < 9; ++i)
        cofact.m_matrixLinear[i] *= inverseDeterminant;

      return cofact;
		}

		void Mat3::Invert()
		{
			*this = CreateInverse();
		}

		Mat3 Mat3::CreateTranspose()
		{
			return Mat3(Vec3(m_matrix[0][0], m_matrix[0][1], m_matrix[0][2]),
				Vec3(m_matrix[1][0], m_matrix[1][1], m_matrix[1][2]),
				Vec3(m_matrix[2][0], m_matrix[2][1], m_matrix[2][2]));
		}

		Mat3 Mat3::operator*(const Mat3 & rhs) const
		{
			Mat3 mult;

			mult.m_matrix[0][0] = (m_matrix[0][0] * rhs.m_matrix[0][0]) + (m_matrix[0][1] * rhs.m_matrix[1][0]) + (m_matrix[0][2] * rhs.m_matrix[2][0]);
			mult.m_matrix[0][1] = (m_matrix[0][0] * rhs.m_matrix[0][1]) + (m_matrix[0][1] * rhs.m_matrix[1][1]) + (m_matrix[0][2] * rhs.m_matrix[2][1]);
			mult.m_matrix[0][2] = (m_matrix[0][0] * rhs.m_matrix[0][2]) + (m_matrix[0][1] * rhs.m_matrix[1][2]) + (m_matrix[0][2] * rhs.m_matrix[2][2]);
			
			mult.m_matrix[1][0] = (m_matrix[1][0] * rhs.m_matrix[0][0]) + (m_matrix[1][1] * rhs.m_matrix[1][0]) + (m_matrix[1][2] * rhs.m_matrix[2][0]);
			mult.m_matrix[1][1] = (m_matrix[1][0] * rhs.m_matrix[0][1]) + (m_matrix[1][1] * rhs.m_matrix[1][1]) + (m_matrix[1][2] * rhs.m_matrix[2][1]);
			mult.m_matrix[1][2] = (m_matrix[1][0] * rhs.m_matrix[0][2]) + (m_matrix[1][1] * rhs.m_matrix[1][2]) + (m_matrix[1][2] * rhs.m_matrix[2][2]);
			
			mult.m_matrix[2][0] = (m_matrix[2][0] * rhs.m_matrix[0][0]) + (m_matrix[2][1] * rhs.m_matrix[1][0]) + (m_matrix[2][2] * rhs.m_matrix[2][0]);
			mult.m_matrix[2][1] = (m_matrix[2][0] * rhs.m_matrix[0][1]) + (m_matrix[2][1] * rhs.m_matrix[1][1]) + (m_matrix[2][2] * rhs.m_matrix[2][1]);
			mult.m_matrix[2][2] = (m_matrix[2][0] * rhs.m_matrix[0][2]) + (m_matrix[2][1] * rhs.m_matrix[1][2]) + (m_matrix[2][2] * rhs.m_matrix[2][2]);

			return mult;
		}

		Vec3 Mat3::operator*(const Vec3 & rhs) const
		{
			float X =	m_matrix[0][0] * rhs.x +
						m_matrix[0][1] * rhs.y +
						m_matrix[0][2] * rhs.z;

			float Y =	m_matrix[1][0] * rhs.x +
						m_matrix[1][1] * rhs.y +
						m_matrix[1][2] * rhs.z;
										 
			float Z =	m_matrix[2][0] * rhs.x +
						m_matrix[2][1] * rhs.y +
						m_matrix[2][2] * rhs.z;

			Vec3 result(X, Y, Z);

			return result;
		}

		Mat3 & Mat3::operator=(const Mat3 & rhs)
		{
			for (int i = 0; i < 9; ++i)
			{
				m_matrixLinear[i] = rhs.m_matrixLinear[i];
			}

			return *this;
		}

    bool TestEngine::Math::Mat3::operator==(const Mat3 & rhs) const
    {
      return false;
    }



	}

}

