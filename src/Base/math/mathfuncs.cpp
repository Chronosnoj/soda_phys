/*****************************************************************
Filename: mathfuncs.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "mathconvert.h"

namespace TestEngine
{
  namespace Math
  {
    float Min(float a, float b)
    {
      if (b < a)
        return b;

      return a;
    }

    float Max(float a, float b)
    {
      if (b > a)
        return b;

      return a;
    }

    int Sign(float value)
    {
      if (value == 0)
        return 0;

      else if (value < 0)
        return -1;
      
      return 1;
    }

    float Abs(float value)
    {
      if (value < 0)
        return -value;

      return value;
    }
  }
}
