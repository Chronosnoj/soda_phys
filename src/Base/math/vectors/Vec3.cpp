/*****************************************************************
Filename: Vec3.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "vec3.h"

namespace TestEngine
{
	namespace Math
	{
		Vec3::Vec3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}
		Vec3::Vec3(const Vec3& copy) : x(copy.x), y(copy.y), z(copy.z) {}

		float Vec3::Length()
		{
			return sqrtf(x * x + y * y + z * z);
		}

		float Vec3::SqLength()
		{
			return x * x + y * y + z * z;
		}
		
		Vec3 Vec3::CreateNormal()
		{
			Vec3 normal(y, -x);
			normal.Normalize();

			return normal;
		}
		
		Vec3& Vec3::Normalize()
		{
			float length = Length();

			//check to see if length is zero
			if (length == 0)
				return *this;

			x /= length;
			y /= length;
			z /= length;

			return *this;
		}
		
		float Vec3::Dot(const Vec3 & rhs)
		{
			return (rhs.x * x + rhs.y * y + rhs.z * z);
		}
		
		Vec3 Vec3::Cross(const Vec3 & rhs)
		{
			return Vec3(y * rhs.z - z * rhs.y,
						z * rhs.x - x * rhs.z,
						x * rhs.y - y * rhs.x);
		}
		
		Vec3 Vec3::Scale(float scale)
		{
			return Vec3(x * scale, y * scale, z * scale);
		}
		
		Vec3 & Vec3::Rotate(float radians)
		{
			Vec3 temp(0, 0);
			
			temp.x = x * cosf(radians) - y * sinf(radians);
			temp.y = x * sinf(radians) + y * cosf(radians);
			
			x = temp.x;
			y = temp.y;
			
			return *this;
		}
		
		Vec3 Vec3::operator+(const Vec3 & rhs) const
		{
			return Vec3(rhs.x + x, rhs.y + y, rhs.z + z);
		}
		
		Vec3 & Vec3::operator+=(const Vec3& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			z += rhs.z;

			return *this;
		}
		
		Vec3 Vec3::operator-(const Vec3 & rhs) const
		{
			return Vec3(x - rhs.x, y - rhs.y, z - rhs.z);
		}
		
		Vec3 & Vec3::operator-=(const Vec3 & rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			z -= rhs.z;

			return *this;
		}
		
		Vec3 Vec3::operator-() const
		{
			return Vec3(-x, -y, -z);
		}
		Vec3 Vec3::operator*(const float scale)
		{
			Vec3 scaled;
			scaled.x = x * scale;
			scaled.y = y * scale;
			scaled.z = z * scale;

			return scaled;
		}
    
    bool Vec3::operator==(const Vec3 & rhs) const
    {
      return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
    }
	}
}