#pragma once

namespace SODAGL
{
	namespace Math
	{
		struct Vec2
		{
			Vec2(float x = 0.0f, float y = 0.0f);
			Vec2(const Vec2& copy);
			
			float Length();
			float SquareLength();

			Vec2 CreateNormal();
			Vec2& Normalize();

			float Dot(const Vec2& rhs);

			Vec2& Rotate(float radians);

			union
			{
				struct
				{
					float x;
					float y;
				};

				float vec[2];
			};
		};

	}
}