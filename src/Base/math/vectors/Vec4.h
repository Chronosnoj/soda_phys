/*****************************************************************
Filename: Vec4.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#include <math.h>
#include "vec3.h"

namespace TestEngine
{
	namespace Math
	{
		struct Vec4
		{
			Vec4(float x = 1.0f, float y = 1.0f, float z = 1.0f, float w = 0.0f);
			Vec4(const Vec4& copy);
			Vec4(const Vec3& copy);

			float Length();
			float SqLength();

			Vec4& Normalize();

			float Dot(const Vec4& rhs);
			Vec4 Cross(const Vec4& rhs);

			Vec4 operator+(const Vec4& rhs) const;
			Vec4& operator+=(const Vec4& rhs);
			Vec4  operator-(const Vec4& rhs) const;
			Vec4& operator-=(const Vec4& rhs);
			Vec4& operator=(const Vec4& rhs);
			Vec4& operator=(const Vec3& rhs);
			Vec4  operator-() const;
			Vec4 operator*(float scale);

			union
			{
				struct
				{
					float x;
					float y;
					float z;
					float w;
				};

				float vec[4];
			};
		};

	}
}