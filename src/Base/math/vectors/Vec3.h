/*****************************************************************
Filename: Vec3.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#include <math.h>

namespace TestEngine
{
	namespace Math
	{
		struct Vec3
		{
			Vec3(float x = 0.0f, float y = 0.0f, float z = 0.0f);
			Vec3(const Vec3& copy);

			float	Length();
			float	SqLength();

			Vec3	CreateNormal();
			Vec3&	Normalize();

			float	Dot(const Vec3& rhs);
			Vec3	Cross(const Vec3& rhs);
			Vec3	Scale(float scale);

			Vec3&	Rotate(float radians);

			Vec3	operator+(const Vec3& rhs) const;
			Vec3&	operator+=(const Vec3& rhs);
			Vec3	operator-(const Vec3& rhs) const;    
			Vec3&	operator-=(const Vec3& rhs);         
			Vec3	operator-() const;
			Vec3	operator*(const float scale);
      //used for testing introspection and serialization, should not be used otherwise
      bool  operator==(const Vec3& rhs) const;  

			union
			{
				struct
				{
					float x;
					float y;
					float z;
				};

				float vec[3];
			};
		};

	}
}