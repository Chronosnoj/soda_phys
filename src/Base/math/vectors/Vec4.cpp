/*****************************************************************
Filename: Vec4.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "vec4.h"
namespace TestEngine
{
	namespace Math
	{
		Vec4::Vec4(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_) {}

		Vec4::Vec4(const Vec4& copy) : x(copy.x), y(copy.y), z(copy.z), w(copy.w) {}

		Vec4::Vec4(const Vec3 & copy)
		{
			x = copy.x;
			y = copy.y;
			z = copy.z;
			w = 0.0f;
		}

		float Vec4::Length()
		{
			return sqrtf(x * x + y * y + z * z);
		}

		float Vec4::SqLength()
		{
			return (x * x + y * y + z * z);
		}

		Vec4 & Vec4::Normalize()
		{
			float length = Length();
			//check to see if length is zero
			if (length == 0)
				return *this;

			x /= length;
			y /= length;
			z /= length;

			return *this;
		}

		float Vec4::Dot(const Vec4 & rhs)
		{
			return (rhs.x * x + rhs.y * y + rhs.z * z + rhs.w * w);
		}

		Vec4 Vec4::Cross(const Vec4 & rhs)
		{
			return Vec4(y * rhs.z - z * rhs.y,
				z * rhs.x - x * rhs.z,
				x * rhs.y - y * rhs.x);
		}

		Vec4 Vec4::operator+(const Vec4 & rhs) const
		{
			return Vec4(rhs.x + x, rhs.y + y, rhs.z + z, rhs.w + w);
		}

		Vec4& Vec4::operator+=(const Vec4 &rhs)
		{
			*this = Vec4(rhs.x + x, rhs.y + y, rhs.z + z, rhs.w + w);
			return *this;
		}

		Vec4 Vec4::operator-(const Vec4 & rhs) const
		{
			return Vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
		}

		Vec4& Vec4::operator-=(const Vec4 & rhs)
		{
			*this = Vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
			return *this;
		}

		Vec4& Vec4::operator=(const Vec4 & rhs)
		{
			x = rhs.x;
			y = rhs.y;
			z = rhs.z;
			w = rhs.w;

			return *this;
		}

		Vec4& Vec4::operator=(const Vec3 & rhs)
		{
			x = rhs.x;
			y = rhs.y;
			z = rhs.z;
			w = 0.0f;

			return *this;
		}

		Vec4 Vec4::operator-() const
		{
			return Vec4(-x, -y, -z, -w);
		}

		Vec4 Vec4::operator*(float scale)
		{
			Vec4 mult(x, y, z, w);
			mult.x = x * scale;
			mult.y = y * scale;
			mult.z = z * scale;
			mult.w = w * scale;

			return mult;
		}
	}

}

