/*****************************************************************
Filename: MathConvert.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <fstream>

#include "../../../external/glew-1.13.0/include/GL/glew.h"
#include "../../../external/glfw-3.1.2/include/GLFW/glfw3.h"
#include "../../../external/glm-0.9.7.1/glm/glm.hpp"
#include "../../../external/glm-0.9.7.1/glm/gtc/matrix_transform.hpp"

  //math stuff
#include "matrices\mat3.h"
#include "matrices\mat4.h"
#include "matrices\mat1x12.h"

namespace TestEngine
{
  const static double PI = 3.14159265358979323846;
  const static unsigned UNSIGNED_MAX = UINT32_MAX;

  namespace Math
  {
    float Min(float a, float b);
    
    float Max(float a, float b);

    int   Sign(float value);

    float Abs(float value);
  }

}

typedef TestEngine::Math::Vec3 Vec3;
typedef TestEngine::Math::Vec4 Vec4;
typedef TestEngine::Math::Vec12 Vec12;

typedef TestEngine::Math::Mat3 Mat3;
typedef TestEngine::Math::Mat4 Mat4;
typedef TestEngine::Math::Mat1x12 Mat1x12;