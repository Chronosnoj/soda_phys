/*****************************************************************
Filename: Core.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once
#include "../System/System.h"
#include <vector>

namespace TestEngine
{
  namespace Base
  {
    class Core
    {
      public:
        Core();
        ~Core();

        void Initialize(float dt);

        void UpdateLoop();

        void AddSystem(System* system);
        void DestroySystems();

          //broadcast message to all systems
        void BroadcastToAll(Message& message);

      private:
        std::vector<System*> m_systems;

        float m_dt;
        bool  m_active;

    };


    extern Core* CORE;
  }
}