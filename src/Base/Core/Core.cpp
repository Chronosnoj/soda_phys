/*****************************************************************
Filename: Core.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "Core.h"

namespace TestEngine
{
  namespace Base
  {
    Core* CORE;

    Core::Core()
    {
      CORE = this;
      m_active = true;
    }

    Core::~Core()
    {

    }

    void Core::Initialize(float dt)
    {
      m_dt = dt;

      for (unsigned i = 0; i < m_systems.size(); ++i)
        m_systems[i]->Initialize();
    }

    void Core::UpdateLoop()
    {
      while (m_active)
        for (unsigned i = 0; i < m_systems.size(); ++i)
          m_systems[i]->Update(m_dt);
    }

    void Core::AddSystem(System* system)
    {
      m_systems.push_back(system);
    }


    void Core::DestroySystems()
    {
      unsigned size = m_systems.size();
      for (unsigned i = 0; i < size; ++i)
        delete m_systems[size - i - 1];
    }

    void Core::BroadcastToAll(Message& message)
    {
      if (message.m_type == MessageType::Quit)
        m_active = false;

      unsigned size = m_systems.size();

      for (unsigned i = 0; i < size; ++i)
        m_systems[i]->SendMessage(message);
    }
  }

}