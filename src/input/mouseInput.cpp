/*****************************************************************
Filename: mouseInput.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "input.h"

namespace TestEngine
{
  namespace InputGL
  {
    void InputManager::CheckMouse()
    {

      //these are required for imgui to detect input
      glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
      glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE);
      glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
      glfwGetCursorPos(window, &x, &y);

      if (!focused)
        return;

      if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) != GLFW_PRESS
        && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) != GLFW_PRESS
        && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) != GLFW_PRESS)
        mouseActive = false;

      if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
      {
        mouseClick();
      }

      if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS)
      {
        mouseScrollPress();
      }

      if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
      {
        mouseRotate();
      }

      if (scrollz != 0)
      {
        camera->Forward(static_cast<float>(scrollz));

        if (bodyFocused)
          camera->Look(static_cast<float>(scrollz));

        scrollz = 0;
      }
    }

    void InputManager::mouseClick()
    {
      if (!focused)
        return;

      if (!mouseActive)
      {
        mouseActive = true;
        glfwGetCursorPos(window, &x, &y);
        Vec3 origin(camera->eye.x, camera->eye.y, camera->eye.z);

        float xAdjusted = (static_cast<float>(x) / 512.0f) - 1.0f;
        float yAdjusted = (static_cast<float>(y) / 384.0f) - 1.0f;
        Vec4 finalPoint(xAdjusted, yAdjusted, 0.0, 1.0f);

        glm::mat4 unproj = camera->CameraProj();

        Mat4 unProject;
        unProject.m_matrix[0][0] = unproj[0][0];
        unProject.m_matrix[0][1] = unproj[0][1];
        unProject.m_matrix[0][2] = unproj[0][2];
        unProject.m_matrix[0][3] = unproj[0][3];
        unProject.m_matrix[1][0] = unproj[1][0];
        unProject.m_matrix[1][1] = unproj[1][1];
        unProject.m_matrix[1][2] = unproj[1][2];
        unProject.m_matrix[1][3] = unproj[1][3];
        unProject.m_matrix[2][0] = unproj[2][0];
        unProject.m_matrix[2][1] = unproj[2][1];
        unProject.m_matrix[2][2] = unproj[2][2];
        unProject.m_matrix[2][3] = unproj[2][3];
        unProject.m_matrix[3][0] = unproj[3][0];
        unProject.m_matrix[3][1] = unproj[3][1];
        unProject.m_matrix[3][2] = unproj[3][2];
        unProject.m_matrix[3][3] = unproj[3][3];

        unProject.Invert();

        finalPoint = unProject * finalPoint;
        Vec3 finalDir = (Vec3(camera->right.x, camera->right.y, camera->right.z) * finalPoint.x)
          + (Vec3(camera->up.x, camera->up.y, camera->up.z) * -finalPoint.y)
          + (Vec3(camera->back.x, camera->back.y, camera->back.z) * finalPoint.z);
        finalDir.Normalize();

        if (phySpace->GetActiveBody() != UNSIGNED_MAX)
        {
          phySpace->GetBodyFromRefID(phySpace->GetActiveBody())->SetActive(false);
          phySpace->SetNoActiveBody();
          bodyFocused = false;
        }
          
        RaycastResult result = phySpace->CastAndStoreRay(origin, finalDir);

        PhysicsGL::Body* body;
        switch (result.collider)
        {
        case colliderType::eBOX:
          body = phySpace->GetBox(result.objectID);
          break;

        case colliderType::eSPHERE:
          body = phySpace->GetSphere(result.objectID);
          break;

        case colliderType::eCAPSULE:
          body = phySpace->GetCapsule(result.objectID);
          break;

        default:
          break;
        }


        
      }
    }

    void InputManager::mouseScrollPress()
    {
      if (!mouseActive)
      {
        mouseActive = true;
        glfwGetCursorPos(window, &x, &y);
        startX = x;
        startY = y;
      }

      else
      {
        double xChange, yChange;
        glfwGetCursorPos(window, &xChange, &yChange);
        float diffX = static_cast<float>((xChange - startX) / (camera->halfScreenWidth));
        float diffY = static_cast<float>((yChange - startY) / (camera->halfScreenHeight));

        if (!focused)
          return;

        camera->Right(diffX);
        camera->Up(-diffY);
      }
    }

    void InputManager::mouseRotate()
    {
      if (!mouseActive)
      {
        mouseActive = true;
        glfwGetCursorPos(window, &x, &y);
        startX = x;
        startY = y;
      }

      else
      {
        double xChange, yChange;
        glfwGetCursorPos(window, &xChange, &yChange);

        if (!focused)
          return;

        float diffX = static_cast<float>((xChange - startX) / (camera->halfScreenWidth));
        float diffY = static_cast<float>((yChange - startY) / (camera->halfScreenHeight));

        Vec4 currLook = camera->lookAtPoint;

        Vec4 camVec = camera->eye - camera->lookAtPoint;
        float dist = camVec.Length();

        if (abs(diffX) > abs(diffY))
          camera->RotateRight(diffX);
        else
          camera->Up(-diffY);

        camera->lookAtPoint = currLook;
        Vec4 newEye = camera->eye - camera->lookAtPoint;
        newEye.Normalize();

        Vec4 newDir = newEye * dist;
        newDir.w = 1.0f;
        camera->eye = newDir + camera->lookAtPoint;

        camera->Reset();
      }
    }

    void mouseScroll(GLFWwindow * window, double xOffset, double yOffset)
    {
      scrollz = yOffset;
    }
  }
}