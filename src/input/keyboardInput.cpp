/*****************************************************************
Filename: keyboardInput.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "input.h"

namespace TestEngine
{
  namespace InputGL
  {
    void InputManager::CheckKeys()
    {

      if (!focused)
      {
        //glfwGetKey(window, );
        return;
      }


      //key presses
      if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
      {
        camera->Forward(1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
      {
        camera->Forward(-1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
      {
        camera->Right(-1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
      {
        camera->Right(1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
      {
        camera->Up(-1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
      {
        camera->Up(1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
      {
        camera->Yaw(.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
      {
        camera->Yaw(-.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
      {
        camera->Pitch(.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
      {
        camera->Pitch(-.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
      {
        camera->Roll(.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
      {
        camera->Roll(-.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
      {
        camera->Zoom(.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
      {
        camera->Zoom(-.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_SEMICOLON) == GLFW_PRESS)
      {
        camera->Look(1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_APOSTROPHE) == GLFW_PRESS)
      {
        camera->Look(-1.0f);
      }

      if (glfwGetKey(window, GLFW_KEY_TAB) == GLFW_PRESS)
      {
        setWireFrame();
      }

      if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
      {
        setDrawAabbs();
      }

      if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
      {
        setDrawConstraints();
      }

      if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS)
      {
        if (phySpace->BodyActive())
        {
          camera->lookAtPoint = phySpace->GetBodyFromRefID(phySpace->GetActiveBody())->GetPosition();
          bodyFocused = true;
        }
          
      }

      if (glfwGetKey(window, GLFW_KEY_GRAVE_ACCENT) == GLFW_PRESS)
      {
        setSimplexDraw();
      }

      if (glfwGetKey(window, GLFW_KEY_DELETE) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          phySpace->DeleteBody();
          disableContinuousPress = true;
        }
      }

      if (glfwGetKey(window, GLFW_KEY_TAB) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_V) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_0) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_GRAVE_ACCENT) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_9) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_P) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_N) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_M) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_B) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_Y) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_7) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_4) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_5) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_6) == GLFW_RELEASE
        && glfwGetKey(window, GLFW_KEY_DELETE) == GLFW_RELEASE
        )
      {
        disableContinuousPress = false;
      }

      if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          pausePhysics = !pausePhysics;
          disableContinuousPress = true;
        }

      }

      if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          disableContinuousPress = true;
          float dt = phySpace->GetDT();
          phySpace->SetDT(-dt);
        }
      }

      if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          disableContinuousPress = true;
          phySpace->SwitchAxis();
        }
      }

      if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS)
      {
        phySpace->ModifyBodyDimensions(.1f);
      }

      if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS)
      {
        phySpace->ModifyBodyDimensions(-.1f);
      }

      if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
      {
        setDrawVecs();
      }

      if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
      {
        setDrawNorms();
      }

      //create constraints
      if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          ConstraintInfo infoConstraint;
          
          infoConstraint.createBodies = true;

          BodyInfo infoA;
          infoA.collider = MemoryManager::eBOX;
          infoA.position = Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z);
          infoA.dimensions[0] = 1.0f;
          infoA.dimensions[1] = 1.0f;
          infoA.dimensions[2] = 1.0f;
          infoA.orientation = Mat3();
          infoA.density = 1.0f;
          infoA.rotationLock = false;
          infoA.staticObj = true;
          infoA.deleted = false;

          BodyInfo infoB;
          infoB.collider = MemoryManager::eBOX;
          infoB.position = Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y - 6, camera->lookAtPoint.z);
          infoB.dimensions[0] = 1.0f;
          infoB.dimensions[1] = 1.0f;
          infoB.dimensions[2] = 1.0f;
          infoB.orientation = Mat3();
          infoB.density = 1.0f;
          infoB.rotationLock = false;
          infoB.staticObj = false;
          infoB.deleted = false;
          
          infoConstraint.createBodies = true;
          infoConstraint.bodyA = infoA;
          infoConstraint.bodyB = infoB;
          infoConstraint.typeA = infoA.collider;
          infoConstraint.typeB = infoB.collider;
          infoConstraint.connectionPoint[0] = Vec3(0, -1, 0);
          infoConstraint.connectionPoint[1] = Vec3(-.2f, 1, 0);
          infoConstraint.distance         = 5.0f;
          infoConstraint.stiffness        = 1.5f;
          infoConstraint.damping          = 7.995f;
          infoConstraint.rotateA = false;
          infoConstraint.rotateB = false;
          infoConstraint.type = MemoryManager::constraintType::eROPE;
          infoConstraint.deleted = false;
          phySpace->AddConstraint(infoConstraint);

          disableContinuousPress = true;
        }
      }

      if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          ConstraintInfo infoConstraint;

          infoConstraint.createBodies = true;

          BodyInfo infoA;
          infoA.collider = MemoryManager::eBOX;
          infoA.position = Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z);
          infoA.dimensions[0] = 1.0f;
          infoA.dimensions[1] = 1.0f;
          infoA.dimensions[2] = 1.0f;
          infoA.orientation = Mat3();
          infoA.density = 1.0f;
          infoA.rotationLock = false;
          infoA.staticObj = true;
          infoA.deleted = false;

          BodyInfo infoB;
          infoB.collider = MemoryManager::eBOX;
          infoB.position = Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y - 7, camera->lookAtPoint.z);
          infoB.dimensions[0] = 1.0f;
          infoB.dimensions[1] = 1.0f;
          infoB.dimensions[2] = 1.0f;
          infoB.orientation = Mat3();
          infoB.density = 1.0f;
          infoB.rotationLock = false;
          infoB.staticObj = false;
          infoB.deleted = false;

          infoConstraint.createBodies = true;
          infoConstraint.bodyA = infoA;
          infoConstraint.bodyB = infoB;
          infoConstraint.typeA = infoA.collider;
          infoConstraint.typeB = infoB.collider;
          infoConstraint.connectionPoint[0] = Vec3(0, -1, 0);
          infoConstraint.connectionPoint[1] = Vec3(0, 1, 0);
          infoConstraint.distance = 5.0f;
          infoConstraint.stiffness = 1.5f;
          infoConstraint.damping = 7.995f;
          infoConstraint.rotateA = false;
          infoConstraint.rotateB = false;
          infoConstraint.type = MemoryManager::constraintType::eROD;
          infoConstraint.deleted = false;
          phySpace->AddConstraint(infoConstraint);

          disableContinuousPress = true;
        }
      }

      if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          ConstraintInfo infoConstraint;

          infoConstraint.createBodies = true;

          BodyInfo infoA;
          infoA.collider = MemoryManager::eBOX;
          infoA.position = Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z);
          infoA.dimensions[0] = 1.0f;
          infoA.dimensions[1] = 1.0f;
          infoA.dimensions[2] = 1.0f;
          infoA.orientation = Mat3();
          infoA.density = 1.0f;
          infoA.rotationLock = false;
          infoA.staticObj = true;
          infoA.deleted = false;

          BodyInfo infoB;
          infoB.collider = MemoryManager::eBOX;
          infoB.position = Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y - 4, camera->lookAtPoint.z);
          infoB.dimensions[0] = 1.0f;
          infoB.dimensions[1] = 1.0f;
          infoB.dimensions[2] = 1.0f;
          infoB.orientation = Mat3();
          infoB.density = 1.0f;
          infoB.rotationLock = false;
          infoB.staticObj = false;
          infoB.deleted = false;

          infoConstraint.bodyA = infoA;
          infoConstraint.bodyB = infoB;
          infoConstraint.typeA = infoA.collider;
          infoConstraint.typeB = infoB.collider;
          infoConstraint.connectionPoint[0] = Vec3(0, -1, 0);
          infoConstraint.connectionPoint[1] = Vec3(-.2f, 1, 0);
          infoConstraint.distance = 5.0f;
          infoConstraint.stiffness = 1.5f;
          infoConstraint.damping = 7.995f;
          infoConstraint.rotateA = false;
          infoConstraint.rotateB = false;
          infoConstraint.type = MemoryManager::constraintType::eSOFT;
          infoConstraint.deleted = false;
          phySpace->AddConstraint(infoConstraint);

          disableContinuousPress = true;
        }
      }

      //create body
      if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          phySpace->SetBodyInfo(MemoryManager::eBOX,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(15.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          disableContinuousPress = true;
        }
      }

      if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          phySpace->SetBodyInfo(MemoryManager::eBOX,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(1.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          disableContinuousPress = true;
        }
      }

      if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          phySpace->SetBodyInfo(MemoryManager::eSPHERE,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(1.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          disableContinuousPress = true;
        }
      }

      if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          phySpace->SetBodyInfo(MemoryManager::eCAPSULE,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(1.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          disableContinuousPress = true;
        }
      }

      //select active body
      if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          phySpace->GetNextBody();
          disableContinuousPress = true;
        }
      }

      //deselect body
      if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS)
      {
        phySpace->SetNoActiveBody();
      }

      //move bodies
      if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
      {
        phySpace->MoveBody(Vec3(0, 1, 0));
      }

      if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
      {
        phySpace->MoveBody(Vec3(0, -1, 0));
      }

      if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
      {
        phySpace->MoveBody(Vec3(-1, 0, 0));
      }

      if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
      {
        phySpace->MoveBody(Vec3(1, 0, 0));
      }

      if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
      {
        phySpace->MoveBody(Vec3(0, 0, -1));
      }

      if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
      {
        phySpace->MoveBody(Vec3(0, 0, 1));
      }

      //rotation of bodies
      if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS)
      {
        phySpace->RotateBody(-.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS)
      {
        phySpace->RotateBody(.01f);
      }

      if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS)
      {
        if (!disableContinuousPress)
        {
          advanceFrame = true;
          disableContinuousPress = true;
        }
      }
    }

    bool InputManager::CheckEscape()
    {
      if (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
        return true;

      return false;
    }

    bool InputManager::CheckSpace()
    {
      if (!phySpace)
        return false;

      return pausePhysics;
    }

  }
}