/*****************************************************************
Filename: editorInput.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "input.h"
#include "../Base/Type/TypeInfo.h"
#include "../Base/Manager/Manager.h"
#include "../Base/RegisteredTypes/RegisteredTypes.h"
#include "../Base/Serialization/Serializer.h"
#include "../Base/Serialization/Space/SerializeSpace.h"
#include "../Base/Serialization/Serializer.h"
#include "../Base/Serialization/Space/SerializeSpace.h"

namespace TestEngine
{
  namespace InputGL
  {
    void InputManager::LoadSpace(const char * fileName)
    {

      Base::File file;

      std::string fileLocation = "spaces/" + std::string(fileName);
      if (fileLocation.find(".txt") == std::string::npos)
        fileLocation += ".txt";

      file.Open(fileLocation.c_str(), TestEngine::Base::FileAccess::Read);

      if (file.IsOpen())
      {
        delete phySpace;
        phySpace = new PhysicsGL::Space();
        Base::DeserializeSpace(&file, phySpace);
        phySpace->SetJobManager(*jobmanager);
      }

      file.Close();
    }

    void InputManager::SaveSpace(const char * fileName)
    {
      Base::File file;

      std::string fileLocation = "spaces/" + std::string(fileName);
      if (fileLocation.find(".txt") == std::string::npos)
        fileLocation += ".txt";

      file.Open(fileLocation.c_str(), TestEngine::Base::FileAccess::Write);

      if (file.IsOpen())
        Base::SerializeSpace(&file, phySpace);

      file.Close();
    }

    void InputManager::AddObject(TestEngine::colliderType collider)
    {
      switch (collider)
      {
        case TestEngine::colliderType::eBOX:
          phySpace->SetBodyInfo(MemoryManager::eBOX,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(1.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          break;

        case TestEngine::colliderType::eSPHERE:
          phySpace->SetBodyInfo(MemoryManager::eSPHERE,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(1.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          break;

        case TestEngine::colliderType::eCAPSULE:
          phySpace->SetBodyInfo(MemoryManager::eCAPSULE,
            Vec3(camera->lookAtPoint.x, camera->lookAtPoint.y, camera->lookAtPoint.z),
            Vec3(1.0f, 1.0f, 1.0f), Mat3(), 1.0f, false, false);
          phySpace->AddBody();
          break;

      }
    }
    void InputManager::ToggleThreading(bool island)
    {
      phySpace->SetIslanding(island);
    }
    float InputManager::GetFPS()
    {
      float totalFPS = 0;
      for (unsigned i = 0; i < FPSBuffer; ++i)
        totalFPS += frameRate[i];

      return totalFPS / static_cast<float>(FPSBuffer);
    }
    void InputManager::SetFPS(double deltaTime)
    {
      frameRate[currIter] = 1.0f / static_cast<float>(deltaTime);

      if (frameRate[currIter] > 1000)
        frameRate[currIter] = 0;

      ++currIter;
      if (currIter >= FPSBuffer)
        currIter = 0;
    }
  }
}