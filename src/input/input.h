/*****************************************************************
Filename: input.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../graphics/camera/camera.h"
#include "../physics/space/space.h"

namespace TestEngine
{
	namespace InputGL
	{
    const static unsigned FPSBuffer = 60;

		class InputManager
		{
			public:
			
				InputManager(GLFWwindow* window, Graphics::Camera* camera, PhysicsGL::Space* space);

				void    CheckKeys();
        void    CheckMouse();
				bool    CheckEscape();
				bool    CheckSpace();
        bool    CheckAdvance();

        void    PauseSpace()                                { pausePhysics = !pausePhysics; }
        
        void    StopAdvance()                               { advanceFrame = false; }
        bool    IsFocused()                                 { return focused; }
        double  GetX()                                      { return x; }
        double  GetY()                                      { return y; }
        
        void    SetFocused(bool focus)                      { focused = focus; }

          //systems tied to input
        Graphics::Camera* GetCamera()                       { return camera; }
        PhysicsGL::Space* GetSpace()                        { return phySpace; }
        
          //editor access

          //islanding for editor
        bool*             GetIslanding()                    { return &islanding; }

        void LoadSpace(const char* fileName);
        void SaveSpace(const char* fileName);

        void AddObject(TestEngine::colliderType collider);
        void ToggleThreading(bool island);
        void AddJobManager(Base::JobManager* newJobManager) { jobmanager = newJobManager; }

        float GetFPS();
        void  SetFPS(double deltaTime);

			private:
				  //helpers
				void setWireFrame();
        void setDrawAabbs();
				void setSimplexDraw();
				void setDrawVecs();
        void setDrawNorms();
        void setDrawConstraints();
        void mouseClick();
				void mouseScrollPress();
				void mouseRotate();

			      //members
				GLFWwindow* window;
				Graphics::Camera* camera;
				bool disableContinuousPress;

          //focus
        bool focused;
        bool bodyFocused;

				  //physics stuff
				bool pausePhysics;
        bool advanceFrame;
				PhysicsGL::Space* phySpace;
        
          //editor
        bool constraintAdd = false;

          //thread management
        Base::JobManager* jobmanager;
        bool islanding;
        float frameRate[FPSBuffer];
        unsigned currIter;

				  //mouse movement
				bool mouseActive;
				double x, y;

          //stored mouse movement variables
        double startX, startY;
		};

		static double scrollz;
		void mouseScroll(GLFWwindow* window, double xOffset, double yOffset);
	}

	
	
}