/*****************************************************************
Filename: input.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "input.h"

namespace TestEngine
{
	namespace InputGL
	{
		InputManager::InputManager(GLFWwindow* win, Graphics::Camera* cam, PhysicsGL::Space* space) : window(win), camera(cam), phySpace(space), disableContinuousPress(false)
		{
			  //set input
			glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
			disableContinuousPress = false;

			pausePhysics = true;
      advanceFrame = false;

			mouseActive = false;
      focused = true;

      islanding = false;
      
      for (unsigned i = 0; i < FPSBuffer; ++i)
        frameRate[i] = 0.0f;

      currIter = 0;

			scrollz = 0;
			glfwSetScrollCallback(window, TestEngine::InputGL::mouseScroll);
		}

    bool InputManager::CheckAdvance()
    {
      if (!phySpace)
        return false;

      return advanceFrame;
    }

    void InputManager::setWireFrame()
		{
			if (!disableContinuousPress)
			{
				camera->SetWireFrame();
				disableContinuousPress = true;
			}
		}

    void InputManager::setDrawAabbs()
    {
      if (!disableContinuousPress)
      {
        camera->SetDrawAabbs();
        disableContinuousPress = true;
      }
    }

		void InputManager::setSimplexDraw()
		{
			if (!disableContinuousPress)
			{
				camera->SetSimplexDraw();
				disableContinuousPress = true;
			}
		}

		void InputManager::setDrawVecs()
		{
			if (!disableContinuousPress)
			{
				camera->SetDrawVecs();
				disableContinuousPress = true;
			}
		}

    void InputManager::setDrawNorms()
    {
      if (!disableContinuousPress)
      {
        camera->SetDrawNorms();
        disableContinuousPress = true;
      }
    }

    void InputManager::setDrawConstraints()
    {
      if (!disableContinuousPress)
      {
        camera->SetDrawConstraints();
        disableContinuousPress = true;
      }
    }

	}
}