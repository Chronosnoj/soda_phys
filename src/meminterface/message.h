/*****************************************************************
Filename: message.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "../Base/math/mathconvert.h"

namespace TestEngine
{
	namespace MemoryManager
	{
		enum colliderType
		{
			eBOX,
			eSPHERE,
      eCAPSULE,
			eCOLLIDERS,
			eNONE
		};

    enum constraintType
    {
      eROD,
      eROPE,
      eSOFT,
      eNUMBER
    };

		struct BodyInfo
		{
			BodyInfo() {}
			  //basic info
			Vec3 position;
			Mat3 orientation;
			Vec3 scale;
      Vec3 offset;
      Vec3 velocity;
      Vec3 angularVelocity;

			  //rigidBody info
			float density;
			bool staticObj;
			bool rotationLock;


			  //collider info
			colliderType collider;
			bool ghost;

			union
			{

				float dimensions[3];
				float radius;

			};

      bool deleted;

		};


    struct ConstraintInfo
    {
      ConstraintInfo() {}
      ConstraintInfo(const ConstraintInfo& rhs) { *this = rhs; }

      ConstraintInfo& operator=(const ConstraintInfo& rhs)
      {
        createBodies = rhs.createBodies;
        bodyA = rhs.bodyA;
        idA = rhs.idA;
        bodyB = rhs.bodyB;
        idB = rhs.idB;
        typeA = rhs.typeA;
        typeB = rhs.typeB;
        worldSpaceConnectionPointA = rhs.worldSpaceConnectionPointA;
        worldSpaceConnectionPointB = rhs.worldSpaceConnectionPointB;
        connectionPointA = rhs.connectionPointA;
        connectionPointB = rhs.connectionPointB;
        distance = rhs.distance;
        stiffness = rhs.stiffness;
        damping = rhs.damping;
        rotateA = rhs.rotateA;
        rotateB = rhs.rotateB;
        type = rhs.type;
        deleted = rhs.deleted;

        return *this;
      }

        //for serialization
      bool operator==(const ConstraintInfo& rhs) const { return false;}


      bool createBodies;

      //Bodies bodies[2];

      BodyInfo bodyA;
      unsigned idA;

      BodyInfo bodyB;
      unsigned idB;

      //colliderType type[2];

      colliderType typeA;
      colliderType typeB;

      union
      {
        Vec3 worldSpaceConnectionPoint[2];
        
        struct
        {
          Vec3 worldSpaceConnectionPointA;
          Vec3 worldSpaceConnectionPointB;
        };
      };
      

      union
      {
        Vec3 connectionPoint[2];

        struct
        {
          Vec3 connectionPointA;
          Vec3 connectionPointB;
        };  
      };
      

      float distance;
      float stiffness = 100.0f;
      float damping = 10000.0f;

      bool rotateA = false;
      bool rotateB = false;

      constraintType type = eROPE;

      bool deleted = false;
    };

    struct RaycastResult
    {
      bool colliding = false;

      colliderType collider = colliderType::eNONE;
      unsigned objectID = UINT32_MAX;

      float depth;
      Vec3 collisionPoint;  //collision point on object in local space
      Vec3 worldCollisionPoint;
    };


    struct DeletionInfo
    {
      colliderType collider;
      unsigned     id;
    };
	}

	typedef MemoryManager::colliderType   colliderType;
  typedef MemoryManager::BodyInfo       BodyInfo;
  typedef MemoryManager::ConstraintInfo ConstraintInfo;
  typedef MemoryManager::RaycastResult  RaycastResult;
  typedef MemoryManager::DeletionInfo   DeletionInfo;
}
