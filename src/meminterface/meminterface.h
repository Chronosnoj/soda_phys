/*****************************************************************
Filename: meminterface.h
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#pragma once
#include "..\physics\collision\collisionpair\collisionpair.h"
#include "..\physics\collision\constraintpair\constraintpair.h"
#include "..\physics\body\aabb\AABB.h"
#include "..\Base\Threads\jobmanager.h"

#include <queue>

namespace TestEngine
{
	namespace MemoryManager
	{
    typedef TestEngine::PhysicsGL::Aabb Aabb;

    const static unsigned s_maxNumberOfIslands = 64;

		class MemInterface
		{
			public:
				typedef std::vector<PhysicsGL::Box> BoxList;
				typedef std::vector<PhysicsGL::Sphere> SphereList;
        typedef std::vector<PhysicsGL::Capsule> CapsuleList;
				typedef std::vector<PhysicsGL::CollisionPair> PairList;
        typedef std::vector<PhysicsGL::Aabb> AabbList;
        typedef std::vector<PhysicsGL::ConstraintPair> ConstraintList;

          //openlist typedefs
        typedef std::queue<unsigned> OpenBoxes;
        typedef std::queue<unsigned> OpenSpheres;
        typedef std::queue<unsigned> OpenCapsules;
        typedef std::queue<unsigned> OpenAabbs;
        typedef std::queue<unsigned> OpenConstraints;

        //aabbs added from deleted list - used by sap
        typedef std::queue<unsigned> AddAabbs;

        //islands
        typedef std::vector<PhysicsGL::CollisionPair> IslandList;

				MemInterface() {};
				~MemInterface();

				void Initialize();

				unsigned AddBody(BodyInfo& info);				
				unsigned AddPair(PhysicsGL::Body* A, PhysicsGL::Body* B);
        unsigned AddConstraint(PhysicsGL::Body* A, PhysicsGL::Body* B);

        void     DeleteBody(DeletionInfo& info);
        void     DeleteConstraint(unsigned id);

				inline BoxList& GetBoxList()		                  { return m_boxes; }
				inline SphereList& GetSphereList()	              { return m_spheres; }
        inline CapsuleList& GetCapsuleList()              { return m_capsules; }
				inline PairList& GetPairList()		                { return m_pairs; }
        inline AabbList& GetAabbList()                    { return m_aabbs; }
        inline ConstraintList& GetConstraintList()        { return m_constraints; }
        inline AddAabbs& GetAddedAabbs()                  { return m_addedAabbs; }

          //islands
        unsigned AddIslandPair(unsigned island, PhysicsGL::Body* A, PhysicsGL::Body* B);
        inline IslandList& GetIslandList(unsigned island) { return m_islands[island]; }

			private:
				unsigned AddBox(BodyInfo& info);
				unsigned AddSphere(BodyInfo& info);
        unsigned AddCapsule(BodyInfo& info);
        unsigned AddAabb(BodyInfo& info, Vec3& aabbDimensions, unsigned id);
				
        void     DeleteBox(DeletionInfo& info);
        void     DeleteSphere(DeletionInfo& info);
        void     DeleteCapsule(DeletionInfo& info);
        void     DeleteAabb(DeletionInfo& info);

				BoxList			    m_boxes;
				SphereList		  m_spheres;
        CapsuleList     m_capsules;
				PairList		    m_pairs;
        AabbList        m_aabbs;
        ConstraintList  m_constraints;

        OpenBoxes       m_openBoxes;
        OpenSpheres     m_openSpheres;
        OpenCapsules    m_openCapsules;
        OpenAabbs       m_openAabbs;
        OpenConstraints m_openConstraints;

          //islands
        IslandList      m_islands[s_maxNumberOfIslands];

        AddAabbs        m_addedAabbs;
		};

	}
}