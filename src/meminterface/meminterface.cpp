/*****************************************************************
Filename: meminterface.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/


#include "meminterface.h"
namespace TestEngine
{
	namespace MemoryManager
	{
		MemInterface::~MemInterface()
		{

		}

		void MemInterface::Initialize()
		{
			
		}

		unsigned MemInterface::AddBody(BodyInfo& info)
		{
			switch (info.collider)
			{
				case eBOX:
					return AddBox(info);

				case eSPHERE:
					return AddSphere(info);

        case eCAPSULE:
          return AddCapsule(info);
			}

			return -1;
		}

    unsigned MemInterface::AddPair(PhysicsGL::Body* A, PhysicsGL::Body* B)
    {
      m_pairs.push_back(PhysicsGL::CollisionPair(A, B));
      return m_pairs.size() - 1;
    }

    unsigned MemInterface::AddConstraint(PhysicsGL::Body* A, PhysicsGL::Body* B)
    {
      if (A == nullptr || B == nullptr)
        return -1;

      unsigned id;

      if (m_openConstraints.size() > 0)
      {
        id = m_openConstraints.front();
        m_constraints[id] = PhysicsGL::ConstraintPair(A, B);
        m_openConstraints.pop();
      }

      else
      {
        m_constraints.push_back(PhysicsGL::ConstraintPair(A, B));
        id = m_constraints.size() - 1;
      }
      
      return id;
    }

		
    unsigned MemInterface::AddIslandPair(unsigned island, PhysicsGL::Body * A, PhysicsGL::Body * B)
    {
      (m_islands[island]).push_back(PhysicsGL::CollisionPair(A, B));
      return (m_islands[island]).size() - 1;
    }

    unsigned MemInterface::AddBox(BodyInfo& info)
		{
			Box box;
			box.Initialize(info);
      Vec3 aabbDim = box.CalculateAABB();

      unsigned id;

      if (m_openBoxes.size() > 0)
      {
        id = m_openBoxes.front();
        box.SetID(id);
        m_boxes[id] = box;
        m_openBoxes.pop();
      }

      else
      {
        id = m_boxes.size();
        box.SetID(id);
        m_boxes.push_back(box);
      }

      unsigned aabb = AddAabb(info, aabbDim, id);
      m_boxes[id].SetAabbID(aabb);

      return id;
		}

		unsigned MemInterface::AddSphere(BodyInfo& info)
		{
			Sphere sphere;
			sphere.Initialize(info);
      Vec3 aabbDim = sphere.CalculateAABB();
      
      unsigned id;

      if (m_openSpheres.size() > 0)
      {
        id = m_openSpheres.front();
        sphere.SetID(id);
        m_spheres[id] = sphere;
        m_openSpheres.pop();
      }

      else
      {
        id = m_spheres.size();
        sphere.SetID(id);
        m_spheres.push_back(sphere);
      }

      unsigned aabb = AddAabb(info, aabbDim, id);
      m_spheres[id].SetAabbID(aabb);

      return id;
		}

    unsigned MemInterface::AddCapsule(BodyInfo& info)
    {
      Capsule capsule;
      capsule.Initialize(info);
      Vec3 aabbDim = capsule.CalculateAABB();

      unsigned id; 

      if (m_openCapsules.size() > 0)
      {
        id = m_openCapsules.front();
        capsule.SetID(id);
        m_capsules[id] = capsule;
        m_openCapsules.pop();
      }

      else
      {
        id = m_capsules.size();
        capsule.SetID(id);
        m_capsules.push_back(capsule);
      }
      
      unsigned aabb = AddAabb(info, aabbDim, id);
      m_capsules[id].SetAabbID(aabb);

      return id;
    }

    unsigned MemInterface::AddAabb(BodyInfo& info, Vec3& aabbDimensions, unsigned id)
    {
      Aabb aabb;
      aabb.Initialize(info, aabbDimensions, id);
      aabb.SetStatic(info.staticObj);

      if (m_openAabbs.size() > 0)
      {
        aabb.SetID(m_openAabbs.front());
        m_addedAabbs.push(m_openAabbs.front());
        m_aabbs[m_openAabbs.front()] = aabb;
        m_openAabbs.pop();
        return aabb.GetID();
      }

      aabb.SetID(m_aabbs.size());
      m_aabbs.push_back(aabb);
      return m_aabbs.size() - 1;

    }

    void MemInterface::DeleteBox(DeletionInfo & info)
    {
      m_boxes[info.id].SetDeletion(true);
      m_openBoxes.push(info.id);
    }

    void MemInterface::DeleteSphere(DeletionInfo & info)
    {
      m_spheres[info.id].SetDeletion(true);
      m_openSpheres.push(info.id);
    }

    void MemInterface::DeleteCapsule(DeletionInfo & info)
    {
      m_capsules[info.id].SetDeletion(true);
      m_openCapsules.push(info.id);
    }

    void MemInterface::DeleteAabb(DeletionInfo & info)
    {
      m_aabbs[info.id].SetDeletion(true);
      m_openAabbs.push(info.id);
    }

    void MemInterface::DeleteConstraint(unsigned id)
    {
      m_constraints[id].SetDeletedFlag(true);
      m_constraints[id].SetToDeleteFlag(false);
      m_openConstraints.push(id);
    }

    void MemInterface::DeleteBody(DeletionInfo& info)
    {
      switch (info.collider)
      {
      case eBOX:
        return DeleteBox(info);

      case eSPHERE:
        return DeleteSphere(info);

      case eCAPSULE:
        return DeleteCapsule(info);
      }

      return;
    }

	}

}


