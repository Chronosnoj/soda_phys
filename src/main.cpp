/*****************************************************************
Filename: main.cpp
Project: TestEngine
Author(s): Jon Sourbeer
All content � 2017 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "Base/Manager/Manager.h"
#include "Base/RegisteredTypes/RegisteredTypes.h"
#include "graphics\graphics.h"
#include "Base\Threads\threadmanager.h"

typedef TestEngine::Base::Manager   Manager;
typedef TestEngine::Base::Variable  Variable;

int main(void)
{
    //serialization
  Manager* manager = Manager::Get();
  TestEngine::Base::RegisterTypes();

    //graphics system
  TestEngine::Graphics::GraphicsGL graphics;
  if (graphics.Initialize() == -1)
    return -1;

    //threadmanager
  TestEngine::Base::ThreadManager threadManager;

    //create space
  TestEngine::PhysicsGL::Space* space = new TestEngine::PhysicsGL::Space;
  space->Initialize(1.0f / 60.0f);
  space->SetJobManager(*(threadManager.GetJobManager()));

    //create input manager and editor
  TestEngine::InputGL::InputManager input(graphics.window, graphics.camera, space);
  TestEngine::SODAEditor::Editor editor(input);

    //create threads
  std::thread thread0(TestEngine::Base::ThreadManager::ThreadFunction, &input, &threadManager, 0);
  std::thread thread1(TestEngine::Base::ThreadManager::ThreadFunction, &input, &threadManager, 1);
  std::thread thread2(TestEngine::Base::ThreadManager::ThreadFunction, &input, &threadManager, 2);
  std::thread thread3(TestEngine::Base::ThreadManager::ThreadFunction, &input, &threadManager, 3);

    //assign systems
  threadManager.Initialize();
  threadManager.SetInputManager(input);
  threadManager.SetThread(&thread0, 0);
  threadManager.SetThread(&thread1, 1);
  threadManager.SetThread(&thread2, 2);
  threadManager.SetThread(&thread3, 3);

  graphics.editor = &editor;
  graphics.m_result = space->GetRaycastResult();
  graphics.m_constraint = space->GetConstraintInfo();

  double currTime = 0;
  double prevTime;

    //display DigiPen logo
  graphics.DisplaySplash(input.GetSpace()->GetMemManager());
  
    //main loop
  double deltaTime = 0;
  currTime = glfwGetTime();
  prevTime = currTime;

  do
  {
    if (threadManager.CheckThreads())
    {
      if (threadManager.GetJobManager()->m_jobQueue.empty())
      {
          //FPS
        prevTime = currTime;
        currTime = glfwGetTime();

        input.SetFPS(currTime - prevTime);
        graphics.Update(input.GetSpace()->GetMemManager());
      }
        
      input.GetSpace()->UpdateSpace();
        

      if (input.CheckSpace())
        input.GetSpace()->RunStep();

      else
      {
        if (input.CheckAdvance())
        {
          input.GetSpace()->RunStep();
          input.StopAdvance();
        }
      }

      input.GetSpace()->FinishStep();

      input.CheckKeys();
      input.CheckMouse();
    }
    
  } while (input.CheckEscape());
  
    //clean up threads
  thread0.join();
  thread1.join();
  thread2.join();
  thread3.join();

  return 0;
}