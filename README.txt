README

TestEngine developed by Jonathan Sourbeer 2017
All rights and content belong to DigiPen

To Run:
  Double click TestEngine.exe at TestEngine_EXE/TestEngine.exe - ensure that msvcp140.dll is
  present within the directory or available through the path (available from Microsoft).  
  
  The .exe has been confirmed to work on Edison computer DIT2328US.

To Compile:
  TestEngine requires MS platform toolset v140 to compile, present in Visual Studio 2015.
  Alternatively, the v140 toolset can be installed in prior versions to ensure compatibility.
  If VS2015 is not the default program, simply right click the solution and open with VS2015
  to ensure v140 is present.
  
  Ensure that the program is built using x86 to ensure compatibility with present GLEW libraries
  for OpenGL.  The physics system and engine can run in x64 if disconnected from GLEW and GLFW.
  
  The program will build in Visual Studio without other settings being adjusted.

Notes: Please report bugs to j.sourbeer@digipen.edu
  As with any islanding system, different scenarios impact the effectiveness of the
  islanding system.  The system does not currently sort on the y-axis, so large stacks
  and vertical groupings will all be on one island.  Overall engine speed will be impacted accordingly.
  
Controls:
GUI:  Open Drop Down Bars to Interact
      Vec3 sets are ordered in X, Y, Z
     
      Available Drop Downs:
      Space Controls  - Opens the following 3 menus  
     
          Load/Save Space - Type in File Name - do not include '.txt',
                              capitalization does not matter
                          - Load Button will load file if it exists
                          - Save Button will create file if it doesn't exist
                             or rewrite it if it does
           
                          - Available files:
                              - mainspace, mainspace2... mainspace16
                              - mainspace5 - most useful for islanding tests
                              - mainspace6 - another version of islanding
                              - mainspace15 - useful for constraints
                              - mainspace16 - chained constraints

          Space Attributes  - Pause Simulation button will pause/resume the space
                              
          Add Objects To Space  - Add Object Menu
                                - Can add the following objects at 
                                    Camera LookAt Position:
                                      - Box
                                      - Capsule
                                      - Sphere
      
      Camera          - Position (Rotation controlled via keyboard/mouse - see below)
      
      Selected Object - Will only open if object selected
                      - Displays body information
                      - Density is changed when 'Update Density' button is pressed
                      - Position, Orientation, Velocity, and Angular Velocity are 
                          all modifiable
                      - Static button makes an object static, but has known 
                          issues when object is already colliding
                      - Drag button activates the 'Drag Multiplier' field.  This field
                        is multiplied times object velocity every frame.  Values below 1
                        will cause the object to slow over time (lower values produce a
                        greater drag force).  Values above 1 will cause the object to accelerate.
      
      Constraint Manager - handles the 2 menus detailed below
      
          Constraints - Provides a list of all constraints currently active within the simulation.
                        Each constraint is listed by its ID, plus the two engaged bodies.
                        The Delete button will remove that constraint, while the Modify button
                        will highlight the constraint in green and provide boxes to modify
                        the distance, damping and stiffness of the constraint, and radio buttons
                        to adjust the constraint's type (see next section).
                        
          Constraint Adder - adds constraints to the simulation.
                       
                      - Add Constraint will start the process, allowing objects to be added once
                        at least one body is selected.  If add constraint is pushed while a body is
                        selected, that body will automatically be selected as the first body in the
                        new constraint
                      - Attach Body (1 & 2) - these buttons will select the objects that are part of the
                        new constraint.  This selection will preserve the exact position of the contact point
                        on the object selected (blue dot when raycasting).  This point will be used for the 
                        constraint.  Once an object is 'attached' its Id and Type will display in the menu.  
                        These bodies can be adjusted at any time before the constraint is completed.
                        Only after two bodies have been attached will the next set of buttons become available.
                      - Type radio buttons - allows selection of the three types of available constraints.
                          - Rod - a solid connection that does not allow the points on each object
                                  change their relative distance
                          - Rope - a connection that simulates ropes, not allowing the objects to drift apart,
                                   but allowing them to come closer together
                          - Soft - simulates a variety of systems, including springs or bungee cables,
                                   depending on damping and stiffness used.
                      - Distance field  - sets the distance of the constraint (used for testing the
                                          above types) - the distance between the two points is listed 
                                          directed above in the 'Distance Between Connections' text field.
                      - Stiffness field - sets the stiffness of the constraint (most important for soft 
                                          constraints)
                      - Damping field   - sets the damping constant of the constraint (mostly for soft 
                                          constraints)                      
                      - Easy Constraint - A simple method for generating constraints.  Will keep the stiffness 
                                          and damping, but will replace the distance and contact points with 
                                          the closest point (center of a side on a box, or middle of 
                                          the capsule) on each object and the distance between them.  Can be 
                                          used to quickly generate precise constraints.  Adds the constraint 
                                          to the engine and collapses the menu.
                      - Complete Constraint - Preserves the exact contact data from selection and the distance
                                          within the distance field.  Creates a constraint with the assumption 
                                          that the values listed are the exact values the users desires.  
                                          Allows for more robust and unique constraints.  Adds the constraint 
                                          to the engine and collapses the menu.

      Thread Manager  - Threading can be turned on and off with
                        'Threading Active' box.
                      - Islanding always runs
                      - Active Islands - current islands that are 
                          available for separate jobs
                      - Number of Masters - number of Master-Servant pairs
                      - Largest Island - The largest island created up to 
                          this point.  Does not shrink.
       
Keyboard:
  General:
  ESC - Exits simulation

  Camera:
  W/S - Forward/Back
  A/D - Left/Right
  Q/E - Down/Up
  Z/C - Yaw Left/Right
  1/3 - Pitch Up/Down
  2/X - Roll Right/Left
  R/F - Zoom In/Out
  ;/' - Move Look Vector In/Out
  
  Debugging:
  TAB   - Set Wireframe/Model View
  `     - (Accent)Rotate among simplex draws and contact points
  SPACE - Pause/Continue Timestep
  V     - Draw velocity vectors
  N     - Draw collision normals
  M     - (When paused) Move timestep forward one frame
  Y     - Draw contraint lines (Default: ON)
  
  Create Constraints: Wireframe mode required to view
  4   - Add a rope constraint pair
  5   - Add a rod constraint pair
  6   - Add a soft constraint pair
  
  Body selection:
  7   - Select body, rotates among all active bodies in space
  8   - Deselect bodies
  
  Adjust Selected Body:
  P   - Rotate among object axes for other controls (X,Y,Z)
  [/] - Adjust body dimensions along axis
  I/K - Move body Up/Down
  J/L - Move body, Left/Right
  U/O - Move body, Forward/Back
  ,/. - Rotate body along current axis
  
  Create Bodies:
  9   - Add a long rectangular box at lookat point (useful for connecting islands)
  0   - Add a box at lookat point
  -   - Add a sphere at look at point
  =   - Add a capsule at look at point
  
  
Mouse:
  Hold Left Click  - Move Up/Down/Left/Right
  Hold Right Click - Rotate camera around look at point
  Scroll wheel     - Move Forward/Back