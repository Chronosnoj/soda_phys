/*****************************************************************
Filename: Quaternion.h
Author(s): Jon Sourbeer
Description:
All content © 2015 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#pragma once

  //matrices
#include "Base/math/matrices/Mat3.h"
#include "Base/math/matrices/Mat4.h"

  //vectors
#include "Base/math/vectors/Vec3.h"
#include "Base/math/vectors/Vec4.h"

  //sqrt
#include "Base/math/extras/SheepMath.h"

namespace Base
{
  namespace Math
  {
    struct Quaternion
    {
      Quaternion();
      Quaternion(float real, float ipart, float jpart, float kpart);
      Quaternion(Base::Mat3& matrix);
      Quaternion(const Vec3& sourcePoint, const Vec3& destPoint, const Vec3& up = Vec3(0, 1, 0));
      Quaternion(const Vec3& eulerAngles);

      Quaternion(const Vec4& copy);
      Quaternion(float* data);

      float* ToFloats();

        //Quaternions representing rotations should always be kept
        //to a magnitude of 1.
      void Normalize();

        //sign value used for quat conversions
      inline float Sign(float x)   { return ( (x >= 0.0f) ? 1.0f : -1.0f ); }

        //return the euler angle
      Vec3 GetEulerAngles() const;

      void SetEulerAngles(Vec3 eulerAngles);

        //Create conjugate of the quat
      Quaternion Conjugate();
      Quaternion Inverse();

        //Quaternion multiplication - not commutative - rotates left side by right
      void operator*=(const Quaternion& rhs);
      Quaternion operator*(const Quaternion& rhs) const;

        //dot between quaternions?
      float Dot(const Quaternion& rhs);

        //rotate 

        //convert Quaternion into 3x3
      Mat3 ToMatrix();

        //specialized Physics functions -
        //no one else touchie
      Base::Mat3 PhysConvert();

        //convert Quaternion in 4x4 with position
      Mat4 ToMatrix(Vec3 position);

        //rotate quaternion by a vector
      Quaternion& RotateAroundAxis(const Vec3& vector, float radians);
      Quaternion& RotateAroundAxis(const Vec4& vector, float radians);

      void RotateAroundAxisV(Vec3 vector, float radeens) {
        RotateAroundAxis(vector, radeens);
      }

      Quaternion& LookAt(const Vec3& sourcePoint, const Vec3& destPoint, const Vec3& up);
      Quaternion LookAt(const Vec3& newLook, const Vec3& oldLook);

      void LookAtV(Vec3 source, Vec3 dest, Vec3 up) {
        LookAt(source, dest, up);
      }

      //lerp and slerp
      Quaternion Lerp(const Quaternion& end, float t);
      Quaternion LuaLerp(const Quaternion end, float t);
      Quaternion Slerp(const Quaternion& end, float t);
      Quaternion LuaSlerp(const Quaternion end, float t);

        //angular veloc
      void AddScaledVector(const Vec3& vector, float scale);

      void Reset() {
        r = 1;
        i = 0;
        j = 0;
        k = 0;
      }

      union
      {
        struct
        {
          //real component
          float r;

          //complex components
          float i;
          float j;
          float k;
        };

        float quat[4];

      };
    };

    typedef Quaternion Quat;
  }

  typedef Math::Quaternion Quaternion;
  typedef Math::Quaternion Quat;
}