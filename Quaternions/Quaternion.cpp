/*****************************************************************
Filename: Quaternion.cpp
Author(s): Jon Sourbeer
Description:
All content © 2015 DigiPen (USA) Corporation, all rights reserved.
*****************************************************************/

#include "pch/precompiled.h"

#include "Quaternion.h"

namespace Base
{
  namespace Math
  {

      //constructors
    Quaternion::Quaternion()
    {
      r = 1;
      i = 0;
      j = 0;
      k = 0;
    }

    Quaternion::Quaternion(float real, float ipart, float jpart, float kpart)
    {
      r = real;
      i = ipart;
      j = jpart;
      k = kpart;
    }

    Quaternion::Quaternion(Base::Mat3& matrix)
    {
      float w = matrix.m_matrix[0][0] + matrix.m_matrix[1][1] + matrix.m_matrix[2][2];

      float value;

      if (w > 0)
      {
        value = .5f / sqrt(w + 1.0f);
        r = .25f / value;
        i = (matrix.m_matrix[2][1] - matrix.m_matrix[1][2]) * value;
        j = (matrix.m_matrix[0][2] - matrix.m_matrix[2][0]) * value;
        k = (matrix.m_matrix[1][0] - matrix.m_matrix[0][1]) * value;
      }

      else if (matrix.m_matrix[0][0] > matrix.m_matrix[1][1]
        && matrix.m_matrix[0][0] > matrix.m_matrix[2][2])
      {
        value = sqrt(1.0f + matrix.m_matrix[0][0] - matrix.m_matrix[1][1]
          - matrix.m_matrix[2][2]) * 2.0f;
        i = .25f * value;
        j = (matrix.m_matrix[1][0] + matrix.m_matrix[0][1]) / value;
        k = (matrix.m_matrix[0][2] + matrix.m_matrix[2][0]) / value;
        r = (matrix.m_matrix[2][1] - matrix.m_matrix[1][2]) / value;
      }

      else if (matrix.m_matrix[1][1] > matrix.m_matrix[2][2])
      {
        value = sqrt(1.0f - matrix.m_matrix[0][0] + matrix.m_matrix[1][1]
          - matrix.m_matrix[2][2]) * 2.0f;
        j = .25f * value;
        i = (matrix.m_matrix[1][0] + matrix.m_matrix[0][1]) / value;
        k = (matrix.m_matrix[1][2] - matrix.m_matrix[2][1]) / value;
        r = (matrix.m_matrix[0][2] - matrix.m_matrix[2][0]) / value;
      }

      else
      {
        value = sqrt(1.0f - matrix.m_matrix[0][0] - matrix.m_matrix[1][1]
          + matrix.m_matrix[2][2]) * 2.0f;
        k = .25f * value;
        i = (matrix.m_matrix[0][2] + matrix.m_matrix[2][0]) / value;
        j = (matrix.m_matrix[1][2] + matrix.m_matrix[2][1]) / value;
        r = (matrix.m_matrix[1][0] - matrix.m_matrix[0][1]) / value;
      }

      Normalize();
    }

    Quaternion::Quaternion(const Vec3& sourcePoint, const Vec3& destPoint, const Vec3& up) :
      r(1), i(0), j(0), k(0)
    {
      this->LookAt(sourcePoint, destPoint, up);
    }

    Quaternion::Quaternion(const Vec3& eulerAngles)
    {
      r = cosf(eulerAngles.x / 2) * cosf(eulerAngles.y / 2) * cosf(eulerAngles.z / 2) + 
        sinf(eulerAngles.x / 2) * sinf(eulerAngles.y / 2) * sinf(eulerAngles.z / 2);
      i = sinf(eulerAngles.x / 2) * cosf(eulerAngles.y / 2) * cosf(eulerAngles.z / 2) -
        cosf(eulerAngles.x / 2) * sinf(eulerAngles.y / 2) * sinf(eulerAngles.z / 2);
      j = cosf(eulerAngles.x / 2) * sinf(eulerAngles.y / 2) * cosf(eulerAngles.z / 2) +
        sinf(eulerAngles.x / 2) * cosf(eulerAngles.y / 2) * sinf(eulerAngles.z / 2);
      k = cosf(eulerAngles.x / 2) * cosf(eulerAngles.y / 2) * sinf(eulerAngles.z / 2) +
        sinf(eulerAngles.x / 2) * sinf(eulerAngles.y / 2) * cosf(eulerAngles.z / 2);

      Normalize();
    }

    Quaternion::Quaternion(const Vec4& copy) : r(copy.x), i(copy.y), j(copy.z), k(copy.w)
    {
    }

    // assuming vec4
    Quaternion::Quaternion(float* data) : r(data[0]), i(data[1]), j(data[2]), k(data[3])
    {
    }

    float* Quaternion::ToFloats()
    {
      return quat;
    }

      //normalization - quats for rotations must be kept to length 1
    void Quaternion::Normalize()
    {
      //square length of quat
      float sqLength = r * r + i * i + j * j + k * k;

      if (!sqLength)
      {
        r = 1;
        return;
      }

      sqLength = 1.0f / std::sqrtf(sqLength);
      r *= sqLength;
      i *= sqLength;
      j *= sqLength;
      k *= sqLength;

    }

    Vec3 Quaternion::GetEulerAngles() const
    {
      Vec3 eulerAngles;
      double test = i * j + r * k;

      if (test > .499)
      {
        eulerAngles.y = 2 * atan2(i, r);
        eulerAngles.z = Math::PI / 2.0f;
        eulerAngles.x = 0;
      }

      else if (test < -.499)
      {
        eulerAngles.y = -2 * atan2(i, r);
        eulerAngles.z = -Math::PI / 2.0f;
        eulerAngles.x = 0;
      }

      else
      {
        eulerAngles.y = atan2(2 * (r * j + i * k), 1 - 2 * (j * j - k * k));
        eulerAngles.z = static_cast<float>(asin(2 * test));
        eulerAngles.x = atan2(2 * (r * i - j * k), 1 - 2 * (i * i - k * k));
      }

      return eulerAngles;
    }

    void Quaternion::SetEulerAngles(Vec3 eulerAngles)
    {
      *this = Quaternion(eulerAngles);
    }

    Quaternion Quaternion::Conjugate()
    {
      return Quaternion(r, -i, -j, -k);
    }

    Quaternion Quaternion::Inverse()
    {
      Quaternion conjugate = Conjugate();

      float dot = Dot(*this);

      Quaternion inverse;
      inverse.r = conjugate.r / dot;
      inverse.i = conjugate.i / dot;
      inverse.j = conjugate.j / dot;
      inverse.k = conjugate.k / dot;

      return inverse;
    }

      //quaternion multiplication
    void Quaternion::operator*=(const Quaternion& rhs)
    {
      Quaternion quat = *this;

      r = quat.r * rhs.r - quat.i * rhs.i - quat.j * rhs.j - quat.k * rhs.k;
      i = quat.r * rhs.i + quat.i * rhs.r + quat.j * rhs.k - quat.k * rhs.j;
      j = quat.r * rhs.j + quat.j * rhs.r + quat.k * rhs.i - quat.i * rhs.k;
      k = quat.r * rhs.k + quat.k * rhs.r + quat.i * rhs.j - quat.j * rhs.i;

    }

    Quaternion Quaternion::operator*(const Quaternion& rhs) const
    {
      Quaternion quat;

      quat.r = r * rhs.r - i * rhs.i - j * rhs.j - k * rhs.k;
      quat.i = i * rhs.r + r * rhs.i + j * rhs.k - k * rhs.j;
      quat.j = j * rhs.r + k * rhs.i + r * rhs.j - i * rhs.k;
      quat.k = k * rhs.r + i * rhs.j + r * rhs.k - j * rhs.i;

      return quat;
    }

    float Quaternion::Dot(const Quaternion& rhs)
    {
      float dot = r * rhs.r + i * rhs.i + j * rhs.j + k * rhs.k;
      return dot;
    }

      //matrix conversions
    Mat3 Quaternion::ToMatrix()
    {
      Mat3 matrix;

      matrix.m_matrixLinear[0] = 1 - (2 * j * j + 2 * k * k);
      matrix.m_matrixLinear[1] = 2 * i * j + 2 * k * r;
      matrix.m_matrixLinear[2] = 2 * i * k - 2 * j * r;
      matrix.m_matrixLinear[3] = 2 * i * j - 2 * k * r;
      matrix.m_matrixLinear[4] = 1 - (2 * i * i + 2 * k * k);
      matrix.m_matrixLinear[5] = 2 * j * k + 2 * i * r;
      matrix.m_matrixLinear[6] = 2 * i * k + 2 * j * r;
      matrix.m_matrixLinear[7] = 2 * j * k - 2 * i * r;
      matrix.m_matrixLinear[8] = 1 - (2 * i * i + 2 * j * j);

      return matrix;
    }

    //matrix conversions
    Mat3 Quaternion::PhysConvert()
    {
      Mat3 matrix;

      matrix.m_matrix[0][0] = 1 - (2 * j * j + 2 * k * k);
      matrix.m_matrix[0][1] = 2 * i * j - 2 * k * r;
      matrix.m_matrix[0][2] = 2 * i * k + 2 * j * r;
      matrix.m_matrix[1][0] = 2 * i * j + 2 * k * r;
      matrix.m_matrix[1][1] = 1 - (2 * i * i + 2 * k * k);
      matrix.m_matrix[1][2] = 2 * j * k - 2 * i * r;
      matrix.m_matrix[2][0] = 2 * i * k - 2 * j * r;
      matrix.m_matrix[2][1] = 2 * j * k + 2 * i * r;
      matrix.m_matrix[2][2] = 1 - (2 * i * i + 2 * j * j);

      return matrix;
    }

    //convert Quaternion in 4x4 with position
    Mat4 Quaternion::ToMatrix(Vec3 position)
    {
      Mat4 matrix;

      matrix.m_matrixLinear[0] = 1 - (2 * j * j + 2 * k * k);
      matrix.m_matrixLinear[1] = 2 * i * j + 2 * k * r;
      matrix.m_matrixLinear[2] = 2 * i * k - 2 * j * r;
      matrix.m_matrixLinear[3] = position.x;
      matrix.m_matrixLinear[4] = 2 * i * j - 2 * k * r;
      matrix.m_matrixLinear[5] = 1 - (2 * i * i + 2 * k * k);
      matrix.m_matrixLinear[6] = 2 * j * k + 2 * i * r;
      matrix.m_matrixLinear[7] = position.y;
      matrix.m_matrixLinear[8] = 2 * i * k + 2 * j * r;
      matrix.m_matrixLinear[9] = 2 * j * k - 2 * i * r;
      matrix.m_matrixLinear[10] = 1 - (2 * i * i + 2 * j * j);
      matrix.m_matrixLinear[11] = position.z;

      return matrix;

    }

      //quat rotation
    Quaternion& Quaternion::RotateAroundAxis(const Vec3& vector, float radians)
    {
      //q0 = cos(r/2),  q1 = sin(r/2) a,  q2 = sin(r/2) b,  q3 = sin(r/2) c
      Quaternion quat(cosf(radians / 2.0f), sinf(radians / 2.0f) * vector.x, sinf(radians / 2.0f) * vector.y, sinf(radians / 2.0f) * vector.z);

      (*this) *= quat;

      return *this;
    }

    Quaternion& Quaternion::RotateAroundAxis(const Vec4& vector, float radians)
    {
      //q0 = cos(r/2),  q1 = sin(r/2) a,  q2 = sin(r/2) b,  q3 = sin(r/2)
      Quaternion quat(cosf(radians / 2.0f), sinf(radians / 2.0f) * vector.x, sinf(radians / 2.0f) * vector.y, sinf(radians / 2.0f) * vector.z);

      (*this) *= quat;

      return *this;
    }

    Quaternion& Quaternion::LookAt(const Vec3& sourcePoint, const Vec3& destPoint, const Vec3& up)
    {
      Base::Vec3 source = sourcePoint;
      source.Normalize();

      Base::Vec3 dest = destPoint;
      dest.Normalize();

      float dot = source.Dot(dest);

      Quaternion quat;
      // If 180 degrees
      if (abs(dot - (-1.0f)) < 0.000001f)
      {
        this->RotateAroundAxis(up, (float)Base::Math::PI);
        return *this;
      }
      // If same orientation
      else if (abs(dot - (1.0f)) < 0.000001f)
      {
        (*this) = Quaternion();
        return *this;
      }

      float angle = (float)acos(dot);
      Vec3 axis = Cross(dest, source).Normalize();

      this->RotateAroundAxis(axis, angle);

      return *this;
    }

    Quaternion Quaternion::LookAt(const Vec3& newLook, const Vec3& oldLook)
    {
      Base::Vec3 cross = newLook.Cross(oldLook);
      Quaternion quat;
      quat.i = cross.x;
      quat.j = cross.y;
      quat.k = cross.z;

      quat.r = 1 + newLook.Dot(oldLook);

      return quat;
    }

    //Lerp and Slerp - credit DigiPen Zero and Jack Morrison
    Quaternion Quaternion::Lerp(const Quaternion& end, float t)
    {
      float minusT = 1 - t;
      Quaternion quat;
      quat.r = r * minusT + end.r * t;
      quat.i = i * minusT + end.i * t;
      quat.j = j * minusT + end.j * t;
      quat.k = k * minusT + end.k * t;

      quat.Normalize();
      return quat;
    }

    Quaternion Quaternion::Slerp(const Quaternion& end, float t)
    {
      const float slerpEp = 0.00001f;

      bool flip;
      float theta = Dot(end);

      flip = theta < 0.0f;

      if (flip)
        theta = -theta;

      float startVal, endVal;

      if ((1.0f - theta) > slerpEp)
      {
        float arcTheta = acosf(theta);
        float sinTheta = sin(arcTheta);
        startVal = sin((1.0f - t) * arcTheta) / sinTheta;
        endVal = sin(t * arcTheta) / sinTheta;
      }
      else
      {
        startVal = 1.0f - t;
        endVal = t;
      }

      if (flip)
      {
        endVal = -endVal;
      }

      Quaternion quat;
      quat.r = r * startVal + end.r * endVal;
      quat.i = i * startVal + end.i * endVal;
      quat.j = j * startVal + end.j * endVal;
      quat.k = k * startVal + end.k * endVal;

      return quat;

    }

    Quaternion Quaternion::LuaLerp(const Quaternion end, float t)
    {
      float minusT = 1 - t;
      Quaternion quat;
      quat.r = r * minusT + end.r * t;
      quat.i = i * minusT + end.i * t;
      quat.j = j * minusT + end.j * t;
      quat.k = k * minusT + end.k * t;

      quat.Normalize();
      return quat;
    }

    Quaternion Quaternion::LuaSlerp(const Quaternion end, float t)
    {
      const float slerpEp = 0.00001f;

      bool flip;
      float theta = Dot(end);

      flip = theta < 0.0f;

      if (flip)
        theta = -theta;

      float startVal, endVal;

      if ((1.0f - theta) > slerpEp)
      {
        float arcTheta = acosf(theta);
        float sinTheta = sin(arcTheta);
        startVal = sin((1.0f - t) * arcTheta) / sinTheta;
        endVal = sin(t * arcTheta) / sinTheta;
      }
      else
      {
        startVal = 1.0f - t;
        endVal = t;
      }

      if (flip)
      {
        endVal = -endVal;
      }

      Quaternion quat;
      quat.r = r * startVal + end.r * endVal;
      quat.i = i * startVal + end.i * endVal;
      quat.j = j * startVal + end.j * endVal;
      quat.k = k * startVal + end.k * endVal;

      return quat;
    }

    void Quaternion::AddScaledVector(const Vec3& vector, float scale)
    {
      Quaternion quat(0, vector.x * scale, vector.y * scale, vector.z * scale);

      quat *= (*this);

      r += quat.r * 0.5f;
      i += quat.i * 0.5f;
      j += quat.j * 0.5f;
      k += quat.k * 0.5f; 
    }

  }
}